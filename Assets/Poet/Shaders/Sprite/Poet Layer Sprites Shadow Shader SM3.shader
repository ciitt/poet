﻿Shader "Custom/Poet/Poet Layer Sprites Shadow Shader SM3"
{
	Properties
	{

		[PerRendererData] _MainTex("Sprite Texture", 2D) = "white" {}

		[Enum(UnityEngine.Rendering.CullMode)] _Cull("Cull mode", Float) = 0
		[Enum(UnityEngine.Rendering.CompareFunction)] _Compare("Compare function", Float) = 8
		
		[Space(30)]

		[Toggle(ClampOutside)] _ClampOutside("Clamp Outside", Float) = 1
		[Toggle(UseLayer5AsLayerMinus1)] _UseLayer5AsLayerMinus1("Use Layer5 as Layer -1", Float) = 0

		[Space(30)]

		[Toggle(FlipX)] _FlipX("Flip X", Float) = 0
		[Toggle(FlipY)] _FlipY("Flip Y", Float) = 0

		[Space(30)] _Layer0Tex("Layer 0 Texture", 2D) = "black" {}
		[Space(30)] _Layer1Tex("Layer 1 Texture", 2D) = "black" {}
		[Space(30)] _Layer2Tex("Layer 2 Texture", 2D) = "black" {}
		[Space(30)] _Layer3Tex("Layer 3 Texture", 2D) = "black" {}
		[Space(30)] _Layer4Tex("Layer 4 Texture", 2D) = "black" {}
		[Space(30)] _Layer5Tex("Layer 5 Texture", 2D) = "black" {}
		[Space(30)] _OverlayTex("Overlay Texture", 2D) = "black" {}

		[HideInInspector]
		_Fade("Fade", Range(0, 1)) = 1.0

		[HideInInspector]
		[Toggle(FlipXPrev)]
		_FlipXPrev("Flip X Prev", Float) = 0

		[HideInInspector]
		[Toggle(FlipYPrev)]
		_FlipYPrev("Flip Y Prev", Float) = 0

		[HideInInspector] _Layer0TexPrev("Layer 0 Texture Prev", 2D) = "black" {}
		[HideInInspector] _Layer1TexPrev("Layer 1 Texture Prev", 2D) = "black" {}
		[HideInInspector] _Layer2TexPrev("Layer 2 Texture Prev", 2D) = "black" {}
		[HideInInspector] _Layer3TexPrev("Layer 3 Texture Prev", 2D) = "black" {}
		[HideInInspector] _Layer4TexPrev("Layer 4 Texture Prev", 2D) = "black" {}
		[HideInInspector] _Layer5TexPrev("Layer 5 Texture Prev", 2D) = "black" {}
		[HideInInspector] _OverlayTexPrev("Overlay Texture Prev", 2D) = "black" {}

	}

	SubShader
	{
		Tags
		{ 
			"Queue"="Transparent" 
			"IgnoreProjector"="True" 
			"RenderType"="Transparent" 
			"PreviewType"="Plane"
			"CanUseSpriteAtlas"="True"
		}

		Cull [_Cull]
		Lighting Off
		ZWrite Off
		ZTest[_Compare]
		Blend One OneMinusSrcAlpha

		UsePass "Custom/Poet/Poet Layer Sprites Shader SM3/POETBASE"

		Pass
		{

			Tags{ "LightMode" = "ShadowCaster" }

			ZWrite On ZTest LEqual

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile _ FlipX
			#pragma multi_compile _ FlipXPrev
			#pragma multi_compile _ FlipY
			#pragma multi_compile _ FlipYPrev
			#include "UnityCG.cginc"
			#include "PoetCommon.cginc"

			struct appdata_t
			{
				float4 vertex   : POSITION;
				float4 color    : COLOR;
				float2 texcoord : TEXCOORD;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct v2f
			{
				float4 vertex   : SV_POSITION;
				fixed4 color    : COLOR;
				float4 texcoordLayer0 : TEXCOORD0;
				float4 texcoordLayer1 : TEXCOORD1;
				float4 texcoordLayer2 : TEXCOORD2;
				float4 texcoordLayer3 : TEXCOORD3;
				float4 texcoordLayer4 : TEXCOORD4;
				float4 texcoordLayer5 : TEXCOORD5;
				UNITY_VERTEX_OUTPUT_STEREO
			};

			fixed _Fade;

			float4 _Layer0Tex_ST;
			float4 _Layer1Tex_ST;
			float4 _Layer2Tex_ST;
			float4 _Layer3Tex_ST;
			float4 _Layer4Tex_ST;
			float4 _Layer5Tex_ST;

			float4 _Layer0TexPrev_ST;
			float4 _Layer1TexPrev_ST;
			float4 _Layer2TexPrev_ST;
			float4 _Layer3TexPrev_ST;
			float4 _Layer4TexPrev_ST;
			float4 _Layer5TexPrev_ST;

			v2f vert(appdata_t IN)
			{
				v2f OUT;
				UNITY_SETUP_INSTANCE_ID(IN);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(OUT);
				OUT.vertex = UnityObjectToClipPos(IN.vertex);
				OUT.color = IN.color;

				float2 curr = getUv(IN.texcoord);
				float2 prev = getUvForPrev(IN.texcoord);

				OUT.texcoordLayer0.xy = tillingFromCenter(curr, _Layer0Tex_ST);
				OUT.texcoordLayer0.zw = tillingFromCenter(prev, _Layer0TexPrev_ST);

				OUT.texcoordLayer1.xy = tillingFromCenter(curr, _Layer1Tex_ST);
				OUT.texcoordLayer1.zw = tillingFromCenter(prev, _Layer1TexPrev_ST);

				OUT.texcoordLayer2.xy = tillingFromCenter(curr, _Layer2Tex_ST);
				OUT.texcoordLayer2.zw = tillingFromCenter(prev, _Layer2TexPrev_ST);

				OUT.texcoordLayer3.xy = tillingFromCenter(curr, _Layer3Tex_ST);
				OUT.texcoordLayer3.zw = tillingFromCenter(prev, _Layer3TexPrev_ST);

				OUT.texcoordLayer4.xy = tillingFromCenter(curr, _Layer4Tex_ST);
				OUT.texcoordLayer4.zw = tillingFromCenter(prev, _Layer4TexPrev_ST);

				OUT.texcoordLayer5.xy = tillingFromCenter(curr, _Layer5Tex_ST);
				OUT.texcoordLayer5.zw = tillingFromCenter(prev, _Layer5TexPrev_ST);

				return OUT;

			}

			sampler2D _Layer0Tex;
			sampler2D _Layer1Tex;
			sampler2D _Layer2Tex;
			sampler2D _Layer3Tex;
			sampler2D _Layer4Tex;
			sampler2D _Layer5Tex;

			sampler2D _Layer0TexPrev;
			sampler2D _Layer1TexPrev;
			sampler2D _Layer2TexPrev;
			sampler2D _Layer3TexPrev;
			sampler2D _Layer4TexPrev;
			sampler2D _Layer5TexPrev;

			fixed4 frag(v2f IN) : SV_Target
			{

				fixed4 current = combineTextures(
					_Layer0Tex,
					_Layer1Tex,
					_Layer2Tex,
					_Layer3Tex,
					_Layer4Tex,
					_Layer5Tex,
					IN.texcoordLayer0.xy,
					IN.texcoordLayer1.xy,
					IN.texcoordLayer2.xy,
					IN.texcoordLayer3.xy,
					IN.texcoordLayer4.xy,
					IN.texcoordLayer5.xy
				);

				fixed4 previous = combineTextures(
					_Layer0TexPrev,
					_Layer1TexPrev,
					_Layer2TexPrev,
					_Layer3TexPrev,
					_Layer4TexPrev,
					_Layer5TexPrev,
					IN.texcoordLayer0.zw,
					IN.texcoordLayer1.zw,
					IN.texcoordLayer2.zw,
					IN.texcoordLayer3.zw,
					IN.texcoordLayer4.zw,
					IN.texcoordLayer5.zw
				);

				clip(lerp(previous, current, _Fade).a - 0.001);

				return lerp(previous, current, _Fade);

			}

		ENDCG

		}

	}
	
}
