﻿Shader "Custom/Poet/Poet Layer SpriteAtlas Mobile Shader"
{
	Properties
	{

		[PerRendererData] _MainTex("Sprite Texture", 2D) = "white" {}

		[Enum(UnityEngine.Rendering.CullMode)] _Cull("Cull mode", Float) = 0
		[Enum(UnityEngine.Rendering.CompareFunction)] _Compare("Compare function", Float) = 8
			
		[Space(30)]

		[Toggle(FlipX)] _FlipX("Flip X", Float) = 0
		[Toggle(FlipY)] _FlipY("Flip Y", Float) = 0

		[Space(30)] _Layer0Tex("Layer 0 Texture", 2D) = "black" {}
		[Space(30)] _Layer1Tex("Layer 1 Texture", 2D) = "black" {}

		[HideInInspector]
		_Fade("Fade", Range(0, 1)) = 1.0

		[HideInInspector]
		[Toggle(FlipXPrev)]
		_FlipXPrev("Flip X Prev", Float) = 0

		[HideInInspector]
		[Toggle(FlipYPrev)]
		_FlipYPrev("Flip Y Prev", Float) = 0

		[HideInInspector] _Layer0TexPrev("Layer 0 Texture Prev", 2D) = "black" {}
		[HideInInspector] _Layer1TexPrev("Layer 1 Texture Prev", 2D) = "black" {}
		
		[HideInInspector] _Layer0TexPos("Layer 0 Texture Pos", Vector) = (0, 1, 0 ,1)
		[HideInInspector] _Layer1TexPos("Layer 1 Texture Pos", Vector) = (0, 1, 0 ,1)

		[HideInInspector] _Layer0TexPosPrev("Layer 0 Texture Pos Prev", Vector) = (0, 1, 0 ,1)
		[HideInInspector] _Layer1TexPosPrev("Layer 1 Texture Pos Prev", Vector) = (0, 1, 0 ,1)
	}

	SubShader
	{
		Tags
		{ 
			"Queue"="Transparent" 
			"IgnoreProjector"="True" 
			"RenderType"="Transparent" 
			"PreviewType"="Plane"
			"CanUseSpriteAtlas"="True"
		}

		Cull [_Cull]
		Lighting Off
		ZWrite Off
		ZTest [_Compare]
		Blend One OneMinusSrcAlpha

		Pass
		{

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 2.0
			#pragma multi_compile _ FlipX
			#pragma multi_compile _ FlipXPrev
			#pragma multi_compile _ FlipY
			#pragma multi_compile _ FlipYPrev
			#include "UnityCG.cginc"
			#include "PoetCommon.cginc"
			
			struct appdata_t
			{
				float4 vertex   : POSITION;
				float4 color    : COLOR;
				float2 texcoord : TEXCOORD;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct v2f
			{
				float4 vertex   : SV_POSITION;
				fixed4 color    : COLOR;
				float4 texcoordLayer0 : TEXCOORD0;
				float4 texcoordLayer1 : TEXCOORD1;
				UNITY_VERTEX_OUTPUT_STEREO
			};

			fixed _Fade;

			float4 _Layer0Tex_ST;
			float4 _Layer1Tex_ST;

			float4 _Layer0TexPrev_ST;
			float4 _Layer1TexPrev_ST;

			float4 _Layer0TexPos;
			float4 _Layer1TexPos;

			float4 _Layer0TexPosPrev;
			float4 _Layer1TexPosPrev;

			v2f vert(appdata_t IN)
			{
				v2f OUT;
				UNITY_SETUP_INSTANCE_ID(IN);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(OUT);
				OUT.vertex = UnityObjectToClipPos(IN.vertex);
				OUT.color = IN.color;
				OUT.color.rgb *= OUT.color.a;

				float2 curr = getUv(IN.texcoord);
				float2 prev = getUvForPrev(IN.texcoord);

				OUT.texcoordLayer0.xy = calcTexcoordSA(curr, _Layer0TexPos, _Layer0Tex_ST);
				OUT.texcoordLayer0.zw = calcTexcoordSA(prev, _Layer0TexPosPrev, _Layer0TexPrev_ST);

				OUT.texcoordLayer1.xy = calcTexcoordSA(curr, _Layer1TexPos, _Layer1Tex_ST);
				OUT.texcoordLayer1.zw = calcTexcoordSA(prev, _Layer1TexPosPrev, _Layer1TexPrev_ST);

				return OUT;
			}

			sampler2D _Layer0Tex;
			sampler2D _Layer1Tex;

			sampler2D _Layer0TexPrev;
			sampler2D _Layer1TexPrev;

			fixed4 frag(v2f IN) : SV_Target
			{

				fixed4 current = combineTexturesSA(
					trimColorSA(_Layer0Tex, IN.texcoordLayer0.xy, _Layer0TexPos),
					trimColorSA(_Layer1Tex, IN.texcoordLayer1.xy, _Layer1TexPos)
				);

				fixed4 previous = combineTexturesSA(
					trimColorSA(_Layer0TexPrev, IN.texcoordLayer0.zw, _Layer0TexPosPrev),
					trimColorSA(_Layer1TexPrev, IN.texcoordLayer1.zw, _Layer1TexPosPrev)
				);

				return lerp(previous, current, _Fade) * IN.color;

			}

		ENDCG

		}

	}

}
