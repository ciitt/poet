float2 getUv(float2 texcoord)
{

#if FlipX && FlipY
	return 1 - texcoord;
#elif FlipY
	return float2(texcoord.x, 1 - texcoord.y);
#elif FlipX
	return float2(1 - texcoord.x, texcoord.y);
#else
	return texcoord;
#endif

}

float2 getUvForPrev(float2 texcoord)
{

#if FlipXPrev && FlipYPrev
	return 1 - texcoord;
#elif FlipYPrev
	return float2(texcoord.x, 1 - texcoord.y);
#elif FlipXPrev
	return float2(1 - texcoord.x, texcoord.y);
#else
	return texcoord;
#endif

}

float2 tillingFromCenter(float2 texcoord, float4 st)
{

	texcoord.x += st.z;
	texcoord.y += st.w;

	texcoord.x *= st.x;
	texcoord.y *= st.y;

	texcoord.x -= (st.x - 1) * 0.5;
	texcoord.y -= (st.y - 1) * 0.5;

	return texcoord;

}

fixed4 tex2DWithClampOutside(sampler2D s2d, float2 uv)
{

	fixed4 color = tex2D(s2d, uv);

	color.a *= step(0, uv.x) * step(-1, -uv.x);
	color.a *= step(0, uv.y) * step(-1, -uv.y);

	return color;

}

fixed4 combineTextures(
	sampler2D s2d0,
	sampler2D s2d1,
	float2 uv0,
	float2 uv1
)
{

	fixed4 color0 = tex2D(s2d0, uv0);

#if ClampOutside
	fixed4 color1 = tex2DWithClampOutside(s2d1, uv1);
#else
	fixed4 color1 = tex2D(s2d1, uv1);
#endif

	color0.rgb = color0.rgb * color0.a;

	color0.rgb = lerp(color0.rgb, color1.rgb, color1.a);

	color0.a = color0.a + (color1.a * (1 - color0.a));

	return color0;

}

fixed4 combineTextures(
	sampler2D s2d0,
	sampler2D s2d1,
	sampler2D s2d2,
	sampler2D s2d3,
	sampler2D s2d4,
	sampler2D s2d5,
	float2 uv0,
	float2 uv1,
	float2 uv2,
	float2 uv3,
	float2 uv4,
	float2 uv5
)
{

	fixed4 color0 = tex2D(s2d0, uv0);

#if ClampOutside

	fixed4 color1 = tex2DWithClampOutside(s2d1, uv1);
	fixed4 color2 = tex2DWithClampOutside(s2d2, uv2);
	fixed4 color3 = tex2DWithClampOutside(s2d3, uv3);
	fixed4 color4 = tex2DWithClampOutside(s2d4, uv4);
	fixed4 color5 = tex2DWithClampOutside(s2d5, uv5);

#else

	fixed4 color1 = tex2D(s2d1, uv1);
	fixed4 color2 = tex2D(s2d2, uv2);
	fixed4 color3 = tex2D(s2d3, uv3);
	fixed4 color4 = tex2D(s2d4, uv4);
	fixed4 color5 = tex2D(s2d5, uv5);

#endif

	color0.rgb = color0.rgb * color0.a;

	color0.rgb = lerp(color0.rgb, color1.rgb, color1.a);
	color0.rgb = lerp(color0.rgb, color2.rgb, color2.a);
	color0.rgb = lerp(color0.rgb, color3.rgb, color3.a);
	color0.rgb = lerp(color0.rgb, color4.rgb, color4.a);
	color0.rgb = lerp(color0.rgb, color5.rgb, color5.a);

	color0.a = color0.a + (color1.a * (1 - color0.a));
	color0.a = color0.a + (color2.a * (1 - color0.a));
	color0.a = color0.a + (color3.a * (1 - color0.a));
	color0.a = color0.a + (color4.a * (1 - color0.a));
	color0.a = color0.a + (color5.a * (1 - color0.a));

	return color0;

}

float2 calcTexcoordSA(float2 texcoord, float4 pos, float4 st)
{

	st.z *= pos.y;
	st.w *= pos.w;

	texcoord.x *= pos.y;
	texcoord.y *= pos.w;

	texcoord.x += pos.x;
	texcoord.y += pos.z;

	float tempX = pos.x + (pos.y * 0.5);
	float tempY = pos.z + (pos.w * 0.5);

	texcoord.x += st.z;
	texcoord.y += st.w;

	texcoord.x -= tempX;
	texcoord.y -= tempY;

	texcoord.x *= st.x;
	texcoord.y *= st.y;

	texcoord.x += tempX;
	texcoord.y += tempY;

	return texcoord;

}

fixed4 trimColorSA(sampler2D s2d, float2 uv, float4 pos)
{

	fixed alpha = 1.0;

	alpha *= step(pos.x, uv.x);
	alpha *= step(-pos.x - pos.y, -uv.x);
	alpha *= step(pos.z, uv.y);
	alpha *= step(-pos.z - pos.w, -uv.y);

	fixed4 color = tex2D(s2d, uv) * alpha;

	return color;

}

fixed4 combineTexturesSA(
	fixed4 color0,
	fixed4 color1
)
{

	color0.rgb = color0.rgb * color0.a;

	color0.rgb = lerp(color0.rgb, color1.rgb, color1.a);

	color0.a = color0.a + (color1.a * (1 - color0.a));

	return color0;

}

fixed4 combineTexturesSA(
	fixed4 color0,
	fixed4 color1,
	fixed4 color2,
	fixed4 color3,
	fixed4 color4,
	fixed4 color5
)
{

	color0.rgb = color0.rgb * color0.a;

	color0.rgb = lerp(color0.rgb, color1.rgb, color1.a);
	color0.rgb = lerp(color0.rgb, color2.rgb, color2.a);
	color0.rgb = lerp(color0.rgb, color3.rgb, color3.a);
	color0.rgb = lerp(color0.rgb, color4.rgb, color4.a);
	color0.rgb = lerp(color0.rgb, color5.rgb, color5.a);

	color0.a = color0.a + (color1.a * (1 - color0.a));
	color0.a = color0.a + (color2.a * (1 - color0.a));
	color0.a = color0.a + (color3.a * (1 - color0.a));
	color0.a = color0.a + (color4.a * (1 - color0.a));
	color0.a = color0.a + (color5.a * (1 - color0.a));

	return color0;

}
