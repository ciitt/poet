﻿Shader "Custom/Poet/Poet Layer Sprites Mobile Shader"
{
	Properties
	{

		[PerRendererData] _MainTex("Sprite Texture", 2D) = "white" {}

		[Enum(UnityEngine.Rendering.CullMode)] _Cull("Cull mode", Float) = 0
		[Enum(UnityEngine.Rendering.CompareFunction)] _Compare("Compare function", Float) = 8
			
		[Space(30)]

		[Toggle(ClampOutside)] _ClampOutside("Clamp Outside", Float) = 1
			
		[Space(30)]

		[Toggle(FlipX)] _FlipX("Flip X", Float) = 0
		[Toggle(FlipY)] _FlipY("Flip Y", Float) = 0

		[Space(30)] _Layer0Tex("Layer 0 Texture", 2D) = "black" {}
		[Space(30)] _Layer1Tex("Layer 1 Texture", 2D) = "black" {}

		[HideInInspector]
		_Fade("Fade", Range(0, 1)) = 1.0

		[HideInInspector]
		[Toggle(FlipXPrev)]
		_FlipXPrev("Flip X Prev", Float) = 0

		[HideInInspector]
		[Toggle(FlipYPrev)]
		_FlipYPrev("Flip Y Prev", Float) = 0

		[HideInInspector] _Layer0TexPrev("Layer 0 Texture Prev", 2D) = "black" {}
		[HideInInspector] _Layer1TexPrev("Layer 1 Texture Prev", 2D) = "black" {}

	}

	SubShader
	{
		Tags
		{ 
			"Queue"="Transparent" 
			"IgnoreProjector"="True" 
			"RenderType"="Transparent" 
			"PreviewType"="Plane"
			"CanUseSpriteAtlas"="True"
		}

		Cull [_Cull]
		Lighting Off
		ZWrite Off
		ZTest [_Compare]
		Blend One OneMinusSrcAlpha

		Pass
		{

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 2.0
			#pragma multi_compile _ FlipX
			#pragma multi_compile _ FlipXPrev
			#pragma multi_compile _ FlipY
			#pragma multi_compile _ FlipYPrev
			#pragma multi_compile _ ClampOutside
			#include "UnityCG.cginc"
			#include "PoetCommon.cginc"
			
			struct appdata_t
			{
				float4 vertex   : POSITION;
				float4 color    : COLOR;
				float2 texcoord : TEXCOORD;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct v2f
			{
				float4 vertex   : SV_POSITION;
				fixed4 color    : COLOR;
				float4 texcoordLayer0 : TEXCOORD0;
				float4 texcoordLayer1 : TEXCOORD1;
				UNITY_VERTEX_OUTPUT_STEREO
			};

			fixed _Fade;

			float4 _Layer0Tex_ST;
			float4 _Layer1Tex_ST;

			float4 _Layer0TexPrev_ST;
			float4 _Layer1TexPrev_ST;

			v2f vert(appdata_t IN)
			{
				v2f OUT;
				UNITY_SETUP_INSTANCE_ID(IN);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(OUT);
				OUT.vertex = UnityObjectToClipPos(IN.vertex);
				OUT.color = IN.color;
				OUT.color.rgb *= OUT.color.a;

				float2 curr = getUv(IN.texcoord);
				float2 prev = getUvForPrev(IN.texcoord);

				OUT.texcoordLayer0.xy = tillingFromCenter(curr, _Layer0Tex_ST);
				OUT.texcoordLayer0.zw = tillingFromCenter(prev, _Layer0TexPrev_ST);

				OUT.texcoordLayer1.xy = tillingFromCenter(curr, _Layer1Tex_ST);
				OUT.texcoordLayer1.zw = tillingFromCenter(prev, _Layer1TexPrev_ST);

				return OUT;
			}

			sampler2D _Layer0Tex;
			sampler2D _Layer1Tex;

			sampler2D _Layer0TexPrev;
			sampler2D _Layer1TexPrev;

			fixed4 frag(v2f IN) : SV_Target
			{

				fixed4 current = combineTextures(
					_Layer0Tex,
					_Layer1Tex,
					IN.texcoordLayer0.xy,
					IN.texcoordLayer1.xy
				);

				fixed4 previous = combineTextures(
					_Layer0TexPrev,
					_Layer1TexPrev,
					IN.texcoordLayer0.zw,
					IN.texcoordLayer1.zw
				);

				return lerp(previous, current, _Fade) * IN.color;

			}

		ENDCG

		}

	}

}
