﻿Shader "Custom/Poet/Poet Layer SpriteAtlas Shader SM3"
{
	Properties
	{

		[PerRendererData] _MainTex("Sprite Texture", 2D) = "white" {}

		[Enum(UnityEngine.Rendering.CullMode)] _Cull("Cull mode", Float) = 0
		[Enum(UnityEngine.Rendering.CompareFunction)] _Compare("Compare function", Float) = 8
		
		[Space(30)]

		[Toggle(UseLayer5AsLayerMinus1)] _UseLayer5AsLayerMinus1("Use Layer5 as Layer -1", Float) = 0

		[Space(30)]

		[Toggle(FlipX)] _FlipX("Flip X", Float) = 0
		[Toggle(FlipY)] _FlipY("Flip Y", Float) = 0

		[Space(30)] _Layer0Tex("Layer 0 Texture", 2D) = "black" {}
		[Space(30)] _Layer1Tex("Layer 1 Texture", 2D) = "black" {}
		[Space(30)] _Layer2Tex("Layer 2 Texture", 2D) = "black" {}
		[Space(30)] _Layer3Tex("Layer 3 Texture", 2D) = "black" {}
		[Space(30)] _Layer4Tex("Layer 4 Texture", 2D) = "black" {}
		[Space(30)] _Layer5Tex("Layer 5 Texture", 2D) = "black" {}
		[Space(30)] _OverlayTex("Overlay Texture", 2D) = "black" {}

		[HideInInspector]
		_Fade("Fade", Range(0, 1)) = 1.0

		[HideInInspector]
		[Toggle(FlipXPrev)]
		_FlipXPrev("Flip X Prev", Float) = 0

		[HideInInspector]
		[Toggle(FlipYPrev)]
		_FlipYPrev("Flip Y Prev", Float) = 0

		[HideInInspector] _Layer0TexPrev("Layer 0 Texture Prev", 2D) = "black" {}
		[HideInInspector] _Layer1TexPrev("Layer 1 Texture Prev", 2D) = "black" {}
		[HideInInspector] _Layer2TexPrev("Layer 2 Texture Prev", 2D) = "black" {}
		[HideInInspector] _Layer3TexPrev("Layer 3 Texture Prev", 2D) = "black" {}
		[HideInInspector] _Layer4TexPrev("Layer 4 Texture Prev", 2D) = "black" {}
		[HideInInspector] _Layer5TexPrev("Layer 5 Texture Prev", 2D) = "black" {}
		[HideInInspector] _OverlayTexPrev("Overlay Texture Prev", 2D) = "black" {}

		 _Layer0TexPos("Layer 0 Texture Pos", Vector) = (0, 1, 0 ,1)
		 _Layer1TexPos("Layer 1 Texture Pos", Vector) = (0, 1, 0 ,1)
		 _Layer2TexPos("Layer 2 Texture Pos", Vector) = (0, 1, 0 ,1)
		 _Layer3TexPos("Layer 3 Texture Pos", Vector) = (0, 1, 0 ,1)
		 _Layer4TexPos("Layer 4 Texture Pos", Vector) = (0, 1, 0 ,1)
		 _Layer5TexPos("Layer 5 Texture Pos", Vector) = (0, 1, 0 ,1)
		 _OverlayTexPos("Overlay Texture Pos", Vector) = (0, 1, 0 ,1)

		[HideInInspector] _Layer0TexPosPrev("Layer 0 Texture Pos Prev", Vector) = (0, 1, 0 ,1)
		[HideInInspector] _Layer1TexPosPrev("Layer 1 Texture Pos Prev", Vector) = (0, 1, 0 ,1)
		[HideInInspector] _Layer2TexPosPrev("Layer 2 Texture Pos Prev", Vector) = (0, 1, 0 ,1)
		[HideInInspector] _Layer3TexPosPrev("Layer 3 Texture Pos Prev", Vector) = (0, 1, 0 ,1)
		[HideInInspector] _Layer4TexPosPrev("Layer 4 Texture Pos Prev", Vector) = (0, 1, 0 ,1)
		[HideInInspector] _Layer5TexPosPrev("Layer 5 Texture Pos Prev", Vector) = (0, 1, 0 ,1)
		[HideInInspector] _OverlayTexPosPrev("Overlay Texture Pos Prev", Vector) = (0, 1, 0 ,1)

	}

	SubShader
	{
		Tags
		{ 
			"Queue"="Transparent" 
			"IgnoreProjector"="True" 
			"RenderType"="Transparent" 
			"PreviewType"="Plane"
			"CanUseSpriteAtlas"="True"
		}

		Cull [_Cull]
		Lighting Off
		ZWrite Off
		ZTest [_Compare]
		Blend One OneMinusSrcAlpha

		Pass
		{

			Name "POETSPRITEATLASBASE"

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile _ FlipX
			#pragma multi_compile _ FlipXPrev
			#pragma multi_compile _ FlipY
			#pragma multi_compile _ FlipYPrev
			#pragma multi_compile _ UseLayer5AsLayerMinus1
			#include "UnityCG.cginc"
			#include "PoetCommon.cginc"
			
			struct appdata_t
			{
				float4 vertex   : POSITION;
				float4 color    : COLOR;
				float2 texcoord : TEXCOORD;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct v2f
			{
				float4 vertex   : SV_POSITION;
				fixed4 color    : COLOR;
				float4 texcoordLayer0 : TEXCOORD0;
				float4 texcoordLayer1 : TEXCOORD1;
				float4 texcoordLayer2 : TEXCOORD2;
				float4 texcoordLayer3 : TEXCOORD3;
				float4 texcoordLayer4 : TEXCOORD4;
				float4 texcoordLayer5 : TEXCOORD5;
				float4 texcoordOverlay : TEXCOORD6;
				UNITY_VERTEX_OUTPUT_STEREO
			};

			fixed _Fade;

			float4 _Layer0Tex_ST;
			float4 _Layer1Tex_ST;
			float4 _Layer2Tex_ST;
			float4 _Layer3Tex_ST;
			float4 _Layer4Tex_ST;
			float4 _Layer5Tex_ST;
			float4 _OverlayTex_ST;

			float4 _Layer0TexPrev_ST;
			float4 _Layer1TexPrev_ST;
			float4 _Layer2TexPrev_ST;
			float4 _Layer3TexPrev_ST;
			float4 _Layer4TexPrev_ST;
			float4 _Layer5TexPrev_ST;
			float4 _OverlayTexPrev_ST;

			float4 _Layer0TexPos;
			float4 _Layer1TexPos;
			float4 _Layer2TexPos;
			float4 _Layer3TexPos;
			float4 _Layer4TexPos;
			float4 _Layer5TexPos;
			float4 _OverlayTexPos;

			float4 _Layer0TexPosPrev;
			float4 _Layer1TexPosPrev;
			float4 _Layer2TexPosPrev;
			float4 _Layer3TexPosPrev;
			float4 _Layer4TexPosPrev;
			float4 _Layer5TexPosPrev;
			float4 _OverlayTexPosPrev;

			v2f vert(appdata_t IN)
			{
				v2f OUT;
				UNITY_SETUP_INSTANCE_ID(IN);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(OUT);
				OUT.vertex = UnityObjectToClipPos(IN.vertex);
				OUT.color = IN.color;
				OUT.color.rgb *= OUT.color.a;

				float2 curr = getUv(IN.texcoord);
				float2 prev = getUvForPrev(IN.texcoord);

				OUT.texcoordLayer0.xy = calcTexcoordSA(curr, _Layer0TexPos, _Layer0Tex_ST);
				OUT.texcoordLayer0.zw = calcTexcoordSA(prev, _Layer0TexPosPrev, _Layer0TexPrev_ST);

				OUT.texcoordLayer1.xy = calcTexcoordSA(curr, _Layer1TexPos, _Layer1Tex_ST);
				OUT.texcoordLayer1.zw = calcTexcoordSA(prev, _Layer1TexPosPrev, _Layer1TexPrev_ST);

				OUT.texcoordLayer2.xy = calcTexcoordSA(curr, _Layer2TexPos, _Layer2Tex_ST);
				OUT.texcoordLayer2.zw = calcTexcoordSA(prev, _Layer2TexPosPrev, _Layer2TexPrev_ST);

				OUT.texcoordLayer3.xy = calcTexcoordSA(curr, _Layer3TexPos, _Layer3Tex_ST);
				OUT.texcoordLayer3.zw = calcTexcoordSA(prev, _Layer3TexPosPrev, _Layer3TexPrev_ST);

				OUT.texcoordLayer4.xy = calcTexcoordSA(curr, _Layer4TexPos, _Layer4Tex_ST);
				OUT.texcoordLayer4.zw = calcTexcoordSA(prev, _Layer4TexPosPrev, _Layer4TexPrev_ST);

				OUT.texcoordLayer5.xy = calcTexcoordSA(curr, _Layer5TexPos, _Layer5Tex_ST);
				OUT.texcoordLayer5.zw = calcTexcoordSA(prev, _Layer5TexPosPrev, _Layer5TexPrev_ST);

				OUT.texcoordOverlay.xy = calcTexcoordSA(curr, _OverlayTexPos, _OverlayTex_ST);
				OUT.texcoordOverlay.zw = calcTexcoordSA(prev, _OverlayTexPosPrev, _OverlayTexPrev_ST);

				return OUT;
			}

			sampler2D _MainTex;

			sampler2D _Layer0Tex;
			sampler2D _Layer1Tex;
			sampler2D _Layer2Tex;
			sampler2D _Layer3Tex;
			sampler2D _Layer4Tex;
			sampler2D _Layer5Tex;
			sampler2D _OverlayTex;

			sampler2D _Layer0TexPrev;
			sampler2D _Layer1TexPrev;
			sampler2D _Layer2TexPrev;
			sampler2D _Layer3TexPrev;
			sampler2D _Layer4TexPrev;
			sampler2D _Layer5TexPrev;
			sampler2D _OverlayTexPrev;

			fixed4 frag(v2f IN) : SV_Target
			{

#if UseLayer5AsLayerMinus1

				fixed4 current = combineTexturesSA(
					trimColorSA(_Layer5Tex, IN.texcoordLayer5.xy, _Layer5TexPos),
					trimColorSA(_Layer0Tex, IN.texcoordLayer0.xy, _Layer0TexPos),
					trimColorSA(_Layer1Tex, IN.texcoordLayer1.xy, _Layer1TexPos),
					trimColorSA(_Layer2Tex, IN.texcoordLayer2.xy, _Layer2TexPos),
					trimColorSA(_Layer3Tex, IN.texcoordLayer3.xy, _Layer3TexPos),
					trimColorSA(_Layer4Tex, IN.texcoordLayer4.xy, _Layer4TexPos)
				);

				fixed4 previous = combineTexturesSA(
					trimColorSA(_Layer5TexPrev, IN.texcoordLayer5.zw, _Layer5TexPosPrev),
					trimColorSA(_Layer0TexPrev, IN.texcoordLayer0.zw, _Layer0TexPosPrev),
					trimColorSA(_Layer1TexPrev, IN.texcoordLayer1.zw, _Layer1TexPosPrev),
					trimColorSA(_Layer2TexPrev, IN.texcoordLayer2.zw, _Layer2TexPosPrev),
					trimColorSA(_Layer3TexPrev, IN.texcoordLayer3.zw, _Layer3TexPosPrev),
					trimColorSA(_Layer4TexPrev, IN.texcoordLayer4.zw, _Layer4TexPosPrev)
				);
#else

				fixed4 current = combineTexturesSA(
					trimColorSA(_Layer0Tex, IN.texcoordLayer0.xy, _Layer0TexPos),
					trimColorSA(_Layer1Tex, IN.texcoordLayer1.xy, _Layer1TexPos),
					trimColorSA(_Layer2Tex, IN.texcoordLayer2.xy, _Layer2TexPos),
					trimColorSA(_Layer3Tex, IN.texcoordLayer3.xy, _Layer3TexPos),
					trimColorSA(_Layer4Tex, IN.texcoordLayer4.xy, _Layer4TexPos),
					trimColorSA(_Layer5Tex, IN.texcoordLayer5.xy, _Layer5TexPos)
				);

				fixed4 previous = combineTexturesSA(
					trimColorSA(_Layer0TexPrev, IN.texcoordLayer0.zw, _Layer0TexPosPrev),
					trimColorSA(_Layer1TexPrev, IN.texcoordLayer1.zw, _Layer1TexPosPrev),
					trimColorSA(_Layer2TexPrev, IN.texcoordLayer2.zw, _Layer2TexPosPrev),
					trimColorSA(_Layer3TexPrev, IN.texcoordLayer3.zw, _Layer3TexPosPrev),
					trimColorSA(_Layer4TexPrev, IN.texcoordLayer4.zw, _Layer4TexPosPrev),
					trimColorSA(_Layer5TexPrev, IN.texcoordLayer5.zw, _Layer5TexPosPrev)
				);
#endif



				fixed4 overlay = tex2D(_OverlayTex, IN.texcoordOverlay.xy);
				fixed4 overlayPrev = tex2D(_OverlayTexPrev, IN.texcoordOverlay.zw);

				overlay.rgb *= current.a;
				overlayPrev.rgb *= previous.a;

				current.rgb = lerp(current.rgb, overlay.rgb, overlay.a);
				previous.rgb = lerp(previous.rgb, overlayPrev.rgb, overlayPrev.a);

				return lerp(previous, current, _Fade) * IN.color;

			}

		ENDCG

		}

	}

}
