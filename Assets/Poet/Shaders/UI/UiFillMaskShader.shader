﻿Shader "Custom/Poet/UI Fill Mask Shader"
{
	Properties
	{
		[PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
		[Enum(UnityEngine.Rendering.BlendMode)] _Blend("Blend mode", Float) = 1
		_FillAmount("Fill Amount", Range(0.0, 1.0)) = 1.0
		_MaskTex("Mask Texture", 2D) = "black" {}

	}

	SubShader
	{
		Tags
		{ 
			"Queue"="Transparent" 
			"IgnoreProjector"="True" 
			"RenderType"="Transparent" 
			"PreviewType"="Plane"
			"CanUseSpriteAtlas"="True"
		}

		Cull Off
		Lighting Off
		ZWrite Off
		Blend One [_Blend]

		Pass
		{
		CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 2.0
			#pragma multi_compile _ PIXELSNAP_ON
			#pragma multi_compile _ ETC1_EXTERNAL_ALPHA
			#include "UnityCG.cginc"
			
			struct appdata_t
			{
				float4 vertex   : POSITION;
				float4 color    : COLOR;
				float2 texcoord : TEXCOORD0;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct v2f
			{
				float4 vertex   : SV_POSITION;
				fixed4 color    : COLOR;
				float2 texcoord  : TEXCOORD0;
				float2 texcoordMask  : TEXCOORD1;
				UNITY_VERTEX_OUTPUT_STEREO
			};
			
			float _FillAmount;

			float4 _MaskTex_ST;

			v2f vert(appdata_t IN)
			{
				v2f OUT;
				UNITY_SETUP_INSTANCE_ID(IN);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(OUT);
				OUT.vertex = UnityObjectToClipPos(IN.vertex);
				OUT.texcoord = IN.texcoord;
				OUT.color = IN.color;

				OUT.texcoordMask = TRANSFORM_TEX(IN.texcoord, _MaskTex);

				return OUT;
			}

			sampler2D _MainTex;
			sampler2D _MaskTex;
			sampler2D _AlphaTex;

			fixed4 SampleSpriteTexture (float2 uv)
			{
				fixed4 color = tex2D (_MainTex, uv);

#if ETC1_EXTERNAL_ALPHA
				// get the color from an external texture (usecase: Alpha support for ETC1 on android)
				color.a = tex2D (_AlphaTex, uv).r;
#endif //ETC1_EXTERNAL_ALPHA

				return color;
			}

			fixed4 frag(v2f IN) : SV_Target
			{

				fixed4 c = SampleSpriteTexture (IN.texcoord) * IN.color;
				c.rgb *= c.a;

				fixed4 firstColor = fixed4(0, 0, 0, 0);

				fixed mask = tex2D(_MaskTex, IN.texcoordMask).a;

				return lerp(firstColor, c, saturate((_FillAmount * 2) + mask - 1));

			}
		ENDCG
		}
	}
}
