﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Poet
{

    /// <summary>
    /// User progress data preview
    /// </summary>
    public class UserProgressUiDataPreviewScript : UiDataPreviewScript
    {

        /// <summary>
        /// Reference to data number Text
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to data number Text")]
        protected Text m_refDataNumber;

        /// <summary>
        /// Reference to date Text
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to date Text")]
        protected Text m_refDateText;

        /// <summary>
        /// Reference to description Text
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to description Text")]
        protected Text m_refDescriptionText;

        /// <summary>
        /// Reference to thumbnail Image
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to thumbnail Image")]
        protected RawImage m_refDataImage;

        /// <summary>
        /// Reference to now loading panel
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to now loading panel")]
        protected RectTransform m_refLoadingPanel;

        /// <summary>
        /// Data number
        /// </summary>
        protected int m_currentDataNumber = 0;

        /// <summary>
        /// Current data
        /// </summary>
        protected PoetUserProgressDataSO m_currentData = null;

        /// <summary>
        /// IEnumerator for readData
        /// </summary>
        protected IEnumerator m_readDataIEnumerator = null;

        /// <summary>
        /// Start
        /// </summary>
        // -------------------------------------------------------------------------------------------
        protected virtual void Start()
        {

            if (this.m_refLoadingPanel)
            {
                this.m_refLoadingPanel.gameObject.SetActive(false);
            }

        }

        /// <summary>
        /// onClick
        /// </summary>
        // -------------------------------------------------------------------------
        public override void onClick()
        {

            if (PoetCommonUiManager.Instance.containsIdentifier(PoetUiIdentifiers.Common.Save))
            {
                this.onClickSaveButton();
            }

            else if (PoetCommonUiManager.Instance.containsIdentifier(PoetUiIdentifiers.Common.Load))
            {
                this.onClickLoadButton();
            }

            else
            {
                Debug.LogError("Invalid usage");
            }

        }

        /// <summary>
        /// Function when load button is clicked
        /// </summary>
        // -------------------------------------------------------------------------------------------
        public void onClickLoadButton()
        {

            if (!this.m_currentData || this.m_currentData.isDefaultData())
            {
                return;
            }

            // -----------------------

            PoetDialogManager.Instance.showYesNoDialog(
                PoetDialogMessageManager.Instance.loadUserProgressDataMessage(),
                () =>
                {
                    (PoetSceneChangeManager.Instance as PoetSceneChangeManager).loadSceneWithUserProgressData(this.m_currentData);
                },
                null
                );

        }

        /// <summary>
        /// Function when save panel is clicked
        /// </summary>
        // -------------------------------------------------------------------------------------------
        public void onClickSaveButton()
        {

            PoetDialogManager.Instance.consecutiveShowing = true;

            System.Object message = null;

            if (this.m_currentData == null || this.m_currentData.isDefaultData())
            {
                message = PoetDialogMessageManager.Instance.saveUserProgressDataMessage();
            }

            else
            {
                message = PoetDialogMessageManager.Instance.overwriteUserProgressDataMessage();
            }

            PoetDialogManager.Instance.showYesNoDialog(
                message,
                this.startSavingData,
                () =>
                {
                    PoetDialogManager.Instance.finishDialog(null);
                }
                );

        }

        /// <summary>
        /// Start saving
        /// </summary>
        // -------------------------------------------------------------------------------------------
        protected void startSavingData()
        {

            PoetDialogManager.Instance.showProgressDialog(
                PoetDialogMessageManager.Instance.saveUserProgressDataProgressMessage(),
                () =>
                {
                    SystemManager.Instance.saveCurrentUserProgressData(
                        this.m_currentDataNumber.ToString(),
                        this.saveResultCallback
                        );
                });

        }

        /// <summary>
        /// Save result callback
        /// </summary>
        /// <param name="result">result</param>
        // -------------------------------------------------------------------------------------------
        protected void saveResultCallback(string result)
        {

            PoetDialogManager.Instance.finishProgressDialog(() =>
            {

                if (string.IsNullOrEmpty(result))
                {
                    this.setUiData(this.m_currentDataNumber);
                    PoetDialogManager.Instance.finishDialog(null);
                }

                else
                {
                    PoetDialogManager.Instance.showOkDialog(
                        PoetDialogMessageManager.Instance.saveDataErrorMessage(result),
                        () =>
                        {
                            PoetDialogManager.Instance.finishDialog(null);
                        }
                        );
                }

            });

        }

        /// <summary>
        /// Read data
        /// </summary>
        /// <param name="dataNumber">data number</param>
        /// <returns>IEnumerator</returns>
        // -------------------------------------------------------------------------------------------
        protected virtual IEnumerator readData(int dataNumber)
        {

            this.m_currentDataNumber = dataNumber;

            if (this.m_refLoadingPanel)
            {
                this.m_refLoadingPanel.gameObject.SetActive(true);
                var temp = this.m_refLoadingPanel.anchoredPosition;
                this.m_refLoadingPanel.anchoredPosition += Vector2.one * 0.0001f;
                this.m_refLoadingPanel.anchoredPosition = temp;
            }

            yield return SystemManager.Instance.readUserProgressData<PoetUserProgressDataSO>(dataNumber.ToString(), (ret, message) =>
            {

                this.setPreviewData(ret);

                if (!string.IsNullOrEmpty(message))
                {
                    PoetDialogManager.Instance.showOkDialog(PoetDialogMessageManager.Instance.loadDataErrorMessage(message), null);
                }

            });

            if (this.m_refLoadingPanel)
            {
                this.m_refLoadingPanel.gameObject.SetActive(false);
            }

            this.m_readDataIEnumerator = null;

        }

        /// <summary>
        /// Set preview data
        /// </summary>
        /// <param name="data">data</param>
        // -------------------------------------------------------------------------------------------
        protected virtual void setPreviewData(PoetUserProgressDataSO data)
        {

            if (data == null)
            {
                data = ScriptableObject.CreateInstance<PoetUserProgressDataSO>();
            }

            this.m_currentData = data;

            // -----------------------

            if (this.m_refDataNumber)
            {
                this.m_refDataNumber.text = "No. " + this.m_currentDataNumber.ToString().PadLeft(3, '0');
            }

            if (this.m_refDateText)
            {

                if (!data.isDefaultData())
                {
                    this.m_refDateText.text = data.dateTime().ToString(SystemManager.DateTimeFormat);
                }

                else
                {
                    this.m_refDateText.text = "----/--/-- --:--:--";
                }

            }

            if (this.m_refDescriptionText)
            {
                this.m_refDescriptionText.text = data.descriptionText.ToString();
            }

            if (this.m_refDataImage)
            {

                if (this.m_refDataImage.texture)
                {
                    Destroy(this.m_refDataImage.texture);
                    this.m_refDataImage.texture = null;
                }

                if (data.screenshotPngBytes != null && data.screenshotPngBytes.Length > 0)
                {
                    Texture2D tex = new Texture2D(2, 2);
                    tex.LoadImage(data.screenshotPngBytes);
                    this.m_refDataImage.texture = tex;
                    this.m_refDataImage.color = Color.white;
                }

                else
                {
                    this.m_refDataImage.texture = null;
                    this.m_refDataImage.color = Color.black;
                }

            }

        }

        /// <summary>
        /// Set UI data
        /// </summary>
        /// <param name="data">data</param>
        // -------------------------------------------------------------------------
        public override void setUiData(System.Object data)
        {

            int dataNumber = 0;

            try
            {
                dataNumber = Convert.ToInt32(data);
            }

            catch(Exception e)
            {
                Debug.LogError(e.Message);
                return;
            }
            
            // ------------------------

            // StartCoroutine
            {

                if (this.m_readDataIEnumerator != null)
                {
                    StopCoroutine(this.m_readDataIEnumerator);
                }

                StartCoroutine(this.m_readDataIEnumerator = this.readData(dataNumber));

            }

        }

    }

}
