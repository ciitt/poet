﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poet
{

    /// <summary>
    /// User progress data preview group
    /// </summary>
    public class UserProgressUiDataPreviewGroupScript : UiDataPreviewGroupScript
    {

        /// <summary>
        /// Set page
        /// </summary>
        // --------------------------------------------------------------------------------------------------
        public override void setPage(int pagerIndexFrom0)
        {

            if (pagerIndexFrom0 < 0)
            {
                return;
            }

            // ---------------------------

            int previewListCount = this.m_refPreviewList.Count;

            int from = previewListCount * pagerIndexFrom0;

            for (int i = 0, j = from; i < previewListCount; i++, j++)
            {
                this.m_refPreviewList[i].setUiData(j + 1);
            }

        }

    }

}
