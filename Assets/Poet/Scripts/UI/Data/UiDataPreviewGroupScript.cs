﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poet
{

    public abstract class UiDataPreviewGroupScript : MonoBehaviour
    {

        /// <summary>
        /// Reference to AlbumGraphicDataPreviewScript List
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to AlbumGraphicDataPreviewScript List")]
        protected List<UiDataPreviewScript> m_refPreviewList = new List<UiDataPreviewScript>();

        /// <summary>
        /// Reference to data List
        /// </summary>
        protected IList m_refDataList = null;

        // --------------------------------------------------------------------------------------------

        /// <summary>
        /// Preview list count
        /// </summary>
        public int previewListCount { get { return this.m_refPreviewList.Count; } }

        // --------------------------------------------------------------------------------------------

        /// <summary>
        /// Awake
        /// </summary>
        // --------------------------------------------------------------------------------------------
        protected virtual void Awake()
        {

            foreach (var val in this.m_refPreviewList)
            {

                if (val)
                {
                    val.refPreviewGroupScript = this;
                }

                else
                {
                    Debug.LogWarning("m_refPreviewList has null value : " + this.gameObject.name);
                }

            }

            if (this.m_refPreviewList.Count <= 0)
            {
                Debug.LogWarning("m_refPreviewList is empty : " + this.gameObject.name);
            }

        }

        /// <summary>
        /// Set all data
        /// </summary>
        // --------------------------------------------------------------------------------------------------
        public virtual void setAllData(IList dataList)
        {
            this.m_refDataList = dataList;
        }

        /// <summary>
        /// Set page
        /// </summary>
        // --------------------------------------------------------------------------------------------------
        public virtual void setPage(int pagerIndexFrom0)
        {

            if(pagerIndexFrom0 < 0 || this.m_refDataList == null)
            {
                return;
            }

            // ---------------------------

            int previewListCount = this.m_refPreviewList.Count;

            int from = previewListCount * pagerIndexFrom0;

            int dataCount = this.m_refDataList.Count;

            for (int i = 0, j = from; i < previewListCount; i++, j++)
            {

                if (j < dataCount)
                {
                    this.m_refPreviewList[i].setUiData(this.m_refDataList[j]);
                }

                else
                {
                    this.m_refPreviewList[i].setUiData(null);
                }

            }

        }

    }

}
