﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poet
{

    public abstract class UiDataHolder : MonoBehaviour
    {

        protected abstract IList dataList();

        /// <summary>
        /// Reference to UiDataPreviewGroupScript
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to UiDataPreviewGroupScript")]
        protected UiDataPreviewGroupScript m_refUiDataPreviewGroupScript;

        /// <summary>
        /// Reference to PagerButtonGroupScript
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to PagerButtonGroupScript")]
        protected PagerButtonGroupScript m_refPagerButtonGroupScript;

        /// <summary>
        /// Awake
        /// </summary>
        // --------------------------------------------------------------------------------------------
        protected virtual void Awake()
        {

#if UNITY_EDITOR

            if(!this.m_refUiDataPreviewGroupScript)
            {
                Debug.LogWarning("m_refUiDataPreviewGroupScript is null");
            }

            if (!this.m_refPagerButtonGroupScript)
            {
                Debug.LogWarning("(#if UNITY_EDITOR) m_refPagerButtonGroupScript is null");
            }

#endif

        }

        /// <summary>
        /// Function when button is clicked
        /// </summary>
        /// <param name="pageIndexFrom0">page index</param>
        // -----------------------------------------------------------------------
        public virtual void onClick(int pageIndexFrom0)
        {

            if (pageIndexFrom0 < 0)
            {
                return;
            }

            // --------------

            if (this.m_refUiDataPreviewGroupScript)
            {

                this.m_refUiDataPreviewGroupScript.setAllData(this.dataList());

                if (this.m_refPagerButtonGroupScript)
                {
                    this.m_refPagerButtonGroupScript.activateButtons(this.m_refUiDataPreviewGroupScript.previewListCount, this.dataList().Count);
                    this.m_refPagerButtonGroupScript.sendPageIndexSignal(pageIndexFrom0);
                }

            }

        }

    }

}
