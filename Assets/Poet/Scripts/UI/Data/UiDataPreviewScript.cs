﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poet
{

    public abstract class UiDataPreviewScript : MonoBehaviour
    {

        /// <summary>
        /// Set UI data
        /// </summary>
        /// <param name="data">data</param>
        public abstract void setUiData(System.Object data);

        /// <summary>
        /// onClick
        /// </summary>
        public abstract void onClick();

        /// <summary>
        /// Reference to UiDataPreviewGroupScript
        /// </summary>
        protected UiDataPreviewGroupScript m_refPreviewGroupScript = null;

        /// <summary>
        ///  Reference to UiDataPreviewGroupScript
        /// </summary>
        public UiDataPreviewGroupScript refPreviewGroupScript { get { return this.m_refPreviewGroupScript; } set { this.m_refPreviewGroupScript = value; } }

    }

}
