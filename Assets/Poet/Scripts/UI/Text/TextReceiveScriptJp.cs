﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poet
{

    /// <summary>
    /// TextReceiveScript for Japanese
    /// </summary>
    public class TextReceiveScriptJp : TextReceiveScript
    {

        protected List<char> m_invalidAtStart = new List<char>()
        {

            ',',')',']','｝','、','〕','〉','》','」','』', '】','〙','〗','〟','’','”','｠','»',

            //'ゝ','ゞ', 'ー','ァ','ィ','ゥ','ェ','ォ','ッ','ャ','ュ','ョ',
            //'ヮ','ヵ','ヶ','ぁ','ぃ','ぅ','ぇ','ぉ','っ','ゃ',
            //'ゅ','ょ','ゎ','ゕ','ゖ','ㇰ','ㇱ','ㇲ','ㇳ','ㇴ',
            //'ㇵ','ㇶ','ㇷ','ㇸ','ㇹ','ㇷ','゚','ㇺ','ㇻ','ㇼ',
            //'ㇽ','ㇾ','ㇿ','々','〻',

            '‐','゠','–','〜','～',

            '?','!','‼','⁇','⁈','⁉',

            '・',':',';','/', '。','.'

        };

        protected List<char> m_invalidAtEnd = new List<char>()
        {
            '(','[','｛','〔','〈','《','「','『','【','〘','〖','〝','‘','“','｟','«'
        };

        /// <summary>
        /// TextGenerator
        /// </summary>
        protected TextGenerator m_textGenerator = null;

        /// <summary>
        /// Japanese space char width
        /// </summary>
        protected float m_jpSpaceWidth = 0.0f;

        /// <summary>
        /// Called in Awake
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected override void initOnAwake()
        {

            base.initOnAwake();

            // m_textGenerator
            {
                this.m_textGenerator = new TextGenerator();
            }

        }

        /// <summary>
        /// Start
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected override void Start()
        {

            base.Start();

            Invoke("calcJpSpaceWidth", 0.1f);

        }

        /// <summary>
        /// Calc japanese space width
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected void calcJpSpaceWidth()
        {

            this.m_textGenerator.Populate("　", this.m_refText.GetGenerationSettings(this.m_refText.rectTransform.sizeDelta));

            var characters = this.m_textGenerator.characters;

            if (characters.Count > 0)
            {
                this.m_jpSpaceWidth = characters[0].charWidth;
            }

        }

        /// <summary>
        /// Get TextGenerator
        /// </summary>
        /// <returns>TextGenerator</returns>
        // ------------------------------------------------------------------------------------------
        protected override TextGenerator textGenerator()
        {

            if (SystemManager.Instance.systemLanguage == SystemLanguage.Japanese)
            {
                return this.m_textGenerator;
            }

            else
            {
                return base.textGenerator();
            }
                
        }

        /// <summary>
        /// Validate
        /// </summary>
        /// <param name="text">raw text</param>
        /// <returns>validated text</returns>
        // ------------------------------------------------------------------------------------------
        protected override IEnumerator validateString(string text)
        {

            if (SystemManager.Instance.systemLanguage != SystemLanguage.Japanese)
            {
                yield return base.validateString(text);
                yield break;
            }

            // --------------------------

            // horizontalOverflow
            {
                this.m_refText.horizontalOverflow = HorizontalWrapMode.Overflow;
            }

            //TextGenerationSettings settings = this.m_refText.GetGenerationSettings(this.m_refText.rectTransform.sizeDelta);

            // Populate
            {
                this.m_textGenerator.Populate(text, this.m_refText.GetGenerationSettings(this.m_refText.rectTransform.sizeDelta));
            }

            //
            {

                float width = this.m_refText.rectTransform.sizeDelta.x;

                //var characters = this.m_refText.cachedTextGeneratorForLayout.characters;
                var characters = this.m_textGenerator.characters;

                int size = Mathf.Min(text.Length, characters.Count);
                float lineWidthCounter = 0.0f;

                List<int> addIndexes = new List<int>();

                float screenAdjuster = this.m_canvasReferenceResolution.x / (float)Screen.width;

                //
                {

                    bool startKagiKakko = (text.Length > 0 && (text[0] == '「' || text[0] == '『'));

                    for (int i = 0; i < size - 1; i++)
                    {

                        lineWidthCounter += characters[i].charWidth * screenAdjuster;

                        if (lineWidthCounter + (characters[i + 1].charWidth * screenAdjuster) > width)
                        {

                            if (!this.m_invalidAtEnd.Contains(text[i]) && !this.m_invalidAtStart.Contains(text[i + 1]))
                            {
                                addIndexes.Add(i + 1);
                                lineWidthCounter = (startKagiKakko) ? this.m_jpSpaceWidth * screenAdjuster : 0.0f;
                            }

                        }

                    }

                }

                // add
                {

                    string add = "\n";

                    //
                    {

                        if (
                            text.Length > 0 &&
                            (text[0] == '「' || text[0] == '『')
                            )
                        {
                            add += "　";
                        }

                    }

                    for (int i = addIndexes.Count - 1; i >= 0; i--)
                    {
                        text = text.Insert(addIndexes[i], add);
                    }

                }

            }

            // set text
            {
                this.m_refText.text = text;
            }

            yield return null;

            // Populate
            {
                this.m_textGenerator.Populate(text, this.m_refText.GetGenerationSettings(this.m_refText.rectTransform.sizeDelta));
            }

            yield return null;

        }

    }

}
