﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Poet
{

    /// <summary>
    /// Text receiver
    /// </summary>
    [RequireComponent(typeof(Text))]
    public class TextReceiveScript : PoetBaseReceiverScript
    {

        /// <summary>
        /// User progress data
        /// </summary>
        [Serializable]
        protected class UserProgressData
        {

            public string rawText = "";
            public string replaceStrings = "";

            public void clear()
            {
                this.rawText = "";
                this.replaceStrings = "";
            }

        }

        protected class OneLineWidthAndWaitPoints
        {

            public float oneLineWidth = 0.0f;
            public List<float> waitPoints = new List<float>();

            public int currentIndex = 0;

            public OneLineWidthAndWaitPoints(float _oneLineWidth, List<float> _waitPoints)
            {
                this.oneLineWidth = _oneLineWidth;

                this.waitPoints.Clear();
                this.waitPoints.AddRange(_waitPoints);
            }

            public float update(float currentVisibleWidth)
            {

                if (currentIndex < this.waitPoints.Count)
                {

                    if (this.waitPoints[currentIndex] < currentVisibleWidth)
                    {
                        return this.waitPoints[currentIndex++];
                    }

                }

                return -1.0f;

            }

        }

#if UNITY_EDITOR

        /// <summary>
        /// static instance
        /// </summary>
        protected static TextReceiveScript instanceEditorOnly = null;

#endif

        /// <summary>
        /// Min text speed
        /// </summary>
        [SerializeField]
        [Tooltip("Min text speed")]
        [Range(0.001f, 0.05f)]
        protected float m_minTextSpeed = 0.002f;

        /// <summary>
        /// Min text speed
        /// </summary>
        [SerializeField]
        [Tooltip("Min text speed")]
        [Range(0.001f, 0.05f)]
        protected float m_maxTextSpeed = 0.04f;

        /// <summary>
        /// Chars for waiting
        /// </summary>
        [SerializeField]
        [Tooltip("Chars for waiting")]
        protected List<char> m_waitChars = new List<char>();

        /// <summary>
        /// Wait seconds for m_waitChars
        /// </summary>
        [SerializeField]
        [Tooltip("Wait seconds for m_waitChars")]
        protected float m_waitCharSeconds = 0.2f;

        /// <summary>
        /// Pixel adjuster for m_waitChars
        /// </summary>
        [SerializeField]
        [Tooltip("Pixel adjuster for m_waitChars")]
        protected float m_waitCharPixelAdjuster = 0.0f;

        /// <summary>
        /// Reference to ScrollRect
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to ScrollRect")]
        protected ScrollRect m_refScrollRect = null;

        /// <summary>
        /// Visible line count
        /// </summary>
        [SerializeField]
        [Tooltip("Visible line count")]
        protected int m_visibleLineCount = 3;

        /// <summary>
        /// Reference to Masks
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to Masks")]
        protected List<Image> m_refMaskLines = new List<Image>();

        /// <summary>
        /// Csv header for text replacement
        /// </summary>
        [SerializeField]
        [Tooltip("Csv header for text replacement")]
        protected string m_csvHeaderForTextReplacement = "Text Replacement";

        /// <summary>
        /// Languaget and its header
        /// </summary>
        [SerializeField]
        [Tooltip("Languaget and its header")]
        protected LanguageAndHeaders m_languageAndHeaders = new LanguageAndHeaders("Text");

        /// <summary>
        /// Reference to Text
        /// </summary>
        protected Text m_refText = null;

        /// <summary>
        /// Show all text immediately
        /// </summary>
        protected bool m_showAllTextImmediately = false;

        /// <summary>
        /// Mask size
        /// </summary>
        protected Vector2 m_refDefaultMaskSize = Vector2.zero;

        /// <summary>
        /// Canvas referenceResolution
        /// </summary>
        protected Vector2 m_canvasReferenceResolution = new Vector2(1920, 1080);

        /// <summary>
        /// Original text color
        /// </summary>
        protected Color m_oriTextColor = Color.white;

        /// <summary>
        /// UserProgressData
        /// </summary>
        protected UserProgressData m_userProgressData = new UserProgressData();

        /// <summary>
        /// Called in Awake
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected override void initOnAwake()
        {

            // instance
            {

#if UNITY_EDITOR

                if(instanceEditorOnly)
                {
                    Debug.LogWarning("(#if UNITY_EDITOR) TextReceiveScript should be singleton.");
                }

                instanceEditorOnly = this;

#endif

            }

            // set
            {

                this.m_refText = this.GetComponent<Text>();
                this.m_refText.text = "";

                if (this.m_refMaskLines.Count > 0)
                {
                    this.m_refDefaultMaskSize = this.m_refMaskLines[0].rectTransform.sizeDelta;
                }

            }

            // m_languageAndHeaders
            {
                this.m_languageAndHeaders.initDictionary();
            }

            // m_oriTextColor
            {
                this.m_oriTextColor = this.m_refText.color;
            }

        }

        /// <summary>
        /// Start
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected virtual void Start()
        {

            // m_canvasReferenceResolution
            {

                CanvasScaler cs = this.m_refText.canvas.GetComponent<CanvasScaler>();
                
                if (cs)
                {
                    this.m_canvasReferenceResolution = cs.referenceResolution;
                }

                else
                {
                    Debug.LogWarning("GetComponentInParent<CanvasScaler> not found : " + this.gameObject.name);
                }

            }
            
        }

        /// <summary>
        /// Create description text
        /// </summary>
        /// <returns>description text</returns>
        // ------------------------------------------------------------------------------------------
        protected virtual string createDescriptionText(string text)
        {

            int descriptionTextLength = SystemManager.Instance.descriptionTextLength;

            if (descriptionTextLength <= 0)
            {
                return text;
            }

            if (text.Length <= 3)
            {
                return text;
            }

            if (text.Length >= descriptionTextLength)
            {
                text = text.Remove(descriptionTextLength - 3);
                text = text + "...";
            }

            return text;

        }

        /// <summary>
        /// Change text color
        /// </summary>
        // -----------------------------------------------------------------------------------------------
        protected virtual void changeTextColorIfNeeded()
        {

            PoetConfigDataSO conf = SystemManager.Instance.configDataSO<PoetConfigDataSO>();

            if (!conf.changeAlreadyReadTextColor)
            {
                this.m_refText.color = this.m_oriTextColor;
            }

            else
            {

                if (PoetOneSceneManager.isAvailable())
                {

                    if (PoetOneSceneManager.Instance.isCurrentSequenceAlreadyRead())
                    {
                        this.m_refText.color = conf.alreadyReadTextColor;
                    }

                    else
                    {
                        this.m_refText.color = this.m_oriTextColor;
                    }

                }

            }

        }

        /// <summary>
        /// Clear texts
        /// </summary>
        // -----------------------------------------------------------------------------------------------
        protected void clearTexts()
        {

            StopAllCoroutines();

            if (this.m_refScrollRect)
            {
                this.m_refScrollRect.verticalNormalizedPosition = 1.0f;
            }

            for (int i = this.m_refMaskLines.Count - 1; i >= 0; i--)
            {
                this.m_refMaskLines[i].rectTransform.sizeDelta = new Vector2(this.m_refDefaultMaskSize.x, this.m_refDefaultMaskSize.y);
            }

            {
                this.m_refText.text = "";
            }

            this.m_userProgressData.clear();

        }

        /// <summary>
        /// SceneChangeState receiver
        /// </summary>
        /// <param name="scState">SceneChangeState</param>
        // -----------------------------------------------------------------------------------------------
        protected override void onSceneChangeStateReceiver(SSC.SceneChangeState scState)
        {

            if (scState.stateEnum == SSC.SceneChangeState.StateEnum.NowLoadingIntro)
            {
                this.clearTexts();
            }

            else if (scState.stateEnum == SSC.SceneChangeState.StateEnum.ScenePlaying)
            {

                PoetSceneChangeManager pscm = PoetSceneChangeManager.Instance as PoetSceneChangeManager;

                if (pscm.isLoadingCurrentSceneWithUserProgressData())
                {

                    this.clearTexts();

                    this.m_userProgressData = pscm.getDataFromCurrentUserProgressData<UserProgressData>(this.transform, this);

                    // set
                    {
                        this.startShowingText(this.m_userProgressData.rawText, true, this.m_userProgressData.replaceStrings);
                    }

                }

            }

        }

        /// <summary>
        /// Replace
        /// </summary>
        /// <param name="rawText">raw text</param>
        /// <param name="replaceStrings">replace targets</param>
        /// <returns>replaced text</returns>
        // ------------------------------------------------------------------------------------------
        protected virtual string replaceText(string rawText, string replaceStrings)
        {

            if(string.IsNullOrEmpty(replaceStrings) || !PoetTextReplacementManager.isAvailable())
            {
                return rawText;
            }

            // ----------------------

            string[] replace = replaceStrings.Split('\n');

            try
            {

                foreach (var val in replace)
                {
                    rawText = rawText.Replace(val, PoetTextReplacementManager.Instance.getReplacementText(val));
                }

            }

            catch (Exception e)
            {
                Debug.LogError(e.Message);
            }

            return rawText;

        }

        /// <summary>
        /// Validate
        /// </summary>
        /// <param name="text">raw text</param>
        /// <returns>validated text</returns>
        // ------------------------------------------------------------------------------------------
        protected virtual IEnumerator validateString(string text)
        {

            this.m_refText.horizontalOverflow = HorizontalWrapMode.Wrap;
            this.m_refText.text = text;

            yield return null;

        }

        /// <summary>
        /// Get TextGenerator
        /// </summary>
        /// <returns>TextGenerator</returns>
        // ------------------------------------------------------------------------------------------
        protected virtual TextGenerator textGenerator()
        {
            return this.m_refText.cachedTextGenerator;
        }

        /// <summary>
        /// Create wait points
        /// </summary>
        /// <param name="validadedText">validaded text</param>
        /// <returns>OneLineWidthAndWaitPoints list</returns>
        // ------------------------------------------------------------------------------------------
        protected virtual List<OneLineWidthAndWaitPoints> createWaitPoints(string validadedText)
        {

            TextGenerator gen = this.textGenerator();

            List<OneLineWidthAndWaitPoints> ret = new List<OneLineWidthAndWaitPoints>();

            if (gen == null)
            {
                return ret;
            }
            
            // -------------------

            // width
            {

                float screenAdjuster = this.m_canvasReferenceResolution.x / (float)Screen.width;
                
                float width = 0.0f;

                int startIndex = 0;
                int endIndex = 0;

                int lineCount = gen.lines.Count;

                List<float> waitPoints = new List<float>();

                float anchoredPositionX = this.m_refText.rectTransform.anchoredPosition.x;

                for (int i = 0; i < lineCount; i++)
                {

                    // reset
                    {

                        startIndex = gen.lines[i].startCharIdx;
                        endIndex =
                            (i == lineCount - 1) ?
                            Mathf.Min(gen.characters.Count, validadedText.Length) :
                            Mathf.Min(gen.lines[i + 1].startCharIdx, validadedText.Length)
                            ;

                        width = 0.0f;

                        waitPoints.Clear();

                    }

                    // count
                    {

                        for (int j = startIndex; j < endIndex; j++)
                        {

                            width += gen.characters[j].charWidth;

                            if(i == lineCount - 1 && j + 2 >= endIndex)
                            {
                                break;
                            }

                            if (this.m_waitChars.Contains(validadedText[j]))
                            {
                                waitPoints.Add(anchoredPositionX + ((width + this.m_waitCharPixelAdjuster) * screenAdjuster));
                            }

                        }

                    }

                    ret.Add(new OneLineWidthAndWaitPoints(width * screenAdjuster, waitPoints));

                }

            }
            
            return ret;

        }

        /// <summary>
        /// Show text
        /// </summary>
        /// <param name="text">text from csv</param>
        /// <returns>IEnumerator</returns>
        // ------------------------------------------------------------------------------------------
        protected virtual IEnumerator showText(string text)
        {

            if (!PoetOneSceneManager.isAvailable())
            {
                yield break;
            }

            // changeTextColorIfNeeded
            {
                this.changeTextColorIfNeeded();
            }

            PoetConfigDataSO conf = SystemManager.Instance.configDataSO<PoetConfigDataSO>();

            PoetReduxManager prm = (PoetReduxManager.Instance as PoetReduxManager);

            var pauseState = prm.PauseStateWatcher.state();

            // ShowingTextStarted
            {

                prm.TextFinishedStateWatcher.state().setState(
                    prm.TextFinishedStateWatcher,
                    TextShowingState.StateEnum.ShowingTextStarted,
                    this.m_userProgressData.rawText
                    );

                yield return null;

            }

            //
            {

                float currentCounter = 0.0f;

                // init
                {

                    if (this.m_refScrollRect)
                    {
                        this.m_refScrollRect.verticalNormalizedPosition = 1.0f;
                    }

                    if (
                        !(PoetOneSceneManager.isAvailable() && PoetOneSceneManager.Instance.shouldSkip()) &&
                        conf.textSpeed01 < 1.0f
                        )
                    {

                        for (int i = this.m_refMaskLines.Count - 1; i >= 0; i--)
                        {
                            this.m_refMaskLines[i].rectTransform.sizeDelta = new Vector2(0.0f, this.m_refDefaultMaskSize.y);
                        }

                    }

                    // validateString
                    {
                        yield return this.validateString(text);
                    }

                }

                if (this.m_refMaskLines.Count > 0 && this.m_userProgressData.rawText.Length > 0)
                {

                    Vector2 tempSize = this.m_refMaskLines[0].rectTransform.sizeDelta;

                    var gen = this.textGenerator();

                    int loopSize = Mathf.Min(gen.lineCount, this.m_refMaskLines.Count);

                    float targetNPos = 0.0f;
                    float first = 0.0f;

                    float anyTemp = 0.0f;

                    List<OneLineWidthAndWaitPoints> waitPoints = this.createWaitPoints(this.m_refText.text);

                    OneLineWidthAndWaitPoints currentWaitPoint = null;

                    for (int i = 0; i < loopSize; i++)
                    {
                        
                        if (PoetOneSceneManager.isAvailable() && PoetOneSceneManager.Instance.shouldSkip())
                        {
                            break;
                        }

                        // ------------------

                        currentCounter = 0.0f;

                        currentWaitPoint = (i < waitPoints.Count) ? waitPoints[i] : null;

                        anyTemp = 0.0f;

                        // ------------------

                        // setReadingTextSoundLoop
                        {
                            PoetAudioManager.Instance.setReadingTextSoundLoop(true);
                        }

                        //
                        {

                            if(conf.textSpeed01 >= 1.0f && i == this.m_visibleLineCount)
                            {
                                for (int j = 1; j <= 30; j++)
                                {
                                    yield return null;
                                }
                            }

                        }

                        // scroll
                        {

                            if (this.m_refScrollRect && i >= this.m_visibleLineCount && gen.lineCount > this.m_visibleLineCount)
                            {

                                // setReadingTextSoundLoop
                                {
                                    PoetAudioManager.Instance.setReadingTextSoundLoop(false);
                                }

                                targetNPos = 1.0f - ((float)(1 + i - this.m_visibleLineCount) / (float)(gen.lineCount - this.m_visibleLineCount));

                                first = this.m_refScrollRect.verticalNormalizedPosition;

                                for (int j = 1; j <= 10; j++)
                                {
                                    this.m_refScrollRect.verticalNormalizedPosition = Mathf.Lerp(
                                        first,
                                        targetNPos,
                                        (float)j / 10.0f
                                        );
                                    yield return null;
                                }

                                this.m_showAllTextImmediately = false;

                                // setReadingTextSoundLoop
                                {
                                    PoetAudioManager.Instance.setReadingTextSoundLoop(true);
                                }

                            }

                        }

                        while (currentCounter < 1.0f)
                        {

                            if (
                                this.m_showAllTextImmediately ||
                                conf.textSpeed01 >= 1.0f ||
                                (PoetOneSceneManager.isAvailable() && PoetOneSceneManager.Instance.shouldSkip()))
                            {
                                break;
                            }

                            // pauseState
                            {

                                if (pauseState.pause)
                                {

                                    // setReadingTextSoundLoop
                                    {
                                        PoetAudioManager.Instance.setReadingTextSoundLoop(false);
                                    }

                                    yield return null;
                                    continue;

                                }

                            }

                            // ----------------------------

                            yield return null;

                            // ----------------------------

                            currentCounter +=
                                Mathf.Lerp(this.m_minTextSpeed, this.m_maxTextSpeed, conf.textSpeed01);

                            tempSize.x = Mathf.Clamp01(currentCounter) * this.m_refDefaultMaskSize.x;

                            if (currentWaitPoint != null)
                            {

                                if (tempSize.x > currentWaitPoint.oneLineWidth)
                                {
                                    break;
                                }

                                // ----------

                                anyTemp = currentWaitPoint.update(tempSize.x);

                                if (anyTemp > 0.0f)
                                {

                                    tempSize.x = anyTemp;
                                    this.m_refMaskLines[i].rectTransform.sizeDelta = tempSize;

                                    anyTemp = 0.0f;

                                    // setReadingTextSoundLoop
                                    {
                                        PoetAudioManager.Instance.setReadingTextSoundLoop(false);
                                    }

                                    while (anyTemp < this.m_waitCharSeconds)
                                    {

                                        if (
                                            this.m_showAllTextImmediately ||
                                            conf.textSpeed01 >= 1.0f ||
                                            (PoetOneSceneManager.isAvailable() && PoetOneSceneManager.Instance.shouldSkip()))
                                        {
                                            break;
                                        }

                                        anyTemp += Time.deltaTime;
                                        yield return null;

                                    }

                                    // setReadingTextSoundLoop
                                    {
                                        PoetAudioManager.Instance.setReadingTextSoundLoop(true);
                                    }

                                }

                                else
                                {
                                    this.m_refMaskLines[i].rectTransform.sizeDelta = tempSize;
                                }

                            }

                            else
                            {
                                this.m_refMaskLines[i].rectTransform.sizeDelta = tempSize;
                            }

                        } // while (currentCounter < 1.0f)

                        // line finish
                        {
                            tempSize.x = this.m_refDefaultMaskSize.x;
                            this.m_refMaskLines[i].rectTransform.sizeDelta = tempSize;
                        }

                    } // for (int i = 0; i < loopSize; i++)

                    // show all masks
                    {

                        tempSize.x = this.m_refDefaultMaskSize.x;

                        for (int i = this.m_refMaskLines.Count - 1; i >= 0; i--)
                        {
                            this.m_refMaskLines[i].rectTransform.sizeDelta = tempSize;
                        }

                    }

                } // if (this.m_refMaskLines.Count > 0)

            }

            // ShowingTextFinished
            {

                prm.TextFinishedStateWatcher.state().setState(
                    prm.TextFinishedStateWatcher,
                    TextShowingState.StateEnum.ShowingTextFinished,
                    this.m_userProgressData.rawText
                    );

            }

        }

        /// <summary>
        /// Show text immediately
        /// </summary>
        // ------------------------------------------------------------------------------------------
        public void showTextImmediately()
        {

            TextShowingState tsState = (PoetReduxManager.Instance as PoetReduxManager).TextFinishedStateWatcher.state();

            if (tsState.stateEnum == TextShowingState.StateEnum.ShowingTextStarted)
            {
                this.m_showAllTextImmediately = true;
            }

        }

        /// <summary>
        /// TextShowingState receiver
        /// </summary>
        /// <param name="tfState">TextShowingState</param>
        // ------------------------------------------------------------------------------------------
        protected override void onTextShowingState(TextShowingState tsState)
        {

        }

        /// <summary>
        /// PoetInitState receiver
        /// </summary>
        /// <param name="piState">PoetInitState</param>
        // ------------------------------------------------------------------------------------------
        protected override void onPoetInitState(PoetInitState piState)
        {

        }

        /// <summary>
        /// PoetSequenceState receiver
        /// </summary>
        /// <param name="state">PoetSequenceState</param>
        // ------------------------------------------------------------------------------------------
        protected override void onPoetSequenceState(PoetSequenceState psState)
        {

            if (psState.stateEnum == PoetSequenceState.StateEnum.PreProcess)
            {

                PoetConfigDataSO conf = SystemManager.Instance.configDataSO<PoetConfigDataSO>();

                if (
                    conf.textSpeed01 < 1.0f &&
                    (PoetOneSceneManager.isAvailable() && !PoetOneSceneManager.Instance.shouldSkip())
                    )
                {
                    this.m_refText.text = "";
                }

            }

            else if (psState.stateEnum == PoetSequenceState.StateEnum.MainProcess)
            {

                SystemLanguage sl = SystemManager.Instance.systemLanguage;

                if (this.m_languageAndHeaders.languageAndHeaderDictionary.ContainsKey(sl))
                {

                    string header = this.m_languageAndHeaders.languageAndHeaderDictionary[sl];

                    this.m_userProgressData.rawText =
                        psState.headerAndCells.ContainsKey(header) ?
                        psState.headerAndCells[header] :
                        ""
                        ;

                    this.m_userProgressData.replaceStrings =
                        psState.headerAndCells.ContainsKey(this.m_csvHeaderForTextReplacement) ?
                        psState.headerAndCells[this.m_csvHeaderForTextReplacement] :
                        ""
                        ;

                    this.startShowingText(this.m_userProgressData.rawText, true, this.m_userProgressData.replaceStrings);

                }

            }

            else if (psState.stateEnum == PoetSequenceState.StateEnum.FinishOperation)
            {
                this.m_refText.text = "";
                StopAllCoroutines();
            }

        }

        /// <summary>
        /// Start showing text IEnumerator
        /// </summary>
        /// <param name="text">text</param>
        /// <param name="updateTextLog">update text log</param>
        /// <param name="replaceStrings">replace strings</param>
        // ------------------------------------------------------------------------------------------
        protected void startShowingText(string text, bool updateTextLog, string replaceStrings)
        {

            // StopAllCoroutines
            {
                StopAllCoroutines();
            }

            // text
            {
                text = this.replaceText(text, replaceStrings);
            }

            // reset
            {
                this.m_showAllTextImmediately = false;
            }

            // TextLogManager
            {
                if (TextLogManager.isAvailable() && updateTextLog)
                {
                    TextLogManager.Instance.updateMainText(text);
                }
            }

            // setCurrentUserProgressDataDescription
            {
                SystemManager.Instance.setCurrentUserProgressDataDescription(this.createDescriptionText(text));
            }

            StartCoroutine(this.showText(text));

        }

        /// <summary>
        /// UserProgressDataSignal receiver
        /// </summary>
        /// <param name="tfState">UserProgressDataSignal</param>
        // ------------------------------------------------------------------------------------------
        protected override void onUserProgressDataSignal(UserProgressDataSignal updSignal)
        {
            updSignal.addDataAction(this.transform, this, JsonUtility.ToJson(this.m_userProgressData));
        }

        /// <summary>
        /// PauseState receiver
        /// </summary>
        /// <param name="pState">PauseState</param>
        // ------------------------------------------------------------------------------------------
        protected override void onPauseStateReceiver(SSC.PauseState pState)
        {

        }

        /// <summary>
        /// Reset
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected override void Reset()
        {

            this.m_receiverMask =
                ReceiverMask.UsePoetSequenceStateReceiver |
                ReceiverMask.UseUserProgressDataSignalReceiver |
                ReceiverMask.UseSceneChangeStateReceiver
                ;

        }

    }

}
