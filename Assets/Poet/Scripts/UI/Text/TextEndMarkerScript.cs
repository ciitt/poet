﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Poet
{

    /// <summary>
    /// Showing text finished marker
    /// </summary>
    [RequireComponent(typeof(Image))]
    public class TextEndMarkerScript : MonoBehaviour
    {

        /// <summary>
        /// Reference to Image
        /// </summary>
        protected Image m_refImage = null;

        /// <summary>
        /// Original position
        /// </summary>
        protected Vector2 m_oriPos = Vector2.zero;

        /// <summary>
        /// Awake
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected virtual void Awake()
        {

            this.m_refImage = this.GetComponent<Image>();
            this.m_oriPos = this.m_refImage.rectTransform.anchoredPosition;
            this.setColor(0.0f);

            PoetReduxManager prm = PoetReduxManager.Instance as PoetReduxManager;
            prm.addTextFinishedStateReceiver(this.onTextShowingState);

        }

        /// <summary>
        /// Set color
        /// </summary>
        /// <param name="val"></param>
        // ------------------------------------------------------------------------------------------
        protected void setColor(float alpha)
        {
            Color color = this.m_refImage.color;
            color.a = alpha;
            this.m_refImage.color = color;
        }

        /// <summary>
        /// TextShowingState receiver
        /// </summary>
        /// <param name="tsState">TextShowingState</param>
        // ------------------------------------------------------------------------------------------
        protected virtual void onTextShowingState(TextShowingState tsState)
        {

            if (tsState.stateEnum == TextShowingState.StateEnum.ShowingTextFinished)
            {

                this.setColor(1.0f);
                StopAllCoroutines();
                StartCoroutine(this.animIE());

            }

            else if(tsState.stateEnum == TextShowingState.StateEnum.ShowingTextStarted)
            {
                this.setColor(0.0f);
                StopAllCoroutines();
            }

        }

        /// <summary>
        /// SceneChangeState receiver
        /// </summary>
        /// <param name="scState">SceneChangeState</param>
        // -----------------------------------------------------------------------------------------------
        protected virtual void onSceneChangeStateReceiver(SSC.SceneChangeState scState)
        {

            if (scState.stateEnum == SSC.SceneChangeState.StateEnum.NowLoadingIntro)
            {
                this.setColor(0.0f);
                StopAllCoroutines();
            }

        }

        /// <summary>
        /// Anim IE
        /// </summary>
        /// <returns>IEnumerator</returns>
        // ------------------------------------------------------------------------------------------
        protected virtual IEnumerator animIE()
        {

            // init
            {
                this.m_refImage.rectTransform.anchoredPosition = this.m_oriPos;
            }

            yield return null;

            // loop
            {

                Vector2 from = this.m_oriPos;
                Vector2 to = from + new Vector2(0.0f, 5.0f);

                while (true)
                {

                    // move
                    {
                        for (int i = 0; i <= 10; i++)
                        {
                            this.m_refImage.rectTransform.anchoredPosition = Vector2.Lerp(from, to, (float)i / 10.0f);
                            yield return null;
                        }

                        for (int i = 0; i <= 10; i++)
                        {
                            this.m_refImage.rectTransform.anchoredPosition = Vector2.Lerp(to, from, (float)i / 10.0f);
                            yield return null;
                        }
                    }

                    // stop
                    {

                        for (int i = 0; i < 40; i++)
                        {
                            yield return null;
                        }

                    }

                }

            }


        }

    }

}
