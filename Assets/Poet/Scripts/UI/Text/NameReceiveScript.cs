﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Poet
{

    /// <summary>
    /// Name receiver
    /// </summary>
    [RequireComponent(typeof(Text))]
    public class NameReceiveScript : PoetBaseReceiverScript
    {

        /// <summary>
        /// Reference to UiControllerScript
        /// </summary>
        [SerializeField]
        SSC.UiControllerScript m_refNameUi;

        /// <summary>
        /// Languaget and its header
        /// </summary>
        [SerializeField]
        [Tooltip("Languaget and its header")]
        protected LanguageAndHeaders m_languageAndHeaders = new LanguageAndHeaders("Name");

        /// <summary>
        /// Reference to Text
        /// </summary>
        protected Text m_refName;

        /// <summary>
        /// Called in Awake
        /// </summary>
        // -------------------------------------------------------------------------------------------------
        protected override void initOnAwake()
        {

            // set
            {
                this.m_refName = this.GetComponent<Text>();
                this.m_refName.text = "";
            }

            // m_languageAndHeaders
            {
                this.m_languageAndHeaders.initDictionary();
            }

        }

        /// <summary>
        /// PoetInitState receiver
        /// </summary>
        /// <param name="piState">PoetInitState</param>
        // -------------------------------------------------------------------------------------------------
        protected override void onPoetInitState(PoetInitState piState)
        {

        }

        /// <summary>
        /// PoetSequenceState receiver
        /// </summary>
        /// <param name="state">PoetSequenceState</param>
        // -------------------------------------------------------------------------------------------------
        protected override void onPoetSequenceState(PoetSequenceState psState)
        {

            if(psState.stateEnum == PoetSequenceState.StateEnum.MainProcess)
            {

                SystemLanguage sl = SystemManager.Instance.systemLanguage;

                if (this.m_languageAndHeaders.languageAndHeaderDictionary.ContainsKey(sl))
                {

                    string header = this.m_languageAndHeaders.languageAndHeaderDictionary[sl];

                    if (psState.headerAndCells.ContainsKey(header))
                    {

                        string text = psState.headerAndCells[header];

                        this.m_refName.text = text;

                        if (this.m_refNameUi)
                        {

                            if (string.IsNullOrEmpty(text))
                            {
                                this.m_refNameUi.startHiding();
                            }

                            else
                            {
                                this.m_refNameUi.startShowing();
                            }

                        }

                    }

                }

            }

            else if (psState.stateEnum == PoetSequenceState.StateEnum.FinishOperation)
            {

                this.m_refName.text = "";

                if (this.m_refNameUi)
                {
                    this.m_refNameUi.startHiding();
                }

            }

        }

        /// <summary>
        /// TextShowingState receiver
        /// </summary>
        /// <param name="tsState">TextShowingState</param>
        // ------------------------------------------------------------------------------------------
        protected override void onTextShowingState(TextShowingState tsState)
        {

            if (tsState.stateEnum == TextShowingState.StateEnum.ShowingTextStarted)
            {

                // TextLogManager
                {
                    if (TextLogManager.isAvailable())
                    {
                        TextLogManager.Instance.updateNameText(this.m_refName.text);
                    }
                }

            }

        }

        /// <summary>
        /// UserProgressDataSignal receiver
        /// </summary>
        /// <param name="updSignal">UserProgressDataSignal</param>
        // ------------------------------------------------------------------------------------------
        protected override void onUserProgressDataSignal(UserProgressDataSignal updSignal)
        {

            if (this.m_refName)
            {
                updSignal.addDataAction(this.transform, this, this.m_refName.text);
            }

        }

        /// <summary>
        /// SceneChangeState receiver
        /// </summary>
        /// <param name="scState">SceneChangeState</param>
        // ------------------------------------------------------------------------------------------
        protected override void onSceneChangeStateReceiver(SSC.SceneChangeState scState)
        {

            if (scState.stateEnum == SSC.SceneChangeState.StateEnum.AllStartupsDone)
            {

                PoetSceneChangeManager pscm = PoetSceneChangeManager.Instance as PoetSceneChangeManager;

                if (this.m_refName && pscm.isLoadingCurrentSceneWithUserProgressData())
                {

                    this.m_refName.text = pscm.getJsonDataFromCurrentUserProgressData(this.transform, this);

                    if (string.IsNullOrEmpty(this.m_refName.text))
                    {
                        this.m_refNameUi.startHiding();
                    }

                    else
                    {
                        this.m_refNameUi.startShowing();
                    }

                }

            }

        }

        /// <summary>
        /// PauseState receiver
        /// </summary>
        /// <param name="pState">PauseState</param>
        // ------------------------------------------------------------------------------------------
        protected override void onPauseStateReceiver(SSC.PauseState pState)
        {

        }

        /// <summary>
        /// Reset
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected override void Reset()
        {

            this.m_receiverMask =
                ReceiverMask.UsePoetSequenceStateReceiver |
                ReceiverMask.UseTextShowingStateReceiver |
                ReceiverMask.UseUserProgressDataSignalReceiver |
                ReceiverMask.UseSceneChangeStateReceiver
                ;

        }

    }

}
