﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Poet
{

    /// <summary>
    /// Text log panel
    /// </summary>
    public class TextLogPanelScript : MonoBehaviour
    {

        /// <summary>
        /// Reference to Text
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to Text")]
        protected Text m_refText = null;

        /// <summary>
        /// Reference to voice Button
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to voice Button")]
        protected Button m_refVoiceButton = null;

        /// <summary>
        /// Preloaded voice id
        /// </summary>
        protected string m_preloadedVoiceId = "";

        /// <summary>
        /// Resource voice id
        /// </summary>
        protected string m_resourceVoiceId = "";

        /// <summary>
        /// AssetBundle voice id
        /// </summary>
        protected string m_assetBundleVoiceId = "";

        /// <summary>
        /// Character voice id
        /// </summary>
        protected string m_characterVoiceId = "";

        /// <summary>
        /// Set text
        /// </summary>
        /// <param name="nameText"></param>
        /// <param name="mainText"></param>
        // -----------------------------------------------------------------------------------------------------------
        public virtual void setText(string nameText, string mainText)
        {

            if(this.m_refText)
            {
                this.m_refText.text = nameText + "\n" + mainText;
            }

        }

        /// <summary>
        /// Set preloaded voice id
        /// </summary>
        /// <param name="id"></param>
        // -----------------------------------------------------------------------------------------------------------
        public void setPreloadedVoiceId(string id)
        {
            this.m_preloadedVoiceId = id;
        }

        /// <summary>
        /// Set resource voice id
        /// </summary>
        /// <param name="id"></param>
        // -----------------------------------------------------------------------------------------------------------
        public void setResourceVoiceId(string id)
        {
            this.m_resourceVoiceId = id;
        }

        /// <summary>
        /// Set AssetBundle voice id
        /// </summary>
        /// <param name="id"></param>
        // -----------------------------------------------------------------------------------------------------------
        public void setAssetBundleVoiceId(string id)
        {
            this.m_assetBundleVoiceId = id;
        }

        /// <summary>
        /// Set Character voice id
        /// </summary>
        /// <param name="id"></param>
        // -----------------------------------------------------------------------------------------------------------
        public void setCharacterVoiceId(string id)
        {
            this.m_characterVoiceId = id;
        }

        /// <summary>
        /// Set button interactable
        /// </summary>
        // -----------------------------------------------------------------------------------------------------------
        public void setButtonInteractable()
        {

            this.m_refVoiceButton.interactable =
                (
                this.m_preloadedVoiceId.Length +
                this.m_resourceVoiceId.Length +
                this.m_assetBundleVoiceId.Length
                ) > 0;

        }

        /// <summary>
        /// onClick for text log voice button
        /// </summary>
        // -----------------------------------------------------------------------------------------------------------
        public void onClickLogVoiceButton()
        {

            if(!PoetAudioManager.Instance.refVoice)
            {
                return;
            }

            // -------------

            PoetAudioManager.Instance.setCurrentVoiceId(this.m_characterVoiceId);

            if (!string.IsNullOrEmpty(this.m_preloadedVoiceId))
            {
                PoetAudioManager.Instance.refVoice.playPreloadedAudio(this.m_preloadedVoiceId, 0.0f, false);
            }

            else if (!string.IsNullOrEmpty(this.m_resourceVoiceId))
            {
                PoetAudioManager.Instance.refVoice.playResourceAudio(this.m_resourceVoiceId, 0.0f, false);
            }

            else if (!string.IsNullOrEmpty(this.m_assetBundleVoiceId))
            {
                PoetAudioManager.Instance.refVoice.playAssetBundleAudio(this.m_assetBundleVoiceId, 0.0f, false);
            }

        }

    }

}
