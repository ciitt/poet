﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Poet
{

    /// <summary>
    /// Goto next sequence
    /// </summary>
    [RequireComponent(typeof(Button))]
    public class GotoNextSequenceScript : MonoBehaviour
    {

        Button m_refButton = null;

        /// <summary>
        /// Awake
        /// </summary>
        // ----------------------------------------------------------------------------------------
        void Awake()
        {
            this.m_refButton = this.GetComponent<Button>();
        }

        /// <summary>
        /// Function when GotoNext button is clicked
        /// </summary>
        // ----------------------------------------------------------------------------------------
        public void onClickGoToNextButton()
        {

            if (
                SSC.CommonUiManager.Instance.containsIdentifier(PoetUiIdentifiers.Common.Text) &&
                !SSC.CommonUiManager.Instance.nowInShowingOrHidingTransition
                )
            {

                if(PoetOneSceneManager.isAvailable())
                {

                    if(SystemManager.Instance.isSkip)
                    {
                        SystemManager.Instance.resetAutoAndSkipModes();
                    }

                    else
                    {
                        PoetOneSceneManager.Instance.onClickGoToNextSequenceButton();
                    }
                    
                }

            }

            else
            {
                PoetCommonUiManager.Instance.back(true);
            }

            this.m_refButton.Select();

        }

        /// <summary>
        /// Invoke onClick
        /// </summary>
        // ----------------------------------------------------------------------------------------
        public void invokeOnClick()
        {
            this.m_refButton.onClick.Invoke();
        }

        /// <summary>
        /// Is interactable
        /// </summary>
        /// <returns>interactable</returns>
        // ----------------------------------------------------------------------------------------
        public bool isInteractable()
        {
            return this.m_refButton.IsInteractable();
        }

    }

}
