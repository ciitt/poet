﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Poet
{

    /// <summary>
    /// Pager button script
    /// </summary>
    [RequireComponent(typeof(Toggle))]
    public class PagerButtonScript : MonoBehaviour
    {

        /// <summary>
        /// Reference to PagerButtonGroupScript
        /// </summary>
        protected PagerButtonGroupScript m_refPagerButtonGroupScript = null;

        /// <summary>
        /// Reference to Toggle
        /// </summary>
        protected Toggle m_refToggle = null;

        ///// <summary>
        ///// Original alpha for CanvasGroup
        ///// </summary>
        //protected float m_oriAlpha = 1.0f;

        /// <summary>
        /// Pager index
        /// </summary>
        protected int m_pagerIndexFrom0 = 0;

        /// <summary>
        /// Awake
        /// </summary>
        // -------------------------------------------------------------------------------------------
        protected virtual void Awake()
        {

            this.m_refToggle = this.GetComponent<Toggle>();

        }

        /// <summary>
        /// Set pager index
        /// </summary>
        /// <param name="pagerIndexFrom0">page index from 0</param>
        /// <param name="pbgs">PagerButtonGroupScript</param>
        // -------------------------------------------------------------------------------------------
        public void setPagerIndex(int pagerIndexFrom0, PagerButtonGroupScript pbgs)
        {
            this.m_pagerIndexFrom0 = pagerIndexFrom0;
            this.m_refPagerButtonGroupScript = pbgs;
        }

        /// <summary>
        /// Activate
        /// </summary>
        // -------------------------------------------------------------------------------------------
        public virtual void activatePagerButton()
        {
            this.m_refToggle.interactable = true;
        }

        /// <summary>
        /// Deactivate
        /// </summary>
        // -------------------------------------------------------------------------------------------
        public virtual void deactivatePagerButton()
        {
            this.m_refToggle.interactable = false;
        }

        /// <summary>
        /// Function when toggle is clicked
        /// </summary>
        // -------------------------------------------------------------------------------------------
        public virtual void onClickOnly()
        {

            if (this.m_refPagerButtonGroupScript)
            {
                this.m_refPagerButtonGroupScript.sendPageIndexSignal(this.m_pagerIndexFrom0);
            }

        }

        /// <summary>
        /// Set toggle on
        /// </summary>
        // -------------------------------------------------------------------------------------------
        public void setToggleOn()
        {
            this.m_refToggle.isOn = true;
        }

    }

}
