﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace Poet
{

    /// <summary>
    /// Pager button group script
    /// </summary>
    public class PagerButtonGroupScript : MonoBehaviour
    {

        [Serializable]
        protected class PagerButtonEvent : UnityEvent<int>
        {

        }

        /// <summary>
        /// Reference to SSC.UiControllerScript
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to SSC.UiControllerScript")]
        protected SSC.UiControllerScript m_refUiController = null;

        /// <summary>
        /// Reference to PagerButtonScript list
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to PagerButtonScript list")]
        protected List<PagerButtonScript> m_pagerButtonList = new List<PagerButtonScript>();

        /// <summary>
        /// UnityEvent for pager buttons
        /// </summary>
        [SerializeField]
        [Tooltip("UnityEvent for pager buttons")]
        protected PagerButtonEvent m_pagerButtonEvent;

        /// <summary>
        /// Previous selected page index
        /// </summary>
        protected int m_previousSelectedPageIndex = 0;

        /// <summary>
        /// Awake
        /// </summary>
        // ---------------------------------------------------------------------------------------
        protected virtual void Awake()
        {

            int size = this.m_pagerButtonList.Count;

            for (int i = 0; i < size; i++)
            {

                if(!this.m_pagerButtonList[i])
                {
                    Debug.LogWarning("m_pagerButtonList contains null : " + i + " : " + this.gameObject.name);
                }

                else
                {
                    this.m_pagerButtonList[i].setPagerIndex(i, this);
                }

            }

        }

        /// <summary>
        /// Activate pager buttons
        /// </summary>
        /// <param name="onePageItemCount">item count per one page</param>
        /// <param name="allItemsCount">all items count</param>
        // ---------------------------------------------------------------------------------------
        public void activateButtons(int onePageItemCount, int allItemsCount)
        {

            int size = this.m_pagerButtonList.Count;

            int _max = 1;

            if (onePageItemCount > 0)
            {
                _max = Mathf.CeilToInt((float)allItemsCount / (float)onePageItemCount);
            }

            for (int i = 0; i < size; i++)
            {

                if(this.m_pagerButtonList[i])
                {

                    if (_max > i)
                    {
                        this.m_pagerButtonList[i].activatePagerButton();
                    }

                    else
                    {
                        this.m_pagerButtonList[i].deactivatePagerButton();
                    }

                }

            }

        }

        /// <summary>
        /// Function for pager buttons
        /// </summary>
        /// <param name="pageIndexFrom0"></param>
        // ---------------------------------------------------------------------------------------
        public virtual void sendPageIndexSignal(int pageIndexFrom0)
        {
            StopAllCoroutines();
            StartCoroutine(this.sendPageIndexSignalIE(pageIndexFrom0));
        }

        /// <summary>
        /// IEnumerator for pager buttons
        /// </summary>
        /// <param name="pageIndexFrom0"></param>
        /// <returns>IEnumerator</returns>
        // ---------------------------------------------------------------------------------------
        protected IEnumerator sendPageIndexSignalIE(int pageIndexFrom0)
        {

            if (pageIndexFrom0 < 0)
            {
                pageIndexFrom0 = this.m_previousSelectedPageIndex;
            }

            if (pageIndexFrom0 < 0 || this.m_pagerButtonList.Count <= pageIndexFrom0)
            {
                yield break;
            }

            // ------------------

            // m_refUiController
            {
                if (this.m_refUiController)
                {
                    this.m_refUiController.startShowing(true, 0.0f, null);
                }
            }

            yield return null; // startShowing, wait 1 frame, set values 

            // select
            {

                var target = this.m_pagerButtonList[pageIndexFrom0];

                if (target && target.gameObject != EventSystem.current.currentSelectedGameObject)
                {
                    target.setToggleOn();
                }

            }

            // m_previousSelectedPageIndex
            {
                this.m_previousSelectedPageIndex = pageIndexFrom0;
            }

            this.m_pagerButtonEvent.Invoke(pageIndexFrom0);

        }

    }

}
