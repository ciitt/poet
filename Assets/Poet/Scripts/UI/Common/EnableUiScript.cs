﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Poet
{
    /// <summary>
    /// Enable UI
    /// </summary>
    public class EnableUiScript : MonoBehaviour
    {

        /// <summary>
        /// Enable UiState mask
        /// </summary>
        [SerializeField]
        [Tooltip("Enable UiState mask")]
        [SSC.BiMask]
        protected UiState.StateEnum m_enableMask;

        /// <summary>
        /// If valid, disable Selectable. If null, disable GameObject
        /// </summary>
        [SerializeField]
        [Tooltip("If valid, disable Selectable. If null, disable GameObject")]
        protected Selectable m_refSelectable = null;

        /// <summary>
        /// Start
        /// </summary>
        // -------------------------------------------------------------------------------
        protected virtual void Start()
        {

            PoetReduxManager prm = (PoetReduxManager.Instance as PoetReduxManager);
            prm.addUiStateReceiver(this.onUiStateReceiver);

        }

        /// <summary>
        /// Set enable state
        /// </summary>
        /// <param name="enable">enable</param>
        // -----------------------------------------------------------------------------------------------
        protected void setEnableState(bool enable)
        {

            if (this.m_refSelectable)
            {
                this.m_refSelectable.interactable = enable;
            }

            else
            {
                this.gameObject.SetActive(enable);
            }

        }

        /// <summary>
        /// UiState receiver
        /// </summary>
        /// <param name="uiState">UiState</param>
        // -----------------------------------------------------------------------------------------------
        protected virtual void onUiStateReceiver(UiState uiState)
        {

            if ((this.m_enableMask & uiState.stateEnum) == uiState.stateEnum)
            {
                this.setEnableState(true);

            }

            else
            {
                this.setEnableState(false);
            }

        }

    }

}
