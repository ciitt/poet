﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Poet
{

    /// <summary>
    /// Disable QL button
    /// </summary>
    //[RequireComponent(typeof(Button))]
    public class DisableQuickLoadButtonScript : EnableUiScript
    {

        /// <summary>
        /// Start
        /// </summary>
        // -------------------------------------------------------------------------------
        protected override void Start()
        {

            base.Start();

            this.setEnableState(false);

            StartCoroutine(this.check());

        }

        /// <summary>
        /// UiState receiver
        /// </summary>
        /// <param name="uiState">UiState</param>
        // -----------------------------------------------------------------------------------------------
        protected override void onUiStateReceiver(UiState uiState)
        {

            if ((this.m_enableMask & uiState.stateEnum) == uiState.stateEnum)
            {
                this.setEnableState(SystemManager.Instance.isAvailableQuickSaveData());

            }

            else
            {
                this.setEnableState(false);
            }

        }

        /// <summary>
        /// Check
        /// </summary>
        /// <returns>IEnumerator</returns>
        // -------------------------------------------------------------------------------
        protected IEnumerator check()
        {

            while (!SystemManager.Instance.isAvailableQuickSaveData())
            {
                yield return new WaitForSeconds(1.0f);
            }

            PoetReduxManager prm = (PoetReduxManager.Instance as PoetReduxManager);
            UiState uiState = prm.UiStateWatcher.state();

            if ((this.m_enableMask & uiState.stateEnum) == uiState.stateEnum)
            {
                this.setEnableState(true);
            }

        }

    }

}
