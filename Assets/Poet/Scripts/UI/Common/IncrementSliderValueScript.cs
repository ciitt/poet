﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Poet
{

    [RequireComponent(typeof(Slider))]
    public class IncrementSliderValueScript : MonoBehaviour
    {

        /// <summary>
        /// Normalized
        /// </summary>
        [SerializeField]
        [Tooltip("Normalized")]
        protected bool m_normalized = true;

        Slider m_refSlider = null;

        void Awake()
        {
            this.m_refSlider = this.GetComponent<Slider>();
        }

        public void increment(float val)
        {

            if(this.m_normalized)
            {
                this.m_refSlider.normalizedValue = Mathf.Clamp01(this.m_refSlider.normalizedValue + val);
            }

            else
            {
                this.m_refSlider.value = this.m_refSlider.value + val;
            }
            
        }

    }

}
