﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poet
{

    /// <summary>
    /// Group for ChangeNavigationScript
    /// </summary>
    public class ChangeNavigationGroupScript : MonoBehaviour
    {

        /// <summary>
        /// ChangeNavigationScript list
        /// </summary>
        [SerializeField]
        [Tooltip("ChangeNavigationScript list")]
        protected List<ChangeNavigationScript> m_list = new List<ChangeNavigationScript>();


#if UNITY_EDITOR

        /// <summary>
        /// Awake
        /// </summary>
        // -------------------------------------------------------------------------------------------
        protected void Awake()
        {

            foreach (var val in this.m_list)
            {

                if(!val)
                {
                    Debug.LogWarning("(#if UNITY_EDITOR) m_list contains null : " + this.gameObject.name);
                }

            }

        }

#endif

        /// <summary>
        /// Change Navigation
        /// </summary>
        /// <param name="identifier">identifier</param>
        // -------------------------------------------------------------------------------------------
        public void changeNavigations(string identifier)
        {

            foreach(var val in this.m_list)
            {

                if(val)
                {
                    val.changeNavigation(identifier);
                }

            }

        }

        /// <summary>
        /// Resume original Navigation
        /// </summary>
        // -------------------------------------------------------------------------------------------
        public void resumeOriginalNavigations()
        {

            foreach (var val in this.m_list)
            {

                if (val)
                {
                    val.resumeOriginalNavigation();
                }

            }

        }
    }

}
