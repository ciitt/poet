﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Poet
{

    [RequireComponent(typeof(Text))]
    public class ChangeUiTextScript : MonoBehaviour
    {

        /// <summary>
        /// Languaget and its text
        /// </summary>
        [SerializeField]
        [Tooltip("Languaget and its text")]
        protected LanguageAndTexts m_languageAndHeaders = new LanguageAndTexts("");

        /// <summary>
        /// Reference to Text
        /// </summary>
        protected Text m_refText = null;

        /// <summary>
        /// Start
        /// </summary>
        // -------------------------------------------------------------------------------
        protected virtual void Start()
        {

            this.m_refText = this.GetComponent<Text>();

            this.m_languageAndHeaders.initDictionary();

            PoetReduxManager prm = (PoetReduxManager.Instance as PoetReduxManager);
            prm.addChangeLanguageSignalReceiver(this.onChangeLanguageSignalReceiver);

        }

        /// <summary>
        /// ChangeLanguageSignal receiver
        /// </summary>
        /// <param name="scState">ChangeLanguageSignal</param>
        // -----------------------------------------------------------------------------------------------
        protected virtual void onChangeLanguageSignalReceiver(ChangeLanguageSignal clSignal)
        {

            SystemLanguage sl = SystemManager.Instance.systemLanguage;

            if(this.m_languageAndHeaders.languageAndTextDictionary.ContainsKey(sl))
            {
                this.m_refText.text = this.m_languageAndHeaders.languageAndTextDictionary[sl];
            }

        }

    }

}
