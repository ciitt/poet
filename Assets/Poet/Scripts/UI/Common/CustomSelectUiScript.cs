﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Poet
{

    /// <summary>
    /// Custom OnSelect
    /// </summary>
    [RequireComponent(typeof(Selectable))]
    public class CustomSelectUiScript : MonoBehaviour,
        ISelectHandler,
        IDeselectHandler,
        IPointerEnterHandler,
        IPointerExitHandler
    {

        /// <summary>
        /// Reference to RectTransform target
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to RectTransform target")]
        RectTransform m_refTransformTarget;

        /// <summary>
        /// Translate value
        /// </summary>
        [SerializeField]
        [Tooltip("Translate value")]
        Vector2 m_relativeTranslate = Vector2.zero;

        /// <summary>
        /// Rotate value
        /// </summary>
        [SerializeField]
        [Tooltip("Rotate value")]
        Vector3 m_rotate = Vector3.zero;

        /// <summary>
        /// Scale value
        /// </summary>
        [SerializeField]
        [Tooltip("Scale value")]
        Vector3 m_scale = Vector3.one;

        /// <summary>
        /// Transform seconds
        /// </summary>
        [SerializeField]
        [Tooltip("Transform seconds")]
        float m_transformSeconds = 0.1f;

        /// <summary>
        /// Preloaded SE sound identifier when select
        /// </summary>
        [SerializeField]
        [Tooltip("Preloaded SE sound identifier when select")]
        string m_preloadedSeIdentifierWhenSelect = "";

        /// <summary>
        /// Preloaded SE sound identifier when click
        /// </summary>
        [SerializeField]
        [Tooltip("Preloaded SE sound identifier when click")]
        string m_preloadedSeIdentifierWhenClick = "";

        /// <summary>
        /// Original position
        /// </summary>
        Vector2 m_oriPosition = Vector2.zero;

        /// <summary>
        /// Original rotation
        /// </summary>
        Vector3 m_oriRotation = Vector3.zero;

        /// <summary>
        /// Original scale
        /// </summary>
        Vector3 m_oriScale = Vector3.one;

        /// <summary>
        /// IEnumerator for transformAnim
        /// </summary>
        IEnumerator m_transformAnim = null;

        /// <summary>
        /// Reference to Selectable
        /// </summary>
        Selectable m_refSelectable = null;

        /// <summary>
        /// Start
        /// </summary>
        // ------------------------------------------------------------------------------
        IEnumerator Start()
        {

            yield return new WaitForEndOfFrame();

            if (this.m_refTransformTarget)
            {
                this.m_oriPosition = this.m_refTransformTarget.anchoredPosition;
                this.m_oriRotation = this.m_refTransformTarget.localEulerAngles;
                this.m_oriScale = this.m_refTransformTarget.localScale;
            }

            this.m_refSelectable = this.GetComponent<Selectable>();

        }

        /// <summary>
        /// OnDeselect
        /// </summary>
        /// <param name="eventData">BaseEventData</param>
        // ------------------------------------------------------------------------------
        public void OnDeselect(BaseEventData eventData)
        {

            // m_transformAnim
            {

                if (this.m_transformAnim != null)
                {
                    StopCoroutine(this.m_transformAnim);
                }

                StartCoroutine(this.m_transformAnim = this.transformAnim(false));

            }

            // remove button highlight
            {
                this.m_refSelectable.enabled = false;
                this.m_refSelectable.enabled = true;
            }

        }

        /// <summary>
        /// OnPointerEnter
        /// </summary>
        /// <param name="eventData">PointerEventData</param>
        // ------------------------------------------------------------------------------
        public void OnPointerEnter(PointerEventData eventData)
        {

            if (this.m_refSelectable.IsInteractable())
            {
                this.m_refSelectable.Select();
            }

        }

        /// <summary>
        /// OnPointerExit
        /// </summary>
        /// <param name="eventData">PointerEventData</param>
        // ------------------------------------------------------------------------------
        public void OnPointerExit(PointerEventData eventData)
        {

            if (this.m_refSelectable.IsInteractable())
            {
                EventSystem.current.SetSelectedGameObject(null);
            }

        }

        /// <summary>
        /// OnSelect
        /// </summary>
        /// <param name="eventData">BaseEventData</param>
        // ------------------------------------------------------------------------------
        public void OnSelect(BaseEventData eventData)
        {

            // m_transformAnim
            {

                if(this.m_refTransformTarget)
                {

                    if (this.m_transformAnim != null)
                    {
                        StopCoroutine(this.m_transformAnim);
                    }

                    StartCoroutine(this.m_transformAnim = this.transformAnim(true));

                }

            }

            // SE
            {

                if (!string.IsNullOrEmpty(this.m_preloadedSeIdentifierWhenSelect))
                {

                    var scState = PoetReduxManager.Instance.SceneChangeStateWatcher.state();

                    if (scState.stateEnum != SSC.SceneChangeState.StateEnum.NowLoadingIntro && Poet.PoetAudioManager.Instance.refSE)
                    {
                        Poet.PoetAudioManager.Instance.refSE.playPreloadedAudio(this.m_preloadedSeIdentifierWhenSelect, 0.0f, false);
                    }

                }

            }

        }

        /// <summary>
        /// Play spund when click
        /// </summary>
        // ------------------------------------------------------------------------------
        public void playSoundWhenClick()
        {

            if (!string.IsNullOrEmpty(this.m_preloadedSeIdentifierWhenClick))
            {

                var scState = PoetReduxManager.Instance.SceneChangeStateWatcher.state();

                if (scState.stateEnum != SSC.SceneChangeState.StateEnum.NowLoadingIntro && Poet.PoetAudioManager.Instance.refSE)
                {
                    Poet.PoetAudioManager.Instance.refSE.playPreloadedAudio(this.m_preloadedSeIdentifierWhenClick, 0.0f, false);
                }

            }

        }

        /// <summary>
        /// Transform animation
        /// </summary>
        /// <param name="forward">forward or back</param>
        /// <returns>IEnumerator</returns>
        // -------------------------------------------------------------------------------
        IEnumerator transformAnim(bool forward)
        {

            if (!this.m_refTransformTarget)
            {
                yield break;
            }

            // ----------------

            Vector2 firstPosition = this.m_refTransformTarget.anchoredPosition;
            Vector2 targetPosition = (forward) ? this.m_oriPosition + this.m_relativeTranslate : this.m_oriPosition;

            Quaternion firstRotation = Quaternion.Euler(this.m_refTransformTarget.localEulerAngles);
            Quaternion targetRotation = Quaternion.Euler((forward) ? this.m_rotate : this.m_oriRotation);

            Vector3 firstScale = this.m_refTransformTarget.localScale;
            Vector3 targetScale = (forward) ? this.m_scale : this.m_oriScale;

            float timeCounter = 0.0f;
            float val01 = 0.0f;

            while (timeCounter < this.m_transformSeconds)
            {

                timeCounter += Time.deltaTime;

                val01 = timeCounter / this.m_transformSeconds;

                // position
                {
                    this.m_refTransformTarget.anchoredPosition = Vector2.Lerp(firstPosition, targetPosition, val01);
                }

                // rotation
                {
                    this.m_refTransformTarget.localRotation = Quaternion.Lerp(firstRotation, targetRotation, val01);
                }

                // scale
                {
                    this.m_refTransformTarget.localScale = Vector3.Lerp(firstScale, targetScale, val01);
                }

                yield return null;

            }

            // finish
            {
                this.m_refTransformTarget.anchoredPosition = targetPosition;
            }

            this.m_transformAnim = null;

            yield return null;

        }

    }

}
