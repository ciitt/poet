﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Poet
{

    /// <summary>
    /// Change Navigaion
    /// </summary>
    [RequireComponent(typeof(Selectable))]
    public class ChangeNavigationScript : MonoBehaviour
    {

        /// <summary>
        /// Identifier at Start
        /// </summary>
        [SerializeField]
        [Tooltip("Identifier at Start")]
        protected string m_startIdentifier = "";

        /// <summary>
        /// Navigation list
        /// </summary>
        [SerializeField]
        [Tooltip("Navigation list")]
        protected List<IdAndNavigation> m_navigationList = new List<IdAndNavigation>();

        /// <summary>
        /// Reference to Selectable
        /// </summary>
        protected Selectable m_refSelectable = null;

        /// <summary>
        /// Preloaded audio dictionary
        /// </summary>
        protected Dictionary<string, IdAndNavigation> m_navigationDict = new Dictionary<string, IdAndNavigation>();

        /// <summary>
        /// Navigation
        /// </summary>
        protected Navigation m_oriNavigation = new Navigation();

        /// <summary>
        /// Awake
        /// </summary>
        // -------------------------------------------------------------------------------------------
        protected virtual void Awake()
        {

            this.m_refSelectable = this.GetComponent<Selectable>();
            this.m_navigationDict = Funcs.listToDictionary<IdAndNavigation>(this.m_navigationList);

        }

        /// <summary>
        /// Start
        /// </summary>
        // -------------------------------------------------------------------------------------------
        protected virtual void Start()
        {

            if(!string.IsNullOrEmpty(this.m_startIdentifier))
            {

                if (this.m_navigationDict.ContainsKey(this.m_startIdentifier))
                {

                    Navigation navigation = new Navigation();

                    navigation.mode = Navigation.Mode.Explicit;

                    navigation.selectOnUp = this.m_navigationDict[this.m_startIdentifier].selectOnUp;
                    navigation.selectOnDown = this.m_navigationDict[this.m_startIdentifier].selectOnDown;
                    navigation.selectOnLeft = this.m_navigationDict[this.m_startIdentifier].selectOnLeft;
                    navigation.selectOnRight = this.m_navigationDict[this.m_startIdentifier].selectOnRight;

                    this.m_oriNavigation = this.m_refSelectable.navigation;

                }

                this.changeNavigation(this.m_startIdentifier);

            }

            else
            {
                this.m_oriNavigation = this.m_refSelectable.navigation;
            }

        }

        /// <summary>
        /// Change Navigation
        /// </summary>
        /// <param name="identifier">identifier</param>
        // -------------------------------------------------------------------------------------------
        public void changeNavigation(string identifier)
        {

#if UNITY_IOS || UNITY_ANDROID
            return;
#endif

            if (this.m_navigationDict.ContainsKey(identifier))
            {

                Navigation navigation = new Navigation();

                navigation.mode = Navigation.Mode.Explicit;

                navigation.selectOnUp = this.m_navigationDict[identifier].selectOnUp;
                navigation.selectOnDown = this.m_navigationDict[identifier].selectOnDown;
                navigation.selectOnLeft = this.m_navigationDict[identifier].selectOnLeft;
                navigation.selectOnRight = this.m_navigationDict[identifier].selectOnRight;

                if(this.gameObject.activeInHierarchy)
                {
                    StartCoroutine(this.changeNavigationIE(navigation));
                }

                else
                {
                    this.m_refSelectable.navigation = navigation;
                }

            }

#if UNITY_EDITOR

            else
            {
                Debug.LogWarning("(#if UNITY_EDITOR) m_navigationDict does not contain a key : " + identifier + " : " + this.gameObject.name);
            }

#endif

        }

        /// <summary>
        /// Resume original Navigation
        /// </summary>
        // -------------------------------------------------------------------------------------------
        public void resumeOriginalNavigation()
        {

#if UNITY_IOS || UNITY_ANDROID
            return;
#endif

            if (this.gameObject.activeInHierarchy)
            {
                StartCoroutine(this.changeNavigationIE(this.m_oriNavigation));
            }

            else
            {
                this.m_refSelectable.navigation = this.m_oriNavigation;
            }

        }

        /// <summary>
        /// Change Navigation
        /// </summary>
        /// <param name="navigation">Navigation</param>
        /// <returns>IEnumerator</returns>
        // -------------------------------------------------------------------------------------------
        protected IEnumerator changeNavigationIE(Navigation navigation)
        {

            // ------------------------

            yield return null;

            // ------------------------

            Selectable temp = null;

            // selectOnDown
            {

                temp = navigation.selectOnDown;

                while(temp)
                {

                    if(temp.gameObject.activeInHierarchy && temp.interactable)
                    {
                        navigation.selectOnDown = temp;
                        break;
                    }

                    else
                    {
                        navigation.selectOnDown = null;
                    }

                    temp = temp.navigation.selectOnDown;

                }

            }

            // selectOnLeft
            {

                temp = navigation.selectOnLeft;

                while (temp)
                {

                    if (temp.gameObject.activeInHierarchy && temp.interactable)
                    {
                        navigation.selectOnLeft = temp;
                        break;
                    }

                    else
                    {
                        navigation.selectOnLeft = null;
                    }

                    temp = temp.navigation.selectOnLeft;

                }

            }

            // selectOnRight
            {

                temp = navigation.selectOnRight;

                while (temp)
                {

                    if (temp.gameObject.activeInHierarchy && temp.interactable)
                    {
                        navigation.selectOnRight = temp;
                        break;
                    }

                    else
                    {
                        navigation.selectOnRight = null;
                    }

                    temp = temp.navigation.selectOnRight;

                }

            }

            // selectOnUp
            {

                temp = navigation.selectOnUp;

                while (temp)
                {

                    if (temp.gameObject.activeInHierarchy && temp.interactable)
                    {
                        navigation.selectOnUp = temp;
                        break;
                    }

                    else
                    {
                        navigation.selectOnUp = null;
                    }

                    temp = temp.navigation.selectOnUp;

                }

            }

            this.m_refSelectable.navigation = navigation;

        }

    }

}
