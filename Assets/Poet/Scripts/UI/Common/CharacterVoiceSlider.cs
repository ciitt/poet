﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Poet
{

    public class CharacterVoiceSlider : Slider
    {

        [Space(20.0f)]
        public string characterVoiceId = "CharacterA";

        [Space(20.0f)]
        public CharacterVoiceSliderEvent onCharacterVoiceValueChanged;

        protected bool m_quitting = false;

        protected override void Awake()
        {

            base.Awake();

            this.onValueChanged.RemoveListener(this.callCharacterVoiceValueChanged);
            this.onValueChanged.AddListener(this.callCharacterVoiceValueChanged);

        }

        [System.Serializable]
        public class CharacterVoiceSliderEvent : UnityEvent<string, float>
        {
            public CharacterVoiceSliderEvent() : base()
            {

            }
        }

        public void callCharacterVoiceValueChanged(float value)
        {
            
            if(this.m_quitting)
            {
                return;
            }

            this.onCharacterVoiceValueChanged.Invoke(this.characterVoiceId, value);

        }

        protected virtual void OnApplicationQuit()
        {
            this.m_quitting = true;
        }

    }

}
