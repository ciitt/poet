﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Poet
{

    [RequireComponent(typeof(Image))]
    public class ChangeUiImageScript : MonoBehaviour
    {

        /// <summary>
        /// Languaget and its image
        /// </summary>
        [SerializeField]
        [Tooltip("Languaget and its image")]
        protected LanguageAndImages m_languageAndImages = new LanguageAndImages();

        /// <summary>
        /// Reference to Image
        /// </summary>
        protected Image m_refImage = null;

        /// <summary>
        /// Start
        /// </summary>
        // -------------------------------------------------------------------------------
        protected virtual void Start()
        {

            this.m_refImage = this.GetComponent<Image>();

            this.m_languageAndImages.initDictionary();

            PoetReduxManager prm = (PoetReduxManager.Instance as PoetReduxManager);
            prm.addChangeLanguageSignalReceiver(this.onChangeLanguageSignalReceiver);

        }

        /// <summary>
        /// ChangeLanguageSignal receiver
        /// </summary>
        /// <param name="clSignal">ChangeLanguageSignal</param>
        // -----------------------------------------------------------------------------------------------
        protected virtual void onChangeLanguageSignalReceiver(ChangeLanguageSignal clSignal)
        {

            SystemLanguage sl = SystemManager.Instance.systemLanguage;

            if (this.m_languageAndImages.languageAndImageDictionary.ContainsKey(sl))
            {
                this.m_refImage.sprite = this.m_languageAndImages.languageAndImageDictionary[sl];
            }

        }

    }

}
