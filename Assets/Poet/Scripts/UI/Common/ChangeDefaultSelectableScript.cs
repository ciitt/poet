﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Poet
{

    /// <summary>
    /// Change default Selectable in UiManager
    /// </summary>
    [RequireComponent(typeof(Selectable))]
    public class ChangeDefaultSelectableScript : MonoBehaviour, ISelectHandler
    {

        /// <summary>
        /// Target UI manager type
        /// </summary>
        [SerializeField]
        [Tooltip("Target UI manager type")]
        private SSC.UiManager.UiManagerType m_uiManagerType = SSC.UiManager.UiManagerType.Common;

        /// <summary>
        /// Target UI identifier
        /// </summary>
        [SerializeField]
        [Tooltip("Target UI identifier")]
        private List<string> m_uiIdentifiers = new List<string>();

        /// <summary>
        /// Reference to Selectable
        /// </summary>
        private Selectable m_refSelectable = null;

        /// <summary>
        /// Awake
        /// </summary>
        void Awake()
        {
            this.m_refSelectable = this.GetComponent<Selectable>();
        }

        /// <summary>
        /// OnSelect
        /// </summary>
        /// <param name="eventData">BaseEventData</param>
        public void OnSelect(BaseEventData eventData)
        {

            foreach(string identifier in this.m_uiIdentifiers)
            {

                if (this.m_uiManagerType == SSC.UiManager.UiManagerType.Common)
                {

                    var dic = SSC.CommonUiManager.Instance.uiDictionary;

                    if (!string.IsNullOrEmpty(identifier) && dic.ContainsKey(identifier))
                    {
                        dic[identifier].defaultSelectable = this.m_refSelectable;
                    }

#if UNITY_EDITOR

                    else
                    {
                        Debug.LogWarning("(#if UNITY_EDITOR) Not contains a key : " + identifier + " : " + this.gameObject.name);
                    }

#endif

                }

                else if (this.m_uiManagerType == SSC.UiManager.UiManagerType.Scene && SSC.SceneUiManager.isAvailable())
                {

                    var dic = SSC.SceneUiManager.Instance.uiDictionary;

                    if (!string.IsNullOrEmpty(identifier) && dic.ContainsKey(identifier))
                    {
                        dic[identifier].defaultSelectable = this.m_refSelectable;
                    }

#if UNITY_EDITOR

                    else
                    {
                        Debug.LogWarning("(#if UNITY_EDITOR) Not contains a key : " + identifier + " : " + this.gameObject.name);
                    }

#endif

                }

            }

        }

    }

}
