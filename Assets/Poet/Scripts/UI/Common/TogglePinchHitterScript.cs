﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Poet
{

    /// <summary>
    /// Pinch hitter for toggle event
    /// </summary>
    public class TogglePinchHitterScript : MonoBehaviour
    {

        [SerializeField]
        UnityEvent m_unityEvent;

        public void swingIfStrike(bool isOn)
        {

            if(isOn && this.m_unityEvent != null)
            {
                this.m_unityEvent.Invoke();
            }

        }

    }

}