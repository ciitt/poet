﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Poet
{

    /// <summary>
    /// Screen effect
    /// </summary>
    [RequireComponent(typeof(RectTransform))]
    [RequireComponent(typeof(CanvasGroup))]
    public class ScreenEffectScript : PoetBaseReceiverScript
    {

        /// <summary>
        /// IdAndTexture and tilling
        /// </summary>
        [Serializable]
        protected class TextureAndTilling : IdAndTexture
        {
            public Vector2 tilling = Vector2.one;
        }

        /// <summary>
        /// User progress data
        /// </summary>
        [Serializable]
        protected class UserProgressData
        {

            public float resumeTime = 0.0f;
            public FuncNameAndParamsList fnapList = null;

            public void clear()
            {
                this.resumeTime = 0.0f;
                this.fnapList = null;
            }

        }

        /// <summary>
        /// Csv header
        /// </summary>
        [SerializeField]
        [Tooltip("Csv header")]
        protected string m_csvHeader = "Screen";

        /// <summary>
        /// Function name to start
        /// </summary>
        [SerializeField]
        [Tooltip("Function name to start")]
        protected string m_functionName = "Curtain";

        /// <summary>
        /// Reference to UI Image
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to UI Image")]
        protected Image m_refTargetUiImage;

        /// <summary>
        /// TextureAndTilling list
        /// </summary>
        [SerializeField]
        [Tooltip("TextureAndTilling list")]
        protected List<TextureAndTilling> m_textureList = new List<TextureAndTilling>();

        /// <summary>
        /// TextureAndTilling dictionary
        /// </summary>
        protected Dictionary<string, TextureAndTilling> m_textureDict = new Dictionary<string, TextureAndTilling>();

        /// <summary>
        /// Material
        /// </summary>
        protected Material m_material = null;

        /// <summary>
        /// Reference to RectTransform
        /// </summary>
        protected RectTransform m_refRectTransform = null;

        /// <summary>
        /// Reference to CanvasGroup
        /// </summary>
        protected CanvasGroup m_refCanvasGroup = null;

        /// <summary>
        /// UserProgressData
        /// </summary>
        protected UserProgressData m_userProgressData = new UserProgressData();

        /// <summary>
        /// initOnAwake() is called after Awake().
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected override void initOnAwake()
        {

            this.m_refRectTransform = this.GetComponent<RectTransform>();
            this.m_refCanvasGroup = this.GetComponent<CanvasGroup>();

            {
                this.m_refCanvasGroup.alpha = 0.0f;
                this.m_refCanvasGroup.interactable = false;
                this.m_refCanvasGroup.blocksRaycasts = false;
            }

            // m_refTargetUiImage
            {

                if (this.m_refTargetUiImage)
                {
                    this.m_material = new Material(this.m_refTargetUiImage.material);
                    this.m_refTargetUiImage.material = this.m_material;
                }

                else
                {
                    Debug.LogWarning("m_refTargetUiImage is null");
                }

            }

            // m_textureDict
            {
                this.m_textureDict = Funcs.listToDictionary<TextureAndTilling>(this.m_textureList);
            }

        }

        /// <summary>
        /// OnDestroy
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected virtual void OnDestroy()
        {

            if (this.m_material)
            {
                Destroy(this.m_material);
            }

        }

        /// <summary>
        /// Solve functions
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected void solveFunctions(FuncNameAndParamsList fnapList, float resumeTime)
        {

            if (fnapList == null)
            {
                return;
            }

            // -------------------

            foreach (var fnap in fnapList.list)
            {

                if (fnap.functionName == this.m_functionName)
                {
                    StopAllCoroutines();
                    StartCoroutine(this.startShowEffect(fnap, fnapList, resumeTime));
                    break;
                }

            }

        }

        /// <summary>
        /// Start showEffectIE
        /// </summary>
        /// <param name="fnap">FuncNameAndParams</param>
        /// <param name="fnapList">FuncNameAndParamsList</param>
        // ------------------------------------------------------------------------------------------
        protected IEnumerator startShowEffect(FuncNameAndParams fnap, FuncNameAndParamsList fnapList, float resumeTime)
        {

            if (fnap == null || fnapList == null)
            {
                yield break;
            }

            // -------------------

            // m_userProgressData
            {
                this.m_userProgressData.resumeTime = resumeTime;
                this.m_userProgressData.fnapList = fnapList;
            }

            // wait for invoke
            {
                yield return this.waitInvokeTime(fnapList.invokeTime, resumeTime, () =>
                {
                    this.m_userProgressData.resumeTime += Time.deltaTime;
                });
                this.m_userProgressData.resumeTime = Mathf.Max(fnapList.invokeTime, resumeTime);
            }

            StartCoroutine(this.showEffectIE(
                fnap.getStringParameter(0),
                fnap.getFloatParameter(1, 0.0f),
                fnap.getFloatParameter(2, 0.0f),
                fnap.getFloatParameter(3, 0.0f),
                fnapList.invokeTime,
                resumeTime
                ));

        }

        /// <summary>
        /// showEffectIE
        /// </summary>
        /// <param name="targetTextureIdentifier">target texture identifier</param>
        /// <param name="fadeInSeconds">fade in seconds</param>
        /// <param name="staySeconds">stay seconds</param>
        /// <param name="fadeOutSeconds">fade out seconds</param>
        /// <param name="invokeTime">invoke seconds</param>
        /// <param name="resumeTime">resume seconds</param>
        /// <returns>IEnumerator</returns>
        // ------------------------------------------------------------------------------------------
        protected IEnumerator showEffectIE(
            string targetTextureIdentifier,
            float fadeInSeconds,
            float staySeconds,
            float fadeOutSeconds,
            float invokeTime,
            float resumeTime
            )
        {

            if (!this.m_material)
            {
                yield break;
            }

            // -----------------------

            float timer = 0.0f;

            var pauseState = PoetReduxManager.Instance.PauseStateWatcher.state();

            // init
            {

                this.m_material.SetFloat("_FillAmount", 0.0f);
                this.m_refCanvasGroup.blocksRaycasts = true;
                this.m_refCanvasGroup.alpha = 1.0f;

            }

            // texture tilling
            {

                Texture2D tex = null;
                Vector2 tilling = Vector2.one;

                if (this.m_textureDict.ContainsKey(targetTextureIdentifier))
                {
                    tex = this.m_textureDict[targetTextureIdentifier].texture;
                    tilling = this.m_textureDict[targetTextureIdentifier].tilling;
                }

                this.m_material.SetTexture("_MaskTex", tex);
                this.m_material.SetTextureScale("_MaskTex", tilling);

            }

            // fade in
            {

                timer = Mathf.Max(resumeTime - invokeTime, 0.0f);
                this.m_userProgressData.resumeTime = Mathf.Max(invokeTime, resumeTime);

                while (timer < fadeInSeconds)
                {

                    // break
                    {
                        if (PoetOneSceneManager.isAvailable() && PoetOneSceneManager.Instance.shouldSkip())
                        {
                            break;
                        }
                    }

                    // pauseState
                    {
                        if (pauseState.pause)
                        {
                            yield return null;
                            continue;
                        }
                    }

                    // SetFloat
                    {
                        this.m_material.SetFloat("_FillAmount", timer / fadeInSeconds);
                    }

                    // timer
                    {
                        timer += Time.deltaTime;
                        this.m_userProgressData.resumeTime += Time.deltaTime;
                    }

                    yield return null;

                }

                // finish
                {
                    this.m_material.SetFloat("_FillAmount", 1.0f);
                }

            }

            // stay
            {

                timer = Mathf.Max(resumeTime - invokeTime - fadeInSeconds, 0.0f);
                this.m_userProgressData.resumeTime = Mathf.Max(invokeTime + fadeInSeconds, resumeTime);

                while (timer < staySeconds)
                {

                    // break
                    {
                        if (PoetOneSceneManager.isAvailable() && PoetOneSceneManager.Instance.shouldSkip())
                        {
                            break;
                        }
                    }

                    // pauseState
                    {
                        if (pauseState.pause)
                        {
                            yield return null;
                            continue;
                        }
                    }

                    // timer
                    {
                        timer += Time.deltaTime;
                        this.m_userProgressData.resumeTime += Time.deltaTime;
                    }

                    yield return null;

                }

            }

            // fade out
            {

                timer = Mathf.Max(resumeTime - invokeTime - fadeInSeconds - staySeconds, 0.0f);
                this.m_userProgressData.resumeTime = Mathf.Max(invokeTime + fadeInSeconds + staySeconds, resumeTime);

                while (timer < fadeOutSeconds)
                {

                    // break
                    {
                        if (PoetOneSceneManager.isAvailable() && PoetOneSceneManager.Instance.shouldSkip())
                        {
                            break;
                        }
                    }

                    // pauseState
                    {
                        if (pauseState.pause)
                        {
                            yield return null;
                            continue;
                        }
                    }

                    // SetFloat
                    {
                        this.m_material.SetFloat("_FillAmount", 1.0f - (timer / fadeInSeconds));
                    }

                    // timer
                    {
                        timer += Time.deltaTime;
                        this.m_userProgressData.resumeTime += Time.deltaTime;
                    }

                    yield return null;

                }

                // finish
                {
                    this.m_material.SetFloat("_FillAmount", 0.0f);
                }

            }

            // finish
            {
                this.m_material.SetFloat("_FillAmount", 0.0f);
                this.m_refCanvasGroup.alpha = 0.0f;
                this.m_refCanvasGroup.blocksRaycasts = false;
            }

            // m_userProgressData
            {
                this.m_userProgressData.clear();
            }

        }

        /// <summary>
        /// PoetSequenceState receiver
        /// </summary>
        /// <param name="psState">PoetSequenceState</param>
        // ------------------------------------------------------------------------------------------
        protected override void onPoetSequenceState(PoetSequenceState psState)
        {

            if (psState.stateEnum == PoetSequenceState.StateEnum.PreProcess)
            {

                if (psState.headerAndCells.ContainsKey(this.m_csvHeader))
                {

                    this.m_currentFnapList.clear();

                    CsvParser.parseFunctionsInCell(psState.headerAndCells[this.m_csvHeader], this.m_currentFnapList);

                    if (this.m_currentFnapList.preProcess)
                    {
                        this.solveFunctions(new FuncNameAndParamsList(this.m_currentFnapList), 0.0f);
                    }

                }

            }

            else if (psState.stateEnum == PoetSequenceState.StateEnum.MainProcess)
            {

                if (psState.headerAndCells.ContainsKey(this.m_csvHeader))
                {

                    if (!this.m_currentFnapList.preProcess)
                    {
                        this.solveFunctions(new FuncNameAndParamsList(this.m_currentFnapList), 0.0f);
                    }

                }

            }

        }

        /// <summary>
        /// PoetInitState receiver
        /// </summary>
        /// <param name="piState">PoetInitState</param>
        // ------------------------------------------------------------------------------------------
        protected override void onPoetInitState(PoetInitState piState)
        {

#if UNITY_EDITOR

            if (piState.headerAndCells.ContainsKey(this.m_csvHeader))
            {

                FuncNameAndParamsList fnapList = CsvParser.parseFunctionsInCell(piState.headerAndCells[this.m_csvHeader]);

                foreach(var val in fnapList.list)
                {

                    if(
                        val.functionName != "Curtain" &&
                        val.functionName != "Flash"
                        )
                    {

                        Debug.LogWarning(
                            "Unknown function name : " +
                            val.functionName + " : " +
                            this.m_csvHeader + " : " +
                            piState.textAssetName + " : " +
                            piState.index
                            );

                    }

                }

            }

#endif

        }

        /// <summary>
        /// TextShowingState receiver
        /// </summary>
        /// <param name="tsState">TextShowingState</param>
        // ------------------------------------------------------------------------------------------
        protected override void onTextShowingState(TextShowingState tsState)
        {

        }

        /// <summary>
        /// UserProgressDataSignal receiver
        /// </summary>
        /// <param name="updSignal">UserProgressDataSignal</param>
        // ------------------------------------------------------------------------------------------
        protected override void onUserProgressDataSignal(UserProgressDataSignal updSignal)
        {
            updSignal.addDataAction(this.transform, this, JsonUtility.ToJson(this.m_userProgressData));
        }

        /// <summary>
        /// SceneChangeState receiver
        /// </summary>
        /// <param name="scState">SceneChangeState</param>
        // ------------------------------------------------------------------------------------------
        protected override void onSceneChangeStateReceiver(SSC.SceneChangeState scState)
        {

            if (scState.stateEnum == SSC.SceneChangeState.StateEnum.AllStartupsDone)
            {

                PoetSceneChangeManager pscm = PoetSceneChangeManager.Instance as PoetSceneChangeManager;

                if (pscm.isLoadingCurrentSceneWithUserProgressData())
                {
                    this.m_userProgressData = pscm.getDataFromCurrentUserProgressData<UserProgressData>(this.transform, this);
                }

            }

            else if (scState.stateEnum == SSC.SceneChangeState.StateEnum.ScenePlaying)
            {

                PoetSceneChangeManager pscm = PoetSceneChangeManager.Instance as PoetSceneChangeManager;

                if (pscm.isLoadingCurrentSceneWithUserProgressData())
                {

                    if (this.m_userProgressData.fnapList != null)
                    {
                        this.solveFunctions(this.m_userProgressData.fnapList, this.m_userProgressData.resumeTime);
                    }

                }

            }

        }

        /// <summary>
        /// PauseState receiver
        /// </summary>
        /// <param name="pState">PauseState</param>
        // ------------------------------------------------------------------------------------------
        protected override void onPauseStateReceiver(SSC.PauseState pState)
        {

        }

    }

}
