﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Poet
{

    [RequireComponent(typeof(Text))]
    public class FrameRateScript : MonoBehaviour
    {

        public int m_interval = 60;

        Text m_refText = null;

        void Start()
        {
            this.m_refText = this.GetComponent<Text>();

            this.m_interval = Mathf.Max(1, this.m_interval);
        }

        void Update()
        {

            if(Time.frameCount % this.m_interval == 0 && Time.deltaTime > 0.0f)
            {
                this.m_refText.text = (1.0f / Time.deltaTime).ToString();
            }

        }

    }

}
