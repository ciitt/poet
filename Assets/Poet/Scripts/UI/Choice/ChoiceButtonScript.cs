﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Poet
{

    [RequireComponent(typeof(Button))]
    [RequireComponent(typeof(CanvasGroup))]
    public class ChoiceButtonScript : MonoBehaviour
    {

        /// <summary>
        /// Reference to Text
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to Text")]
        protected Text m_refText = null;

        /// <summary>
        /// Reference to already read marker
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to already read marker")]
        protected Image m_refAlreadyReadMarker = null;

        /// <summary>
        /// Choice sound preloaded id
        /// </summary>
        [SerializeField]
        [Tooltip("Choice sound preloaded id")]
        protected string m_choiceSoundPreloadedId = "";

        /// <summary>
        /// Reference to Button
        /// </summary>
        protected Button m_refButton = null;

        /// <summary>
        /// Reference to CanvasGroup
        /// </summary>
        protected CanvasGroup m_refCanvasGroup = null;

        /// <summary>
        /// Selected id
        /// </summary>
        protected string m_choiceId = "";

        /// <summary>
        /// Awake
        /// </summary>
        // ---------------------------------------------------------------------------------------
        protected virtual void Awake()
        {

            this.m_refButton = this.GetComponent<Button>();

            this.m_refCanvasGroup = this.GetComponent<CanvasGroup>();

            if(!this.m_refText)
            {
                Debug.LogWarning("m_refText is null : " + this.gameObject.name);
            }

        }

        /// <summary>
        /// On click button
        /// </summary>
        // ---------------------------------------------------------------------------------------
        public void onClickButton()
        {

            if (!string.IsNullOrEmpty(this.m_choiceSoundPreloadedId))
            {
                PoetAudioManager.Instance.refSE.playPreloadedAudio(this.m_choiceSoundPreloadedId, 0.0f, false);
            }

            PoetCommonUiManager.Instance.showUi("", true, false, 0.0f, null, () =>
            {
                if (PoetOneSceneManager.isAvailable())
                {
                    PoetOneSceneManager.Instance.lockGoingToNext = false;
                    PoetOneSceneManager.Instance.goToSequence(this.m_choiceId);
                }
            });

        }

        /// <summary>
        /// Clear data
        /// </summary>
        // ---------------------------------------------------------------------------------------
        public virtual void clear()
        {

            // m_choiceId
            {
                this.m_choiceId = "";
            }

            // m_refCanvasGroup
            {

                this.m_refCanvasGroup.interactable = false;
                this.m_refCanvasGroup.alpha = 0.0f;
                this.m_refButton.interactable = false;

            }

            // m_refText
            {

                if (this.m_refText)
                {
                    this.m_refText.text = "";
                }

            }

            // m_refAlreadyReadMarker
            {

                if (this.m_refAlreadyReadMarker)
                {

                    Color temp = this.m_refAlreadyReadMarker.color;
                    temp.a = 0.0f;
                    this.m_refAlreadyReadMarker.color = temp;

                }

            }

        }

        /// <summary>
        /// Select button
        /// </summary>
        // ---------------------------------------------------------------------------------------
        public void selectButton()
        {

            if(this.m_refButton)
            {

                if(EventSystem.current.currentSelectedGameObject != this.m_refButton.gameObject)
                {
                    EventSystem.current.SetSelectedGameObject(null);
                    this.m_refButton.Select();
                }
                
            }

        }

        /// <summary>
        /// Set choice text
        /// </summary>
        /// <param name="choiceId">id</param>
        /// <param name="choiceText">text</param>
        /// <param name="anyInfo">any info</param>
        // ---------------------------------------------------------------------------------------
        public virtual void setChoiceText(string choiceId, string choiceText, System.Object anyInfo)
        {

            // m_choiceId
            {
                this.m_choiceId = choiceId;
            }

            // m_refCanvasGroup
            {

                if (string.IsNullOrEmpty(choiceId))
                {
                    this.m_refCanvasGroup.interactable = false;
                    this.m_refCanvasGroup.alpha = 0.0f;
                    this.m_refButton.interactable = false;
                }

                else
                {
                    this.m_refCanvasGroup.interactable = true;
                    this.m_refCanvasGroup.alpha = 1.0f;
                    this.m_refButton.interactable = true;
                }

            }

            // m_refText
            {

                if(this.m_refText)
                {
                    this.m_refText.text = choiceText;
                }

            }

            // m_refAlreadyReadMarker
            {

                if(this.m_refAlreadyReadMarker && PoetOneSceneManager.isAvailable())
                {

                    Color temp = this.m_refAlreadyReadMarker.color;

                    temp.a = (PoetOneSceneManager.Instance.isAlreadyRead(choiceId)) ? 1.0f : 0.0f;

                    this.m_refAlreadyReadMarker.color = temp;

                }

            }

        }

    }

}
