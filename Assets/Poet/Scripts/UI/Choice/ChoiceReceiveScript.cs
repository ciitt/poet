﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Poet
{

    /// <summary>
    /// Choice receiver
    /// </summary>
    public class ChoiceReceiveScript : PoetBaseReceiverScript
    {

        /// <summary>
        /// User progress data
        /// </summary>
        [Serializable]
        protected class UserProgressData
        {

            /// <summary>
            /// ChoiceIdAndText list
            /// </summary>
            public List<IdAndText> choiceIdAndTexts = new List<IdAndText>();

            /// <summary>
            /// Clear
            /// </summary>
            public void clear()
            {
                this.choiceIdAndTexts.Clear();
            }

        }

        /// <summary>
        /// User progress data
        /// </summary>
        protected UserProgressData m_userProgressData = new UserProgressData();

        /// <summary>
        /// Show previous choices button
        /// </summary>
        [SerializeField]
        [Tooltip("Show previous choices button")]
        protected Button m_refPreviousPageButton = null;

        /// <summary>
        /// Show next choices button
        /// </summary>
        [SerializeField]
        [Tooltip("Show next choices button")]
        protected Button m_refNextPageButton = null;

        /// <summary>
        /// Reference to each ChoiceButtonScript
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to each ChoiceButtonScript")]
        protected List<ChoiceButtonScript> m_refChoices = new List<ChoiceButtonScript>();

        /// <summary>
        /// Languaget and its header
        /// </summary>
        [SerializeField]
        [Tooltip("Languaget and its header")]
        protected LanguageAndHeaders m_languageAndHeaders = new LanguageAndHeaders("Choice");

        /// <summary>
        /// Current choice page index
        /// </summary>
        protected int m_currentChoicePageIndex = 0;

        /// <summary>
        /// Called in Awake
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected override void initOnAwake()
        {

            // m_languageAndHeaders
            {
                this.m_languageAndHeaders.initDictionary();
            }

#if UNITY_EDITOR

            if (this.m_refChoices.Count <= 0)
            {
                Debug.LogWarning("(#if UNITY_EDITOR) m_refChoices is empty");
            }

            for(int i = this.m_refChoices.Count - 1; i >= 0; i--)
            {

                if (!this.m_refChoices[i])
                {
                    Debug.LogError("(#if UNITY_EDITOR) Empty element in m_refChoices");
                    this.m_refChoices.RemoveAt(i);
                }

            }

#endif

        }

        /// <summary>
        /// PoetInitState receiver
        /// </summary>
        /// <param name="piState">PoetInitState</param>
        // ------------------------------------------------------------------------------------------
        protected override void onPoetInitState(PoetInitState piState)
        {

#if UNITY_EDITOR

            SystemLanguage sl = SystemManager.Instance.systemLanguage;

            if (this.m_languageAndHeaders.languageAndHeaderDictionary.ContainsKey(sl))
            {

                string header = this.m_languageAndHeaders.languageAndHeaderDictionary[sl];

                if (piState.headerAndCells.ContainsKey(header))
                {

                    FuncNameAndParamsList fnapList = CsvParser.parseFunctionsInCell(piState.headerAndCells[header]);

                    foreach (var val in fnapList.list)
                    {

                        if (
                            val.functionName != "Choice"
                            )
                        {

                            Debug.LogWarning(
                                "Unknown function name : " +
                                val.functionName + " : " +
                                header + " : " +
                                piState.textAssetName + " : " +
                                piState.index
                                );

                        }

                    }

                }

            }

#endif

        }

        /// <summary>
        /// PoetSequenceState receiver
        /// </summary>
        /// <param name="state">PoetSequenceState</param>
        // ------------------------------------------------------------------------------------------
        protected override void onPoetSequenceState(PoetSequenceState psState)
        {

            SystemLanguage sl = SystemManager.Instance.systemLanguage;

            if (psState.stateEnum == PoetSequenceState.StateEnum.PreProcess)
            {

                this.m_userProgressData.clear();

                if (this.m_languageAndHeaders.languageAndHeaderDictionary.ContainsKey(sl))
                {

                    string header = this.m_languageAndHeaders.languageAndHeaderDictionary[sl];

                    if (psState.headerAndCells.ContainsKey(header) && PoetOneSceneManager.isAvailable())
                    {
                        PoetOneSceneManager.Instance.lockGoingToNext = !string.IsNullOrEmpty(psState.headerAndCells[header]);
                        this.initChoiceIdAndTexts(CsvParser.parseFunctionsInCell(psState.headerAndCells[header]));
                    }

                }

            }

            else if(psState.stateEnum == PoetSequenceState.StateEnum.FinishOperation)
            {
                this.m_userProgressData.clear();
            }

        }

        /// <summary>
        /// TextShowingState receiver
        /// </summary>
        /// <param name="tfState">TextShowingState</param>
        // ------------------------------------------------------------------------------------------
        protected override void onTextShowingState(TextShowingState tsState)
        {

        }

        /// <summary>
        /// Init choice dictionary
        /// </summary>
        /// <param name="fnapList">FuncNameAndParamsList</param>
        // ------------------------------------------------------------------------------------------
        protected void initChoiceIdAndTexts(FuncNameAndParamsList fnapList)
        {

            // reset
            {
                this.m_userProgressData.choiceIdAndTexts.Clear();
            }

            if (fnapList == null)
            {
                return;
            }

            // --------------------------

            //
            {

                foreach (var val in fnapList.list)
                {

                    if (val.functionName == "Choice")
                    {
                        this.m_userProgressData.choiceIdAndTexts.Add(new IdAndText(val.getStringParameter(0), val.getStringParameter(1)));
                    }

                }

            }

            // set
            {

                if (this.m_userProgressData.choiceIdAndTexts.Count > 0)
                {
                    this.setChoiceTexts(0, false);
                    //PoetCommonUiManager.Instance.showUi(PoetUiIdentifiers.Common.Choice, false, false);
                }

            }

        }

        /// <summary>
        /// Next page button is clicked
        /// </summary>
        // ------------------------------------------------------------------------------------------
        public virtual void onClickNextPage()
        {
            this.setChoiceTexts(this.m_currentChoicePageIndex + 1, true);
        }

        /// <summary>
        /// Previous page button is clicked
        /// </summary>
        // ------------------------------------------------------------------------------------------
        public virtual void onClickPreviousPage()
        {
            this.setChoiceTexts(this.m_currentChoicePageIndex - 1, true);
        }

        /// <summary>
        /// Set choices
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected virtual void setChoiceTexts(int choicePageIndex, bool select)
        {

            int allChoicesSize = this.m_userProgressData.choiceIdAndTexts.Count;
            int uiSize = this.m_refChoices.Count;

            int firstIndex = uiSize * choicePageIndex;

            if (firstIndex < 0 || allChoicesSize <= firstIndex)
            {
                return;
            }

            this.m_currentChoicePageIndex = choicePageIndex;

            // UI
            {

                string id = "";
                string text = "";

                for (int i = 0; i < uiSize; i++)
                {

                    if (firstIndex + i < allChoicesSize)
                    {
                        id = this.m_userProgressData.choiceIdAndTexts[firstIndex + i].identifier;
                        text = this.m_userProgressData.choiceIdAndTexts[firstIndex + i].text;
                    }

                    else
                    {
                        id = "";
                        text = "";
                    }

                    this.m_refChoices[i].setChoiceText(id, text, null);

                }

            }

            // next and previous
            {

                if (this.m_refNextPageButton)
                {
                    this.m_refNextPageButton.interactable = (firstIndex + uiSize < allChoicesSize);
                }

                if (this.m_refPreviousPageButton)
                {

                    this.m_refPreviousPageButton.interactable = choicePageIndex >= 1;
                }

                if (select && this.m_refChoices.Count > 0)
                {
                    this.m_refChoices[0].selectButton();
                }

            }

        }

        /// <summary>
        /// UserProgressDataSignal receiver
        /// </summary>
        /// <param name="updSignal">UserProgressDataSignal</param>
        // ------------------------------------------------------------------------------------------
        protected override void onUserProgressDataSignal(UserProgressDataSignal updSignal)
        {
            updSignal.addDataAction(this.transform, this, JsonUtility.ToJson(this.m_userProgressData));
        }

        /// <summary>
        /// SceneChangeState receiver
        /// </summary>
        /// <param name="scState">SceneChangeState</param>
        // ------------------------------------------------------------------------------------------
        protected override void onSceneChangeStateReceiver(SSC.SceneChangeState scState)
        {

            if (scState.stateEnum == SSC.SceneChangeState.StateEnum.AllStartupsDone)
            {

                this.clearChoicePanels();

                PoetSceneChangeManager pscm = PoetSceneChangeManager.Instance as PoetSceneChangeManager;

                if (pscm.isLoadingCurrentSceneWithUserProgressData())
                {
                    this.m_userProgressData = pscm.getDataFromCurrentUserProgressData<UserProgressData>(this.transform, this);
                    this.setChoiceTexts(0, false);
                }

            }

        }

        /// <summary>
        /// PauseState receiver
        /// </summary>
        /// <param name="pState">PauseState</param>
        // ------------------------------------------------------------------------------------------
        protected override void onPauseStateReceiver(SSC.PauseState pState)
        {

        }

        /// <summary>
        /// Clear
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected void clearChoicePanels()
        {

            foreach(var val in this.m_refChoices)
            {
                if(val)
                {
                    val.clear();
                }
            }

        }

    }

}
