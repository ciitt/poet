﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Poet
{

    /// <summary>
    /// Show popup
    /// </summary>
    public class PopUpUiScript : PoetBaseReceiverScript
    {

        /// <summary>
        /// Reference to UiControllerScript
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to UiControllerScript")]
        protected SSC.UiControllerScript m_refUiController = null;

        /// <summary>
        /// Reference to Button
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to Button")]
        protected Button m_refGoToNextButton = null;

        /// <summary>
        /// Reference to Text
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to Text")]
        protected Text m_refText = null;

        /// <summary>
        /// Preloaded SE id for popup
        /// </summary>
        [SerializeField]
        [Tooltip("Preloaded SE id for popup")]
        protected string m_preloadedSeIdForPopUp = "";

        /// <summary>
        /// Languaget and its header
        /// </summary>
        [SerializeField]
        [Tooltip("Languaget and its header")]
        protected LanguageAndHeaders m_languageAndHeaders = new LanguageAndHeaders("PopUp");

        /// <summary>
        /// initOnAwake() is called after Awake().
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected override void initOnAwake()
        {

            if (!this.m_refText)
            {
                Debug.LogWarning("m_refText is null");
            }

            if (!this.m_refGoToNextButton)
            {
                Debug.LogWarning("m_refGoToNextButton is null");
            }

            if (!this.m_refUiController)
            {
                Debug.LogWarning("m_refUiController is null");
            }

            // m_languageAndHeaders
            {
                this.m_languageAndHeaders.initDictionary();
            }

        }

        /// <summary>
        /// Goto next sequence
        /// </summary>
        // ------------------------------------------------------------------------------------------
        public void onClickGoToNextSequenceButton()
        {

            this.m_refUiController.startHiding();

            EventSystem.current.SetSelectedGameObject(null);

            if (PoetOneSceneManager.isAvailable())
            {
                PoetOneSceneManager.Instance.lockGoingToNext = false;
            }

            PoetOneSceneManager.Instance.goToNextSequence(true);

        }

        /// <summary>
        /// Show popup
        /// </summary>
        /// <param name="text">text</param>
        // ------------------------------------------------------------------------------------------
        protected void showPopUp(string text)
        {

            if (this.m_refText)
            {
                this.m_refText.text = text;
            }

            if (this.m_refUiController)
            {

                PoetCommonUiManager.Instance.showUi("", true, false);

                //
                {

                    if(!string.IsNullOrEmpty(this.m_preloadedSeIdForPopUp))
                    {
                        PoetAudioManager.Instance.refSE.playPreloadedAudio(this.m_preloadedSeIdForPopUp, 0.0f, false);
                    }

                }

                this.m_refUiController.startShowing(() =>
                {

                    if (this.m_refGoToNextButton)
                    {
                        EventSystem.current.SetSelectedGameObject(this.m_refGoToNextButton.gameObject);
                    }

                });

            }

        }

        /// <summary>
        /// PoetSequenceState receiver
        /// </summary>
        /// <param name="psState">PoetSequenceState</param>
        // ------------------------------------------------------------------------------------------
        protected override void onPoetSequenceState(PoetSequenceState psState)
        {

            if (psState.stateEnum == PoetSequenceState.StateEnum.PreProcess)
            {

                if(PoetOneSceneManager.isAvailable())
                {
                    PoetOneSceneManager.Instance.lockGoingToNext = true;
                }

            }

            else if (psState.stateEnum == PoetSequenceState.StateEnum.MainProcess)
            {

                if (this.m_refText)
                {
                    this.m_refText.text = "";
                }

                // ---------------

                SystemLanguage sl = SystemManager.Instance.systemLanguage;

                if (this.m_languageAndHeaders.languageAndHeaderDictionary.ContainsKey(sl))
                {

                    string header = this.m_languageAndHeaders.languageAndHeaderDictionary[sl];

                    if(psState.headerAndCells.ContainsKey(header))
                    {

                        var fnapList = CsvParser.parseFunctionsInCell(psState.headerAndCells[header]);

                        foreach (var val in fnapList.list)
                        {

                            if (val.functionName == "PopUp")
                            {
                                this.showPopUp(val.getStringParameter(0));
                            }

                        }

                    }

                }

            }

            else if(psState.stateEnum == PoetSequenceState.StateEnum.FinishOperation)
            {

                if (this.m_refText)
                {
                    this.m_refText.text = "";
                }

            }

        }

        /// <summary>
        /// PoetInitState receiver
        /// </summary>
        /// <param name="piState">PoetInitState</param>
        // ------------------------------------------------------------------------------------------
        protected override void onPoetInitState(PoetInitState piState)
        {

#if UNITY_EDITOR

            SystemLanguage sl = SystemManager.Instance.systemLanguage;

            if (this.m_languageAndHeaders.languageAndHeaderDictionary.ContainsKey(sl))
            {

                string header = this.m_languageAndHeaders.languageAndHeaderDictionary[sl];

                if (piState.headerAndCells.ContainsKey(header))
                {

                    FuncNameAndParamsList fnapList = CsvParser.parseFunctionsInCell(piState.headerAndCells[header]);

                    foreach (var val in fnapList.list)
                    {

                        if (
                            val.functionName != "PopUp"
                            )
                        {

                            Debug.LogWarning(
                                "Unknown function name : " +
                                val.functionName + " : " +
                                header + " : " +
                                piState.textAssetName + " : " +
                                piState.index
                                );

                        }

                    }

                }

            }
#endif

        }

        /// <summary>
        /// TextShowingState receiver
        /// </summary>
        /// <param name="tsState">TextShowingState</param>
            // ------------------------------------------------------------------------------------------
        protected override void onTextShowingState(TextShowingState tsState)
        {

        }

        /// <summary>
        /// UserProgressDataSignal receiver
        /// </summary>
        /// <param name="updSignal">UserProgressDataSignal</param>
        // ------------------------------------------------------------------------------------------
        protected override void onUserProgressDataSignal(UserProgressDataSignal updSignal)
        {
            //updSignal.addDataAction(this.transform, this, JsonUtility.ToJson(this.m_userProgressData));
        }

        /// <summary>
        /// SceneChangeState receiver
        /// </summary>
        /// <param name="scState">SceneChangeState</param>
        // ------------------------------------------------------------------------------------------
        protected override void onSceneChangeStateReceiver(SSC.SceneChangeState scState)
        {

        }

        /// <summary>
        /// PauseState receiver
        /// </summary>
        /// <param name="pState">PauseState</param>
        // ------------------------------------------------------------------------------------------
        protected override void onPauseStateReceiver(SSC.PauseState pState)
        {

        }

        /// <summary>
        /// Reset
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected override void Reset()
        {

            this.m_receiverMask =
                ReceiverMask.UsePoetSequenceStateReceiver |
                ReceiverMask.UsePoetInitStateReceiver
                ;

        }

    }

}
