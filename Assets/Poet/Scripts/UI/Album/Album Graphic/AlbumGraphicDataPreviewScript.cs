﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Poet
{

    /// <summary>
    /// AlbumGraphicData preview script
    /// </summary>
    public class AlbumGraphicDataPreviewScript : UiDataPreviewScript
    {

        /// <summary>
        /// Reference to Selectable
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to Selectable")]
        protected Selectable m_refSelectable = null;

        /// <summary>
        /// Reference to thumbnail Image
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to thumbnail Image")]
        protected Image m_refThumbnailImage = null;

        /// <summary>
        /// Reference to lock Image
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to lock Image")]
        protected Image m_refLockImage = null;

        /// <summary>
        /// Original Sprite for m_refThumbnailImage
        /// </summary>
        protected Sprite m_oriThumbnail = null;

        /// <summary>
        /// Reference to data
        /// </summary>
        protected AlbumGraphicData m_refAlbumGraphicData = null;

        /// <summary>
        /// Awake
        /// </summary>
        // ---------------------------------------------------------------------------------------
        protected virtual void Awake()
        {

            if (this.m_refThumbnailImage)
            {
                this.m_oriThumbnail = this.m_refThumbnailImage.sprite;
            }

        }

        /// <summary>
        /// Set UI data
        /// </summary>
        /// <param name="data">data</param>
        // ---------------------------------------------------------------------------------------
        public override void setUiData(System.Object data)
        {

            this.m_refAlbumGraphicData = data as AlbumGraphicData;

            if (this.m_refAlbumGraphicData == null)
            {

                if (this.m_refThumbnailImage)
                {
                    this.m_refThumbnailImage.GetComponent<Image>().enabled = false;
                }

                if (this.m_refLockImage)
                {
                    this.m_refLockImage.GetComponent<Image>().enabled = false;
                }

                if (this.m_refSelectable)
                {
                    this.m_refSelectable.interactable = false;
                }

            }

            else
            {

                if (this.m_refThumbnailImage)
                {
                    this.m_refThumbnailImage.GetComponent<Image>().enabled = true;
                }

                if (this.m_refLockImage)
                {
                    this.m_refLockImage.GetComponent<Image>().enabled = true;
                }

                if (this.m_refSelectable)
                {
                    this.m_refSelectable.interactable = true;
                }

                // -----------------

                if (this.m_refAlbumGraphicData.unlock.isLocked())
                {

                    if (this.m_refThumbnailImage)
                    {
                        this.m_refThumbnailImage.sprite = this.m_oriThumbnail;
                    }

                    if (this.m_refLockImage)
                    {
                        this.m_refLockImage.GetComponent<Image>().enabled = true;
                    }

                }

                else
                {

                    if (this.m_refThumbnailImage)
                    {
                        this.m_refThumbnailImage.sprite = this.m_refAlbumGraphicData.thumbnail;
                    }

                    if (this.m_refLockImage)
                    {
                        this.m_refLockImage.GetComponent<Image>().enabled = false;
                    }

                }

            }

        }

        /// <summary>
        /// Function when button is clicked
        /// </summary>
        // ---------------------------------------------------------------------------------------
        public override void onClick()
        {

            if (this.m_refAlbumGraphicData != null && !this.m_refAlbumGraphicData.unlock.isLocked() && AlbumSceneUiManager.isAvailable())
            {

                AlbumSceneUiManager manager = AlbumSceneUiManager.Instance as AlbumSceneUiManager;

                manager.showAlbumGraphicView(this.m_refAlbumGraphicData.holder, this.m_refAlbumGraphicData.csvRowIdentifier);

                if(this.m_refSelectable)
                {
                    manager.setDefaultSelectableForGraphicUi(this.m_refSelectable);
                }

            }

        }

    }

}
