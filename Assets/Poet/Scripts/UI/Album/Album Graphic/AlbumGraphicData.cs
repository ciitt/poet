﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poet
{

    [Serializable]
    public class AlbumGraphicData
    {

        /// <summary>
        /// Reference to AlbumGraphicCsvHolder
        /// </summary>
        [HideInInspector]
        public AlbumGraphicCsvHolder holder = null;

        /// <summary>
        /// Target csv row identifier in AlbumGraphicCsvHolder
        /// </summary>
        [Tooltip("Target csv row identifier in AlbumGraphicCsvHolder")]
        public string csvRowIdentifier = "";

#if UNITY_EDITOR

        /// <summary>
        /// Thumbnail Editor Only
        /// </summary>
        [Tooltip("Thumbnail Editor Only")]
        public Sprite thumbnailEditorOnly = null;

#endif

        /// <summary>
        /// Thumbnail
        /// </summary>
        [Tooltip("Thumbnail")]
        public Sprite thumbnail = null;

        /// <summary>
        /// AssetBundle name
        /// </summary>
        [Tooltip("AssetBundle name")]
        public string assetBundleName = "";

        /// <summary>
        /// AssetBundle variant
        /// </summary>
        [Tooltip("AssetBundle variant")]
        public string assetBundleVariant = "";

        /// <summary>
        /// Asset name in AssetBundle. Ignore if GetAllAssetNames().Count == 1
        /// </summary>
        [Tooltip("Asset name in AssetBundle. Ignore if GetAllAssetNames().Count == 1")]
        public string assetNameInAssetBundle = "";

        /// <summary>
        /// Conditions to unlock
        /// </summary>
        [Tooltip("Conditions to unlock")]
        public UnlockDataConditions unlock = new UnlockDataConditions();

    }

}
