﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poet
{

    /// <summary>
    /// Common UiDataHolder
    /// </summary>
    public abstract class AlbumCommonDataHolder : UiDataHolder
    {

        protected class AssetBundleInfo
        {

            public int index = 0;
            public string assetName = "";

            public AssetBundleInfo(int _index, string _assetName)
            {
                this.index = _index;
                this.assetName = _assetName;
            }

        }

        /// <summary>
        /// Set Sprite
        /// </summary>
        /// <param name="index">index</param>
        /// <param name="sprite">Sprite</param>
        // -----------------------------------------------------------------------------------
        protected abstract void setSprite(int index, Sprite sprite);

        /// <summary>
        /// Success callback for AssetBundle
        /// </summary>
        /// <param name="ab">AssetBundle</param>
        /// <param name="info">int list number</param>
        /// <param name="finishCallback">callback to finish</param>
        // -----------------------------------------------------------------------------------
        protected void onSuccessAssetBundleLoading(AssetBundle ab, System.Object info, Action finishCallback)
        {
            StartCoroutine(this.onSuccessAssetBundleLoadingIE(ab, info, finishCallback));
        }

        /// <summary>
        /// Success callback for AssetBundle
        /// </summary>
        /// <param name="ab">AssetBundle</param>
        /// <param name="info">int list number</param>
        /// <param name="finishCallback">callback to finish</param>
        /// <returns>IEnumerator</returns>
        // -----------------------------------------------------------------------------------
        protected IEnumerator onSuccessAssetBundleLoadingIE(AssetBundle ab, System.Object info, Action finishCallback)
        {

            yield return null;

            if (ab && info is AssetBundleInfo)
            {

                AssetBundleInfo abInfo = info as AssetBundleInfo;

                string[] temps = ab.GetAllAssetNames();

                AssetBundleRequest request = null;

                if (temps.Length == 1)
                {
                    request = ab.LoadAssetAsync(temps[0]);
                }

                else if (temps.Length >= 2)
                {
                    request = ab.LoadAssetAsync(abInfo.assetName);
                }

                if (request != null)
                {

                    while (!request.isDone)
                    {
                        yield return null;
                    }

                    if (this.dataList().Count > abInfo.index && abInfo.index >= 0)
                    {

                        if (request.asset is Sprite)
                        {
                            this.setSprite(abInfo.index, request.asset as Sprite);
                        }

                        else if(request.asset is Texture2D)
                        {
                            Texture2D tex = request.asset as Texture2D;
                            this.setSprite(abInfo.index,
                                Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(tex.width / 2, tex.height / 2))
                                );
                        }

#if UNITY_EDITOR
                        else
                        {
                            Debug.LogError("(#if UNITY_EDITOR) Asset is not Sprite or Texture2D : " + abInfo.assetName + " : " + request.asset.GetType().Name);
                        }
#endif

                    }

                }

            }

            if (finishCallback != null)
            {
                finishCallback();
            }

        }

    }

}
