﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poet
{

    /// <summary>
    /// Album memory data
    /// </summary>
    [Serializable]
    public class AlbumMemoryData
    {

        /// <summary>
        /// Scene name
        /// </summary>
        public string sceneName = "";

        /// <summary>
        /// Csv name
        /// </summary>
        public string csvName = "";

        /// <summary>
        /// Start csv row identifier
        /// </summary>
        public string startCsvRowIdentifier = "";

        /// <summary>
        /// End csv row identifier
        /// </summary>
        public string endCsvRowIdentifier = "";

#if UNITY_EDITOR

        /// <summary>
        /// Thumbnail Editor Only
        /// </summary>
        [Tooltip("Thumbnail Editor Only")]
        public Sprite thumbnailEditorOnly = null;

#endif

        /// <summary>
        /// Thumbnail
        /// </summary>
        public Sprite thumbnail = null;

        /// <summary>
        /// AssetBundle name
        /// </summary>
        [Tooltip("AssetBundle name")]
        public string assetBundleName = "";

        /// <summary>
        /// AssetBundle variant
        /// </summary>
        [Tooltip("AssetBundle variant")]
        public string assetBundleVariant = "";

        /// <summary>
        /// Asset name in AssetBundle. Ignore if GetAllAssetNames().Count == 1
        /// </summary>
        [Tooltip("Asset name in AssetBundle. Ignore if GetAllAssetNames().Count == 1")]
        public string assetNameInAssetBundle = "";

        /// <summary>
        /// Conditions to unlock
        /// </summary>
        [Tooltip("Conditions to unlock")]
        public UnlockDataConditions unlock = new UnlockDataConditions();

    }

}
