﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poet
{

    /// <summary>
    /// PagerButtonGroupScript for album memory
    /// </summary>
    public class AlbumMemoryPagerButtonGroupScript : PagerButtonGroupScript
    {

        /// <summary>
        /// Current selected pager index
        /// </summary>
        public static int currentSelectedPagetIndex = 0;

        /// <summary>
        /// Function for pager buttons
        /// </summary>
        /// <param name="pageIndexFrom0"></param>
        // ---------------------------------------------------------------------------------------
        public override void sendPageIndexSignal(int pageIndexFrom0)
        {
            currentSelectedPagetIndex = pageIndexFrom0;
            base.sendPageIndexSignal(pageIndexFrom0);
        }

    }

}
