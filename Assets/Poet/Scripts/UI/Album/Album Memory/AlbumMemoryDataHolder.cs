﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Poet
{

    /// <summary>
    /// Album memory data holder
    /// </summary>
    [RequireComponent(typeof(Toggle))]
    public class AlbumMemoryDataHolder : AlbumCommonDataHolder
    {

        /// <summary>
        /// LoadingMode for thumbnail
        /// </summary>
        [SerializeField]
        [Tooltip("LoadingMode for thumbnail")]
        protected LoadingMode m_loadingModeForThumbnail = LoadingMode.Normal;

        /// <summary>
        /// AlbumMemoryData list
        /// </summary>
        [SerializeField]
        [Tooltip("AlbumMemoryData list")]
        protected List<AlbumMemoryData> m_dataList = new List<AlbumMemoryData>();

        /// <summary>
        /// Reference to Toggle
        /// </summary>
        protected Toggle m_refToggle = null;

        /// <summary>
        /// Current selected AlbumMemoryDataHolder hierarchy path
        /// </summary>
        protected static string currentSelectedHierarchyPath = "";

        // -----------------------------------------------------------------------------------

        /// <summary>
        /// LoadingMode
        /// </summary>
        public LoadingMode loadingMode { get { return this.m_loadingModeForThumbnail; } }

        // -----------------------------------------------------------------------------------

        /// <summary>
        /// Awake
        /// </summary>
        // -----------------------------------------------------------------------------------
        protected override void Awake()
        {

            base.Awake();

            //
            {
                this.m_refToggle = this.GetComponent<Toggle>();
            }

        }

        /// <summary>
        /// Start
        /// </summary>
        // -----------------------------------------------------------------------------------
        protected virtual void Start()
        {

            if (this.m_loadingModeForThumbnail == LoadingMode.AssetBundle)
            {

                int size = this.m_dataList.Count;

                for (int i = 0; i < size; i++)
                {

#if UNITY_EDITOR

                    if (string.IsNullOrEmpty(this.m_dataList[i].assetBundleName))
                    {
                        Debug.LogWarning("(#if UNITY_EDITOR) assetBundleName is empty : " + this.gameObject.name);
                    }

#endif

                    SSC.AssetBundleStartupManager.Instance.addSceneStartupAssetBundle(
                        this.m_dataList[i].assetBundleName,
                        this.m_dataList[i].assetBundleVariant,
                        this.onSuccessAssetBundleLoading,
                        null,
                        null,
                        new AssetBundleInfo(i, this.m_dataList[i].assetNameInAssetBundle)
                        );

                }

            }

            // addSceneChangeStateReceiver
            {
                PoetReduxManager.Instance.addSceneChangeStateReceiver(this.onSceneChangeStateReceiver);
            }

        }

        /// <summary>
        /// OnDestroy
        /// </summary>
        // -----------------------------------------------------------------------------------
        protected virtual void OnDestroy()
        {

            if(this.m_loadingModeForThumbnail == LoadingMode.AssetBundle)
            {

                foreach (var val in this.m_dataList)
                {

                    if(val.thumbnail)
                    {
                        Destroy(val.thumbnail);
                    }
                    
                }

            }

        }

        /// <summary>
        /// SceneChangeState receiver
        /// </summary>
        /// <param name="scState">SceneChangeState</param>
        // -----------------------------------------------------------------------------------------------
        protected virtual void onSceneChangeStateReceiver(SSC.SceneChangeState scState)
        {

            if (scState.stateEnum == SSC.SceneChangeState.StateEnum.AllStartupsDone)
            {

                if (!string.IsNullOrEmpty(currentSelectedHierarchyPath))
                {

                    if (currentSelectedHierarchyPath == Funcs.createHierarchyPath(this.transform))
                    {
                        this.onClick(AlbumMemoryPagerButtonGroupScript.currentSelectedPagetIndex);
                        this.setToggleOn(true);
                    }

                }

                else
                {
                    if (this.m_refToggle.isOn)
                    {
                        this.onClick(0);
                    }
                }

            }

        }

        /// <summary>
        /// Function when button is clicked
        /// </summary>
        /// <param name="pageIndexFrom0">page index</param>
        // -----------------------------------------------------------------------
        public override void onClick(int pageIndexFrom0)
        {
            currentSelectedHierarchyPath = Funcs.createHierarchyPath(this.transform);
            base.onClick(pageIndexFrom0);
        }

        /// <summary>
        /// Set toggle on
        /// </summary>
        /// <param name="isOn">isOn</param>
        // -----------------------------------------------------------------------------------------------
        public void setToggleOn(bool isOn)
        {

            if(this.m_refToggle.isOn != isOn)
            {
                this.m_refToggle.isOn = isOn;
            }

        }

        /// <summary>
        /// AlbumGraphicData data list
        /// </summary>
        /// <returns>AlbumGraphicData list</returns>
        // ----------------------------------------------------------------------------
        protected override IList dataList()
        {
            return this.m_dataList;
        }

        /// <summary>
        /// Set Sprite
        /// </summary>
        /// <param name="index">index</param>
        /// <param name="asset">Sprite</param>
        // -----------------------------------------------------------------------------------
        protected override void setSprite(int index, Sprite sprite)
        {

            if (this.m_dataList.Count > index && index >= 0)
            {
                this.m_dataList[index].thumbnail = sprite;
            }

        }

    }

}
