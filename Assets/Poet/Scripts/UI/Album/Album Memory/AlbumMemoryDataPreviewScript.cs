﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Poet
{

    /// <summary>
    /// AlbumMemoryData preview script
    /// </summary>
    public class AlbumMemoryDataPreviewScript : UiDataPreviewScript
    {

        /// <summary>
        /// Reference to Selectable
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to Selectable")]
        protected Selectable m_refSelectable = null;

        /// <summary>
        /// Reference to thumbnail Image
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to thumbnail Image")]
        protected Image m_refThumbnailImage = null;

        /// <summary>
        /// Reference to lock Image
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to lock Image")]
        protected Image m_refLockImage = null;

        /// <summary>
        /// Reference to Button
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to Button")]
        protected Button m_refButton = null;

        /// <summary>
        /// Original Sprite for m_refThumbnailImage
        /// </summary>
        protected Sprite m_oriThumbnail = null;

        /// <summary>
        /// Reference to data
        /// </summary>
        protected AlbumMemoryData m_refAlbumMemoryData = null;

        /// <summary>
        /// Current selected AlbumMemoryDataPreviewScript hierarchy path
        /// </summary>
        protected static string currentSelectedHierarchyPath = "";

        /// <summary>
        /// Awake
        /// </summary>
        // ---------------------------------------------------------------------------------------
        protected virtual void Awake()
        {

            if (this.m_refThumbnailImage)
            {
                this.m_oriThumbnail = this.m_refThumbnailImage.sprite;
            }

        }

        /// <summary>
        /// Start
        /// </summary>
        // -----------------------------------------------------------------------------------
        protected virtual void Start()
        {

            if (!string.IsNullOrEmpty(currentSelectedHierarchyPath))
            {

                if (currentSelectedHierarchyPath == Funcs.createHierarchyPath(this.transform))
                {

                    if (AlbumSceneUiManager.isAvailable())
                    {
                        (AlbumSceneUiManager.Instance as AlbumSceneUiManager).setDefaultSelectableForMemoryUi(this.m_refButton);
                    }

                }

            }

        }

        /// <summary>
        /// Set UI data
        /// </summary>
        /// <param name="data">data</param>
        // ---------------------------------------------------------------------------------------
        public override void setUiData(System.Object data)
        {

            this.m_refAlbumMemoryData = data as AlbumMemoryData;

            if (this.m_refAlbumMemoryData == null)
            {

                if (this.m_refThumbnailImage)
                {
                    this.m_refThumbnailImage.GetComponent<Image>().enabled = false;
                }

                if (this.m_refLockImage)
                {
                    this.m_refLockImage.GetComponent<Image>().enabled = false;
                }

                if (this.m_refSelectable)
                {
                    this.m_refSelectable.interactable = false;
                }

            }

            else
            {

                if (this.m_refThumbnailImage)
                {
                    this.m_refThumbnailImage.GetComponent<Image>().enabled = true;
                }

                if (this.m_refLockImage)
                {
                    this.m_refLockImage.GetComponent<Image>().enabled = true;
                }

                if (this.m_refSelectable)
                {
                    this.m_refSelectable.interactable = true;
                }

                // -----------------

                if (this.m_refAlbumMemoryData.unlock.isLocked())
                {

                    if (this.m_refThumbnailImage)
                    {
                        this.m_refThumbnailImage.sprite = this.m_oriThumbnail;
                    }

                    if (this.m_refLockImage)
                    {
                        this.m_refLockImage.GetComponent<Image>().enabled = true;
                    }

                }

                else
                {

                    if (this.m_refThumbnailImage)
                    {
                        this.m_refThumbnailImage.sprite = this.m_refAlbumMemoryData.thumbnail;
                    }

                    if (this.m_refLockImage)
                    {
                        this.m_refLockImage.GetComponent<Image>().enabled = false;
                    }

                }

            }

        }

        /// <summary>
        /// Function when button is clicked
        /// </summary>
        // ---------------------------------------------------------------------------------------
        public override void onClick()
        {

            if (this.m_refAlbumMemoryData != null && !this.m_refAlbumMemoryData.unlock.isLocked())
            {

                currentSelectedHierarchyPath = Funcs.createHierarchyPath(this.transform);
                
                PoetSceneChangeManager pscm = PoetSceneChangeManager.Instance as PoetSceneChangeManager;

                pscm.loadSceneWithAlbumMemoryData(this.m_refAlbumMemoryData);

            }

        }

    }

}
