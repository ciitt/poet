﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poet
{

    /// <summary>
    /// Album music data holder
    /// </summary>
    public class AlbumMusicDataHolder : UiDataHolder
    {

        protected class AssetBundleInfo
        {

            public int index = 0;
            public string assetName = "";

            public AssetBundleInfo(int _index, string _assetName)
            {
                this.index = _index;
                this.assetName = _assetName;
            }

        }

        /// <summary>
        /// AlbumMusicData list
        /// </summary>
        [SerializeField]
        [Tooltip("AlbumMusicData list")]
        protected List<AlbumMusicData> m_dataList = new List<AlbumMusicData>();

        /// <summary>
        /// AlbumMusicData data list
        /// </summary>
        /// <returns>AlbumMusicData list</returns>
        // ----------------------------------------------------------------------------
        protected override IList dataList()
        {
            return this.m_dataList;
        }

        /// <summary>
        /// Start
        /// </summary>
        // -----------------------------------------------------------------------------------
        protected virtual void Start()
        {

            // init
            {

                int size = this.m_dataList.Count;

                for (int i = 0; i < size; i++)
                {

                    if (this.m_dataList[i].loadingMode == LoadingMode.AssetBundle)
                    {

                        SSC.AssetBundleStartupManager.Instance.addSceneStartupAssetBundle(
                            this.m_dataList[i].assetBundleName,
                            this.m_dataList[i].assetBundleVariant,
                            this.onSuccessAssetBundleLoading,
                            null,
                            null,
                            new AssetBundleInfo(i, this.m_dataList[i].assetNameInAssetBundle)
                            );

                    }

                }

            }

        }

        /// <summary>
        /// Success callback for AssetBundle
        /// </summary>
        /// <param name="ab">AssetBundle</param>
        /// <param name="info">int list number</param>
        /// <param name="finishCallback">callback to finish</param>
        // -----------------------------------------------------------------------------------
        protected void onSuccessAssetBundleLoading(AssetBundle ab, System.Object info, Action finishCallback)
        {
            StartCoroutine(this.onSuccessAssetBundleLoadingIE(ab, info, finishCallback));
        }

        /// <summary>
        /// Success callback for AssetBundle
        /// </summary>
        /// <param name="ab">AssetBundle</param>
        /// <param name="info">int list number</param>
        /// <param name="finishCallback">callback to finish</param>
        /// <returns>IEnumerator</returns>
        // -----------------------------------------------------------------------------------
        protected IEnumerator onSuccessAssetBundleLoadingIE(AssetBundle ab, System.Object info, Action finishCallback)
        {

            yield return null;

            AssetBundleInfo abInfo = info as AssetBundleInfo;

            if (abInfo == null || !ab)
            {
                Debug.LogError("Invald usage : " + this.gameObject.name);
            }

            else if (finishCallback == null)
            {
                Debug.LogError("finishCallback == null : " + this.gameObject.name);
                yield break;
            }

            else
            {

                string assetName = "";

                if (string.IsNullOrEmpty(abInfo.assetName))
                {

                    string[] allNames = ab.GetAllAssetNames();

                    if (allNames.Length > 0)
                    {
                        assetName = allNames[0];
                    }

                }

                else
                {
                    assetName = abInfo.assetName;
                }

                if (!string.IsNullOrEmpty(assetName))
                {

                    AssetBundleRequest request = ab.LoadAssetAsync(assetName);

                    while (!request.isDone)
                    {
                        yield return null;
                    }

                    if (this.m_dataList.Count > abInfo.index && abInfo.index >= 0)
                    {

                        AudioClip audioClip = request.asset as AudioClip;

                        if (audioClip)
                        {
                            this.m_dataList[abInfo.index].audioClip = audioClip;
                        }

                        else
                        {
                            Debug.LogWarning("Target asset is not AudioClip : " + assetName);
                        }

                    }

                }

            }

            finishCallback();

        }

    }

}
