﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poet
{

    [Serializable]
    public class AlbumMusicData
    {

        /// <summary>
        /// LoadingMode
        /// </summary>
        [SerializeField]
        [Tooltip("LoadingMode")]
        public LoadingMode loadingMode = LoadingMode.Normal;

        /// <summary>
        /// Audio title
        /// </summary>
        [Tooltip("Audio title")]
        public string audioTitle = "Audio 01";

#if UNITY_EDITOR

        /// <summary>
        /// AudioClip Editor Only
        /// </summary>
        [Tooltip("AudioClip Editor Only")]
        public AudioClip audioClipEditorOnly = null;

#endif

        /// <summary>
        /// AudioClip
        /// </summary>
        public AudioClip audioClip = null;

        /// <summary>
        /// AssetBundle name
        /// </summary>
        [Tooltip("AssetBundle name")]
        public string assetBundleName = "";

        /// <summary>
        /// AssetBundle variant
        /// </summary>
        [Tooltip("AssetBundle variant")]
        public string assetBundleVariant = "";

        /// <summary>
        /// Asset name in AssetBundle. Ignore if GetAllAssetNames().Count == 1
        /// </summary>
        [Tooltip("Asset name in AssetBundle. Ignore if GetAllAssetNames().Count == 1")]
        public string assetNameInAssetBundle = "";

    }

}
