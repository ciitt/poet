﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Poet
{

    /// <summary>
    /// AlbumMusicData preview script
    /// </summary>
    public class AlbumMusicDataPreviewScript : UiDataPreviewScript
    {

        /// <summary>
        /// Reference to Selectable
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to Selectable")]
        protected Selectable m_refSelectable = null;

        /// <summary>
        /// Reference to CanvasGroup
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to Text")]
        protected Text m_refText = null;

        /// <summary>
        /// Reference to Toggle
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to Toggle")]
        protected Toggle m_refToggle = null;

        /// <summary>
        /// Reference to data
        /// </summary>
        protected AlbumMusicData m_refAlbumMusicData = null;

        // ---------------------------------------------------------------------------------------

        /// <summary>
        /// Reference to AlbumMusicData
        /// </summary>
        public AlbumMusicData refData { get { return this.m_refAlbumMusicData; } }

        // ---------------------------------------------------------------------------------------

        /// <summary>
        /// Set toggle state
        /// </summary>
        // -----------------------------------------------------------------------------------------------
        public void setToggleState(bool isOn)
        {
            if (this.m_refToggle)
            {
                this.m_refToggle.isOn = isOn;
            }
        }

        /// <summary>
        /// Function when button is clicked
        /// </summary>
        // ---------------------------------------------------------------------------------------
        public override void onClick()
        {

            //
            if(this.m_refPreviewGroupScript is AlbumMusicDataPreviewGroupScript)
            {

                (this.m_refPreviewGroupScript as AlbumMusicDataPreviewGroupScript).setCurrentPlayingAlbumMusicData(this.m_refAlbumMusicData);

                if (this.m_refAlbumMusicData != null)
                {

                    if (this.m_refToggle)
                    {
                        
                        if(this.m_refToggle.isOn)
                        {
                            (this.m_refPreviewGroupScript as AlbumMusicDataPreviewGroupScript).playCurrentBgm();
                        }

                        else
                        {
                            (this.m_refPreviewGroupScript as AlbumMusicDataPreviewGroupScript).stopCurrentBgm();
                        }

                    }

                    else
                    {
                        (this.m_refPreviewGroupScript as AlbumMusicDataPreviewGroupScript).playCurrentBgm();
                    }

                }

            }

        }

        /// <summary>
        /// Function when button is clicked
        /// </summary>
        // ---------------------------------------------------------------------------------------
        public override void setUiData(object data)
        {

            this.m_refAlbumMusicData = data as AlbumMusicData;

            if (this.m_refAlbumMusicData == null)
            {

                if (this.m_refText)
                {
                    this.m_refText.text = "";
                }

                if (this.m_refSelectable)
                {
                    this.m_refSelectable.interactable = false;
                }

            }

            else
            {

                if (this.m_refText)
                {
                    this.m_refText.text = this.m_refAlbumMusicData.audioTitle;
                }

                if (this.m_refSelectable)
                {
                    this.m_refSelectable.interactable = true;
                }

            }

        }

    }

}
