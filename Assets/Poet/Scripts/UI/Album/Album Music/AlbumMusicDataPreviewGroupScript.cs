﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poet
{

    public class AlbumMusicDataPreviewGroupScript : UiDataPreviewGroupScript
    {

        /// <summary>
        /// Current AlbumMusicData
        /// </summary>
        protected AlbumMusicData m_refCurrentAlbumMusicData = null;

        /// <summary>
        /// Stop current BGM
        /// </summary>
        // --------------------------------------------------------------------------
        public void stopCurrentBgm()
        {

            if (Poet.PoetAudioManager.Instance.refBGM)
            {
                Poet.PoetAudioManager.Instance.refBGM.playAudio(null, 0.0f);
            }

        }

        /// <summary>
        /// Resume and play current BGM
        /// </summary>
        // --------------------------------------------------------------------------
        public virtual void playCurrentBgm()
        {

            if(this.m_refCurrentAlbumMusicData != null)
            {

                if (Poet.PoetAudioManager.Instance.refBGM)
                {
                    Poet.PoetAudioManager.Instance.refBGM.playAudio(this.m_refCurrentAlbumMusicData.audioClip, 0.2f, 0.5f, 0.0f);
                }

            }

        }

        /// <summary>
        /// Set page
        /// </summary>
        // --------------------------------------------------------------------------------------------------
        public override void setPage(int pagerIndexFrom0)
        {

            base.setPage(pagerIndexFrom0);

            foreach (AlbumMusicDataPreviewScript preview in this.m_refPreviewList)
            {

                if (preview)
                {

                    preview.setToggleState(
                        preview &&
                        preview.refData != null &&
                        preview.refData == this.m_refCurrentAlbumMusicData
                        );

                }

            }

        }

        /// <summary>
        /// Set current playing data number
        /// </summary>
        /// <param name="dataNumber"></param>
        // --------------------------------------------------------------------------------------------------
        public void setCurrentPlayingAlbumMusicData(AlbumMusicData data)
        {
            this.m_refCurrentAlbumMusicData = data;
        }

    }

}
