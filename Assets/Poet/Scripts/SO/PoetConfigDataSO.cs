﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poet
{

    /// <summary>
    /// Poet config data SO
    /// </summary>
    [Serializable]
    public class PoetConfigDataSO : ScriptableObject
    {

        /// <summary>
        /// Window mode enum
        /// </summary>
        public enum WindowMode
        {
            Window,
            FullScreen,
        }

        /// <summary>
        /// Skip mode enum
        /// </summary>
        public enum SkipMode
        {
            All,
            AlreadyRead,
        }

        /// <summary>
        /// Voice stop mode enum
        /// </summary>
        public enum VoiceStopMode
        {
            NextText,
            NextVoice,
        }

        /// <summary>
        /// Target SystemLanguage
        /// </summary>
        [Tooltip("Target SystemLanguage")]
        public SystemLanguage systemLanguage = SystemLanguage.English;

        /// <summary>
        /// Window mode
        /// </summary>
        [Tooltip("Window mode")]
        public WindowMode windowMode = WindowMode.Window;

        /// <summary>
        /// Skip mode
        /// </summary>
        [Tooltip("Skip mode")]
        public SkipMode skipMode = SkipMode.AlreadyRead;

        /// <summary>
        /// Voice stop mode
        /// </summary>
        [Tooltip("Voice stop mode")]
        public VoiceStopMode voiceStopMode = VoiceStopMode.NextVoice;

        /// <summary>
        /// Text window opacity
        /// </summary>
        [Range(0.0f, 1.0f)]
        [Tooltip("Text window opacity")]
        public float textWindowOpacity01 = 0.5f;

        /// <summary>
        /// Showing text speed
        /// </summary>
        [Range(0.0f, 1.0f)]
        [Tooltip("Showing text speed")]
        public float textSpeed01 = 0.5f;

        /// <summary>
        /// Auto mode speed
        /// </summary>
        [Range(0.0f, 1.0f)]
        [Tooltip("Auto mode speed")]
        public float autoModeSpeed01 = 0.5f;

        /// <summary>
        /// Auto mode wait seconds 0
        /// </summary>
        [Tooltip("Auto mode wait seconds 0")]
        public float autoModeWaitSeconds0 = 3.0f;

        /// <summary>
        /// Auto mode wait seconds 0
        /// </summary>
        [Tooltip("Auto mode wait seconds 1")]
        public float autoModeWaitSeconds1 = 0.2f;

        /// <summary>
        /// Change already read text color
        /// </summary>
        [Tooltip("Change already read text color")]
        public bool changeAlreadyReadTextColor = true;

        /// <summary>
        /// Already read text color
        /// </summary>
        [Tooltip("Already read text color")]
        public Color alreadyReadTextColor = Color.white;

        /// <summary>
        /// Show quick save dialog
        /// </summary>
        [Tooltip("Show quick save dialog")]
        public bool showQuickSaveDialog = true;

        /// <summary>
        /// Show quick load dialog
        /// </summary>
        [Tooltip("Show quick load dialog")]
        public bool showQuickLoadDialog = true;

        /// <summary>
        /// Master volume
        /// </summary>
        [Range(0.0f, 1.0f)]
        [Tooltip("Master volume")]
        public float masterVolume01 = 1.0f;

        /// <summary>
        /// Volume for bgm
        /// </summary>
        [Range(0.0f, 1.0f)]
        [Tooltip("Volume for bgm")]
        public float bgmVolume01 = 1.0f;

        /// <summary>
        /// Volume for sound effect
        /// </summary>
        [Range(0.0f, 1.0f)]
        [Tooltip("Volume for sound effect")]
        public float seVolume01 = 1.0f;

        /// <summary>
        /// Volume for voice
        /// </summary>
        [Range(0.0f, 1.0f)]
        [Tooltip("Volume for voice")]
        public float voiceVolume01 = 1.0f;

        /// <summary>
        /// Volume for environment
        /// </summary>
        [Range(0.0f, 1.0f)]
        [Tooltip("Volume for environment")]
        public float environmentVolume01 = 1.0f;

        /// <summary>
        /// Volume for extra
        /// </summary>
        [Range(0.0f, 1.0f)]
        [Tooltip("Volume for extra")]
        public float extraVolume01 = 1.0f;

        /// <summary>
        /// Already read index ranges list
        /// </summary>
        [HideInInspector]
        public List<KeyAndAlreadyReadList> keyAndAlreadyReadList = new List<KeyAndAlreadyReadList>();

        /// <summary>
        /// Character voice id and volume list
        /// </summary>
        public List<CharacterVoiceIdAndVolume> characterVoiceIdAndVolumeList = new List<CharacterVoiceIdAndVolume>();

        // -------------------------------------------------------------------------------------------

        /// <summary>
        /// Already read index ranges dictionary
        /// </summary>
        public Dictionary<string, KeyAndAlreadyReadList> keyAndAlreadyReadDict = new Dictionary<string, KeyAndAlreadyReadList>();

        /// <summary>
        /// Character voice id and volume dictionary
        /// </summary>
        public Dictionary<string, CharacterVoiceIdAndVolume> characterVoiceIdAndVolumeDict = new Dictionary<string, CharacterVoiceIdAndVolume>();

        // -------------------------------------------------------------------------------------------

        /// <summary>
        /// Init dictionary
        /// </summary>
        // -------------------------------------------------------------------------------------------
        public virtual void initDictionary()
        {

            this.keyAndAlreadyReadDict = Funcs.listToDictionary<KeyAndAlreadyReadList>(this.keyAndAlreadyReadList);
            this.characterVoiceIdAndVolumeDict = Funcs.listToDictionary<CharacterVoiceIdAndVolume>(this.characterVoiceIdAndVolumeList);

#if !UNITY_EDITOR
            this.keyAndAlreadyReadList.Clear();
            this.characterVoiceIdAndVolumeList.Clear();
#endif

        }

        /// <summary>
        /// Prepare to save
        /// </summary>
        // -------------------------------------------------------------------------------------------
        public virtual void prepareToSave()
        {

            this.keyAndAlreadyReadList.Clear();
            this.keyAndAlreadyReadList.AddRange(this.keyAndAlreadyReadDict.Values);

            this.characterVoiceIdAndVolumeList.Clear();
            this.characterVoiceIdAndVolumeList.AddRange(this.characterVoiceIdAndVolumeDict.Values);

        }

        /// <summary>
        /// Is already read
        /// </summary>
        /// <returns>already read</returns>
        // -------------------------------------------------------------------------------------------
        public virtual bool isAlreadyRead(string textAssetName, CsvReadRange range)
        {

            if(string.IsNullOrEmpty(textAssetName))
            {
                return true;
            }

            if (this.keyAndAlreadyReadDict.ContainsKey(textAssetName))
            {
                return this.keyAndAlreadyReadDict[textAssetName].isAlreadyRead(range);
            }

            return false;

        }

        /// <summary>
        /// Update already read range list
        /// </summary>
        /// <param name="textAssetName">csv name</param>
        /// <param name="range">range</param>
        // -------------------------------------------------------------------------------------------
        public void updateAlreadyReadRange(string textAssetName, CsvReadRange range)
        {

            if (string.IsNullOrEmpty(textAssetName) || range == null)
            {
                return;
            }

            // --------------

            if(this.keyAndAlreadyReadDict.ContainsKey(textAssetName))
            {
                this.keyAndAlreadyReadDict[textAssetName].updateAlreadyReadRange(range);
            }

            else
            {
                this.keyAndAlreadyReadDict.Add(textAssetName, new KeyAndAlreadyReadList(textAssetName, range));
            }

        }

    }

}
