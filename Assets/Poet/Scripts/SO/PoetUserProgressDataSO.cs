﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Poet
{

    [Serializable]
    public class KeyAndData : IdentifierInterface
    {

        public string key = "";
        public string stringOrJson = "";

        public string getIdentifier()
        {
            return this.key;
        }

        public KeyAndData(string _key, string _stringOrJson)
        {
            this.key = _key;
            this.stringOrJson = _stringOrJson;
        }

    }

    [Serializable]
    public class PoetUserProgressDataSO : ScriptableObject
    {

        [HideInInspector]
        public string sceneName = "";

        [HideInInspector]
        public string dateTimeStr = "";

        [HideInInspector]
        public string descriptionText = "";

        [HideInInspector]
        public byte[] screenshotPngBytes = null;

        public List<KeyAndData> dataList = new List<KeyAndData>();

        public Dictionary<string, KeyAndData> dataDict = new Dictionary<string, KeyAndData>();

        public virtual bool isDefaultData()
        {
            return string.IsNullOrEmpty(dateTimeStr);
        }

        public virtual DateTime dateTime()
        {

            if(string.IsNullOrEmpty(dateTimeStr))
            {
                return DateTime.MinValue;
            }

            return DateTime.ParseExact(this.dateTimeStr, SystemManager.DateTimeFormat, System.Globalization.CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Init dictionary
        /// </summary>
        public virtual void initDictionary()
        {

            this.dataDict.Clear();

            var temp = Funcs.listToDictionary<KeyAndData>(this.dataList);

            foreach(var kv in temp)
            {
                this.dataDict.Add(kv.Key, kv.Value);
            }
            
        }

    }

}
