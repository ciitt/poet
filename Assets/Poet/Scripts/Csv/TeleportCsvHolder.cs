﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poet
{

    /// <summary>
    /// Poet csv holder for teleport
    /// </summary>
    public class TeleportCsvHolder : PoetCsvHolder
    {

#if UNITY_EDITOR

        /// <summary>
        /// Use m_receiverMask == 0 warning
        /// </summary>
        /// <returns>use</returns>
        // ------------------------------------------------------------------------------------------
        protected override bool useReceiverMask0Warning()
        {
            return false;
        }

#endif

        /// <summary>
        /// Awake
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected override void Awake()
        {
            this.initOnAwake();
        }

        /// <summary>
        /// SceneChangeState receiver
        /// </summary>
        /// <param name="scState">SceneChangeState</param>
        // -----------------------------------------------------------------------------------------------
        protected override void onSceneChangeStateReceiver(SSC.SceneChangeState scState)
        {
            // nothing
        }


        /// <summary>
        /// UserProgressDataSignal receiver
        /// </summary>
        /// <param name="tsState">UserProgressDataSignal</param>
        // ------------------------------------------------------------------------------------------
        protected override void onUserProgressDataSignal(UserProgressDataSignal updSignal)
        {
            // nothing
        }

        /// <summary>
        /// Reset
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected override void Reset()
        {
            this.m_receiverMask = 0;
        }

    }

}
