﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poet
{

    /// <summary>
    /// Poet csv holder for album graphic
    /// </summary>
    public class AlbumGraphicCsvHolder : PoetCsvHolder
    {

        /// <summary>
        /// Current identifier
        /// </summary>
        protected string m_currentIdentifier = "";

        /// <summary>
        /// Current index
        /// </summary>
        protected int m_currentIndex = 0;

#if UNITY_EDITOR

        /// <summary>
        /// Use m_receiverMask == 0 warning
        /// </summary>
        /// <returns>use</returns>
        // ------------------------------------------------------------------------------------------
        protected override bool useReceiverMask0Warning()
        {
            return false;
        }

#endif

        /// <summary>
        /// Start showing
        /// </summary>
        /// <param name="csvRowIdentifier">csv row identifier</param>
        // -----------------------------------------------------------------------------------------------
        public void startShowing(string csvRowIdentifier)
        {

            if(!this.m_id_index.ContainsKey(csvRowIdentifier))
            {
                Debug.LogWarning("Key not found : " + csvRowIdentifier);
                return;
            }

            // --------------

            this.m_currentIdentifier = csvRowIdentifier;
            this.m_currentIndex = this.m_id_index[csvRowIdentifier];

            StartCoroutine(this.sendSequenceStateIE(this.m_currentIndex));

        }

        /// <summary>
        /// Show next
        /// </summary>
        // -------------------------------------------------------------------------------------------------
        public void showNext()
        {

            this.m_currentIndex++;

            if (this.m_currentIndex >= this.m_lines.Count)
            {
                this.finishAlbumGraphicView();
            }

            else if(this.getCsvRowIdentifierBelongTo(this.m_currentIndex) != this.m_currentIdentifier)
            {
                this.finishAlbumGraphicView();
            }

            else
            {
                StartCoroutine(this.sendSequenceStateIE(this.m_currentIndex));
            }

        }

        /// <summary>
        /// Finish AlbumGraphicView UI
        /// </summary>
        // -------------------------------------------------------------------------------------------------
        public void finishAlbumGraphicView()
        {

            if (AlbumSceneUiManager.isAvailable())
            {
                (AlbumSceneUiManager.Instance as AlbumSceneUiManager).
                    showUiOnButtonClickForAlbum(PoetUiIdentifiers.AlbumScene.AlbumGraphicSelectForBackFromView, false);
            }

        }


        /// <summary>
        /// Send sequence state IEnumerator
        /// </summary>
        /// <returns>IEnumerator</returns>
        // -----------------------------------------------------------------------------------------------
        protected virtual IEnumerator sendSequenceStateIE(int index)
        {

            PoetReduxManager prm = PoetReduxManager.Instance as PoetReduxManager;
            var psState = prm.PoetSequenceStateWatcher.state();

            var headersAndCells = this.getHeadersAndCells(index);

            // PreProcess
            {

                psState.setState(
                    prm.PoetSequenceStateWatcher,
                    PoetSequenceState.StateEnum.PreProcess,
                    this.csvName(),
                    index,
                    headersAndCells
                    );

            }

            yield return null;

            // MainProcess
            {

                psState.setState(
                    prm.PoetSequenceStateWatcher,
                    PoetSequenceState.StateEnum.MainProcess,
                    this.csvName(),
                    index,
                    headersAndCells
                    );

            }

        }

        /// <summary>
        /// Reset
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected override void Reset()
        {
            this.m_receiverMask = 0;
        }

    }

}
