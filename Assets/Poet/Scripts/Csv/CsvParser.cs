﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace Poet
{

    /// <summary>
    /// Functions for csv
    /// </summary>
    public class CsvParser
    {

        /// <summary>
        /// Get lines from csv text
        /// </summary>
        /// <param name="csv"></param>
        /// <param name="removeFirstLine">Remove first line flag</param>
        /// <returns>Lines</returns>
        // ------------------------------------------------------------------------------------------------
        public static List<string> getLines(string csv, bool removeFirstLine)
        {

            StringBuilder tempSB = new StringBuilder();

            List<string> ret = new List<string>();

            int quoteCounter = 0;

            foreach (char c in csv)
            {

                if (c == '\"')
                {
                    quoteCounter++;
                }

                if (c == '\n' && quoteCounter % 2 == 0)
                {
                    ret.Add(tempSB.ToString());
                    tempSB.Length = 0;
                }

                else
                {
                    tempSB.Append(c);
                }

            }

            if (tempSB.Length > 0)
            {
                ret.Add(tempSB.ToString());
            }

            if (removeFirstLine && ret.Count > 0)
            {
                ret.RemoveAt(0);
            }

            return ret;

        }

        /// <summary>
        /// Get lines from csv text IEnumerator
        /// </summary>
        /// <param name="csv"></param>
        /// <param name="removeFirstLine">Remove first line flag</param>
        /// <param name="ret">return target</param>
        /// <param name="sleepValue">sleep interval threshold</param>
        /// <returns>IEnumerator</returns>
        // ------------------------------------------------------------------------------------------------
        public static IEnumerator getLinesIE(string csv, bool removeFirstLine, List<string> ret, int sleepValue = 10000)
        {

            sleepValue = Mathf.Max(1, sleepValue);

            StringBuilder tempSB = new StringBuilder();

            bool evenTrueOddFalse = true;

            int counterForSleep = 0;

            foreach (char c in csv)
            {

                if (c == '\"')
                {
                    evenTrueOddFalse = !evenTrueOddFalse;
                }

                if (c == '\n' && evenTrueOddFalse)
                {
                    ret.Add(tempSB.ToString());
                    tempSB.Length = 0;
                }

                else
                {
                    tempSB.Append(c);
                }

                // sleep
                {

                    counterForSleep = (counterForSleep + 1) % sleepValue;

                    if (counterForSleep == 0)
                    {
                        yield return null;
                    }

                }

            }

            if (tempSB.Length > 0)
            {
                ret.Add(tempSB.ToString());
            }

            if (removeFirstLine && ret.Count > 0)
            {
                ret.RemoveAt(0);
            }

        }

        /// <summary>
        /// Get cells from one line
        /// </summary>
        /// <param name="oneLine">One line from csv</param>
        /// <returns>Cells</returns>
        // ------------------------------------------------------------------------------------------------
        public static List<string> getCellsFromOneLine(string oneLine, char punctuation = ',')
        {

            StringBuilder tempSB = new StringBuilder();

            List<string> ret = new List<string>();

            int quoteCounter = 0;
            int quoteConsecutiveCounter = 0;

            oneLine = oneLine.TrimEnd('\r', '\n');

            foreach (char c in oneLine)
            {

                if (c == '\"')
                {

                    quoteCounter++;
                    quoteConsecutiveCounter++;

                    if(quoteConsecutiveCounter >= 2)
                    {
                        tempSB.Append('\"');
                        quoteConsecutiveCounter = 0;
                    }

                }

                else
                {

                    quoteConsecutiveCounter = 0;

                    if (c == punctuation && quoteCounter % 2 == 0)
                    {
                        ret.Add(tempSB.ToString());
                        tempSB.Length = 0;
                    }

                    else
                    {
                        tempSB.Append(c);
                    }

                }

            }

            if(tempSB.ToString() == "\n" || tempSB.Length <= 0)
            {
                ret.Add("");
            }

            else if (tempSB.Length > 0)
            {
                ret.Add(tempSB.ToString());
            }

            return ret;

        }

        /// <summary>
        /// Parse string from cell
        /// </summary>
        /// <param name="strFromCell">string from cell</param>
        /// <returns>FuncNameAndParams list</returns>
        // ------------------------------------------------------------------------------------------------
        public static FuncNameAndParamsList parseFunctionsInCell(string strFromCell)
        {

            FuncNameAndParamsList ret = new FuncNameAndParamsList();

            parseFunctionsInCell(strFromCell, ret);

            return ret;

        }

        /// <summary>
        /// Parse string from cell
        /// </summary>
        /// <param name="strFromCell">string from cell</param>
        /// <param name="ret">reference to FuncNameAndParamsList</param>
        // ------------------------------------------------------------------------------------------------
        public static void parseFunctionsInCell(string strFromCell, FuncNameAndParamsList ret)
        {

            if (string.IsNullOrEmpty(strFromCell))
            {
                return;
            }

            // -----------------------------

            string[] lines = strFromCell.Split('\n');

            int leftIndex = 0;
            int rightIndex = 0;
            FuncNameAndParams element = null;
            int i = 0;
            string line = "";

            foreach (string rawLine in lines)
            {

                line = rawLine.TrimEnd('\r', '\n');

                leftIndex = line.IndexOf('(');
                rightIndex = line.IndexOf(')');

                if (leftIndex < 0 && rightIndex < 0)
                {
                    line = line + "()";
                    leftIndex = line.IndexOf('(');
                    rightIndex = line.IndexOf(')');
                }

                if (leftIndex >= 0 && rightIndex >= 0 && leftIndex < rightIndex)
                {

                    element = new FuncNameAndParams();

                    element.functionName = line.Remove(leftIndex);

                    element.parameters = new List<string>(line.Split('(', ')')[1].Split(','));

                    for (i = element.parameters.Count - 1; i >= 0; i--)
                    {

                        if (string.IsNullOrEmpty(element.parameters[i]))
                        {
                            element.parameters.RemoveAt(i);
                            continue;
                        }

                        if (element.parameters[i].Length >= 1 && element.parameters[i][0] == ' ')
                        {
                            element.parameters[i] = element.parameters[i].Remove(0, 1);
                        }

                    }

                    // add
                    {

                        if(element.functionName == "PreProcess")
                        {
                            ret.preProcess = true;
                        }

                        else if (element.functionName == "MainProcess")
                        {
                            ret.mainProcess = true;
                        }

                        else if (element.functionName == "Invoke")
                        {
                            ret.invokeTime = element.getFloatParameter(0, 0.0f);
                        }

                        else
                        {
                            ret.list.Add(element);
                        }

                    }
                    
                }

            }

        }

    }

}
