﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poet
{

    /// <summary>
    /// Poet csv holder
    /// </summary>
    public class PoetCsvHolder : PoetBaseReceiverScript, AssetBundleNameInterface
    {

        /// <summary>
        /// Csv header for identifier
        /// </summary>
        [SerializeField]
        [Tooltip("Csv header for identifier")]
        protected string m_csvHeaderForIdentifier = "Identifier";

#if UNITY_EDITOR

        /// <summary>
        /// Csv TextAsset editor only
        /// </summary>
        [SerializeField]
        [Tooltip("Csv TextAsset editor only")]
        protected TextAsset m_csvEditorOnly = null;

#endif

        /// <summary>
        /// User progress data
        /// </summary>
        [Serializable]
        protected class UserProgressData
        {

            /// <summary>
            /// Is current 
            /// </summary>
            public bool isCurrentCsv = false;

            /// <summary>
            /// ReadRange for user progress data
            /// </summary>
            public CsvReadRange range = null;

        }

        /// <summary>
        /// Loading mode at startup
        /// </summary>
        [SerializeField]
        [Tooltip("Loading mode at startup")]
        protected LoadingMode m_loadingMode = LoadingMode.Normal;

        /// <summary>
        /// Csv TextAsset for runtime
        /// </summary>
        [SerializeField]
        [Tooltip("Csv TextAsset for runtime")]
        protected TextAsset m_csv = null;

        /// <summary>
        /// Set this PoetCsvHolder to PoetOneSceneManager at scene start
        /// </summary>
        [SerializeField]
        [Tooltip("Set this PoetCsvHolder to PoetOneSceneManager at scene start")]
        protected bool m_setThisAtSceneStart = false;

        /// <summary>
        /// Csv row identifier for m_setThisAtSceneStart. If empty, start from index 0
        /// </summary>
        [SerializeField]
        [Tooltip("Csv row identifier for m_setThisAtSceneStart. If empty, start from index 0")]
        protected string m_setThisAtSceneStartIdentifier = "";

        /// <summary>
        /// AssetBundle name
        /// </summary>
        [SerializeField]
        [Tooltip("AssetBundle name")]
        protected string m_assetBundleName = "";

        /// <summary>
        /// AssetBundle variant
        /// </summary>
        [SerializeField]
        [Tooltip("AssetBundle variant")]
        protected string m_assetBundleVariant = "";

        /// <summary>
        /// Asset name in AssetBundle. Ignore if GetAllAssetNames().Count == 1
        /// </summary>
        [SerializeField]
        [Tooltip("Asset name in AssetBundle. This will be ignored if GetAllAssetNames().Count == 1")]
        protected string m_assetNameInAssetBundle = "";

        /// <summary>
        /// User progress data
        /// </summary>
        protected UserProgressData m_userProgressData = new UserProgressData();

        /// <summary>
        /// Csv Headers
        /// </summary>
        protected List<string> m_headers = new List<string>();

        /// <summary>
        /// Lines from m_csv
        /// </summary>
        protected List<string> m_lines = new List<string>();

        /// <summary>
        /// Id and index dictionary
        /// </summary>
        protected Dictionary<string, int> m_id_index = new Dictionary<string, int>();

        // -------------------------------------------------------------------------------------------

        /// <summary>
        /// AssetBundle name
        /// </summary>
        public string AssetBundleName { get { return this.m_assetBundleName; } set { this.m_assetBundleName = value; } }

        /// <summary>
        /// AssetBundle variant
        /// </summary>
        public string AssetBundleVariant { get { return this.m_assetBundleVariant; } set { this.m_assetBundleVariant = value; } }

        /// <summary>
        /// Asset name in AssetBundle
        /// </summary>
        public string AssetNameInAssetBundle { get { return this.m_assetNameInAssetBundle; } set { this.m_assetNameInAssetBundle = value; } }

        // -------------------------------------------------------------------------------------------

        /// <summary>
        /// m_csv getter
        /// </summary>
        public TextAsset csv { get { return this.m_csv; } }

        /// <summary>
        /// m_lines getter
        /// </summary>
        public IList<string> lines { get { return this.m_lines.AsReadOnly(); } }

        /// <summary>
        /// m_headers getter
        /// </summary>
        public IList<string> header { get { return this.m_headers.AsReadOnly(); } }

        /// <summary>
        /// m_id_index getter
        /// </summary>
        public Dictionary<string, int> idIndexDictionary { get { return this.m_id_index; } }

        // -------------------------------------------------------------------------------------------

        /// <summary>
        /// Start
        /// </summary>
        // -------------------------------------------------------------------------------------------
        protected virtual void Start()
        {

            if(this.m_loadingMode == LoadingMode.Normal)
            {

                PoetIEnumeratorStartupManager.Instance.addSceneStartupIEnumerator(
                    this.init(),
                    null,
                    null,
                    SSC.IEnumeratorStartupManager.BeforeAfter.After
                    );

            }

            else if(this.m_loadingMode == LoadingMode.AssetBundle)
            {

#if UNITY_EDITOR

                if(string.IsNullOrEmpty(this.m_assetBundleName))
                {
                    Debug.LogWarning("(#if UNITY_EDITOR) m_assetBundleName is empty : " + this.gameObject.name);
                }

#endif

                PoetAssetBundleStartupManager.Instance.addSceneStartupAssetBundle(
                    this.m_assetBundleName,
                    this.m_assetBundleVariant,
                    this.onSuccessAssetBundleLoading,
                    null,
                    null,
                    null
                    );

            }

        }

        /// <summary>
        /// OnDestroy
        /// </summary>
        // -----------------------------------------------------------------------------------
        protected virtual void OnDestroy()
        {

            if (this.m_loadingMode == LoadingMode.AssetBundle)
            {
                Resources.UnloadAsset(this.m_csv);
            }

        }

        /// <summary>
        /// Get csv name
        /// </summary>
        /// <returns>csv name</returns>
        // -------------------------------------------------------------------------------------------
        public string csvName()
        {

            if(this.m_csv)
            {
                return this.m_csv.name;
            }

            return "";

        }

        /// <summary>
        /// init
        /// </summary>
        // -------------------------------------------------------------------------------------------
        protected virtual IEnumerator init(int sleepValue1 = 10000, int sleepValue2 = 100)
        {
            
            // sleepValue1 = Mathf.Max(1, sleepValue1);
            sleepValue2 = Mathf.Max(1, sleepValue2);

            if (!this.m_csv)
            {
                Debug.LogWarning("csv is null : " + this.gameObject.name);
                yield break;
            }

            // clear
            {
                this.m_headers.Clear();
                this.m_lines.Clear();
                this.m_id_index.Clear();
            }

            // m_lines m_headers
            {

                yield return CsvParser.getLinesIE(csv.text, false, this.m_lines, sleepValue1);

                if(this.m_lines.Count > 0)
                {

                    var headers = CsvParser.getCellsFromOneLine(this.m_lines[0]);

                    foreach(string header in headers)
                    {

                        if(!this.m_headers.Contains(header))
                        {
                            this.m_headers.Add(header);
                        }

                        else
                        {
                            Debug.LogWarning("Duplicated header found : " + csv.name + " : " + header);
                        }

                    }

                    // remove m_lines 0
                    {
                        this.m_lines.RemoveAt(0);
                    }

                }

            }

            // m_id_index and send state
            {

                PoetReduxManager prm = PoetReduxManager.Instance as PoetReduxManager;

                List<string> cells = null;
                int size = this.m_lines.Count;
                int identifierColIndex = -1;

                // identifierColIndex
                {

                    for(int i = 0; i < this.m_headers.Count; i++)
                    {

                        if(this.m_headers[i] == this.m_csvHeaderForIdentifier)
                        {
                            identifierColIndex = 0;
                            break;
                        }

                    }

                    if(identifierColIndex < 0)
                    {
                        Debug.LogError("Header not found : " + this.m_csvHeaderForIdentifier);
                        identifierColIndex = 0;
                        yield break;
                    }

                }

                // i = 0
                {

                    if (size > 0)
                    {

                        cells = CsvParser.getCellsFromOneLine(this.m_lines[0]);

                        if (cells.Count > identifierColIndex && !string.IsNullOrEmpty(cells[identifierColIndex]))
                        {
                            this.m_id_index.Add(cells[identifierColIndex], 0);
                        }

                        else
                        {
                            this.m_id_index.Add("DUMMY_START", 0);
                        }

                        // send init state
                        {

                            prm.PoetInitStateWatcher.state().setState(
                                prm.PoetInitStateWatcher,
                                this.csvName(),
                                0,
                                this.createHeadersAndCells(cells)
                                );

                        }

                    }

                }

                for (int i = 1; i < size; i++)
                {

                    cells = CsvParser.getCellsFromOneLine(this.m_lines[i]);

                    // m_id_index
                    {

                        if (cells.Count > identifierColIndex && !string.IsNullOrEmpty(cells[identifierColIndex]))
                        {

                            if(!this.m_id_index.ContainsKey(cells[identifierColIndex]))
                            {
                                this.m_id_index.Add(cells[identifierColIndex], i);
                            }

                            else
                            {
                                Debug.LogWarning("Already contains an identifier : " + cells[identifierColIndex] + " : " + this.m_csv.name);
                            }
                            
                        }

                    }

                    // send init state
                    {

                        prm.PoetInitStateWatcher.state().setState(
                            prm.PoetInitStateWatcher,
                            this.csvName(),
                            i,
                            this.createHeadersAndCells(cells)
                            );

                    }

                    // sleep
                    {

                        if (i % sleepValue2 == 0)
                        {
                            yield return null;
                        }

                    }
                    
                }

            }

        }

        /// <summary>
        /// Create headers and cells dictionary
        /// </summary>
        /// <param name="cells">cells</param>
        /// <returns>headers and cells dictionary</returns>
        // ------------------------------------------------------------------------------------------------
        protected Dictionary<string, string> createHeadersAndCells(List<string> cells)
        {

            Dictionary<string, string> ret = new Dictionary<string, string>();

            int size = Mathf.Min(cells.Count, this.m_headers.Count);

            for (int i = 0; i < size; i++)
            {
                if (!ret.ContainsKey(this.m_headers[i]))
                {
                    ret.Add(this.m_headers[i], cells[i]);
                }
            }

            if (cells.Count != this.m_headers.Count)
            {
                Debug.LogWarning("Different size : list / header : " + cells.Count + " / " + this.m_headers.Count);
            }

            return ret;

        }

        /// <summary>
        /// Get headers and cells dictionary
        /// </summary>
        /// <param name="index">csv row index</param>
        /// <returns>headers and cells dictionary</returns>
        // ------------------------------------------------------------------------------------------------
        public Dictionary<string, string> getHeadersAndCells(int index)
        {

            if (index < 0 || this.m_lines.Count <= index)
            {
                Debug.LogError("Index out of range : " + index + " / " + this.m_lines.Count);
                return new Dictionary<string, string>();
            }

            return this.createHeadersAndCells(CsvParser.getCellsFromOneLine(this.m_lines[index]));

        }

        /// <summary>
        /// Get headers and cells dictionary
        /// </summary>
        /// <param name="identifier">csv index identifier</param>
        /// <returns>headers and cells dictionary</returns>
        // ------------------------------------------------------------------------------------------------
        public Dictionary<string, string> getHeadersAndCells(string identifier)
        {

            if(!this.m_id_index.ContainsKey(identifier))
            {
                Debug.LogWarning("Key not found : " + identifier + " : " + this.csvName());
                return new Dictionary<string, string>();
            }

            return this.getHeadersAndCells(this.m_id_index[identifier]);

        }

        /// <summary>
        /// Get index by identifier. Return -1 if not exist.
        /// </summary>
        /// <param name="identifier"></param>
        /// <returns>index or error -1</returns>
        // ------------------------------------------------------------------------------------------------
        public int getCsvRowIndex(string identifier)
        {

            if (this.m_id_index.ContainsKey(identifier))
            {
                return this.m_id_index[identifier];
            }

            else
            {
                Debug.LogError("Key not found : " + identifier);
            }

            return -1;

        }

        /// <summary>
        /// Get csv row identifier by csv row index
        /// </summary>
        /// <param name="index">csv row index</param>
        /// <returns>csv row identifier or null</returns>
        // ------------------------------------------------------------------------------------------------
        public string getCsvRowIdentifierBelongTo(int csvRowIndex)
        {

            if (csvRowIndex < 0)
            {
                return "";
            }

            // -------------------------

            List<string> keys = new List<string>(this.m_id_index.Keys);

            for (int i = keys.Count - 1; i >= 0; i--)
            {

                if (this.m_id_index[keys[i]] <= csvRowIndex)
                {
                    return keys[i];
                }

            }

            return "";

        }


        /// <summary>
        /// Get ReadRange
        /// </summary>
        /// <param name="index">csv row index</param>
        /// <returns>ReadRange or null</returns>
        // ------------------------------------------------------------------------------------------------
        public CsvReadRange getReadRange(int csvRowIndex)
        {

            if (csvRowIndex < 0)
            {
                return null;
            }

            // -------------------------

            List<string> keys = new List<string>(this.m_id_index.Keys);

            for (int i = keys.Count - 1; i >= 0; i--)
            {

                if(this.m_id_index[keys[i]] <= csvRowIndex)
                {
                    return new CsvReadRange(keys[i], csvRowIndex - this.m_id_index[keys[i]]);
                }

            }

            return null;

        }

        /// <summary>
        /// Get csv row index from ReadRange
        /// </summary>
        /// <param name="range">ReadRange</param>
        /// <returns>csv row index</returns>
        // ------------------------------------------------------------------------------------------------
        public int getCsvRowIndex(CsvReadRange range)
        {

            if (range == null)
            {
                return -1;
            }

            // -------------------------

            if(this.m_id_index.ContainsKey(range.csvRowIdentifier))
            {
                return this.m_id_index[range.csvRowIdentifier] + range.count;
            }

            return -1;

        }

        /// <summary>
        /// Show this PoetCsvHolder
        /// </summary>
        /// <param name="csvRowIndex">csv row index</param>
        // ------------------------------------------------------------------------------------------------
        public void showThisCsvHolder(int csvRowIndex)
        {

            if (PoetOneSceneManager.isAvailable())
            {
                PoetOneSceneManager.Instance.setCurrentCsvHolderAndGoToSequence(this, csvRowIndex);
            }

#if UNITY_EDITOR

            else
            {
                Debug.LogWarning("(#if UNITY_EDITOR) PoetOneSceneManager not found");
            }

#endif

        }

        /// <summary>
        /// Show this PoetCsvHolder
        /// </summary>
        /// <param name="identifier">csv row identifier</param>
        // ------------------------------------------------------------------------------------------------
        public void showThisCsvHolder(string identifier)
        {

            if (PoetOneSceneManager.isAvailable())
            {
                PoetOneSceneManager.Instance.setCurrentCsvHolderAndGoToSequence(this, identifier);
            }

#if UNITY_EDITOR

            else
            {
                Debug.LogWarning("(#if UNITY_EDITOR) PoetOneSceneManager not found");
            }

#endif


        }

        /// <summary>
        /// SceneChangeState receiver
        /// </summary>
        /// <param name="scState">SceneChangeState</param>
        // -----------------------------------------------------------------------------------------------
        protected override void onSceneChangeStateReceiver(SSC.SceneChangeState scState)
        {

            if (scState.stateEnum == SSC.SceneChangeState.StateEnum.AllStartupsDone)
            {

                PoetSceneChangeManager pscm = PoetSceneChangeManager.Instance as PoetSceneChangeManager;

                if (pscm.isLoadingCurrentSceneWithUserProgressData())
                {

                    this.m_userProgressData = pscm.getDataFromCurrentUserProgressData<UserProgressData>(this.transform, this);

                    if (PoetOneSceneManager.isAvailable() && this.m_userProgressData.isCurrentCsv)
                    {

                        if (this.m_userProgressData.range != null)
                        {

                            PoetOneSceneManager.Instance.setCurrentCsvHolderWithoutGoingToSequence(
                                this,
                                this.getCsvRowIndex(this.m_userProgressData.range)
                                );

                        }

                        else
                        {
                            Debug.LogError("Implementation error : PCH : oSCSR");
                        }

                    }

                }

            }

            else if (scState.stateEnum == SSC.SceneChangeState.StateEnum.ScenePlaying)
            {

                if (PoetOneSceneManager.isAvailable())
                {

                    PoetSceneChangeManager pscm = PoetSceneChangeManager.Instance as PoetSceneChangeManager;

                    if (pscm.isLoadingAlbumMemoryScene())
                    {
                        if(this.csvName() == pscm.albumMemoryData.csvName)
                        {
                            PoetOneSceneManager.Instance.setCurrentCsvHolderAndGoToSequence(this, pscm.albumMemoryData.startCsvRowIdentifier);
                        }

                    }

                    else if (!this.m_userProgressData.isCurrentCsv && this.m_setThisAtSceneStart)
                    {

                        if (string.IsNullOrEmpty(this.m_setThisAtSceneStartIdentifier))
                        {
                            PoetOneSceneManager.Instance.setCurrentCsvHolderAndGoToSequence(this, 0);
                        }

                        else
                        {
                            PoetOneSceneManager.Instance.setCurrentCsvHolderAndGoToSequence(this, this.m_setThisAtSceneStartIdentifier);
                        }

                    }

                }

            }

        }

        /// <summary>
        /// Success loading AssetBundle function
        /// </summary>
        // -------------------------------------------------------------------------------------------
        protected void onSuccessAssetBundleLoading(AssetBundle ab, System.Object info, Action finishCallback)
        {
            StartCoroutine(this.onSuccessAssetBundleLoadingIE(ab, info, finishCallback));
        }

        /// <summary>
        /// Success loading AssetBundle function
        /// </summary>
        // -------------------------------------------------------------------------------------------
        protected IEnumerator onSuccessAssetBundleLoadingIE(AssetBundle ab, System.Object info, Action finishCallback)
        {

            yield return null;

            if (ab)
            {

                string[] names = ab.GetAllAssetNames();

                AssetBundleRequest abr = null;

                if (names.Length == 1)
                {
                    abr = ab.LoadAssetAsync<TextAsset>(names[0]);
                }

                else
                {
                    abr = ab.LoadAssetAsync<TextAsset>(this.m_assetNameInAssetBundle);
                }

                while(!abr.isDone)
                {
                    yield return null;
                }

                this.m_csv = abr.asset as TextAsset;

                SSC.IEnumeratorStartupManager.Instance.addSceneStartupIEnumerator(
                    this.init(),
                    null,
                    null,
                    SSC.IEnumeratorStartupManager.BeforeAfter.After
                    );

            }

            if(finishCallback != null)
            {
                finishCallback();
            }

        }

        /// <summary>
        /// initOnAwake() is called after Awake().
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected override void initOnAwake()
        {

        }

        /// <summary>
        /// PoetSequenceState receiver
        /// </summary>
        /// <param name="state">PoetSequenceState</param>
        // ------------------------------------------------------------------------------------------
        protected override void onPoetSequenceState(PoetSequenceState psState)
        {

        }

        /// <summary>
        /// PoetInitState receiver
        /// </summary>
        /// <param name="piState">PoetInitState</param>
        // ------------------------------------------------------------------------------------------
        protected override void onPoetInitState(PoetInitState piState)
        {

        }

        /// <summary>
        /// TextShowingState receiver
        /// </summary>
        /// <param name="tsState">TextShowingState</param>
        // ------------------------------------------------------------------------------------------
        protected override void onTextShowingState(TextShowingState tsState)
        {

        }

        /// <summary>
        /// UserProgressDataSignal receiver
        /// </summary>
        /// <param name="tsState">UserProgressDataSignal</param>
        // ------------------------------------------------------------------------------------------
        protected override void onUserProgressDataSignal(UserProgressDataSignal updSignal)
        {

            this.m_userProgressData = new UserProgressData();

            if (PoetOneSceneManager.isAvailable())
            {

                this.m_userProgressData.isCurrentCsv = (PoetOneSceneManager.Instance.currentCsvHolder == this);

                if (this.m_userProgressData.isCurrentCsv)
                {
                    this.m_userProgressData.range = this.getReadRange(PoetOneSceneManager.Instance.currentCsvRowIndex);
                }

            }

            updSignal.addDataAction(this.transform, this, JsonUtility.ToJson(this.m_userProgressData));

        }

        /// <summary>
        /// PauseState receiver
        /// </summary>
        /// <param name="pState">PauseState</param>
        // ------------------------------------------------------------------------------------------
        protected override void onPauseStateReceiver(SSC.PauseState pState)
        {

        }

    }

}
