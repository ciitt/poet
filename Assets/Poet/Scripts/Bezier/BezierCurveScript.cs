﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poet
{

    public enum BezierLookAtType
    {
        None,
        HandleDirection,
        Tangent
    }

    [ExecuteInEditMode]
    public class BezierCurveScript : MonoBehaviour
    {

        protected class LengthAndStartAndEnd
        {

            public float startTotalLengthMeter = 0.0f;
            public float endTotalLengthMeter = 0.0f;
            public BezierHandleScript refStart = null;
            public BezierHandleScript refEnd = null;

        }

#if UNITY_EDITOR

        [Header("Debug Params")]
        [Header("<----------------------")]

        public Transform m_refDebugObject = null;

        public BezierLookAtType m_debugLookAtType = BezierLookAtType.None;

        [Range(0.0f, 1.0f)]
        public float m_moveDebugObject = 0.0f;

        [Header("---------------------->")]
#endif

        [Space(30.0f)]

        /// <summary>
        /// Bezter curve sampling rate
        /// </summary>
        [SerializeField]
        [Tooltip("Bezter curve sampling rate")]
        [Range(1, 1000)]
        protected int m_sampling = 50;

        /// <summary>
        /// BezierHandleScript list
        /// </summary>
        [Tooltip("BezierHandleScript list")]
        public List<BezierHandleScript> bezierHandleList = new List<BezierHandleScript>();

        /// <summary>
        /// Sampling meters list
        /// </summary>
        [SerializeField]
        [HideInInspector]
        protected List<LengthAndStartAndEnd> m_samplingMeters = new List<LengthAndStartAndEnd>();

        Vector3 p0 = Vector3.zero;
        Vector3 p1 = Vector3.zero;
        Vector3 p2 = Vector3.zero;
        Vector3 p3 = Vector3.zero;

        float omt = 0f;
        float omt2 = 0f;
        float t2 = 0f;

        /// <summary>
        /// Awake
        /// </summary>
        // -------------------------------------------------------------------------------------------
        protected virtual void Awake()
        {

            this.init();

        }

        /// <summary>
        /// OnValidate
        /// </summary>
        // -------------------------------------------------------------------------------------------
        protected virtual void OnValidate()
        {
            this.init();
        }

#if UNITY_EDITOR

        // -------------------------------------------------------------------------------------------
        void Update()
        {

            if(!Application.isPlaying)
            {

                if (this.m_refDebugObject)
                {

                    BezierPoint bp = this.pointAtBezierCurveDetail(this.m_moveDebugObject);

                    // position
                    {
                        this.m_refDebugObject.position = bp.point;
                    }

                    // rotation
                    {

                        if(this.m_debugLookAtType == BezierLookAtType.HandleDirection)
                        {
                            this.m_refDebugObject.rotation = Quaternion.Slerp(this.m_refDebugObject.rotation, bp.rotation, 1f);
                        }

                        else if (this.m_debugLookAtType == BezierLookAtType.Tangent)
                        {
                            Quaternion quat = Quaternion.LookRotation(bp.tangent, Vector3.up);
                            this.m_refDebugObject.rotation = Quaternion.Slerp(this.m_refDebugObject.rotation, quat, 1f);
                        }

                    }

                }

            }


        }

#endif

        /// <summary>
        /// Sampling
        /// </summary>
        // -------------------------------------------------------------------------------------------
        protected void init()
        {

            this.m_sampling = Mathf.Max(1, this.m_sampling);

            //
            {

                for (int i = this.bezierHandleList.Count - 1; i >= 0; i--)
                {

                    if(Application.isPlaying)
                    {

                        if (!this.bezierHandleList[i])
                        {
                            Debug.LogWarning("bezierHandleList has empty element : " + this.gameObject.name);
                            this.bezierHandleList.RemoveAt(i);
                        }

                    }

                }

            }

            this.samplingBezierLength();

        }

        /// <summary>
        /// Sampling
        /// </summary>
        // -------------------------------------------------------------------------------------------
        protected void samplingBezierLength()
        {

            this.m_samplingMeters.Clear();

            int size = this.bezierHandleList.Count;

            float total = 0.0f;

            Vector3 point0 = Vector3.zero;
            Vector3 point1 = Vector3.zero;

            LengthAndStartAndEnd temp = null;

            for (int i = 0; i < size - 1; i++)
            {

                temp = new LengthAndStartAndEnd();

                temp.refStart = this.bezierHandleList[i];
                temp.refEnd = this.bezierHandleList[i + 1];
                temp.startTotalLengthMeter = total;

                // editor
                {

                    if (!temp.refStart || !temp.refEnd)
                    {
                        continue;
                    }

                }

                point0 = temp.refStart.point();

                for (int j = 1; j <= this.m_sampling; j++)
                {

                    point1 = this.pointAtOneBezierCurve(temp.refStart, temp.refEnd, (float)j / (float)this.m_sampling);

                    total += Vector3.Distance(point0, point1);

                    point0 = point1;

                }

                temp.endTotalLengthMeter = total;

                this.m_samplingMeters.Add(temp);

            }

        }

        /// <summary>
        /// Get point at bezier curve
        /// </summary>
        /// <param name="t">t</param>
        /// <returns>point</returns>
        // -------------------------------------------------------------------------------------------
        public Vector3 pointAtBezierCurve(float t)
        {

            BezierHandleScript start = null;
            BezierHandleScript end = null;
            float new_t = 0.0f;

            this.convert(t, out start, out end, out new_t);

            return this.pointAtOneBezierCurve(
                start,
                end,
                new_t
                );

        }

        /// <summary>
        /// Get BezierPoint at bezier curve
        /// </summary>
        /// <param name="t">t</param>
        /// <returns>BezierPoint</returns>
        // -------------------------------------------------------------------------------------------
        public BezierPoint pointAtBezierCurveDetail(float t)
        {

            BezierPoint ret = new BezierPoint();

            BezierHandleScript start = null;
            BezierHandleScript end = null;
            float new_t = 0.0f;

            this.convert(t, out start, out end, out new_t);

            ret.point = this.pointAtOneBezierCurve(
                start,
                end,
                new_t
                );

            ret.tangent = this.normalizedTangent(t);

            if(start && end)
            {
                ret.rotation = Quaternion.Slerp(start.transform.rotation, end.transform.rotation, new_t);
            }

            else
            {
                ret.rotation = Quaternion.identity;
            }

            return ret;

        }

        /// <summary>
        /// Convert t
        /// </summary>
        /// <param name="t">t</param>
        /// <param name="start">BezierHandleScript start</param>
        /// <param name="end">BezierHandleScript end</param>
        /// <param name="converted_t">new t</param>
        // -------------------------------------------------------------------------------------------
        protected void convert(float t, out BezierHandleScript start, out BezierHandleScript end, out float converted_t)
        {

            if (this.m_samplingMeters.Count <= 0)
            {

                if (this.bezierHandleList.Count > 0)
                {
                    start = this.bezierHandleList[0];
                    end = this.bezierHandleList[0];
                    converted_t = 0.0f;
                }

                else
                {
                    start = null;
                    end = null;
                    converted_t = 0.0f;
                }

                return;

            }

            // -------------------------------

            if (t >= 1.0f)
            {
                start = this.bezierHandleList[this.bezierHandleList.Count - 1];
                end = this.bezierHandleList[this.bezierHandleList.Count - 1];
                converted_t = 0.0f;
                return;
            }

            else if (t <= 0.0f)
            {
                start = this.bezierHandleList[0];
                end = this.bezierHandleList[0];
                converted_t = 0.0f;
                return;
            }

            // -------------------------------

            // this.m_samplingMeters.Count >= 2
            {

                float targetLength = this.m_samplingMeters[this.m_samplingMeters.Count - 1].endTotalLengthMeter * t;

                int size = this.m_samplingMeters.Count;

                for (int i = this.m_samplingMeters.Count - 1; i >= 0; i--)
                {

                    if (targetLength >= this.m_samplingMeters[i].startTotalLengthMeter)
                    {

                        // t
                        {

                            if (this.m_samplingMeters[i].endTotalLengthMeter == this.m_samplingMeters[i].startTotalLengthMeter)
                            {
                                start = this.m_samplingMeters[i].refStart;
                                end = this.m_samplingMeters[i].refEnd;
                                converted_t = 0.0f;
                                return;
                            }

                            t =
                                (targetLength - this.m_samplingMeters[i].startTotalLengthMeter) /
                                (this.m_samplingMeters[i].endTotalLengthMeter - this.m_samplingMeters[i].startTotalLengthMeter)
                                ;

                        }

                        start = this.m_samplingMeters[i].refStart;
                        end = this.m_samplingMeters[i].refEnd;
                        converted_t = t;
                        return;

                    }

                }

            }

            // this.m_samplingMeters.Count == 1
            {

                start = this.m_samplingMeters[0].refStart;
                end = this.m_samplingMeters[0].refEnd;
                converted_t = t;

            }

        }

        /// <summary>
        /// Get point at one bezier curve
        /// </summary>
        /// <param name="start">start BezierHandleScript</param>
        /// <param name="end">end BezierHandleScript</param>
        /// <param name="t">t</param>
        /// <returns>point</returns>
        // -------------------------------------------------------------------------------------------
        protected Vector3 pointAtOneBezierCurve(BezierHandleScript start, BezierHandleScript end, float t)
        {

            if(!start && !end)
            {
                return Vector3.zero;
            }

            else if(!start)
            {
                return end.point();
            }

            else if (!end)
            {
                return start.point();
            }

            // ----------------------------

            t = Mathf.Clamp01(t);

            p0 = start.point();
            p1 = start.startTangentHandlePoint();
            p2 = end.endTangentHandlePoint();
            p3 = end.point();

            omt = 1f - t;
            omt2 = omt * omt;
            t2 = t * t;

            return
                p0 * (omt2 * omt) +
                p1 * (3f * omt2 * t) +
                p2 * (3f * omt * t2) +
                p3 * (t2 * t)
                ;

        }

        /// <summary>
        /// Get tangent at point
        /// </summary>
        /// <param name="t">t</param>
        /// <returns>tangent</returns>
        // -------------------------------------------------------------------------------------------
        public Vector3 normalizedTangent(float t)
        {

            t = Mathf.Clamp(t, 0.0f, 0.999f);

            Vector3 p0 = this.pointAtBezierCurve(t);
            Vector3 p1 = this.pointAtBezierCurve(t + 0.001f);

            return (p1 - p0).normalized;

        }

        /// <summary>
        /// OnDrawGizmos
        /// </summary>
        // -------------------------------------------------------------------------------------------
        protected virtual void OnDrawGizmos()
        {

#if UNITY_EDITOR

            BezierHandleScript current = null;
            BezierHandleScript next = null;

            int size = this.bezierHandleList.Count;

            UnityEditor.Handles.color = Color.green;

            Vector3 arrow0 = Vector3.zero;
            Vector3 arrow1 = Vector3.zero;
            Vector3 arrow2 = Vector3.zero;

            for (int i = 0; i < size - 1; i++)
            {

                if(!this.bezierHandleList[i] || !this.bezierHandleList[i + 1])
                {
                    continue;
                }

                // ----------------

                current = this.bezierHandleList[i];
                next = this.bezierHandleList[i + 1];

                if (!current || !next)
                {
                    continue;
                }

                // ---------

                // DrawAAConvexPolygon
                {
                    arrow0 = current.point() + (current.transform.right * 0.25f);
                    arrow1 = current.point() + (current.transform.forward * 1.0f);
                    arrow2 = current.point() - (current.transform.right * 0.25f);

                    UnityEditor.Handles.DrawAAConvexPolygon(arrow0, arrow1, arrow2);
                }

                // DrawBezier
                {

                    UnityEditor.Handles.DrawBezier(
                        current.transform.position,
                        next.transform.position,
                        current.startTangentHandlePoint(),
                        next.endTangentHandlePoint(),
                        Color.red,
                        null,
                        3.0f
                        );

                }

            }

            if (size > 0)
            {

                current = this.bezierHandleList[this.bezierHandleList.Count - 1];

                if(current)
                {

                    arrow0 = current.point() + (current.transform.right * 0.25f);
                    arrow1 = current.point() + (current.transform.forward * 1.0f);
                    arrow2 = current.point() - (current.transform.right * 0.25f);

                    UnityEditor.Handles.DrawAAConvexPolygon(arrow0, arrow1, arrow2);

                }

            }

#endif

        }

    }

}
