﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poet
{

    public class BezierHandleScript : MonoBehaviour
    {

        ///// <summary>
        ///// BezierPoint list
        ///// </summary>
        //[SerializeField]
        //[Tooltip("BezierPoint list")]
        //protected List<BezierPoint> m_bezierPointList = new List<BezierPoint>();

        public Vector3 startTangent = Vector3.forward;

        public Vector3 endTangent = -Vector3.forward;

        public Vector3 point()
        {
            return this.transform.position;
        }

        public Vector3 startTangentHandlePoint()
        {
            return this.transform.TransformPoint(this.startTangent);
        }

        public Vector3 endTangentHandlePoint()
        {
            return this.transform.TransformPoint(this.endTangent);
        }

    }

}
