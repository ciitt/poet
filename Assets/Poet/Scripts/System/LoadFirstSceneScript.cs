﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Poet
{

    public class LoadFirstSceneScript : MonoBehaviour
    {

#if UNITY_EDITOR

        /// <summary>
        /// Next scene
        /// </summary>
        [SerializeField]
        [Tooltip("Next scene")]
        protected UnityEngine.Object m_nextScene;

#endif

        /// <summary>
        /// Next scene name
        /// </summary>
        [HideInInspector]
        [SerializeField]
        protected string m_nextSceneName = "";

        /// <summary>
        /// Start
        /// </summary>
        // -------------------------------------------------------------------------------------
        IEnumerator Start()
        {

            while (!Poet.SystemManager.Instance.isStartupDone)
            {
                yield return null;
            }

            yield return null;

            (Poet.PoetSceneChangeManager.Instance as Poet.PoetSceneChangeManager).useSimpleNowLoadingOnce();
            Poet.PoetSceneChangeManager.Instance.loadNextScene(this.m_nextSceneName);

        }

        /// <summary>
        /// OnValidate
        /// </summary>
        // -------------------------------------------------------------------------------------
        void OnValidate()
        {

#if UNITY_EDITOR

            if (this.m_nextScene && !string.IsNullOrEmpty(this.m_nextScene.name))
            {
                this.m_nextSceneName = this.m_nextScene.name;
            }

            else
            {
                this.m_nextSceneName = "";
            }

#endif

        }


    }

}
