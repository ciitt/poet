﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poet
{

    /// <summary>
    /// Poet dialog message manager
    /// </summary>
    public partial class PoetDialogMessageManager : SSC.SingletonMonoBehaviour<PoetDialogMessageManager>
    {

        /// <summary>
        /// Called in Awake
        /// </summary>
        // ------------------------------------------------------------------------------------
        protected override void initOnAwake()
        {
            
        }

        /// <summary>
        /// Confirmation message for quit
        /// </summary>
        /// <returns>any</returns>
        // ------------------------------------------------------------------------------------
        public virtual System.Object quitApplicationConfirmationMessage()
        {

            // SystemLanguage sl = SystemManager.Instance.systemLanguage;

            SSC.DialogMessages messages = new SSC.DialogMessages();

            messages.category = SSC.DialogMessages.MessageCategory.Confirmation;
            messages.title = "Confirmation";
            messages.mainMessage = "Quit game ?";

            return messages;

        }

        /// <summary>
        /// Confirmation message for error
        /// </summary>
        /// <returns>any</returns>
        // ------------------------------------------------------------------------------------
        public virtual System.Object backToTitleConfirmationMessage()
        {

            // SystemLanguage sl = SystemManager.Instance.systemLanguage;

            SSC.DialogMessages messages = new SSC.DialogMessages();

            messages.category = SSC.DialogMessages.MessageCategory.Confirmation;
            messages.title = "Confirmation";
            messages.mainMessage = "Back to title scene ?";

            return messages;

        }

        /// <summary>
        /// Confirmation message for album
        /// </summary>
        /// <returns>any</returns>
        // ------------------------------------------------------------------------------------
        public virtual System.Object backToAlbumConfirmationMessage()
        {

            // SystemLanguage sl = SystemManager.Instance.systemLanguage;

            SSC.DialogMessages messages = new SSC.DialogMessages();

            messages.category = SSC.DialogMessages.MessageCategory.Confirmation;
            messages.title = "Confirmation";
            messages.mainMessage = "Back to album scene ?";

            return messages;

        }

        /// <summary>
        /// Confirmation message for back to title
        /// </summary>
        /// <returns>any</returns>
        // ------------------------------------------------------------------------------------
        public virtual System.Object backToTitleBecauseOfErrorMessage()
        {

            // SystemLanguage sl = SystemManager.Instance.systemLanguage;

            SSC.DialogMessages messages = new SSC.DialogMessages();

            messages.category = SSC.DialogMessages.MessageCategory.Confirmation;
            messages.title = "Confirmation";
            messages.mainMessage = "Back to title scene.";

            return messages;

        }

        /// <summary>
        /// Confirmation message for overwriting user progress data
        /// </summary>
        /// <returns>any</returns>
        // ------------------------------------------------------------------------------------
        public virtual System.Object overwriteUserProgressDataMessage()
        {

            // SystemLanguage sl = SystemManager.Instance.systemLanguage;

            SSC.DialogMessages messages = new SSC.DialogMessages();

            messages.category = SSC.DialogMessages.MessageCategory.Confirmation;
            messages.title = "Confirmation";
            messages.mainMessage = "Overwrite data ?";

            return messages;

        }

        /// <summary>
        /// Confirmation message for saving user progress data
        /// </summary>
        /// <returns>any</returns>
        // ------------------------------------------------------------------------------------
        public virtual System.Object saveUserProgressDataMessage()
        {

            // SystemLanguage sl = SystemManager.Instance.systemLanguage;

            SSC.DialogMessages messages = new SSC.DialogMessages();

            messages.category = SSC.DialogMessages.MessageCategory.Confirmation;
            messages.title = "Confirmation";
            messages.mainMessage = "Save data here ?";

            return messages;

        }

        /// <summary>
        /// Confirmation message for saving user progress data
        /// </summary>
        /// <returns>any</returns>
        // ------------------------------------------------------------------------------------
        public virtual System.Object saveUserProgressDataProgressMessage()
        {

            // SystemLanguage sl = SystemManager.Instance.systemLanguage;

            SSC.DialogMessages messages = new SSC.DialogMessages();

            messages.category = SSC.DialogMessages.MessageCategory.Other;
            messages.title = "Save";
            messages.mainMessage = "Now Saving ...";

            return messages;

        }


        /// <summary>
        /// Confirmation message for loading user progress data
        /// </summary>
        /// <returns>any</returns>
        // ------------------------------------------------------------------------------------
        public virtual System.Object loadUserProgressDataMessage()
        {

            // SystemLanguage sl = SystemManager.Instance.systemLanguage;

            SSC.DialogMessages messages = new SSC.DialogMessages();

            messages.category = SSC.DialogMessages.MessageCategory.Confirmation;
            messages.title = "Confirmation";
            messages.mainMessage = "Load this data ?";

            return messages;

        }


        /// <summary>
        /// Confirmation message for quick save
        /// </summary>
        /// <returns>any</returns>
        // ------------------------------------------------------------------------------------
        public virtual System.Object quickSaveMessage()
        {

            // SystemLanguage sl = SystemManager.Instance.systemLanguage;

            SSC.DialogMessages messages = new SSC.DialogMessages();

            messages.category = SSC.DialogMessages.MessageCategory.Confirmation;
            messages.title = "Confirmation";
            messages.mainMessage = "Quick Save ?";

            return messages;

        }

        /// <summary>
        /// Confirmation message for quick load
        /// </summary>
        /// <returns>any</returns>
        // ------------------------------------------------------------------------------------
        public virtual System.Object quickLoadMessage()
        {

            // SystemLanguage sl = SystemManager.Instance.systemLanguage;

            SSC.DialogMessages messages = new SSC.DialogMessages();

            messages.category = SSC.DialogMessages.MessageCategory.Confirmation;
            messages.title = "Confirmation";
            messages.mainMessage = "Quick Load ?";

            return messages;

        }

    }

}
