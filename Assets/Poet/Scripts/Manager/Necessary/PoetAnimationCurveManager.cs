﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poet
{

    /// <summary>
    /// AnimationCurve manager
    /// </summary>
    public class PoetAnimationCurveManager : SSC.SingletonMonoBehaviour<PoetAnimationCurveManager>
    {

        [Serializable]
        protected class IdAndAnimationCurve : IdentifierInterface
        {

            public string identifier = "";
            public AnimationCurve curve = null;

            public string getIdentifier()
            {
                return this.identifier;
            }

        }

        /// <summary>
        /// IdAndAnimationCurve list
        /// </summary>
        [SerializeField]
        [Tooltip("IdAndAnimationCurve list")]
        protected List<IdAndAnimationCurve> m_animCurveList = new List<IdAndAnimationCurve>();

        /// <summary>
        /// IdAndAnimationCurve dictionary
        /// </summary>
        protected Dictionary<string, IdAndAnimationCurve> m_animCurveDict = new Dictionary<string, IdAndAnimationCurve>();

        /// <summary>
        /// Called in Awake
        /// </summary>
        // -------------------------------------------------------------------------------------------------
        protected override void initOnAwake()
        {

            this.m_animCurveDict = Funcs.listToDictionary<IdAndAnimationCurve>(this.m_animCurveList);

        }

        /// <summary>
        /// Evaluate
        /// </summary>
        /// <param name="identifier">easing identifier</param>
        /// <param name="time01">normalized time</param>
        /// <returns>value</returns>
        // -------------------------------------------------------------------------------------------------
        public float evaluate(string identifier, float time01)
        {

            if(this.m_animCurveDict.ContainsKey(identifier))
            {
                return this.m_animCurveDict[identifier].curve.Evaluate(time01);
            }

            else
            {
                Debug.LogWarning("Not contains a key : " + identifier);
            }

            return Mathf.Clamp01(time01);

        }

    }

}
