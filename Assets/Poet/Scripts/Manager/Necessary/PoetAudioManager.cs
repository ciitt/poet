﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poet
{

    /// <summary>
    /// Poet audio manager
    /// </summary>
    public class PoetAudioManager : SSC.SingletonMonoBehaviour<PoetAudioManager>
    {

        /// <summary>
        /// Audio info for nowloading
        /// </summary>
        [Serializable]
        protected class NowLoadingAudio
        {

            /// <summary>
            /// NowLoading identifier
            /// </summary>
            public string nowLoadingIdentifier = "";

            /// <summary>
            /// Preloaded bgm id
            /// </summary>
            public string preloadedBgmId = "";

            /// <summary>
            /// Wait for bgm to finish
            /// </summary>
            public bool waitForBgmToFinish = false;

        }

        /// <summary>
        /// PoetAudioScript for BGM
        /// </summary>
        [SerializeField]
        [Tooltip("PoetAudioScript for BGM")]
        protected PoetAudioScript m_refBGM;

        /// <summary>
        /// PoetAudioScript for SE
        /// </summary>
        [SerializeField]
        [Tooltip("PoetAudioScript for SE")]
        protected PoetAudioScript m_refSE;

        /// <summary>
        /// PoetAudioScript for Voice
        /// </summary>
        [SerializeField]
        [Tooltip("PoetAudioScript for Voice")]
        protected PoetAudioScript m_refVoice;

        /// <summary>
        /// PoetAudioScript for Environment
        /// </summary>
        [SerializeField]
        [Tooltip("PoetAudioScript for Environment")]
        protected PoetAudioScript m_refEnvironment;

        /// <summary>
        /// PoetAudioScript for Extra
        /// </summary>
        [SerializeField]
        [Tooltip("PoetAudioScript for Extra")]
        protected PoetAudioScript m_refExtra;

        /// <summary>
        /// Reference to ReadingTextSoundScript
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to ReadingTextSoundScript")]
        protected ReadingTextSoundScript m_refReadingTextSoundScript;

        /// <summary>
        /// Csv header for character voice ID
        /// </summary>
        [SerializeField]
        [Tooltip("Csv header for character voice ID")]
        protected string m_csvHeaderForCharacterVoiceId = "Character Voice ID";

        /// <summary>
        /// NowLoadingAudio list
        /// </summary>
        [SerializeField]
        [Tooltip("NowLoadingAudio list")]
        protected List<NowLoadingAudio> m_nowLoadingAudioList = new List<NowLoadingAudio>();

        /// <summary>
        /// Current voice boost
        /// </summary>
        protected float m_currentVoiceBoost = 1.0f;

        /// <summary>
        /// NowLoadingAudio dictionary
        /// </summary>
        protected Dictionary<string, NowLoadingAudio> m_nowLoadingAudioDict = new Dictionary<string, NowLoadingAudio>();

        // ----------------------------------------------------------------------------------------------

        /// <summary>
        /// PoetAudioScript for BGM
        /// </summary>
        public PoetAudioScript refBGM { get { return this.m_refBGM; } }

        /// <summary>
        /// PoetAudioScript for SE
        /// </summary>
        public PoetAudioScript refSE { get { return this.m_refSE; } }

        /// <summary>
        /// PoetAudioScript for Voice
        /// </summary>
        public PoetAudioScript refVoice { get { return this.m_refVoice; } }

        /// <summary>
        /// PoetAudioScript for Environment
        /// </summary>
        public PoetAudioScript refEnvironment { get { return this.m_refEnvironment; } }

        /// <summary>
        /// PoetAudioScript for Extra
        /// </summary>
        public PoetAudioScript refExtra { get { return this.m_refExtra; } }

        /// <summary>
        /// Current voice boost
        /// </summary>
        public float currentVoiceBoost { get { return this.m_currentVoiceBoost; } set { this.m_currentVoiceBoost = value; } }

        // ----------------------------------------------------------------------------------------------

        /// <summary>
        /// Called in Awake
        /// </summary>
        // ----------------------------------------------------------------------------------------------
        protected override void initOnAwake()
        {

        }

        /// <summary>
        /// Start
        /// </summary>
        // ----------------------------------------------------------------------------------------------
        protected virtual void Start()
        {

            // addSceneChangeStateReceiver
            {

                PoetReduxManager prm = PoetReduxManager.Instance as PoetReduxManager;

                prm.addSceneChangeStateReceiver(this.onSceneChangeStateReceiver);
                prm.addPoetSequenceStateReceiver(this.onPoetSequenceState);

            }

            // m_nowLoadingAudioDict
            {

                foreach (var val in this.m_nowLoadingAudioList)
                {

                    if(!this.m_nowLoadingAudioDict.ContainsKey(val.nowLoadingIdentifier) && !string.IsNullOrEmpty(val.nowLoadingIdentifier))
                    {
                        this.m_nowLoadingAudioDict.Add(val.nowLoadingIdentifier, val);
                    }

#if UNITY_EDITOR

                    else
                    {
                        Debug.LogWarning("(#if UNITY_EDITOR) m_nowLoadingAudioDict already contains a key : " + val.nowLoadingIdentifier);
                    }
                    
#endif

                }

            }

        }

        /// <summary>
        /// Is now voice playing
        /// </summary>
        /// <returns></returns>
        // ----------------------------------------------------------------------------------------------
        public bool isVoicePlaying()
        {
            
            if(this.m_refVoice)
            {
                return this.m_refVoice.isPlaying();
            }

            return false;

        }

        /// <summary>
        /// PoetSequenceState receiver
        /// </summary>
        /// <param name="psState">PoetSequenceState</param>
        // ------------------------------------------------------------------------------------------
        protected virtual void onPoetSequenceState(PoetSequenceState psState)
        {

            if (psState.stateEnum == PoetSequenceState.StateEnum.PreProcess)
            {

                if (psState.headerAndCells.ContainsKey(this.m_csvHeaderForCharacterVoiceId))
                {
                    this.setCurrentVoiceId(psState.headerAndCells[this.m_csvHeaderForCharacterVoiceId]);
                }

            }

            else if (psState.stateEnum == PoetSequenceState.StateEnum.MainProcess)
            {
                
                if(TextLogManager.isAvailable())
                {

                    if (psState.headerAndCells.ContainsKey(this.m_csvHeaderForCharacterVoiceId))
                    {
                        TextLogManager.Instance.updateCharacterVoiceId(psState.headerAndCells[this.m_csvHeaderForCharacterVoiceId]);
                    }

                }

            }

            else if (psState.stateEnum == PoetSequenceState.StateEnum.FinishOperation)
            {
                this.m_currentVoiceBoost = 1.0f;
            }

        }

        /// <summary>
        /// Set current voice id
        /// </summary>
        /// <param name="voiceId">voice id</param>
        // -----------------------------------------------------------------------------------------------
        public void setCurrentVoiceId(string voiceId)
        {

            PoetConfigDataSO conf = SystemManager.Instance.configDataSO<PoetConfigDataSO>();

            if (conf.characterVoiceIdAndVolumeDict.ContainsKey(voiceId))
            {
                this.m_currentVoiceBoost = conf.characterVoiceIdAndVolumeDict[voiceId].volume01;
            }

            else
            {
                this.m_currentVoiceBoost = 1.0f;
            }

        }

        /// <summary>
        /// SceneChangeState receiver
        /// </summary>
        /// <param name="scState">SceneChangeState</param>
        // -----------------------------------------------------------------------------------------------
        protected virtual void onSceneChangeStateReceiver(SSC.SceneChangeState scState)
        {

            if (scState.stateEnum == SSC.SceneChangeState.StateEnum.NowLoadingIntro)
            {

                this.playAudio(this.m_refBGM, null, 0.5f, 0.0f);
                this.playAudio(this.m_refSE, null, 0.5f, 0.0f);
                this.playAudio(this.m_refVoice, null, 0.5f, 0.0f);
                this.playAudio(this.m_refEnvironment, null, 0.5f, 0.0f);
                this.playAudio(this.m_refExtra, null, 0.5f, 0.0f);

                PoetSceneChangeManager.Instance.addLockToBefore(this);
                Invoke("resetNowLoadingLockBefore", 0.5f);

            }

            else if(scState.stateEnum == SSC.SceneChangeState.StateEnum.NowLoadingMain)
            {

                NowLoadingAudio nla = this.getTargetNowLoadingAudio();

                if(nla != null && this.m_refBGM)
                {

                    if(nla.waitForBgmToFinish)
                    {

                        var audio = this.m_refBGM.getPreloadedAudioClip(nla.preloadedBgmId);

                        if (audio)
                        {
                            this.m_refBGM.refAudioSource.loop = false;
                            PoetSceneChangeManager.Instance.addLockToAfter(this);
                            Invoke("resetNowLoadingLockAfter", audio.length);
                            this.m_refBGM.playPreloadedAudio(nla.preloadedBgmId, 0.0f, false);
                        }

                    }

                    else
                    {
                        this.m_refBGM.playPreloadedAudio(nla.preloadedBgmId, 0.0f, false);
                    }

                }

            }

            else if (scState.stateEnum == SSC.SceneChangeState.StateEnum.AllStartupsDone)
            {

                NowLoadingAudio nla = this.getTargetNowLoadingAudio();

                if(nla != null && this.m_refBGM)
                {

                    if (!nla.waitForBgmToFinish)
                    {
                        this.m_refBGM.stop(0.0f);
                    }

                }

            }

        }

        /// <summary>
        /// Reset now loading lock before
        /// </summary>
        // -----------------------------------------------------------------------------------------------
        protected void resetNowLoadingLockBefore()
        {
            PoetSceneChangeManager.Instance.removeLockFromBefore(this);
        }

        /// <summary>
        /// Reset now loading lock after
        /// </summary>
        // -----------------------------------------------------------------------------------------------
        protected void resetNowLoadingLockAfter()
        {

            PoetSceneChangeManager.Instance.removeLockFromAfter(this);

            if(this.m_refBGM)
            {
                this.m_refBGM.stop(0.0f, 0.0f);
                this.m_refBGM.refAudioSource.loop = true;
            }

        }

        /// <summary>
        /// Get NowLoadingAudio by current NowLoading identifier
        /// </summary>
        /// <returns>NowLoadingAudio</returns>
        // -----------------------------------------------------------------------------------------------
        protected NowLoadingAudio getTargetNowLoadingAudio()
        {

            if(this.m_nowLoadingAudioDict.ContainsKey(PoetSceneChangeManager.Instance.currentNowLoadingUiIdentifier))
            {
                return this.m_nowLoadingAudioDict[PoetSceneChangeManager.Instance.currentNowLoadingUiIdentifier];
            }

            return null;

        }

        /// <summary>
        /// Play audio
        /// </summary>
        /// <param name="refScript">PoetAudioScript</param>
        /// <param name="audioClip">AudioClip</param>
        /// <param name="fadeOutSeconds">fade out seconds</param>
        /// <param name="fadeInSeconds">fade in seconds</param>
        /// <param name="delaySeconds">delay seconds</param>
        // -----------------------------------------------------------------------------------------------
        protected void playAudio(PoetAudioScript refScript, AudioClip audioClip, float fadeOutSeconds, float fadeInSeconds)
        {

            if (refScript)
            {
                refScript.playAudio(audioClip, fadeOutSeconds, fadeInSeconds, 0.0f);
            }

        }

        /// <summary>
        /// Get target volume
        /// </summary>
        /// <param name="refScript">reference to PoetAudioScript</param>
        /// <returns>volume</returns>
        // -----------------------------------------------------------------------------------------------
        public virtual float targetVolume(PoetAudioScript refScript)
        {

            if(!refScript)
            {
                return 1.0f;
            }

            // ----------------

            PoetConfigDataSO conf = SystemManager.Instance.configDataSO<PoetConfigDataSO>();

            // ----------------

            if (refScript == this.m_refBGM)
            {
                return conf.bgmVolume01;
            }

            else if (refScript == this.m_refSE)
            {
                return conf.seVolume01;
            }

            else if (refScript == this.m_refVoice)
            {
                return conf.voiceVolume01 * this.m_currentVoiceBoost;
            }

            else if (refScript == this.m_refEnvironment)
            {
                return conf.environmentVolume01;
            }

            else if (refScript == this.m_refExtra)
            {
                return conf.extraVolume01;
            }

            return 1.0f;

        }

        /// <summary>
        /// Set loop for ReadingTextSoundScript
        /// </summary>
        /// <param name="value">loop</param>
        // -----------------------------------------------------------------------------------------------
        public void setReadingTextSoundLoop(bool value)
        {

            if (this.m_refReadingTextSoundScript)
            {
                this.m_refReadingTextSoundScript.setLoop(value);
            }

        }

        /// <summary>
        /// Set active for ReadingTextSoundScript
        /// </summary>
        /// <param name="value">active</param>
        // -----------------------------------------------------------------------------------------------
        public void setReadingTextSoundActive(bool value)
        {

            if (this.m_refReadingTextSoundScript)
            {
                this.m_refReadingTextSoundScript.setReadingTextSoundActive(value);
            }

        }

        /// <summary>
        /// Set active for ReadingTextSoundScript
        /// </summary>
        /// <param name="value">active</param>
        // -----------------------------------------------------------------------------------------------
        public bool isReadingTextSoundActive()
        {

            if (this.m_refReadingTextSoundScript)
            {
                return this.m_refReadingTextSoundScript.isThisActive;
            }

            return false;

        }

        /// <summary>
        /// Stop all audios
        /// </summary>
        // -----------------------------------------------------------------------------------------------
        public void stopAllAudio()
        {

            if(this.m_refBGM) { this.m_refBGM.stop(0.0f); }
            else if (this.m_refSE) { this.m_refSE.stop(0.0f); }
            else if (this.m_refVoice) { this.m_refVoice.stop(0.0f); }
            else if (this.m_refEnvironment) { this.m_refEnvironment.stop(0.0f); }
            else if (this.m_refExtra) { this.m_refExtra.stop(0.0f); }

        }

    }

}
