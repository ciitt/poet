﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poet
{

    /// <summary>
    /// Poet dialog message manager
    /// </summary>
    public partial class PoetDialogMessageManager : SSC.SingletonMonoBehaviour<PoetDialogMessageManager>
    {

        /// <summary>
        /// Error message for save data error
        /// </summary>
        /// <returns>any</returns>
        // ------------------------------------------------------------------------------------
        public virtual System.Object saveDataErrorMessage(string rawErrorMessage)
        {

            // SystemLanguage sl = SystemManager.Instance.systemLanguage;

            SSC.DialogMessages messages = new SSC.DialogMessages();

            messages.category = SSC.DialogMessages.MessageCategory.Error;
            messages.title = "Error";
            messages.mainMessage = "Save error.";
            messages.anyMessage = rawErrorMessage;

            return messages;

        }

        /// <summary>
        /// Error message for load data error
        /// </summary>
        /// <returns>any</returns>
        // ------------------------------------------------------------------------------------
        public virtual System.Object loadDataErrorMessage(string rawErrorMessage)
        {

            // SystemLanguage sl = SystemManager.Instance.systemLanguage;

            SSC.DialogMessages messages = new SSC.DialogMessages();

            messages.category = SSC.DialogMessages.MessageCategory.Error;
            messages.title = "Error";
            messages.mainMessage = "Load error.";
            messages.anyMessage = rawErrorMessage;

            return messages;

        }


        /// <summary>
        /// Error message for AssetBundle startup error
        /// </summary>
        /// <returns>any</returns>
        // ------------------------------------------------------------------------------------
        public virtual System.Object assetBundleStartupErrorMessage(string errorUrl, string rawErrorMessage)
        {

            // SystemLanguage sl = SystemManager.Instance.systemLanguage;

            SSC.DialogMessages messages = new SSC.DialogMessages();

            messages.category = SSC.DialogMessages.MessageCategory.Error;
            messages.title = "Error";
            messages.urlIfNeeded = errorUrl;
            messages.mainMessage = "AssetBundle startup error.";
            messages.subMessage = "Retry ?";
            messages.anyMessage = rawErrorMessage;

            return messages;

        }

        /// <summary>
        /// Error message for Www startup error
        /// </summary>
        /// <returns>any</returns>
        // ------------------------------------------------------------------------------------
        public virtual System.Object wwwStartupErrorMessage(string errorUrl, string rawErrorMessage)
        {

            // SystemLanguage sl = SystemManager.Instance.systemLanguage;

            SSC.DialogMessages messages = new SSC.DialogMessages();

            messages.category = SSC.DialogMessages.MessageCategory.Error;
            messages.title = "Error";
            messages.urlIfNeeded = errorUrl;
            messages.mainMessage = "WWW startup error.";
            messages.subMessage = "Retry ?";
            messages.anyMessage = rawErrorMessage;

            return messages;

        }

        /// <summary>
        /// Error message for IE startup error
        /// </summary>
        /// <returns>any</returns>
        // ------------------------------------------------------------------------------------
        public virtual System.Object ieStartupErrorMessage(string rawErrorMessage)
        {

            // SystemLanguage sl = SystemManager.Instance.systemLanguage;

            SSC.DialogMessages messages = new SSC.DialogMessages();

            messages.category = SSC.DialogMessages.MessageCategory.Error;
            messages.title = "Error";
            messages.mainMessage = "IEnumerator startup error.";
            messages.subMessage = "Retry ?";
            messages.anyMessage = rawErrorMessage;

            return messages;

        }

        /// <summary>
        /// Error message for user progress data error
        /// </summary>
        /// <returns>any</returns>
        // ------------------------------------------------------------------------------------
        public virtual System.Object convertUserProgressDataErrorMessage(string gameObjectName, string scriptName)
        {

            // SystemLanguage sl = SystemManager.Instance.systemLanguage;

            SSC.DialogMessages messages = new SSC.DialogMessages();

            messages.category = SSC.DialogMessages.MessageCategory.Error;
            messages.title = "Error";
            messages.mainMessage = "Convert data error.";
            messages.anyMessage = gameObjectName + " : " + scriptName;

            return messages;

        }

    }

}
