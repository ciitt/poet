﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

#if UNITY_EDITOR

using System.Linq;

#endif

namespace Poet
{

    /// <summary>
    /// Manager of Poet system
    /// </summary>
    public class SystemManager : SSC.SingletonMonoBehaviour<SystemManager>
    {

        /// <summary>
        /// DateTime format
        /// </summary>
        public static string DateTimeFormat = "yyyy/MM/dd HH:mm:ss";

        /// <summary>
        /// Target frame rate
        /// </summary>
        [SerializeField]
        [Tooltip("Target frame rate")]
        protected int m_targetFrameRate = 60;

        /// <summary>
        /// Save and load PoetConfigDataSO file
        /// </summary>
        [SerializeField]
        [Tooltip("Save and load PoetConfigDataSO file")]
        protected bool m_saveAndLoadConfigFile = true;

        /// <summary>
        /// Use password for data
        /// </summary>
        [SerializeField]
        [Tooltip("Use password for data")]
        protected bool m_usePasswordForData = true;

        /// <summary>
        /// Password for data
        /// </summary>
        [SerializeField]
        [Tooltip("Password for data")]
        protected string m_passwordForData = "";

        /// <summary>
        /// User progress data file name prefix
        /// </summary>
        [SerializeField]
        [Tooltip("User progress data file name prefix")]
        protected string m_userProgressDataFileNamePrefix = "data_";

        /// <summary>
        /// Data file extension
        /// </summary>
        [SerializeField]
        [Tooltip("Data file extension")]
        protected string m_dataFileExtension = "dat";

        /// <summary>
        /// SetNoBackupFlag in iOS
        /// </summary>
        [SerializeField]
        [Tooltip("SetNoBackupFlag in iOS")]
        protected bool m_setNoBackupFlagInIos = true;

        /// <summary>
        /// Default config data
        /// </summary>
        [SerializeField]
        [Tooltip("Default config data")]
        protected PoetConfigDataSO m_configDataSO;

        /// <summary>
        /// Description text length for user progress data
        /// </summary>
        [SerializeField]
        [Tooltip("Description text length for user progress data")]
        [Range(0, 500)]
        protected int m_descriptionTextLength = 50;

        /// <summary>
        /// Thumbnail width for user progress data
        /// </summary>
        [SerializeField]
        [Tooltip("Thumbnail width for user progress data")]
        protected int m_thumbnailWidth = 128;

        /// <summary>
        /// Thumbnail height for user progress data
        /// </summary>
        [SerializeField]
        [Tooltip("Thumbnail height for user progress data")]
        protected int m_thumbnailHeight = 128;

        /// <summary>
        /// Shown when quick save finished
        /// </summary>
        [SerializeField]
        [Tooltip("Shown when quick save finished")]
        protected SSC.UiControllerScript m_quickSaveFinishedUi = null;

        /// <summary>
        /// Use PlayerPrefs
        /// </summary>
        [SerializeField]
        [Tooltip("If true, use PlayerPrefs. If false, use harddisk")]
        protected bool m_usePlayerPrefs = true;

        /// <summary>
        /// Update and save already read text information to config file
        /// </summary>
        [SerializeField]
        [Tooltip("Update and save already read text information to config file")]
        protected bool m_updateAndSaveAlreadyReadText = true;

        /// <summary>
        /// Auto mode
        /// </summary>
        protected bool m_auto = false;

        /// <summary>
        /// Skip
        /// </summary>
        protected bool m_skip = false;

        /// <summary>
        /// Data number and user progress data
        /// </summary>
        protected Dictionary<string, PoetUserProgressDataSO> m_dataId_userProgressData = new Dictionary<string, PoetUserProgressDataSO>();

        /// <summary>
        /// IEnumerator for createUserProgressData
        /// </summary>
        protected IEnumerator m_createUserProgressDataIE = null;

        /// <summary>
        /// Current temporary user progress data
        /// </summary>
        protected PoetUserProgressDataSO m_currentTemporarySavedUserProgressData = null;

        /// <summary>
        /// Current user progress data
        /// </summary>
        protected PoetUserProgressDataSO m_quickSavedUserProgressData = null;

        /// <summary>
        /// IEnumerator saveDataIE
        /// </summary>
        protected IEnumerator m_saveDataIE = null;

        /// <summary>
        /// Current user progress data description
        /// </summary>
        protected string m_currentUserProgressDataDescription = "";

        /// <summary>
        /// Startup done
        /// </summary>
        protected bool m_startupDone = false;

        /// <summary>
        /// Screen resolution for fullscreen
        /// </summary>
        protected Vector2 m_screenFullScreenSize = Vector2.zero;

        /// <summary>
        /// Screen resolution for window size
        /// </summary>
        protected Vector2 m_screenWindowSize = Vector2.zero;

        // -------------------------------------------------------------------------------------

        /// <summary>
        /// Save and load PoetConfigDataSO file
        /// </summary>
        public bool saveAndLoadConfigFileFlag { get { return this.m_saveAndLoadConfigFile; } }

        /// <summary>
        /// SystemLanguage getter setter
        /// </summary>
        public SystemLanguage systemLanguage { get { return this.m_configDataSO.systemLanguage; } }

        /// <summary>
        /// Current auto mode
        /// </summary>
        public bool isAuto { get { return this.m_auto; } set { this.m_auto = value; } }

        /// <summary>
        /// Current skip mode
        /// </summary>
        public bool isSkip { get { return this.m_skip; } set { this.m_skip = value; } }

        /// <summary>
        /// Description text length for user progress data
        /// </summary>
        public int descriptionTextLength { get { return this.m_descriptionTextLength; } set { this.m_descriptionTextLength = value; } }

        /// <summary>
        /// Startup done
        /// </summary>
        public bool isStartupDone { get { return this.m_startupDone; } }

        /// <summary>
        /// Screen resolution for fullscreen
        /// </summary>
        public Vector2 screenFullScreenSize { get { return this.m_screenFullScreenSize; } }

        /// <summary>
        /// Screen resolution for window
        /// </summary>
        public Vector2 screenWindowSize { get { return this.m_screenWindowSize; } }

        // -------------------------------------------------------------------------------------

        /// <summary>
        /// Called in Awake
        /// </summary>
        // -------------------------------------------------------------------------------------
        protected override void initOnAwake()
        {

            // targetFrameRate
            {

#if !UNITY_EDITOR && UNITY_WEBGL
                Application.targetFrameRate = 0;
#else
                Application.targetFrameRate = this.m_targetFrameRate;
#endif

            }

            if (string.IsNullOrEmpty(this.m_userProgressDataFileNamePrefix))
            {
                Debug.LogWarning("m_userProgressDataFileNamePrefix is empty");
                this.m_userProgressDataFileNamePrefix = "data_";
            }

            if (string.IsNullOrEmpty(this.m_dataFileExtension))
            {
                Debug.LogWarning("m_dataFileExtension is empty");
                this.m_dataFileExtension = "dat";
            }

            if (!this.m_configDataSO)
            {
                Debug.LogWarning("m_configDataSO is null");
                this.m_configDataSO = ScriptableObject.CreateInstance<PoetConfigDataSO>();
            }

            // m_currentTemporarySavedUserProgressData m_quickSavedUserProgressData
            {
                this.m_currentTemporarySavedUserProgressData = ScriptableObject.CreateInstance<PoetUserProgressDataSO>();
                this.m_quickSavedUserProgressData = ScriptableObject.CreateInstance<PoetUserProgressDataSO>();
            }

#if UNITY_EDITOR

            if (!this.m_usePasswordForData)
            {
                Debug.LogWarning("(#if UNITY_EDITOR) m_usePasswordForData is false : " + this.gameObject.name);
            }

#endif

            // m_screenFullScreenSize m_screenWindowSize
            {

                if (Screen.fullScreen)
                {
                    this.m_screenFullScreenSize = new Vector2(Screen.width, Screen.height);
                }

                else
                {
                    this.m_screenFullScreenSize = new Vector2(Screen.currentResolution.width, Screen.currentResolution.height);
                }
                
                this.m_screenWindowSize = new Vector2(Screen.width, Screen.height);

            }

        }

        /// <summary>
        /// Start
        /// </summary>
        // -------------------------------------------------------------------------------------
        protected virtual IEnumerator Start()
        {

            PoetReduxManager.Instance.addSceneChangeStateReceiver(this.onSceneChangeStateReceiver);

            if (this.m_saveAndLoadConfigFile)
            {
                yield return this.readConfigData();
            }

            else
            {
                this.m_configDataSO = Instantiate(this.m_configDataSO);
            }

            // m_configDataSO initDictionary
            {
                this.m_configDataSO.initDictionary();
            }

            //
            {
                yield return this.readUserProgressData<PoetUserProgressDataSO>("qs", (result, error) =>
                {
                    
                    if (result != null)
                    {
                        this.m_quickSavedUserProgressData = result;
                    }
                    
                });
            }

            yield return null;

            // setSystemLanguage
            {
                this.setSystemLanguage(this.m_configDataSO.systemLanguage);
            }

            // windowMode
            {
                this.m_configDataSO.windowMode =
                    (Screen.fullScreen) ? PoetConfigDataSO.WindowMode.FullScreen : PoetConfigDataSO.WindowMode.Window;
            }

            // master volume
            {
                AudioListener.volume = this.m_configDataSO.masterVolume01;
            }

            this.m_startupDone = true;

        }

        /// <summary>
        /// Read config data
        /// </summary>
        /// <returns>IEnumerator</returns>
        // -------------------------------------------------------------------------------------
        protected virtual IEnumerator readConfigData()
        {

            yield return this.readDataIE<PoetConfigDataSO>(this.poetConfigFilePath(), (result, error) =>
            {

                if (result != null)
                {

                    this.m_configDataSO = result;

#if UNITY_EDITOR
                    Debug.Log("(#if UNITY_EDITOR) Read config data from " + (this.m_usePlayerPrefs ? "PlayerPrefs" : "hard disk"));
#endif


                }

                else
                {

                    if (!string.IsNullOrEmpty(error))
                    {
                        Debug.LogError(error);
                    }

                    this.m_configDataSO = Instantiate(this.m_configDataSO);

                }

            }, true);

        }

        /// <summary>
        /// Config data
        /// </summary>
        // -----------------------------------------------------------------------------------------------
        public virtual T configDataSO<T>() where T : PoetConfigDataSO
        {
            return this.m_configDataSO as T;
        }

        /// <summary>
        /// SceneChangeState receiver
        /// </summary>
        /// <param name="scState">SceneChangeState</param>
        // -----------------------------------------------------------------------------------------------
        protected virtual void onSceneChangeStateReceiver(SSC.SceneChangeState scState)
        {

            if(scState.stateEnum == SSC.SceneChangeState.StateEnum.NowLoadingIntro)
            {
                this.resetAutoAndSkipModes();
            }

            else if (scState.stateEnum == SSC.SceneChangeState.StateEnum.NowLoadingOutro)
            {

#if !UNITY_STANDALONE

                if (PoetSceneChangeManager.Instance.titleSceneName == PoetSceneChangeManager.Instance.nowLoadingSceneName)
                {
                    this.savePoetConfigData();
                }
                    
#endif

            }

        }

        /// <summary>
        /// Create new current user data
        /// </summary>
        // -------------------------------------------------------------------------------------
        public virtual void createNewCurrentUserProgressData(Action callbackAtCreatingFinished)
        {

            if (this.m_createUserProgressDataIE != null)
            {
                return;
            }

            this.m_createUserProgressDataIE = this.createNewCurrentUserProgressData<PoetUserProgressDataSO>(this.m_thumbnailWidth, this.m_thumbnailHeight, () =>
            {
                if(callbackAtCreatingFinished != null)
                {
                    callbackAtCreatingFinished();
                }
            });

            StartCoroutine(this.m_createUserProgressDataIE);

        }

        /// <summary>
        /// Create new current user data and show Save UI
        /// </summary>
        // -------------------------------------------------------------------------------------
        public virtual void createNewCurrentUserProgressDataAndShowSaveUI()
        {

            if (this.m_createUserProgressDataIE != null)
            {
                return;
            }

            this.m_createUserProgressDataIE = this.createNewCurrentUserProgressData<PoetUserProgressDataSO>(this.m_thumbnailWidth, this.m_thumbnailHeight, () =>
            {
                SSC.CommonUiManager.Instance.showUi(PoetUiIdentifiers.Common.Save, true, false);
            });

            StartCoroutine(this.m_createUserProgressDataIE);

        }

        /// <summary>
        /// Is available quick save data
        /// </summary>
        // -------------------------------------------------------------------------------------
        public bool isAvailableQuickSaveData()
        {
            return !this.m_quickSavedUserProgressData.isDefaultData();
        }

        /// <summary>
        /// Start quick save
        /// </summary>
        // -------------------------------------------------------------------------------------
        protected virtual void quickSaveInternal()
        {

            this.saveCurrentUserProgressData("qs", (result) =>
            {

                if (!string.IsNullOrEmpty(result))
                {
                    SSC.DialogManager.Instance.showOkDialog(
                    PoetDialogMessageManager.Instance.saveDataErrorMessage(result),
                    null
                    );
                }

                else
                {

                    if (this.m_quickSaveFinishedUi)
                    {
                        this.m_quickSaveFinishedUi.startShowing(true, 1.0f, null);
                    }

                    this.m_quickSavedUserProgressData = this.m_currentTemporarySavedUserProgressData;

                }

            });

        }

        /// <summary>
        /// Quick load
        /// </summary>
        // -------------------------------------------------------------------------------------
        public virtual void quickLoad()
        {

            if(!this.isAvailableQuickSaveData())
            {
                return;
            }

            // ---------------------

            if(this.m_configDataSO.showQuickLoadDialog)
            {

                PoetDialogManager.Instance.showYesNoDialog(
                    PoetDialogMessageManager.Instance.quickLoadMessage(),
                    () =>
                    {
                        (PoetSceneChangeManager.Instance as PoetSceneChangeManager).loadSceneWithUserProgressData(this.m_quickSavedUserProgressData);
                    },
                    null
                    );

            }

            else
            {
                (PoetSceneChangeManager.Instance as PoetSceneChangeManager).loadSceneWithUserProgressData(this.m_quickSavedUserProgressData);
            }

        }


        /// <summary>
        /// Quick save
        /// </summary>
        // -------------------------------------------------------------------------------------
        public virtual void quickSave()
        {

            if (this.m_createUserProgressDataIE != null)
            {
                return;
            }

            // ---------------

            if (this.m_configDataSO.showQuickSaveDialog)
            {

                this.m_createUserProgressDataIE = this.createNewCurrentUserProgressData<PoetUserProgressDataSO>(this.m_thumbnailWidth, this.m_thumbnailHeight, () =>
                {

                    SSC.DialogManager.Instance.showYesNoDialog(
                        PoetDialogMessageManager.Instance.quickSaveMessage(),
                        () =>
                        {
                            this.quickSaveInternal();
                        },
                        null
                        );

                });

                StartCoroutine(this.m_createUserProgressDataIE);

            }

            else
            {
                this.quickSaveInternal();
            }

        }

        /// <summary>
        /// Create new current user data
        /// </summary>
        /// <returns>IEnumerator</returns>
        // -------------------------------------------------------------------------------------
        protected virtual IEnumerator createNewCurrentUserProgressData<T>(int thumbnailWidth, int thumbnailHeight, Action nextAction) where T : PoetUserProgressDataSO
        {

            // clear
            {
                this.m_currentTemporarySavedUserProgressData = ScriptableObject.CreateInstance<T>();
            }

            // UserProgressDataSignalWatcher
            {

                PoetReduxManager prm = PoetReduxManager.Instance as PoetReduxManager;
                prm.UserProgressDataSignalWatcher.state().sendCreateSignal(
                    prm.UserProgressDataSignalWatcher,
                    this.addDataToCurrentUserProgressData
                    );

            }

            // screenshotPngBytes
            {

                if (thumbnailWidth > 0 && thumbnailHeight > 0)
                {

                    yield return new WaitForEndOfFrame();

                    int width = Screen.width;
                    int height = Screen.height;

                    Texture2D tex = new Texture2D(width, height, TextureFormat.RGB24, true);

                    tex.ReadPixels(new Rect(0, 0, width, height), 0, 0);
                    tex.Apply();

                    Texture2D newTex = Funcs.resizeTexture(tex, thumbnailWidth, thumbnailHeight, FilterMode.Bilinear);

                    this.m_currentTemporarySavedUserProgressData.screenshotPngBytes = newTex.EncodeToPNG();

                    Destroy(tex);
                    Destroy(newTex);

                }

            }

            // m_createUserProgressDataIE
            {
                this.m_createUserProgressDataIE = null;
            }

            // nextAction
            {
                if (nextAction != null)
                {
                    nextAction();
                }
            }

        }

        /// <summary>
        /// Create key path
        /// </summary>
        /// <param name="trans">Transform</param>
        /// <param name="mb"></param>
        /// <returns>path</returns>
        // -------------------------------------------------------------------------------------
        public string createKeyPath(Transform trans, MonoBehaviour mb)
        {

            if (!trans || !mb)
            {
                return "";
            }

            // ---------------------------------

            //return Funcs.createHierarchyPath(trans) + "/" + trans.GetSiblingIndex() + "/" + mb.GetType().Name;
            return Funcs.createHierarchyPath(trans) + "/" + mb.GetType().Name;

        }

        /// <summary>
        /// Add data to current user progress data
        /// </summary>
        /// <param name="trans">Transform</param>
        /// <param name="mb">MonoBehaviour</param>
        /// <param name="serializableObject">data</param>
        // -------------------------------------------------------------------------------------
        public void addDataToCurrentUserProgressData(Transform trans, MonoBehaviour mb, string json)
        {

            if (!trans || !mb)
            {
                return;
            }

            // ------------------

            string key = this.createKeyPath(trans, mb);

            if (!string.IsNullOrEmpty(key))
            {

#if UNITY_EDITOR

                if (this.m_currentTemporarySavedUserProgressData.dataList.Any(val => val.key == key))
                {
                    Debug.LogError("(#if UNITY_EDITOR) dataList already contains a key : " + key);
                }

#endif

                this.m_currentTemporarySavedUserProgressData.dataList.Add(new KeyAndData(key, json));

            }

        }

        /// <summary>
        /// Save current user progress data
        /// </summary>
        /// <param name="dataNumber"></param>
        // -------------------------------------------------------------------------------------
        public virtual void saveCurrentUserProgressData(string dataId, Action<string> resultCallback)
        {

            if (this.m_saveDataIE != null)
            {
                return;
            }

            // ----------------

#if UNITY_EDITOR

            // debug
            {
                Funcs.listToDictionary<KeyAndData>(this.m_currentTemporarySavedUserProgressData.dataList);
            }

#endif

            this.m_currentTemporarySavedUserProgressData.sceneName = SceneManager.GetActiveScene().name;
            this.m_currentTemporarySavedUserProgressData.dateTimeStr = DateTime.Now.ToString(DateTimeFormat);
            this.m_currentTemporarySavedUserProgressData.descriptionText = this.m_currentUserProgressDataDescription;

            string path = this.saveLoadDataPath(dataId);

            this.m_saveDataIE = this.saveDataIE(path, this.m_currentTemporarySavedUserProgressData,
                (result) =>
                {

                    if (string.IsNullOrEmpty(result))
                    {
                        if (this.m_dataId_userProgressData.ContainsKey(dataId))
                        {
                            this.m_dataId_userProgressData[dataId] = this.m_currentTemporarySavedUserProgressData;
                        }

                        else
                        {
                            this.m_dataId_userProgressData.Add(dataId, this.m_currentTemporarySavedUserProgressData);
                        }
                    }

                    resultCallback(result);

                });

            StartCoroutine(this.m_saveDataIE);

        }

        /// <summary>
        /// Save data to PlayerPrefs
        /// </summary>
        /// <param name="filePath">file path</param>
        /// <param name="data">data</param>
        // -------------------------------------------------------------------------------------
        protected virtual void saveDataToPlayerPrefs(string filePath, System.Object data)
        {

            if (string.IsNullOrEmpty(filePath) || data == null)
            {
                return;
            }

            // -----------------

            string key = Path.GetFileNameWithoutExtension(filePath);
            string json = JsonUtility.ToJson(data);

            try
            {

                if (this.m_usePasswordForData)
                {

                    byte[] bytes = this.encryptTextData(json, this.m_passwordForData);
                    string str = Convert.ToBase64String(bytes);

                    PlayerPrefs.SetString(key, str);

                }

                else
                {
                    PlayerPrefs.SetString(key, Convert.ToBase64String(UTF8Encoding.UTF8.GetBytes(json)));
                }

            }

            catch (Exception e)
            {
                Debug.LogError(e.Message);
            }

        }

        /// <summary>
        /// Save data
        /// </summary>
        /// <param name="filePath">file path</param>
        /// <param name="data">data</param>
        // -------------------------------------------------------------------------------------
        public virtual void saveData(string filePath, System.Object data, Action<string> resultCallback)
        {

            if (string.IsNullOrEmpty(filePath) || data == null)
            {
                return;
            }

            // -----------------

            if (this.m_usePlayerPrefs)
            {
                this.saveDataToPlayerPrefs(filePath, data);
                return;
            }

            // -----------------

            if (this.m_usePasswordForData)
            {

                using (FileStream fs = new FileStream(filePath, FileMode.Create))
                {

                    BinaryFormatter formatter = new BinaryFormatter();

                    string json = JsonUtility.ToJson(data);

                    formatter.Serialize(fs, this.encryptTextData(json, this.m_passwordForData));

#if UNITY_IOS
                    if (this.m_setNoBackupFlagInIos)
                    {
                        UnityEngine.iOS.Device.SetNoBackupFlag(filePath);
                    }
#endif

                }

            }

            else
            {

                try
                {
                    File.WriteAllText(filePath, JsonUtility.ToJson(data));
                }

                catch (Exception e)
                {

                    if (resultCallback != null)
                    {
                        resultCallback(e.Message);
                    }

                    return;

                }

            }

            if (resultCallback != null)
            {
                resultCallback("");
            }

        }

        /// <summary>
        /// Save data
        /// </summary>
        /// <param name="filePath">file path to save</param>
        /// <param name="data">data to save</param>
        /// <param name="resultCallback">result callback</param>
        /// <param name="progressCallback">progress callback</param>
        /// <returns>IEnumerator</returns>
        // -------------------------------------------------------------------------------------
        public virtual IEnumerator saveDataIE(string filePath, System.Object data, Action<string> resultCallback)
        {

            if (string.IsNullOrEmpty(filePath) || filePath.IndexOfAny(Path.GetInvalidPathChars()) >= 0)
            {

                if (resultCallback != null)
                {
                    resultCallback("Invalid file path : " + filePath);
                }

                this.m_saveDataIE = null;

                yield break;
            }

            if (data == null)
            {
                if (resultCallback != null)
                {
                    resultCallback("Empty data");
                }

                this.m_saveDataIE = null;

                yield break;
            }

            else if (!data.GetType().IsSerializable)
            {
                if (resultCallback != null)
                {
                    resultCallback("Not serializable obj");
                }

                this.m_saveDataIE = null;

                yield break;
            }

            // ---------------

#if !UNITY_STANDALONE
            this.savePoetConfigData();
#endif

            // ---------------

            if (this.m_usePlayerPrefs)
            {

                this.saveDataToPlayerPrefs(filePath, data);

                if (resultCallback != null)
                {
                    resultCallback("");
                }

                this.m_saveDataIE = null;

                yield break;

            }

            // ---------------

            // save
            {

                try
                {

                    if (this.m_usePasswordForData)
                    {

                        using (FileStream fs = new FileStream(filePath, FileMode.Create))
                        {

                            BinaryFormatter formatter = new BinaryFormatter();

                            string json = JsonUtility.ToJson(data);

                            formatter.Serialize(fs, this.encryptTextData(json, this.m_passwordForData));

                        }

                    }

                    else
                    {
                        File.WriteAllText(filePath, JsonUtility.ToJson(data));
                    }

#if UNITY_IOS
                    if (this.m_setNoBackupFlagInIos)
                    {
                        UnityEngine.iOS.Device.SetNoBackupFlag(filePath);
                    }
#endif

                }

                catch (Exception e)
                {
                    if (resultCallback != null)
                    {
                        resultCallback(e.Message);
                    }

                    this.m_saveDataIE = null;

                    yield break;
                }

            }

            if (resultCallback != null)
            {
                resultCallback("");
            }

            this.m_saveDataIE = null;

        }

        /// <summary>
        /// Read user progress data
        /// </summary>
        /// <typeparam name="T">PoetUserProgressDataSO</typeparam>
        /// <param name="dataNumber">data number</param>
        /// <param name="callback">result</param>
        /// <returns>IEnumerator</returns>
        // -------------------------------------------------------------------------------------
        public virtual IEnumerator readUserProgressData<T>(string dataId, Action<T, string> callback) where T : PoetUserProgressDataSO
        {

            if (this.m_dataId_userProgressData.ContainsKey(dataId))
            {

                yield return new WaitForSeconds(0.2f);

                if (callback != null)
                {
                    callback(this.m_dataId_userProgressData[dataId] as T, "");
                }

            }

            else
            {

                yield return new WaitForSeconds(0.1f);

                yield return this.readDataIE<T>(this.saveLoadDataPath(dataId), (ret, message) =>
                {

                    if (ret != null)
                    {
                        this.m_dataId_userProgressData.Add(dataId, ret);
                    }

                    if (callback != null)
                    {
                        callback(ret, message);
                    }

                }, false);

            }

        }

        /// <summary>
        /// Read ScriptableObject data from PlayerPrefs
        /// </summary>
        /// <typeparam name="T">ScriptableObject</typeparam>
        /// <param name="filePath">data file path</param>
        /// <param name="callback">result callback</param>
        /// <param name="returnNullIfErrorOrNotFound">return null if error or not found</param>
        // -------------------------------------------------------------------------------------
        protected virtual void readDataFromPlayerPrefs<T>(string filePath, Action<T, string> callback, bool returnNullIfErrorOrNotFound) where T : ScriptableObject
        {

            T result = null;

            string key = Path.GetFileNameWithoutExtension(filePath);

            string prefString = PlayerPrefs.GetString(key);

            if (!string.IsNullOrEmpty(prefString))
            {

                try
                {

                    if (this.m_usePasswordForData)
                    {

                        byte[] temp = System.Convert.FromBase64String(prefString);

                        string json = this.decryptBinaryData(temp, this.m_passwordForData);

                        result = ScriptableObject.CreateInstance<T>();

                        JsonUtility.FromJsonOverwrite(json, result);

                    }

                    else
                    {
                        prefString = UTF8Encoding.UTF8.GetString(Convert.FromBase64String(prefString));
                        result = ScriptableObject.CreateInstance<T>();
                        JsonUtility.FromJsonOverwrite(prefString, result);
                    }

                }

                catch (Exception e)
                {

                    if (callback != null)
                    {
                        callback(
                            returnNullIfErrorOrNotFound ? null : ScriptableObject.CreateInstance<T>(),
                            e.Message
                            );
                    }

                    return;

                }

            }

            else
            {

                callback(
                    returnNullIfErrorOrNotFound ? null : ScriptableObject.CreateInstance<T>(),
                    ""
                    );

                return;

            }

            if (!result)
            {
                result = returnNullIfErrorOrNotFound ? null : ScriptableObject.CreateInstance<T>();
            }

            if (callback != null)
            {
                callback(result, "");
            }

        }

        /// <summary>
        /// Get www local path
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        // -------------------------------------------------------------------------------------
        protected virtual string wwwLocalPath(string filePath)
        {

#if UNITY_ANDROID
            return "file://" + filePath;
#else
            return "file:///" + filePath;
#endif
        }

        /// <summary>
        /// Read ScriptableObject data
        /// </summary>
        /// <typeparam name="T">ScriptableObject</typeparam>
        /// <param name="filePath">data file path</param>
        /// <param name="callback">result callback</param>
        /// <param name="returnNullIfErrorOrNotFound">return null if error or not found</param>
        /// <returns>IEnumerator</returns>
        // -------------------------------------------------------------------------------------
        public virtual IEnumerator readDataIE<T>(string filePath, Action<T, string> callback, bool returnNullIfErrorOrNotFound) where T : ScriptableObject
        {

            if (this.m_usePlayerPrefs)
            {
                this.readDataFromPlayerPrefs(filePath, callback, returnNullIfErrorOrNotFound);
                yield break;
            }

            // -----------------

            if (!File.Exists(filePath))
            {

                if (callback != null)
                {
                    callback(returnNullIfErrorOrNotFound ? null : ScriptableObject.CreateInstance<T>(), "");
                }

                yield break;
            }

            // -----------------

            T result = null;

            using (WWW www = new WWW(this.wwwLocalPath(filePath)))
            {

                while (!www.isDone)
                {
                    yield return null;
                }

                if (string.IsNullOrEmpty(www.error))
                {

                    using (var ms = new MemoryStream(www.bytes))
                    {

                        try
                        {

                            if (this.m_usePasswordForData)
                            {

                                BinaryFormatter formatter = new BinaryFormatter();

                                byte[] temp = (byte[])formatter.Deserialize(ms);

                                string json = this.decryptBinaryData(temp, this.m_passwordForData);

                                result = ScriptableObject.CreateInstance<T>();

                                JsonUtility.FromJsonOverwrite(json, result);

                            }

                            else
                            {
                                result = ScriptableObject.CreateInstance<T>();
                                JsonUtility.FromJsonOverwrite(www.text, result);
                            }

                        }

                        catch (Exception e)
                        {

                            if (callback != null)
                            {
                                callback(
                                    returnNullIfErrorOrNotFound ? null : ScriptableObject.CreateInstance<T>(),
                                    e.Message
                                    );
                            }

                            yield break;

                        }

                    }

                }

                if (!string.IsNullOrEmpty(www.error))
                {

                    if (callback != null)
                    {
                        callback(
                            returnNullIfErrorOrNotFound ? null : ScriptableObject.CreateInstance<T>(),
                            www.error
                            );
                    }

                    yield break;

                }

            }

            if(!result)
            {
                result = returnNullIfErrorOrNotFound ? null : ScriptableObject.CreateInstance<T>();
            }

            if (callback != null)
            {
                callback(result, "");
            }

            yield return null;

        }

        /// <summary>
        /// Root data path
        /// </summary>
        /// <returns>path</returns>
        // -------------------------------------------------------------------------------------
        public virtual string rootDataPath()
        {
            return Application.persistentDataPath;
        }

        /// <summary>
        /// User progress data path
        /// </summary>
        /// <param name="dataNumber">data number</param>
        /// <returns>path</returns>
        // -------------------------------------------------------------------------------------
        public virtual string saveLoadDataPath(string dataId)
        {

            return
                this.rootDataPath() +
                "/" +
                this.m_userProgressDataFileNamePrefix +
                dataId +
                "." +
                this.m_dataFileExtension
                ;

        }

        /// <summary>
        /// Convert json to data
        /// </summary>
        /// <typeparam name="T">T</typeparam>
        /// <param name="json">json</param>
        /// <returns>data</returns>
        // -------------------------------------------------------------------------------------
        public T convertJson<T>(string json) where T : new()
        {
            return this.convertJson<T>(json, false, null, null);
        }

        /// <summary>
        /// Convert json to data
        /// </summary>
        /// <typeparam name="T">T</typeparam>
        /// <param name="json">json</param>
        /// <param name="ifErrorBackToTitle">if error, back to title</param>
        /// <param name="trans">Transform</param>
        /// <param name="mb">MonoBehaviour</param>
        /// <returns>data</returns>
        // -------------------------------------------------------------------------------------
        public virtual T convertJson<T>(string json, bool ifErrorBackToTitle, Transform trans, MonoBehaviour mb) where T : new()
        {

            bool success = true;

            string gameObjectName = (trans) ? trans.gameObject.name : "";
            string scriptName = (mb) ? mb.GetType().Name : "";

            T ret = Funcs.convertJson<T>(json, () =>
            {
                success = false;
            });

            if (!success && ifErrorBackToTitle)
            {

                PoetDialogManager.Instance.showOkDialog(
                PoetDialogMessageManager.Instance.convertUserProgressDataErrorMessage(gameObjectName, scriptName),
                () =>
                {
                    PoetSceneChangeManager.Instance.backToTitleScene();
                }
                );
            }

            return ret;

        }

        /// <summary>
        /// Poet config data file path
        /// </summary>
        /// <returns>file path</returns>
        // -------------------------------------------------------------------------------------
        protected virtual string poetConfigFilePath()
        {
            return this.rootDataPath() + "/config." + this.m_dataFileExtension; ;
        }

        /// <summary>
        /// Poet config data file path
        /// </summary>
        /// <returns>file path</returns>
        // -------------------------------------------------------------------------------------
        protected virtual string quickSaveFilePath()
        {
            return this.rootDataPath() + "/config." + this.m_dataFileExtension; ;
        }

        /// <summary>
        /// Save poet config data
        /// </summary>
        // -------------------------------------------------------------------------------------
        public virtual void savePoetConfigData()
        {

            this.m_configDataSO.prepareToSave();

            this.saveData(this.poetConfigFilePath(), this.m_configDataSO, null);

        }

        /// <summary>
        /// Is already read
        /// </summary>
        /// <param name="textAssetName">csv name</param>
        /// <param name="csvRowIndex">csv row index</param>
        /// <returns>already read</returns>
        // -------------------------------------------------------------------------------------------
        public bool isAlreadyRead(string textAssetName, CsvReadRange range)
        {
            return this.m_configDataSO.isAlreadyRead(textAssetName, range);
        }

        /// <summary>
        /// Is already read
        /// </summary>
        /// <param name="textAssetName">csv name</param>
        /// <param name="csvRowIdentifier">csv row identifier</param>
        /// <returns>already read</returns>
        // -------------------------------------------------------------------------------------------
        public bool isAlreadyRead(string textAssetName, string csvRowIdentifier)
        {
            return this.m_configDataSO.isAlreadyRead(textAssetName, new CsvReadRange(csvRowIdentifier));
        }

        /// <summary>
        /// Update already read range list
        /// </summary>
        // -------------------------------------------------------------------------------------------
        public void updateAlreadyReadRange(string textAssetName, CsvReadRange range)
        {

            if(this.m_updateAndSaveAlreadyReadText)
            {
                this.m_configDataSO.updateAlreadyReadRange(textAssetName, range);
            }
            
        }

        /// <summary>
        /// Reset auto and skip modes
        /// </summary>
        // -------------------------------------------------------------------------------------------
        public void resetAutoAndSkipModes()
        {
            this.m_auto = false;
            this.m_skip = false;
        }

        /// <summary>
        /// Set system language
        /// </summary>
        /// <param name="sl">SystemLanguage</param>
        // -------------------------------------------------------------------------------------
        public virtual void setSystemLanguage(SystemLanguage sl)
        {

            this.m_configDataSO.systemLanguage = sl;

            PoetReduxManager prm = PoetReduxManager.Instance as PoetReduxManager;
            prm.ChangeLanguageSignalWatcher.state().setState(prm.ChangeLanguageSignalWatcher);

        }

        /// <summary>
        /// Set current user progress data description
        /// </summary>
        /// <param name="text">description</param>
        // -------------------------------------------------------------------------------------
        public void setCurrentUserProgressDataDescription(string text)
        {
            this.m_currentUserProgressDataDescription = text;
        }

        /// <summary>
        /// Encrypt text data
        /// </summary>
        /// <param name="data">text data</param>
        /// <param name="pass">pass</param>
        /// <returns>encrypted data</returns>
        // -------------------------------------------------------------------------------------
        protected virtual byte[] encryptTextData(string data, string pass)
        {

#if UNITY_EDITOR

            if (pass.Length != 16 && pass.Length != 24 && pass.Length != 32)
            {
                Debug.LogWarning("(#if UNITY_EDITOR) password length must be 16, 24, or 32");
            }


#endif

            return SSC.Funcs.EncryptTextData(data, pass);

        }

        /// <summary>
        /// Decrypt binary data
        /// </summary>
        /// <param name="data">binary data</param>
        /// <param name="pass">pass</param>
        /// <returns>text data</returns>
        // -------------------------------------------------------------------------------------
        protected virtual string decryptBinaryData(byte[] data, string pass)
        {

#if UNITY_EDITOR

            if (pass.Length != 16 && pass.Length != 24 && pass.Length != 32)
            {
                Debug.LogWarning("(#if UNITY_EDITOR) password length must be 16, 24, or 32");
            }


#endif

            return SSC.Funcs.DecryptBinaryDataToText(data, pass);

        }

        /// <summary>
        /// Quit
        /// </summary>
        // -------------------------------------------------------------------------------------
        public void quitApp()
        {

#if UNITY_STANDALONE

            PoetDialogManager.Instance.showYesNoDialog(
                PoetDialogMessageManager.Instance.quitApplicationConfirmationMessage(),
                () =>
                {

#if UNITY_EDITOR
                    UnityEditor.EditorApplication.isPlaying = false;
#else
                    Application.Quit();
#endif

                },
                null
                );

#endif
            
        }

        /// <summary>
        /// OnApplicationQuit
        /// </summary>
        // -------------------------------------------------------------------------------------
        protected override void OnApplicationQuit()
        {

            base.OnApplicationQuit();

#if UNITY_STANDALONE
            this.savePoetConfigData();
#endif

        }

        /// <summary>
        /// Update
        /// </summary>
        // -------------------------------------------------------------------------------------
        protected virtual void Update()
        {

            if (Input.GetKeyDown(KeyCode.Escape))
            {
                this.quitApp();
            }

        }

    }

}
