﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Poet
{

    /// <summary>
    /// Text log manager
    /// </summary>
    public class TextLogManager : SSC.SingletonMonoBehaviour<TextLogManager>
    {

        [Serializable]
        protected class TextLogStruct
        {
            public string nameText = "";
            public string mainText = "";
            public string preloadedVoiceId = "";
            public string resourceVoiceId = "";
            public string assetBundleVoiceId = "";
            public string characterVoiceId = "";

            public void clear(bool clearVoiceOnly)
            {

                this.preloadedVoiceId = "";
                this.resourceVoiceId = "";
                this.assetBundleVoiceId = "";
                this.characterVoiceId = "";

                if (!clearVoiceOnly)
                {
                    this.nameText = "";
                    this.mainText = "";
                }

            }

            public bool isEmpty()
            {
                return
                    (
                    this.nameText.Length +
                    this.mainText.Length +
                    this.preloadedVoiceId.Length +
                    this.resourceVoiceId.Length +
                    this.assetBundleVoiceId.Length +
                    this.characterVoiceId.Length
                    ) <= 0
                    ;

            }

        }


        /// <summary>
        /// User progress data
        /// </summary>
        [Serializable]
        protected class UserProgressData
        {

            /// <summary>
            /// Text log struct list
            /// </summary>
            public List<TextLogStruct> textLogList = new List<TextLogStruct>();

        }

        /// <summary>
        /// User progress data
        /// </summary>
        protected UserProgressData m_userProgressData = new UserProgressData();

        /// <summary>
        /// Text log size
        /// </summary>
        [SerializeField]
        [Tooltip("Text log size")]
        protected int m_size = 100;

        /// <summary>
        /// Save log to user progress data
        /// </summary>
        [SerializeField]
        [Tooltip("Save log to user progress data")]
        protected bool m_saveLogToUserProgressData = true;

        /// <summary>
        /// If true, clear voice only at scene change. If false, clear text and voice
        /// </summary>
        [SerializeField]
        [Tooltip("If true, clear only voices at scene change. If false, clear texts and voices")]
        protected bool m_clearVoiceOnlyAtSceneChange = true;

        /// <summary>
        /// Reference to Slider
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to Slider")]
        protected Slider m_refSlider = null;

        /// <summary>
        /// Reference to TextLogPanelScript list
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to TextLogPanelScript list")]
        protected List<TextLogPanelScript> m_refTextLogPanels = new List<TextLogPanelScript>();

        /// <summary>
        /// Called in Awake
        /// </summary>
        // ---------------------------------------------------------------------------------------------------------------
        protected override void initOnAwake()
        {

            // m_textLogList
            {

                this.m_size = Mathf.Max(0, this.m_size);

                for(int i = 0; i < this.m_size; i++)
                {
                    this.m_userProgressData.textLogList.Add(new TextLogStruct());
                }
                
            }

            // PoetReduxManager
            {

                PoetReduxManager prm = PoetReduxManager.Instance as PoetReduxManager;

                prm.addPoetSequenceStateReceiver(this.onPoetSequenceState);
                prm.addSceneChangeStateReceiver(this.onSceneChangeStateReceiver);
                prm.addUserProgressDataSignalReceiver(this.onUserProgressDataSignal);

            }

            // m_refSlider
            {

                if(this.m_refSlider)
                {
                    this.m_refSlider.minValue = 0;
                    this.m_refSlider.maxValue = Mathf.Max(0, this.m_userProgressData.textLogList.Count - this.m_refTextLogPanels.Count);
                }

            }

            // init m_refTextLogPanels
            {

                for (int i = this.m_refTextLogPanels.Count - 1; i >= 0; i--)
                {

                    if (this.m_refTextLogPanels[i])
                    {
                        this.m_refTextLogPanels[i].setText("", "");
                    }

                }

            }

        }

        /// <summary>
        /// Clear text log list
        /// </summary>
        // ---------------------------------------------------------------------------------------------------------------
        protected void clearTextLogList()
        {

            int size = this.m_userProgressData.textLogList.Count;

            for (int i = 0; i < size; i++)
            {
                this.m_userProgressData.textLogList[i].clear(this.m_clearVoiceOnlyAtSceneChange);
            }

        }

        /// <summary>
        /// PoetSequenceState receiver
        /// </summary>
        /// <param name="state">PoetSequenceState</param>
        // ------------------------------------------------------------------------------------------
        protected virtual void onPoetSequenceState(PoetSequenceState psState)
        {

            if(psState.stateEnum == PoetSequenceState.StateEnum.PreProcess)
            {

                if (this.m_userProgressData.textLogList.Count > 0)
                {

                    TextLogStruct latest = this.latestTarget();

                    if ( ! (latest != null && latest.isEmpty()))
                    {
                        this.m_userProgressData.textLogList.RemoveAt(this.m_userProgressData.textLogList.Count - 1);
                        this.m_userProgressData.textLogList.Insert(0, new TextLogStruct());
                    }

                }

            }

        }

        /// <summary>
        /// SceneChangeState receiver
        /// </summary>
        /// <param name="scState">SceneChangeState</param>
        // -----------------------------------------------------------------------------------------------
        protected virtual void onSceneChangeStateReceiver(SSC.SceneChangeState scState)
        {

            if (scState.stateEnum == SSC.SceneChangeState.StateEnum.NowLoadingIntro)
            {
                this.clearTextLogList();
            }

            else if (scState.stateEnum == SSC.SceneChangeState.StateEnum.AllStartupsDone)
            {

                PoetSceneChangeManager pscm = PoetSceneChangeManager.Instance as PoetSceneChangeManager;

                if (pscm.isLoadingCurrentSceneWithUserProgressData())
                {

                    this.m_userProgressData = pscm.getDataFromCurrentUserProgressData<UserProgressData>(this.transform, this);

                }

            }

        }

        /// <summary>
        /// Update target
        /// </summary>
        /// <returns></returns>
        // ------------------------------------------------------------------------------------------
        protected TextLogStruct latestTarget()
        {

            if(this.m_userProgressData.textLogList.Count > 0)
            {
                return this.m_userProgressData.textLogList[0];
            }

            return null;

        }

        /// <summary>
        /// update main text of latest element
        /// </summary>
        // ------------------------------------------------------------------------------------------
        public void updateMainText(string mainText)
        {

            var target = this.latestTarget();

            if(target != null)
            {
                target.mainText = mainText;
            }

        }

        /// <summary>
        /// update name text of latest element
        /// </summary>
        // ------------------------------------------------------------------------------------------
        public void updateNameText(string nameText)
        {

            var target = this.latestTarget();

            if (target != null)
            {
                target.nameText = nameText;
            }

        }

        /// <summary>
        /// update preloaded voice id of latest element
        /// </summary>
        // ------------------------------------------------------------------------------------------
        public void updateCharacterVoiceId(string characterVoiceId)
        {

            var target = this.latestTarget();

            if (target != null)
            {
                target.characterVoiceId = characterVoiceId;
            }

        }

        /// <summary>
        /// update preloaded voice id of latest element
        /// </summary>
        // ------------------------------------------------------------------------------------------
        public void updatePreloadedVoiceId(string preloadedVoiceId)
        {

            var target = this.latestTarget();

            if (target != null)
            {
                target.preloadedVoiceId = preloadedVoiceId;
                target.resourceVoiceId = "";
                target.assetBundleVoiceId = "";
            }

        }

        /// <summary>
        /// update resource voice id of latest element
        /// </summary>
        // ------------------------------------------------------------------------------------------
        public void updateResourceVoiceId(string resourceVoiceId)
        {

            var target = this.latestTarget();

            if (target != null)
            {
                target.preloadedVoiceId = "";
                target.resourceVoiceId = resourceVoiceId;
                target.assetBundleVoiceId = "";
            }

        }

        /// <summary>
        /// update AssetBundle voice id of latest element
        /// </summary>
        // ------------------------------------------------------------------------------------------
        public void updateAssetBundleVoiceId(string assetBundleVoiceId)
        {

            var target = this.latestTarget();

            if (target != null)
            {
                target.preloadedVoiceId = "";
                target.resourceVoiceId = "";
                target.assetBundleVoiceId = assetBundleVoiceId;
            }

        }

        /// <summary>
        /// Set params by slider value
        /// </summary>
        /// <param name="sliderValue">slider value</param>
        // ------------------------------------------------------------------------------------------
        public virtual void setParamsBySliderValue(float val)
        {

            int iSliderValue = (int)val;

            int minSize = Mathf.Min(this.m_refTextLogPanels.Count, this.m_userProgressData.textLogList.Count);

            int textLogListSize = this.m_userProgressData.textLogList.Count;

            int textLogListIndex = 0;

            for (int i = 0; i < minSize; i++)
            {

                textLogListIndex = iSliderValue + i;

                if (textLogListIndex >= textLogListSize)
                {
                    break;
                }

                if(this.m_refTextLogPanels[i] != null && this.m_userProgressData.textLogList[textLogListIndex] != null)
                {
                    this.m_refTextLogPanels[i].setText(this.m_userProgressData.textLogList[textLogListIndex].nameText, this.m_userProgressData.textLogList[textLogListIndex].mainText);
                    this.m_refTextLogPanels[i].setPreloadedVoiceId(this.m_userProgressData.textLogList[textLogListIndex].preloadedVoiceId);
                    this.m_refTextLogPanels[i].setResourceVoiceId(this.m_userProgressData.textLogList[textLogListIndex].resourceVoiceId);
                    this.m_refTextLogPanels[i].setAssetBundleVoiceId(this.m_userProgressData.textLogList[textLogListIndex].assetBundleVoiceId);
                    this.m_refTextLogPanels[i].setCharacterVoiceId(this.m_userProgressData.textLogList[textLogListIndex].characterVoiceId);
                    this.m_refTextLogPanels[i].setButtonInteractable();
                }

            }

        }

        /// <summary>
        /// Show TextLog UI
        /// </summary>
        // ------------------------------------------------------------------------------------------
        public virtual void showTextLogUi()
        {

            // setParamsBySliderValue
            {
                this.setParamsBySliderValue(0);
            }

            // m_refSlider
            {
                if (this.m_refSlider)
                {
                    this.m_refSlider.value = 0;
                }
            }

            // showUi
            {
                SSC.CommonUiManager.Instance.showUi(PoetUiIdentifiers.Common.TextLog, true, false);
            }

        }

        /// <summary>
        /// Function when previous button is clicked
        /// </summary>
        /// <param name="moveValue">value</param>
        // ------------------------------------------------------------------------------------------
        public void onClickPreviousButton(int moveValue)
        {

            if(this.m_refSlider)
            {
                this.m_refSlider.value = this.m_refSlider.value + moveValue;
            }

        }

        /// <summary>
        /// Function when next button is clicked
        /// </summary>
        /// <param name="moveValue">value</param>
        // ------------------------------------------------------------------------------------------
        public void onClickNextButton(int moveValue)
        {

            if (this.m_refSlider)
            {
                this.m_refSlider.value = this.m_refSlider.value - moveValue;
            }

        }

        /// <summary>
        /// UserProgressDataSignal receiver
        /// </summary>
        /// <param name="tfState">UserProgressDataSignal</param>
        // ------------------------------------------------------------------------------------------
        protected virtual void onUserProgressDataSignal(UserProgressDataSignal updSignal)
        {

            if(this.m_saveLogToUserProgressData)
            {
                updSignal.addDataAction(this.transform, this, JsonUtility.ToJson(this.m_userProgressData));
            }
            
        }

    }

}
