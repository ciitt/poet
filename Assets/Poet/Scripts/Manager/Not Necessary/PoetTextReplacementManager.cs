﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poet
{

    /// <summary>
    /// One scene manager to replace text
    /// </summary>
    public abstract class PoetTextReplacementManager : SSC.SingletonMonoBehaviour<PoetTextReplacementManager>
    {

        /// <summary>
        /// Called in Awake
        /// </summary>
        // -------------------------------------------------------------------------------------------------
        protected override void initOnAwake()
        {

        }

        /// <summary>
        /// Get replacement text
        /// </summary>
        /// <param name="identifier">identifier</param>
        /// <returns>text</returns>
        // -----------------------------------------------------------------------------------------------
        public abstract string getReplacementText(string identifier);

    }

}
