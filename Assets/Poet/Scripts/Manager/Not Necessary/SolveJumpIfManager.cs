﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poet
{

    /// <summary>
    /// Solve JumpIf manager
    /// </summary>
    public abstract class SolveJumpIfManager : SSC.SingletonMonoBehaviour<SolveJumpIfManager>
    {

        /// <summary>
        /// initOnAwake
        /// </summary>
        // -----------------------------------------------------------------------------------------------------------------
        protected override void initOnAwake()
        {

        }

        /// <summary>
        /// Get identifier to jump by evaluating flag value
        /// </summary>
        /// <param name="flagIdentifier">flag identifier</param>
        /// <param name="comparison">comparison</param>
        /// <param name="val">val</param>
        /// <param name="identifierToJump">identifier to jump</param>
        /// <returns>identifier to jump</returns>
        // -----------------------------------------------------------------------------------------------------------------
        public abstract string solveJumpIfFlag(string flagIdentifier, string comparison, string val, string identifierToJump);

        /// <summary>
        /// Get identifier to jump by evaluating item value
        /// </summary>
        /// <param name="itemIdentifier">item identifier</param>
        /// <param name="comparison">comparison</param>
        /// <param name="val">val</param>
        /// <param name="identifierToJump">identifier to jump</param>
        /// <returns>identifier to jump</returns>
        // -----------------------------------------------------------------------------------------------------------------
        public abstract string solveJumpIfItem(string itemIdentifier, string comparison, string val, string identifierToJump);

        /// <summary>
        /// Compare
        /// </summary>
        /// <param name="lhs">lhs</param>
        /// <param name="rhs">rhs</param>
        /// <param name="comparison">comparison</param>
        /// <returns>compare</returns>
        // -------------------------------------------------------------------------------------
        public virtual bool compare(int lhs, int rhs, string comparison)
        {

            if (comparison == "<" || comparison == "Less")
            {
                return lhs < rhs;
            }

            else if (comparison == "<=" || comparison == "LEqual")
            {
                return lhs <= rhs;
            }

            else if (comparison == "==" || comparison == "Equal")
            {
                return lhs == rhs;
            }

            else if (comparison == "!=" || comparison == "NotEqual")
            {
                return lhs != rhs;
            }

            else if (comparison == ">=" || comparison == "GEqual")
            {
                return lhs >= rhs;
            }

            else if (comparison == ">" || comparison == "Greater")
            {
                return lhs > rhs;
            }

#if UNITY_EDITOR

            else
            {
                Debug.LogWarning("(#if UNITY_EDITOR) Unknown comparison : " + comparison);
            }

#endif

            return false;

        }

    }

}
