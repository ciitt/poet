﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Poet
{

    /// <summary>
    /// Redux manager for Poet
    /// </summary>
    public class PoetReduxManager : SSC.SimpleReduxManager
    {

        /// <summary>
        /// StateWatcher for PoetTextState
        /// </summary>
        protected SSC.StateWatcher<PoetSequenceState> m_poetSequenceStateWatcher = new SSC.StateWatcher<PoetSequenceState>();

        /// <summary>
        /// StateWatcher for PoetInitState
        /// </summary>
        protected SSC.StateWatcher<PoetInitState> m_poetInitStateWatcher = new SSC.StateWatcher<PoetInitState>();

        /// <summary>
        /// StateWatcher for TextShowingState
        /// </summary>
        protected SSC.StateWatcher<TextShowingState> m_textShowingStateWatcher = new SSC.StateWatcher<TextShowingState>();

        /// <summary>
        /// StateWatcher for UserProgressDataSignal
        /// </summary>
        protected SSC.StateWatcher<UserProgressDataSignal> m_userProgressDataSignalWatcher = new SSC.StateWatcher<UserProgressDataSignal>();

        /// <summary>
        /// StateWatcher for UiState
        /// </summary>
        protected SSC.StateWatcher<UiState> m_uiStateWatcher = new SSC.StateWatcher<UiState>();

        /// <summary>
        /// StateWatcher for ChangeLanguageSignal
        /// </summary>
        protected SSC.StateWatcher<ChangeLanguageSignal> m_changeLanguageSignalWatcher = new SSC.StateWatcher<ChangeLanguageSignal>();

        // ------------------------------------------------------------------------------------------------------------------------------

        /// <summary>
        /// StateWatcher<PoetSequenceState> getter
        /// </summary>
        public SSC.StateWatcher<PoetSequenceState> PoetSequenceStateWatcher { get { return this.m_poetSequenceStateWatcher; } }

        /// <summary>
        /// StateWatcher<PoetSequenceState> getter
        /// </summary>
        public SSC.StateWatcher<PoetInitState> PoetInitStateWatcher { get { return this.m_poetInitStateWatcher; } }

        /// <summary>
        /// StateWatcher<TextFinishedState> getter
        /// </summary>
        public SSC.StateWatcher<TextShowingState> TextFinishedStateWatcher { get { return this.m_textShowingStateWatcher; } }

        /// <summary>
        /// StateWatcher<UserProgressDataSignal> getter
        /// </summary>
        public SSC.StateWatcher<UserProgressDataSignal> UserProgressDataSignalWatcher { get { return this.m_userProgressDataSignalWatcher; } }

        /// <summary>
        /// StateWatcher<UiState> getter
        /// </summary>
        public SSC.StateWatcher<UiState> UiStateWatcher { get { return this.m_uiStateWatcher; } }

        /// <summary>
        /// StateWatcher<ChangeLanguageSignal> getter
        /// </summary>
        public SSC.StateWatcher<ChangeLanguageSignal> ChangeLanguageSignalWatcher { get { return this.m_changeLanguageSignalWatcher; } }

        /// <summary>
        /// Reset states on scene loaded
        /// </summary>
        /// <param name="scene">Scene</param>
        /// <param name="mode">LoadSceneMode</param>
        // ----------------------------------------------------------------------------------------------
        protected override void resetOnSceneLoaded(Scene scene, LoadSceneMode mode)
        {

            base.resetOnSceneLoaded(scene, mode);

            if (mode != LoadSceneMode.Single)
            {
                return;
            }

            // ---------------

            this.m_poetSequenceStateWatcher.state().resetOnSceneLevelLoaded();
            this.m_poetInitStateWatcher.state().resetOnSceneLevelLoaded();
            this.m_textShowingStateWatcher.state().resetOnSceneLevelLoaded();
            this.m_userProgressDataSignalWatcher.state().resetOnSceneLevelLoaded();
            this.m_uiStateWatcher.state().resetOnSceneLevelLoaded();
            this.m_changeLanguageSignalWatcher.state().resetOnSceneLevelLoaded();

        }

        /// <summary>
        /// Add PoetSequenceState receiver action
        /// </summary>
        /// <param name="action">PoetSequenceState</param>
        // ------------------------------------------------------------------------------------------------------------------------------
        public void addPoetSequenceStateReceiver(Action<PoetSequenceState> action)
        {
            if (action != null)
            {
                this.m_poetSequenceStateWatcher.addAction(action);
            }
        }

        /// <summary>
        /// Add PoetInitState receiver action
        /// </summary>
        /// <param name="action">PoetInitState</param>
        // ------------------------------------------------------------------------------------------------------------------------------
        public void addPoetInitStateReceiver(Action<PoetInitState> action)
        {
            if (action != null)
            {
                this.m_poetInitStateWatcher.addAction(action);
            }
        }

        /// <summary>
        /// Add TextShowingState receiver action
        /// </summary>
        /// <param name="action">TextShowingState</param>
        // ------------------------------------------------------------------------------------------------------------------------------
        public void addTextFinishedStateReceiver(Action<TextShowingState> action)
        {
            if (action != null)
            {
                this.m_textShowingStateWatcher.addAction(action);
            }
        }

        /// <summary>
        /// Add UserProgressDataSignal receiver action
        /// </summary>
        /// <param name="action">UserProgressDataSignal</param>
        // ------------------------------------------------------------------------------------------------------------------------------
        public void addUserProgressDataSignalReceiver(Action<UserProgressDataSignal> action)
        {
            if (action != null)
            {
                this.m_userProgressDataSignalWatcher.addAction(action);
            }
        }

        /// <summary>
        /// Add UiState receiver action
        /// </summary>
        /// <param name="action">UiState</param>
        // ------------------------------------------------------------------------------------------------------------------------------
        public void addUiStateReceiver(Action<UiState> action)
        {
            if (action != null)
            {
                this.m_uiStateWatcher.addAction(action);
            }
        }

        /// <summary>
        /// Add ChangeLanguageSignal receiver action
        /// </summary>
        /// <param name="action">ChangeLanguageSignal</param>
        // ------------------------------------------------------------------------------------------------------------------------------
        public void addChangeLanguageSignalReceiver(Action<ChangeLanguageSignal> action)
        {
            if (action != null)
            {
                this.m_changeLanguageSignalWatcher.addAction(action);
            }

        }

    }

}
