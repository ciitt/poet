using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Poet
{

    /// <summary>
    /// SceneChangeManager for Poet
    /// </summary>
    public class PoetSceneChangeManager : SSC.SceneChangeManager
    {

        /// <summary>
        /// Identifier for simple now loading
        /// </summary>
        [SerializeField]
        [Tooltip("Identifier for simple now loading")]
        protected string m_simpleNowLoadingUiIdentifier = "NowLoadingBlack";

        /// <summary>
        /// Current loading target PoetUserProgressDataSO
        /// </summary>
        protected PoetUserProgressDataSO m_userProgressDataSO = null;

        /// <summary>
        /// Current loading target AlbumMemoryData
        /// </summary>
        protected AlbumMemoryData m_albumMemoryData = null;

        /// <summary>
        /// Scene name for album
        /// </summary>
        protected string m_backToAlbumSceneName = "";

        /// <summary>
        /// Default now loading ui identifier
        /// </summary>
        protected string m_defaultNowLoadingUiIdentifier = "";

        /// <summary>
        /// Revert NowLoadingUiIdentifier to default
        /// </summary>
        protected bool m_revertNowLoadingUiToDefault = false;

        // -----------------------------------------------------------------------------------------------

        /// <summary>
        /// Current AlbumMemoryData
        /// </summary>
        public AlbumMemoryData albumMemoryData { get { return this.m_albumMemoryData; } }

        /// <summary>
        /// Awake
        /// </summary>
        // -----------------------------------------------------------------------------------------------
        protected override void Awake()
        {

            base.Awake();

            // m_defaultNowLoadingUiIdentifier
            {
                this.m_defaultNowLoadingUiIdentifier = this.m_currentNowLoadingUiIdentifier;
            }

        }

        /// <summary>
        /// Start
        /// </summary>
        // -----------------------------------------------------------------------------------------------
        protected virtual void Start()
        {

            // prm
            {
                PoetReduxManager prm = PoetReduxManager.Instance as PoetReduxManager;
                prm.addSceneChangeStateReceiver(this.onSceneChangeStateReceiver);
            }

            // sceneLoaded
            {
                SceneManager.sceneLoaded += this.resetOnSceneLoaded;
            }

        }

        /// <summary>
        /// Back to title
        /// </summary>
        // -------------------------------------------------------------------------------------------------------
        public override void backToTitleScene()
        {
            base.loadNextScene(this.m_titleSceneName);
        }

        /// <summary>
        /// Use simple now loading once
        /// </summary>
        // -----------------------------------------------------------------------------------------------
        public void useSimpleNowLoadingOnce()
        {
            this.setNowLoadingUiIdentifier(this.m_simpleNowLoadingUiIdentifier, true);
        }

        /// <summary>
        /// Set NowLoading UI identifier
        /// </summary>
        /// <param name="identifier">identifier for now loading</param>
        /// <param name="revertToDefaultAfterNextLoading">revert now loading ui dentifier to default</param>
        // -----------------------------------------------------------------------------------------------
        public void setNowLoadingUiIdentifier(string identifier, bool revertToDefaultAfterNextLoading)
        {
            this.currentNowLoadingUiIdentifier = identifier;
            this.m_revertNowLoadingUiToDefault = revertToDefaultAfterNextLoading;
        }

        /// <summary>
        /// Reset states on scene loaded
        /// </summary>
        /// <param name="scene">Scene</param>
        /// <param name="mode">LoadSceneMode</param>
        // ----------------------------------------------------------------------------------------------
        protected virtual void resetOnSceneLoaded(Scene scene, LoadSceneMode mode)
        {

            if (mode != LoadSceneMode.Single)
            {
                return;
            }

            // ---------------

            if(scene.name == this.m_titleSceneName)
            {

                this.clearCurrentLoadingTargetPUPD();
                this.clearAlbumMemoryData();

                // UiStateWatcher
                {
                    PoetReduxManager prm = PoetReduxManager.Instance as PoetReduxManager;
                    prm.UiStateWatcher.state().setState(prm.UiStateWatcher, UiState.StateEnum.Normal);
                }

            }

        }

        /// <summary>
        /// Back to title messages
        /// </summary>
        /// <returns>message</returns>
        // -------------------------------------------------------------------------------------------------------
        public override System.Object backToTitleMessages()
        {
            return PoetDialogMessageManager.Instance.backToTitleBecauseOfErrorMessage();
        }

        /// <summary>
        /// SceneChangeState receiver
        /// </summary>
        /// <param name="scState">SceneChangeState</param>
        // -----------------------------------------------------------------------------------------------
        protected virtual void onSceneChangeStateReceiver(SSC.SceneChangeState scState)
        {

            if (scState.stateEnum == SSC.SceneChangeState.StateEnum.ScenePlaying)
            {

                Invoke("clearCurrentLoadingTargetPUPD", 0.01f);

                // clear album memory data here for error in album scene
                if(this.m_titleSceneName == this.m_nowLoadingSceneName)
                {
                    this.clearAlbumMemoryData();
                }

                if(this.m_revertNowLoadingUiToDefault)
                {
                    this.m_currentNowLoadingUiIdentifier = this.m_defaultNowLoadingUiIdentifier;
                    this.m_revertNowLoadingUiToDefault = false;
                }

            }

        }

        /// <summary>
        /// Clear current loading user progress data
        /// </summary>
        // -------------------------------------------------------------------------------------
        protected void clearCurrentLoadingTargetPUPD()
        {
            this.m_userProgressDataSO = null;
        }

        /// <summary>
        /// Get json data from current user progress data
        /// </summary>
        /// <typeparam name="T">any</typeparam>
        /// <param name="trans">Transform</param>
        /// <param name="mb">MonoBehaviour</param>
        /// <returns>data</returns>
        // -------------------------------------------------------------------------------------
        public T getDataFromCurrentUserProgressData<T>(Transform trans, MonoBehaviour mb) where T : new()
        {

            string json = this.getJsonDataFromCurrentUserProgressData(trans, mb);
            
            return SystemManager.Instance.convertJson<T>(json, true, this.transform, this);

        }

        /// <summary>
        /// Get json data from current user progress data
        /// </summary>
        /// <param name="trans">Transform</param>
        /// <param name="mb">MonoBehaviour</param>
        /// <returns>data</returns>
        // -------------------------------------------------------------------------------------
        public string getJsonDataFromCurrentUserProgressData(Transform trans, MonoBehaviour mb)
        {

            if (this.m_userProgressDataSO == null)
            {
                return "";
            }

            // ---------------

            string key = SystemManager.Instance.createKeyPath(trans, mb);

            if (!string.IsNullOrEmpty(key))
            {

                if (this.m_userProgressDataSO.dataDict.ContainsKey(key))
                {

                    var temp = this.m_userProgressDataSO.dataDict[key] as KeyAndData;

                    if (temp != null)
                    {
                        return temp.stringOrJson;
                    }

                }

#if UNITY_EDITOR

                else
                {
                    Debug.LogWarning("(#if UNITY_EDITOR) Not contain a key : " + key + " : Did you edit the scene or script?");
                }
#endif

            }

            return "";

        }

        /// <summary>
        /// Load a scene with user progress data
        /// </summary>
        // -------------------------------------------------------------------------------------------------------
        public virtual void loadSceneWithUserProgressData(PoetUserProgressDataSO data)
        {

            if (!data)
            {
                return;
            }

            // --------------------

            // set
            {
                this.m_userProgressDataSO = data;
                this.m_userProgressDataSO.initDictionary();
            }

            // loadNextScene
            {
                this.loadNextScene(this.m_userProgressDataSO.sceneName);
            }

        }

        /// <summary>
        /// Is loading current scene with user progress data
        /// </summary>
        /// <returns>yesno</returns>
        // -------------------------------------------------------------------------------------------------------
        public bool isLoadingCurrentSceneWithUserProgressData()
        {
            return (this.m_userProgressDataSO != null);
        }

        /// <summary>
        /// Is loading album memory scene
        /// </summary>
        /// <returns></returns>
        // -------------------------------------------------------------------------------------------------------
        public bool isLoadingAlbumMemoryScene()
        {
            return (this.m_albumMemoryData != null);
        }

        /// <summary>
        /// Load a scene with album memory data
        /// </summary>
        // -------------------------------------------------------------------------------------------------------
        public virtual void loadSceneWithAlbumMemoryData(AlbumMemoryData data)
        {

            if (data == null)
            {
                return;
            }

            // --------------------

            // m_backToAlbumSceneName
            {
                this.m_backToAlbumSceneName = SceneManager.GetActiveScene().name;
            }

            // set
            {
                this.m_albumMemoryData = data;
            }

            // UiStateWatcher
            {
                PoetReduxManager prm = PoetReduxManager.Instance as PoetReduxManager;
                prm.UiStateWatcher.state().setState(prm.UiStateWatcher, UiState.StateEnum.AlbumMemory);
            }

            // loadNextScene
            {
                this.loadNextScene(this.m_albumMemoryData.sceneName);
            }

        }

        /// <summary>
        /// Clear album memory data
        /// </summary>
        // -------------------------------------------------------------------------------------------------------
        protected void clearAlbumMemoryData()
        {

            this.m_albumMemoryData = null;

            // UiStateWatcher
            {
                PoetReduxManager prm = PoetReduxManager.Instance as PoetReduxManager;
                prm.UiStateWatcher.state().setState(prm.UiStateWatcher, UiState.StateEnum.Normal);
            }

        }

        /// <summary>
        /// Finish album memory scene
        /// </summary>
        // -------------------------------------------------------------------------------------------------------
        public virtual void finishAlbumMemoryScene()
        {

            this.clearAlbumMemoryData();

            // loadNextScene
            {
                this.loadNextScene(this.m_backToAlbumSceneName, true, "", PoetUiIdentifiers.AlbumScene.AlbumMemoryForBackFromScene);
            }

        }

    }

}
