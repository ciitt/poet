﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Poet
{

    /// <summary>
    /// Poet common UI manager
    /// </summary>
    public class PoetCommonUiManager : SSC.CommonUiManager
    {

        /// <summary>
        /// User progress data
        /// </summary>
        [Serializable]
        protected class UserProgressData
        {

            public List<string> uiIdentifiers = new List<string>();

            public void clear()
            {
                this.uiIdentifiers.Clear();
            }

        }

        /// <summary>
        /// Reference to go to next button
        /// </summary>
        protected GotoNextSequenceScript m_refGotoNextButton;

        /// <summary>
        /// Mouse wheel value
        /// </summary>
        protected float m_mouseWheel = 0.0f;

        /// <summary>
        /// Left mouse button down
        /// </summary>
        protected bool m_leftMouseDown = false;

        /// <summary>
        /// Right mouse button down
        /// </summary>
        protected bool m_rightMouseDown = false;

        /// <summary>
        /// User progress data
        /// </summary>
        protected UserProgressData m_userProgressData = new UserProgressData();

        /// <summary>
        /// Awake
        /// </summary>
        // -----------------------------------------------------------------------------------------------
        protected override void Awake()
        {

            base.Awake();

            // prm
            {

                PoetReduxManager prm = PoetReduxManager.Instance as PoetReduxManager;

                prm.addUserProgressDataSignalReceiver(this.onUserProgressDataSignal);
                prm.addSceneChangeStateReceiver(this.onSceneChangeStateReceiver);

            }

            // m_refGotoNextButton
            {
                this.m_refGotoNextButton = GameObject.FindObjectOfType<GotoNextSequenceScript>();
            }

#if UNITY_EDITOR

            if (!this.m_refGotoNextButton)
            {
                Debug.LogWarning("m_refGotoNextButton is null");
            }

#endif

        }

        /// <summary>
        /// SceneChangeState receiver
        /// </summary>
        /// <param name="scState">SceneChangeState</param>
        // -----------------------------------------------------------------------------------------------
        protected virtual void onSceneChangeStateReceiver(SSC.SceneChangeState scState)
        {

            if (scState.stateEnum == SSC.SceneChangeState.StateEnum.ScenePlaying)
            {

                PoetSceneChangeManager pscm = PoetSceneChangeManager.Instance as PoetSceneChangeManager;

                if (pscm.isLoadingCurrentSceneWithUserProgressData())
                {
                    this.m_userProgressData = pscm.getDataFromCurrentUserProgressData<UserProgressData>(this.transform, this);
                    Invoke("showUserProgressDataUi", 0.0f);
                }

            }

        }

        /// <summary>
        /// Show ui by user progress data
        /// </summary>
        // -----------------------------------------------------------------------------------------------
        protected void showUserProgressDataUi()
        {
            this.showUi(this.m_userProgressData.uiIdentifiers, false, false);
        }

        /// <summary>
        /// UserProgressDataSignal receiver
        /// </summary>
        /// <param name="tsState">UserProgressDataSignal</param>
        // ------------------------------------------------------------------------------------------
        protected virtual void onUserProgressDataSignal(UserProgressDataSignal updSignal)
        {

            this.m_userProgressData.clear();

            this.m_userProgressData.uiIdentifiers.AddRange(this.m_currentShowingUi);

            updSignal.addDataAction(this.transform, this, JsonUtility.ToJson(this.m_userProgressData));

        }

        /// <summary>
        /// Update
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected virtual void Update()
        {

            // update values
            {
                this.m_mouseWheel = Input.GetAxis("Mouse ScrollWheel");
                this.m_leftMouseDown = Input.GetMouseButtonDown(0);
                this.m_rightMouseDown = Input.GetMouseButtonDown(1);
            }

            // updateForCommonUi
            {
                this.updateForCommonUi();
            }

        }

        /// <summary>
        /// Update common UI
        /// </summary>
        // -------------------------------------------------------------------------------------------------
        protected virtual void updateForCommonUi()
        {

            if(PoetDialogManager.Instance.nowShowing)
            {
                return;
            }

            // ----------------------------

            if (this.UpdateForTextUi())
            {
                return;
            }

            if (this.UpdateForTextLogUi())
            {
                return;
            }

            if (this.UpdateForGoToNextOnlyUi())
            {
                return;
            }

            if (this.UpdateForChoiceUi())
            {
                return;
            }

            if (this.UpdateForSaveUi())
            {
                return;
            }

            if (this.UpdateForLoadUi())
            {
                return;
            }

            if (this.UpdateForSystemConfigUi())
            {
                return;
            }

            if (this.UpdateForSoundConfigUi())
            {
                return;
            }

        }

        /// <summary>
        /// Update for Text UI
        /// </summary>
        /// <returns>if used</returns>
        // -------------------------------------------------------------------------------------------------
        protected virtual bool UpdateForTextUi()
        {

            if (!this.containsIdentifier(PoetUiIdentifiers.Common.Text))
            {
                return false;
            }

            // ---------------------

            // m_mouseWheel
            {

                if (this.m_mouseWheel > 0.0f)
                {

                    // resetAutoAndSkipModes
                    {
                        SystemManager.Instance.resetAutoAndSkipModes();
                    }

                    if (TextLogManager.isAvailable())
                    {

                        if (this.m_refGotoNextButton)
                        {

                            if (this.m_refGotoNextButton.gameObject == UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject)
                            {
                                TextLogManager.Instance.showTextLogUi();
                            }

                        }

                        else
                        {
                            TextLogManager.Instance.showTextLogUi();
                        }

                    }

                }

                else if (this.m_mouseWheel < 0.0f && PoetOneSceneManager.isAvailable())
                {
                    
                    // resetAutoAndSkipModes
                    {
                        SystemManager.Instance.resetAutoAndSkipModes();
                    }

                    if (this.m_refGotoNextButton && this.m_refGotoNextButton.isInteractable())
                    {

                        if (this.m_refGotoNextButton.gameObject == UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject)
                        {
                            this.m_refGotoNextButton.invokeOnClick();
                        }

                    }

                }

            }

            // right click
            {

                if (this.m_rightMouseDown)
                {

                    if (!SystemManager.Instance.isAuto && !SystemManager.Instance.isSkip)
                    {
                        this.showUi(PoetUiIdentifiers.Common.GoToNextOnly, true, false);
                    }

                    else
                    {
                        SystemManager.Instance.resetAutoAndSkipModes();
                    }

                }

            }

            return true;

        }

        /// <summary>
        /// Update for TextLog UI
        /// </summary>
        /// <returns>if used</returns>
        // -------------------------------------------------------------------------------------------------
        protected virtual bool UpdateForTextLogUi()
        {

            if (!this.containsIdentifier(PoetUiIdentifiers.Common.TextLog))
            {
                return false;
            }

            // ---------------------

            // m_mouseWheel
            {

                if (TextLogManager.isAvailable())
                {

                    if (this.m_mouseWheel > 0.0f)
                    {
                        TextLogManager.Instance.onClickPreviousButton(1);
                    }

                    else if (this.m_mouseWheel < 0.0f)
                    {
                        TextLogManager.Instance.onClickNextButton(1);
                    }

                }

            }

            // right click
            {

                if (this.m_rightMouseDown)
                {
                    this.back(true);
                }

            }

            return true;

        }

        /// <summary>
        /// Update for GoToNextOnly UI
        /// </summary>
        /// <returns>if used</returns>
        // -------------------------------------------------------------------------------------------------
        protected virtual bool UpdateForGoToNextOnlyUi()
        {

            if (!this.containsIdentifier(PoetUiIdentifiers.Common.GoToNextOnly))
            {
                return false;
            }

            // ---------------------

            // right click
            {

                if (this.m_rightMouseDown)
                {
                    this.back(true);
                }

            }

            return true;

        }

        /// <summary>
        /// Update for Choice UI
        /// </summary>
        /// <returns>if used</returns>
        // -------------------------------------------------------------------------------------------------
        protected virtual bool UpdateForChoiceUi()
        {

            if (!this.containsIdentifier(PoetUiIdentifiers.Common.Choice))
            {
                return false;
            }

            // ---------------------

            // m_mouseWheel
            {

                if (this.m_mouseWheel > 0.0f && TextLogManager.isAvailable())
                {
                    TextLogManager.Instance.showTextLogUi();
                }

            }

            // right click
            {

                if (this.m_rightMouseDown)
                {
                    this.showUi(PoetUiIdentifiers.Common.GoToNextOnly, true, false);
                }

            }

            return true;

        }

        /// <summary>
        /// Update for Save UI
        /// </summary>
        /// <returns>if used</returns>
        // -------------------------------------------------------------------------------------------------
        protected virtual bool UpdateForSaveUi()
        {

            if (!this.containsIdentifier(PoetUiIdentifiers.Common.Save))
            {
                return false;
            }

            // ---------------------

            // right click
            {

                if (this.m_rightMouseDown)
                {
                    this.back(true);
                }

            }

            return true;

        }

        /// <summary>
        /// Update for Load UI
        /// </summary>
        /// <returns>if used</returns>
        // -------------------------------------------------------------------------------------------------
        protected virtual bool UpdateForLoadUi()
        {

            if (!this.containsIdentifier(PoetUiIdentifiers.Common.Load))
            {
                return false;
            }

            // ---------------------

            // right click
            {

                if (this.m_rightMouseDown)
                {
                    this.back(true);
                }

            }

            return true;

        }

        /// <summary>
        /// Update for SystemConfig UI
        /// </summary>
        /// <returns>if used</returns>
        // -------------------------------------------------------------------------------------------------
        protected virtual bool UpdateForSystemConfigUi()
        {

            if (!this.containsIdentifier(PoetUiIdentifiers.Common.SystemConfig))
            {
                return false;
            }

            // ---------------------

            // right click
            {

                if (this.m_rightMouseDown)
                {
                    this.back(true);
                }

            }

            return true;

        }

        /// <summary>
        /// Update for SoundConfig UI
        /// </summary>
        /// <returns>if used</returns>
        // -------------------------------------------------------------------------------------------------
        protected virtual bool UpdateForSoundConfigUi()
        {

            if (!this.containsIdentifier(PoetUiIdentifiers.Common.SoundConfig))
            {
                return false;
            }

            // ---------------------

            // right click
            {

                if (this.m_rightMouseDown)
                {
                    this.back(true);
                }

            }

            return true;

        }

    }

}
