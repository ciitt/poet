using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poet
{

    /// <summary>
    /// IEnumeratorStartupManager for Poet
    /// </summary>
    public class PoetIEnumeratorStartupManager : SSC.IEnumeratorStartupManager
    {

        /// <summary>
        /// Create error message for dialog
        /// </summary>
        /// <returns>error message object</returns>
        // -------------------------------------------------------------------------------------------------------
        public override System.Object createErrorMessage()
        {
            return PoetDialogMessageManager.Instance.ieStartupErrorMessage(this.m_error);
        }

    }

}
