using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poet
{

    public class PoetWwwStartupManager : SSC.WwwStartupManager
    {

        /// <summary>
        /// Create error message for dialog
        /// </summary>
        /// <returns>error message object</returns>
        // -------------------------------------------------------------------------------------------------------
        public override System.Object createErrorMessage()
        {
            return PoetDialogMessageManager.Instance.wwwStartupErrorMessage(this.m_errorUrl, this.m_error);
        }

    }

}
