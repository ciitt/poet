﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poet
{

    /// <summary>
    /// Bezier curve manager
    /// </summary>
    public class BezierCurveManager : SSC.SingletonMonoBehaviour<BezierCurveManager>
    {

        /// <summary>
        /// IdAndBezierCurve List
        /// </summary>
        [SerializeField]
        [Tooltip("IdAndBezierCurve List")]
        protected List<IdAndBezierCurve> m_bezierList;

        /// <summary>
        /// IdAndBezierCurve dictionary
        /// </summary>
        protected Dictionary<string, IdAndBezierCurve> m_bezierDict = new Dictionary<string, IdAndBezierCurve>();

        /// <summary>
        /// Called in Awake
        /// </summary>
        // -----------------------------------------------------------------------------------------------
        protected override void initOnAwake()
        {

            this.m_bezierDict = Funcs.listToDictionary<IdAndBezierCurve>(this.m_bezierList);

        }

        /// <summary>
        /// Get BezierCurveScript
        /// </summary>
        /// <param name="identifier">identifier</param>
        /// <returns>BezierCurveScript</returns>
        // -----------------------------------------------------------------------------------------------
        public BezierCurveScript getBezierCurveScript(string identifier)
        {

            if(this.m_bezierDict.ContainsKey(identifier))
            {
                return this.m_bezierDict[identifier].bezier;
            }

            else
            {
                Debug.LogWarning("BezierCurveScript not found : " + identifier);
            }

            return null;

        }

    }

}
