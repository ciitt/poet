﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Poet
{

    /// <summary>
    /// Album scene UI manager
    /// </summary>
    public class AlbumSceneUiManager : SSC.SceneUiManager
    {

        /// <summary>
        /// Reference to first AlbumGraphicDataHolder
        /// </summary>
        [Space(10.0f)]
        [SerializeField]
        [Tooltip("Reference to first AlbumGraphicDataHolder")]
        protected AlbumGraphicDataHolder m_refFirstAlbumGraphicDataHolder = null;

        /// <summary>
        /// Reference to first AlbumMemoryDataHolder
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to first AlbumMemoryDataHolder")]
        protected AlbumMemoryDataHolder m_refFirstAlbumMemoryDataHolder = null;

        /// <summary>
        /// Reference to first AlbumMusicDataHolder
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to first AlbumMusicDataHolder")]
        protected AlbumMusicDataHolder m_refFirstAlbumMusicDataHolder = null;

        /// <summary>
        /// Reference to album memory CustomToggleButton
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to album memory CustomToggleButton")]
        protected CustomToggleButton m_refAlbumMemoryToggle = null;

        /// <summary>
        /// Events for back from memory scene
        /// </summary>
        [SerializeField]
        [Tooltip("Events for back from memory scene")]
        protected UnityEvent m_refEventForBackFromMemoryScene;

        /// <summary>
        /// Mouse wheel value
        /// </summary>
        protected float m_mouseWheel = 0.0f;

        /// <summary>
        /// Right mouse button down
        /// </summary>
        protected bool m_rightMouseDown = false;

        /// <summary>
        /// Current reference to AlbumGraphicCsvHolder
        /// </summary>
        protected AlbumGraphicCsvHolder m_refCurrentAlbumGraphicCsvHolder = null;

        // ----------------------------------------------------------------------------

        /// <summary>
        /// Start
        /// </summary>
        // -----------------------------------------------------------------------------------
        protected virtual void Start()
        {

            // addSceneChangeStateReceiver
            {
                PoetReduxManager.Instance.addSceneChangeStateReceiver(this.onSceneChangeStateReceiver);
            }

        }

        /// <summary>
        /// Update
        /// </summary>
        // ----------------------------------------------------------------------------
        protected virtual void Update()
        {

            // update values
            {
                this.m_mouseWheel = Input.GetAxis("Mouse ScrollWheel");
                this.m_rightMouseDown = Input.GetMouseButtonDown(1);
            }

            // updateForSceneUi
            {
                this.updateForSceneUi();
            }

        }

        /// <summary>
        /// SceneChangeState receiver
        /// </summary>
        /// <param name="scState">SceneChangeState</param>
        // -----------------------------------------------------------------------------------------------
        protected virtual void onSceneChangeStateReceiver(SSC.SceneChangeState scState)
        {

            if (scState.stateEnum == SSC.SceneChangeState.StateEnum.ScenePlaying)
            {

                if (this.containsIdentifier(PoetUiIdentifiers.AlbumScene.AlbumMemoryForBackFromScene))
                {

                    if (this.m_refAlbumMemoryToggle)
                    {
                        this.m_refAlbumMemoryToggle.isOn = true;
                    }

                    this.m_refEventForBackFromMemoryScene.Invoke();

                }

            }

        }

        /// <summary>
        /// Show AlbumGraphicView
        /// </summary>
        /// <param name="holder">AlbumGraphicCsvHolder</param>
        // ----------------------------------------------------------------------------
        public void showAlbumGraphicView(AlbumGraphicCsvHolder holder, string targetCsvRowIdentifier)
        {

            if(holder == null)
            {
                return;
            }

            // ------------

            this.m_refCurrentAlbumGraphicCsvHolder = holder;

            holder.startShowing(targetCsvRowIdentifier);

            this.showUiOnButtonClickForAlbum(PoetUiIdentifiers.AlbumScene.AlbumGraphicView);

        }

        /// <summary>
        /// Show Ui button function
        /// </summary>
        /// <param name="identifier">ui identifier</param>
        // ----------------------------------------------------------------------------------------
        public void showUiOnButtonClickForAlbum(string identifier)
        {
            this.showUiOnButtonClickForAlbum(identifier, true);
        }

        /// <summary>
        /// Show Ui button function
        /// </summary>
        /// <param name="identifier">ui identifier</param>
        /// <param name="show0thData">show first data</param>
        // ----------------------------------------------------------------------------------------
        public void showUiOnButtonClickForAlbum(string identifier, bool show0thData)
        {

            if(!this.containsIdentifier(identifier))
            {

                this.showUi(identifier, true, false);

                if (show0thData)
                {

                    if (identifier == PoetUiIdentifiers.AlbumScene.AlbumGraphicSelectForFirst)
                    {
                        if (this.m_refFirstAlbumGraphicDataHolder)
                        {
                            this.m_refFirstAlbumGraphicDataHolder.setToggleOn(true);
                            this.m_refFirstAlbumGraphicDataHolder.onClick(0);
                        }
                    }

                    else if (identifier == PoetUiIdentifiers.AlbumScene.AlbumMemoryForFirst)
                    {
                        if (this.m_refFirstAlbumMemoryDataHolder)
                        {
                            this.m_refFirstAlbumMemoryDataHolder.setToggleOn(true);
                            this.m_refFirstAlbumMemoryDataHolder.onClick(0);
                        }
                    }

                    else if (identifier == PoetUiIdentifiers.AlbumScene.AlbumMusic)
                    {
                        if (this.m_refFirstAlbumMusicDataHolder)
                        {
                            this.m_refFirstAlbumMusicDataHolder.onClick(0);
                        }
                    }

                }

            }

        }

        /// <summary>
        /// onClick for show next graphic button
        /// </summary>
        // ----------------------------------------------------------------------------
        public void onClickShowNextGraphicButton()
        {

            if(this.m_refCurrentAlbumGraphicCsvHolder)
            {
                this.m_refCurrentAlbumGraphicCsvHolder.showNext();
            }

        }

        /// <summary>
        /// Function for back button
        /// </summary>
        // -------------------------------------------------------------------------------------------------
        public void onClickBackButton()
        {
            (Poet.PoetSceneChangeManager.Instance as Poet.PoetSceneChangeManager).useSimpleNowLoadingOnce();
            PoetSceneChangeManager.Instance.backToTitleScene();
        }

        /// <summary>
        /// Update scene UI
        /// </summary>
        // -------------------------------------------------------------------------------------------------
        protected virtual void updateForSceneUi()
        {

            if (this.UpdateForAlbumGraphicViewUi())
            {
                return;
            }

        }

        /// <summary>
        /// Update for AlbumGraphicView UI
        /// </summary>
        /// <returns>if used</returns>
        // -------------------------------------------------------------------------------------------------
        protected virtual bool UpdateForAlbumGraphicViewUi()
        {

            if (!this.containsIdentifier(PoetUiIdentifiers.AlbumScene.AlbumGraphicView))
            {
                return false;
            }

            // ---------------------

            // m_mouseWheel
            {

                if (this.m_mouseWheel > 0.0f && this.m_refCurrentAlbumGraphicCsvHolder)
                {
                    this.m_refCurrentAlbumGraphicCsvHolder.finishAlbumGraphicView();
                }

                else if (this.m_mouseWheel < 0.0f && this.m_refCurrentAlbumGraphicCsvHolder)
                {
                    this.m_refCurrentAlbumGraphicCsvHolder.showNext();
                }

            }

            // right click
            {

                if (this.m_rightMouseDown && this.m_refCurrentAlbumGraphicCsvHolder)
                {
                    this.m_refCurrentAlbumGraphicCsvHolder.finishAlbumGraphicView();
                }

            }

            return true;

        }

        /// <summary>
        /// Set default selectable for album graphic UI
        /// </summary>
        /// <param name="selectable">Selectable</param>
        // ----------------------------------------------------------------------------
        public void setDefaultSelectableForGraphicUi(Selectable selectable)
        {

            if (!selectable)
            {
                return;
            }

            // -------------

            if (this.m_uiDictionary.ContainsKey(PoetUiIdentifiers.AlbumScene.AlbumGraphicSelectForBackFromView))
            {
                this.m_uiDictionary[PoetUiIdentifiers.AlbumScene.AlbumGraphicSelectForBackFromView].defaultSelectable = selectable;
            }

        }

        /// <summary>
        /// Set default selectable for album memory UI
        /// </summary>
        /// <param name="selectable">Selectable</param>
        // ----------------------------------------------------------------------------
        public void setDefaultSelectableForMemoryUi(Selectable selectable)
        {

            if(!selectable)
            {
                return;
            }

            // -------------

            if(this.m_uiDictionary.ContainsKey(PoetUiIdentifiers.AlbumScene.AlbumMemoryForBackFromScene))
            {
                this.m_uiDictionary[PoetUiIdentifiers.AlbumScene.AlbumMemoryForBackFromScene].defaultSelectable = selectable;
            }

        }

    }

}
