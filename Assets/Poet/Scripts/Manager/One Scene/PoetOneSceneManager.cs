﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace Poet
{

    /// <summary>
    /// Manager for Poet scene
    /// </summary>
    public class PoetOneSceneManager : SSC.SingletonMonoBehaviour<PoetOneSceneManager>
    {

        protected class CurrentOperationValues
        {

            public float delayMainProcessSeconds = 0.0f;
            public string nextSceneName = "";
            public bool finish = false;
            public List<string> finishOperationParameters = new List<string>();

            public virtual void reset()
            {
                this.delayMainProcessSeconds = 0.0f;
                this.nextSceneName = "";
                this.finish = false;
                this.finishOperationParameters.Clear();
            }

        }

        /// <summary>
        /// Csv header for operation
        /// </summary>
        [SerializeField]
        [Tooltip("Csv header for operation")]
        protected string m_csvHeaderForOperation = "Operation";

        /// <summary>
        /// UnityEvent at poet start
        /// </summary>
        [SerializeField]
        [Tooltip("UnityEvent at poet start")]
        protected UnityEvent m_eventAtPoetStart = null;

        /// <summary>
        /// UnityEvent at poet finish
        /// </summary>
        [SerializeField]
        [Tooltip("UnityEvent at poet finish")]
        protected UnityEvent m_eventAtPoetFinish = null;

        /// <summary>
        /// Current PoetCsvHolder
        /// </summary>
        protected PoetCsvHolder m_currentCsvHolder = null;

        /// <summary>
        /// Current csv row index
        /// </summary>
        protected int m_currentCsvRowIndex = -1;

        /// <summary>
        /// Next csv row index
        /// </summary>
        protected int m_nextCsvRowIndex = -1;

        /// <summary>
        /// Current operation values
        /// </summary>
        protected CurrentOperationValues m_operationValues = new CurrentOperationValues();

        /// <summary>
        /// IEnumerator for sendSequenceStateIE
        /// </summary>
        protected IEnumerator m_sendSequenceStateIE = null;

        /// <summary>
        /// IEnumerator for autoAndSkipIE
        /// </summary>
        protected IEnumerator m_autoAndSkipIE = null;

        /// <summary>
        /// Is current sequence alredy read
        /// </summary>
        protected bool m_currentAlreadyRead = false;

        /// <summary>
        /// Lock going to next
        /// </summary>
        protected bool m_lockGoingToNext = false;

        // -----------------------------------------------------------------------------------------------

        /// <summary>
        /// Current PoetCsvHolder
        /// </summary>
        public PoetCsvHolder currentCsvHolder { get { return this.m_currentCsvHolder; } set { this.m_currentCsvHolder = value; } }

        /// <summary>
        /// Is current PoetCsvHolder active
        /// </summary>
        public bool isCurrentCsvHolderActive { get { return this.m_currentCsvHolder; } }

        /// <summary>
        /// Current csv row index
        /// </summary>
        public int currentCsvRowIndex { get { return this.m_currentCsvRowIndex; } }

        /// <summary>
        /// Next csv row index
        /// </summary>
        public int nextCsvRowIndex { get { return this.m_nextCsvRowIndex; } set { this.m_nextCsvRowIndex = value; } }

        /// <summary>
        /// Lock going to next
        /// </summary>
        public bool lockGoingToNext { get { return this.m_lockGoingToNext; } set { this.m_lockGoingToNext = value; } }

        // -----------------------------------------------------------------------------------------------

        /// <summary>
        /// Called in Awake
        /// </summary>
        // -----------------------------------------------------------------------------------------------
        protected override void initOnAwake()
        {
           
        }

        /// <summary>
        /// Start
        /// </summary>
        // -----------------------------------------------------------------------------------------------
        protected virtual void Start()
        {

            PoetReduxManager prm = PoetReduxManager.Instance as PoetReduxManager;

            prm.addTextFinishedStateReceiver(this.onTextShowingState);

        }

        /// <summary>
        /// TextShowingState receiver
        /// </summary>
        /// <param name="tsState">TextShowingState</param>
        // ------------------------------------------------------------------------------------------
        protected virtual void onTextShowingState(TextShowingState tsState)
        {

            if (tsState.stateEnum == TextShowingState.StateEnum.ShowingTextStarted)
            {

                if (this.m_autoAndSkipIE != null)
                {
                    StopCoroutine(this.m_autoAndSkipIE);
                    this.m_autoAndSkipIE = null;
                }

            }

            else if (tsState.stateEnum == TextShowingState.StateEnum.ShowingTextFinished)
            {

                if (this.m_autoAndSkipIE != null)
                {
                    StopCoroutine(this.m_autoAndSkipIE);
                }

                if (!this.m_operationValues.finish && string.IsNullOrEmpty(this.m_operationValues.nextSceneName))
                {
                    StartCoroutine(this.m_autoAndSkipIE = this.autoAndSkipIE());
                }

            }

        }

        /// <summary>
        /// IEnumerator for auto and skip
        /// </summary>
        /// <returns>IEnumerator</returns>
        // -----------------------------------------------------------------------------------------------
        protected virtual IEnumerator autoAndSkipIE()
        {

            PoetConfigDataSO conf = SystemManager.Instance.configDataSO<PoetConfigDataSO>();

            float timerForAuto = Time.realtimeSinceStartup;

            float wait = Mathf.Lerp(
                conf.autoModeWaitSeconds0,
                conf.autoModeWaitSeconds1,
                conf.autoModeSpeed01
                );

            while (true)
            {

                if (PoetCommonUiManager.Instance.containsIdentifier(PoetUiIdentifiers.Common.Choice))
                {
                    //SystemManager.Instance.resetAutoAndSkipModes();
                    break;
                }

                else if (
                    !PoetCommonUiManager.Instance.containsIdentifier(PoetUiIdentifiers.Common.Text) ||
                    PoetDialogManager.Instance.nowShowing
                    )
                {
                    SystemManager.Instance.resetAutoAndSkipModes();
                }

                else if (SystemManager.Instance.isSkip)
                {

                    if (this.shouldSkip())
                    {
                        Invoke("stopAutoAndSkipIEAndGoToNext", 0.0f);
                        break;
                    }

                }

                else if (SystemManager.Instance.isAuto)
                {

                    if (PoetAudioManager.Instance.isVoicePlaying())
                    {
                        timerForAuto = Time.realtimeSinceStartup;
                    }

                    else
                    {

                        if ((Time.realtimeSinceStartup - timerForAuto) > wait)
                        {
                            Invoke("stopAutoAndSkipIEAndGoToNext", 0.0f);
                            break;
                        }

                    }

                }

                yield return null;

            }

            this.m_autoAndSkipIE = null;

        }

        /// <summary>
        /// Stop autoAndSkipIE and got to next
        /// </summary>
        // -----------------------------------------------------------------------------------------------
        protected void stopAutoAndSkipIEAndGoToNext()
        {

            if (this.m_autoAndSkipIE != null)
            {
                StopCoroutine(this.m_autoAndSkipIE);
                this.m_autoAndSkipIE = null;
            }

            this.goToNextSequence(false);

        }

        /// <summary>
        /// Set current csv holder
        /// </summary>
        /// <param name="csvHolder">PoetCsvHolder</param>
        /// <param name="setCurrentIndex">index</param>
        // -----------------------------------------------------------------------------------------------
        public void setCurrentCsvHolderWithoutGoingToSequence(PoetCsvHolder csvHolder, int setCurrentIndex)
        {

            this.m_currentCsvHolder = csvHolder;
            this.m_currentCsvRowIndex = setCurrentIndex;
            this.m_nextCsvRowIndex = setCurrentIndex + 1;

            if (this.m_currentCsvHolder != null)
            {
                this.solveOperations(this.m_currentCsvHolder.getHeadersAndCells(setCurrentIndex));
            }

        }

        /// <summary>
        /// Set current csv holder
        /// </summary>
        /// <param name="csvHolder">PoetCsvHolder</param>
        /// <param name="identifier">identifier</param>
        // -----------------------------------------------------------------------------------------------
        public void setCurrentCsvHolderWithoutGoingToSequence(PoetCsvHolder csvHolder, string identifier)
        {

            if (csvHolder)
            {
                this.setCurrentCsvHolderWithoutGoingToSequence(csvHolder, csvHolder.getCsvRowIndex(identifier));
            }

            else
            {
                this.setCurrentCsvHolderWithoutGoingToSequence(csvHolder, -1);
            }

        }

        /// <summary>
        /// Solve operations
        /// </summary>
        /// <param name="headersAndCells">headers and cells</param>
        // -----------------------------------------------------------------------------------------------
        protected virtual void solveOperations(Dictionary<string, string> headersAndCells)
        {

            // reset operation variables
            {
                this.m_operationValues.reset();
            }

            // ------------

            if (!headersAndCells.ContainsKey(this.m_csvHeaderForOperation))
            {
                return;
            }

            // ------------

            var fnapList = CsvParser.parseFunctionsInCell(headersAndCells[this.m_csvHeaderForOperation]);

            string temp = "";

            bool jumpIf = false;

            foreach (var fnap in fnapList.list)
            {

                if (fnap.functionName == "Jump")
                {
                    this.m_nextCsvRowIndex = this.m_currentCsvHolder.getCsvRowIndex(fnap.getStringParameter(0));
                }

                else if (fnap.functionName == "JumpIfItem")
                {

                    if (!jumpIf && Poet.SolveJumpIfManager.isAvailable())
                    {

                        temp = Poet.SolveJumpIfManager.Instance.solveJumpIfItem(
                            fnap.getStringParameter(0),
                            fnap.getStringParameter(1),
                            fnap.getStringParameter(2),
                            fnap.getStringParameter(3)
                            )
                            ;

                        if (!string.IsNullOrEmpty(temp))
                        {
                            this.m_nextCsvRowIndex = this.m_currentCsvHolder.getCsvRowIndex(temp);
                            jumpIf = true;
                        }

                    }

                }

                else if (fnap.functionName == "JumpIfFlag")
                {

                    if (!jumpIf && Poet.SolveJumpIfManager.isAvailable())
                    {

                        temp = Poet.SolveJumpIfManager.Instance.solveJumpIfFlag(
                            fnap.getStringParameter(0),
                            fnap.getStringParameter(1),
                            fnap.getStringParameter(2),
                            fnap.getStringParameter(3)
                            )
                            ;

                        if (!string.IsNullOrEmpty(temp))
                        {
                            this.m_nextCsvRowIndex = this.m_currentCsvHolder.getCsvRowIndex(temp);
                            jumpIf = true;
                        }

                    }

                }

                else if (fnap.functionName == "DelayMainProcess")
                {
                    this.m_operationValues.delayMainProcessSeconds = fnap.getFloatParameter(0, 0.0f);
                }

                else if (fnap.functionName == "Finish")
                {
                    this.m_operationValues.finish = true;
                    this.m_operationValues.finishOperationParameters.AddRange(fnap.parameters);
                }

                else if (fnap.functionName == "LoadScene")
                {
                    this.m_operationValues.nextSceneName = fnap.getStringParameter(0);
                }

            } // foreach

            // m_currentAlreadyRead
            {

                this.m_currentAlreadyRead = SystemManager.Instance.isAlreadyRead(
                    this.m_currentCsvHolder.csvName(),
                    this.m_currentCsvHolder.getReadRange(this.m_currentCsvRowIndex)
                    );

                this.updateCurrentAlreadyReadRange();

            }

        }

        /// <summary>
        /// Send sequence state
        /// </summary>
        // -----------------------------------------------------------------------------------------------
        protected void sendSequenceState()
        {

            PoetConfigDataSO conf = SystemManager.Instance.configDataSO<PoetConfigDataSO>();

            if (this.m_currentCsvHolder)
            {

                if (this.m_sendSequenceStateIE != null)
                {
                    return;
                }

                // stop voice
                {

                    if (
                        conf.voiceStopMode == PoetConfigDataSO.VoiceStopMode.NextText &&
                        PoetAudioManager.Instance.refVoice
                        )
                    {
                        PoetAudioManager.Instance.refVoice.stop(0.0f);
                    }

                }

                // sendSequenceStateIE
                {

                    PoetSceneChangeManager pscm = PoetSceneChangeManager.Instance as PoetSceneChangeManager;

                    if (pscm.isLoadingAlbumMemoryScene())
                    {
                        StartCoroutine(this.m_sendSequenceStateIE = this.sendSequenceStateForAlbumMemoryIE());
                    }

                    else
                    {
                        StartCoroutine(this.m_sendSequenceStateIE = this.sendSequenceStateIE());
                    }

                }
                

            }

            else
            {
                Debug.LogWarning("m_currentCsvHolder is null");
            }

        }

        /// <summary>
        /// Send sequence state IEnumerator
        /// </summary>
        /// <returns>IEnumerator</returns>
        // -----------------------------------------------------------------------------------------------
        protected virtual IEnumerator sendSequenceStateIE()
        {

            if (!this.m_currentCsvHolder)
            {
                this.m_sendSequenceStateIE = null;
                yield break;
            }

            yield return null;

            // ----------------

            PoetReduxManager prm = PoetReduxManager.Instance as PoetReduxManager;
            var psState = prm.PoetSequenceStateWatcher.state();

            var headersAndCells = this.m_currentCsvHolder.getHeadersAndCells(this.m_currentCsvRowIndex);

            bool skipUiTransition = false;

            // skipUiTransition
            {
                skipUiTransition = (psState.stateEnum == PoetSequenceState.StateEnum.StartOperation);
            }

            // solveOperations
            {
                this.solveOperations(headersAndCells);
            }

            // PreProcess
            {

                psState.setState(
                    prm.PoetSequenceStateWatcher,
                    PoetSequenceState.StateEnum.PreProcess,
                    this.m_currentCsvHolder.csvName(),
                    this.m_currentCsvRowIndex,
                    headersAndCells
                    );

            }

            // hide ui
            {
                if (!this.shouldSkip() && this.m_operationValues.delayMainProcessSeconds > 0.0f)
                {
                    PoetCommonUiManager.Instance.showUi("", true, false);
                }
            }

            // wait
            {

                if (this.shouldSkip())
                {
                    yield return null;
                }

                else
                {

                    float timer = 0.0f;

                    do
                    {

                        timer += Time.deltaTime;
                        yield return null;

                    } while (timer < this.m_operationValues.delayMainProcessSeconds);

                }

            }

            // showUi
            {
                
                if (!this.m_operationValues.finish && string.IsNullOrEmpty(this.m_operationValues.nextSceneName))
                {

                    if(this.m_lockGoingToNext)
                    {
                        PoetCommonUiManager.Instance.showUi(PoetUiIdentifiers.Common.Choice, true, false);
                    }

                    else
                    {
                        PoetCommonUiManager.Instance.showUi(PoetUiIdentifiers.Common.Text, true, false);
                    }

                    // wait nowInShowingOrHidingTransition
                    {

                        PoetCommonUiManager pcuim = PoetCommonUiManager.Instance as PoetCommonUiManager;

                        while (pcuim.nowInShowingOrHidingTransition && !skipUiTransition)
                        {
                            yield return null;
                        }

                    }

                }

            }

            // MainProcess
            {

                psState.setState(
                    prm.PoetSequenceStateWatcher,
                    PoetSequenceState.StateEnum.MainProcess,
                    this.m_currentCsvHolder.csvName(),
                    this.m_currentCsvRowIndex,
                    headersAndCells
                    );

            }

            yield return null;

            // finish
            {

                // showUi
                {

                    if (this.m_operationValues.finish)
                    {
                        PoetCommonUiManager.Instance.showUi("", true, false);
                    }

                }

                if (this.m_operationValues.finish || !string.IsNullOrEmpty(this.m_operationValues.nextSceneName))
                {

                    // resetAutoAndSkipModes
                    {
                        SystemManager.Instance.resetAutoAndSkipModes();
                    }

                    // m_lockGoingToNext
                    {
                        this.m_lockGoingToNext = false;
                    }

                    // setStateForFinishOperation
                    {

                        psState.setStateForFinishOperation(
                            prm.PoetSequenceStateWatcher,
                            this.m_currentCsvHolder.csvName(),
                            this.m_currentCsvRowIndex,
                            headersAndCells,
                            this.m_operationValues.finishOperationParameters
                            );

                    }

                    // m_currentCsvHolder
                    {
                        this.m_currentCsvHolder = null;
                    }

                    // m_eventAtPoetFinish
                    {
                        this.m_eventAtPoetFinish.Invoke();
                    }

                }

                // loadNextScene
                {

                    if (!string.IsNullOrEmpty(this.m_operationValues.nextSceneName))
                    {
                        string newNextSceneName = this.changeNextSceneName(this.m_operationValues.nextSceneName);
                        PoetSceneChangeManager.Instance.loadNextScene(newNextSceneName);
                    }

                }

            }

            this.m_sendSequenceStateIE = null;

        }

        /// <summary>
        /// Send sequence state for album memoryIEnumerator
        /// </summary>
        /// <returns>IEnumerator</returns>
        // -----------------------------------------------------------------------------------------------
        protected virtual IEnumerator sendSequenceStateForAlbumMemoryIE()
        {

            if (!this.m_currentCsvHolder)
            {
                this.m_sendSequenceStateIE = null;
                yield break;
            }

            yield return null;

            // ----------------

            PoetReduxManager prm = PoetReduxManager.Instance as PoetReduxManager;
            var psState = prm.PoetSequenceStateWatcher.state();
            var pscm = PoetSceneChangeManager.Instance as PoetSceneChangeManager;

            var headersAndCells = this.m_currentCsvHolder.getHeadersAndCells(this.m_currentCsvRowIndex);

            bool finishAlbum = false;
            bool skipUiTransition = false;

            // skipUiTransition
            {
                skipUiTransition = (psState.stateEnum == PoetSequenceState.StateEnum.StartOperation);
            }

            // solveOperations
            {
                this.solveOperations(headersAndCells);
            }

            // finishAlbum
            {

                finishAlbum = finishAlbum || this.m_operationValues.finish;
                finishAlbum = finishAlbum || !string.IsNullOrEmpty(this.m_operationValues.nextSceneName);
                finishAlbum = finishAlbum ||
                    (
                    this.m_currentCsvHolder.getCsvRowIdentifierBelongTo(this.m_currentCsvRowIndex) ==
                    pscm.albumMemoryData.endCsvRowIdentifier
                    );

            }

            if (finishAlbum)
            {

                // resetAutoAndSkipModes
                {
                    SystemManager.Instance.resetAutoAndSkipModes();
                }

                // m_lockGoingToNext
                {
                    this.m_lockGoingToNext = false;
                }

                // setStateForFinishOperation
                {

                    psState.setStateForFinishOperation(
                        prm.PoetSequenceStateWatcher,
                        this.m_currentCsvHolder.csvName(),
                        this.m_currentCsvRowIndex,
                        headersAndCells,
                        this.m_operationValues.finishOperationParameters
                        );

                }

                // m_currentCsvHolder
                {
                    this.m_currentCsvHolder = null;
                }

                // m_eventAtPoetFinish
                {
                    this.m_eventAtPoetFinish.Invoke();
                }

                // finishAlbumMemoryScene
                {
                    (PoetSceneChangeManager.Instance as PoetSceneChangeManager).finishAlbumMemoryScene();
                }

            }

            else
            {

                // PreProcess
                {

                    psState.setState(
                        prm.PoetSequenceStateWatcher,
                        PoetSequenceState.StateEnum.PreProcess,
                        this.m_currentCsvHolder.csvName(),
                        this.m_currentCsvRowIndex,
                        headersAndCells
                        );

                }

                // hide ui
                {
                    if (!this.shouldSkip() && this.m_operationValues.delayMainProcessSeconds > 0.0f)
                    {
                        PoetCommonUiManager.Instance.showUi("", true, false);
                    }
                }

                // wait
                {

                    if (this.shouldSkip())
                    {
                        yield return null;
                    }

                    else
                    {

                        float timer = 0.0f;

                        do
                        {

                            timer += Time.deltaTime;
                            yield return null;

                        } while (timer < this.m_operationValues.delayMainProcessSeconds);

                    }

                }

                // showUi
                {

                    if (!this.m_operationValues.finish && string.IsNullOrEmpty(this.m_operationValues.nextSceneName))
                    {

                        if (!this.m_lockGoingToNext)
                        {
                            PoetCommonUiManager.Instance.showUi(PoetUiIdentifiers.Common.Text, true, false);
                        }

                        // wait nowInShowingOrHidingTransition
                        {

                            PoetCommonUiManager pcuim = PoetCommonUiManager.Instance as PoetCommonUiManager;

                            while (pcuim.nowInShowingOrHidingTransition && !skipUiTransition)
                            {
                                yield return null;
                            }

                        }

                    }

                }

                // MainProcess
                {

                    psState.setState(
                        prm.PoetSequenceStateWatcher,
                        PoetSequenceState.StateEnum.MainProcess,
                        this.m_currentCsvHolder.csvName(),
                        this.m_currentCsvRowIndex,
                        headersAndCells
                        );

                }

                yield return null;

            }

            this.m_sendSequenceStateIE = null;

        }

        /// <summary>
        /// Change next scene name
        /// </summary>
        /// <param name="nextSceneName">next scene name</param>
        /// <returns>new next scene name</returns>
        // -----------------------------------------------------------------------------------------------
        protected virtual string changeNextSceneName(string nextSceneName)
        {
            return nextSceneName;
        }

        /// <summary>
        /// Go to next sequence
        /// </summary>
        // -----------------------------------------------------------------------------------------------
        public virtual void onClickGoToNextSequenceButton()
        {

            if (this.m_sendSequenceStateIE != null)
            {
                return;
            }

            if (this.m_lockGoingToNext)
            {
                return;
            }

            // -------------

            if (this.m_autoAndSkipIE != null)
            {
                StopCoroutine(this.m_autoAndSkipIE);
                this.m_autoAndSkipIE = null;
            }

            // -------------

            if (SystemManager.Instance.isSkip)
            {
                SystemManager.Instance.resetAutoAndSkipModes();
            }

            else if (SystemManager.Instance.isAuto)
            {
                this.goToNextSequence(false);
            }

            else
            {
                this.goToNextSequence(false);
            }

        }

        /// <summary>
        /// Go to next sequence
        /// </summary>
        /// <param name="forcibly">forcibly</param>
        // -----------------------------------------------------------------------------------------------
        public virtual void goToNextSequence(bool forcibly)
        {

            PoetReduxManager prm = PoetReduxManager.Instance as PoetReduxManager;
            var tsState = prm.TextFinishedStateWatcher.state();

            if (forcibly)
            {

                if (this.m_sendSequenceStateIE != null)
                {
                    StopCoroutine(this.m_sendSequenceStateIE);
                    this.m_sendSequenceStateIE = null;
                }

                this.m_lockGoingToNext = false;

                this.goToSequence(this.m_nextCsvRowIndex);

            }

            else
            {

                if (this.m_sendSequenceStateIE != null)
                {
                    return;
                }

                if (this.m_lockGoingToNext)
                {
                    return;
                }

                if (tsState.stateEnum == TextShowingState.StateEnum.ShowingTextFinished)
                {
                    this.goToSequence(this.m_nextCsvRowIndex);
                }

            }

        }

        /// <summary>
        /// Send sequence
        /// </summary>
        /// <param name="csvRowIndex">index</param>
        // -----------------------------------------------------------------------------------------------
        public void goToSequence(int csvRowIndex)
        {

            this.m_currentCsvRowIndex = csvRowIndex;
            this.m_nextCsvRowIndex = csvRowIndex + 1;

            this.sendSequenceState();

        }

        /// <summary>
        /// Send sequence
        /// </summary>
        /// <param name="identifier">identifier</param>
        // -----------------------------------------------------------------------------------------------
        public void goToSequence(string identifier)
        {

            if (this.m_currentCsvHolder)
            {
                this.goToSequence(this.m_currentCsvHolder.getCsvRowIndex(identifier));
            }

        }

        /// <summary>
        /// Set current csv holder and send sequence
        /// </summary>
        /// <param name="csvHolder">target PoetCsvHolder</param>
        /// <param name="csvRowIndex">index</param>
        // -----------------------------------------------------------------------------------------------
        public void setCurrentCsvHolderAndGoToSequence(PoetCsvHolder csvHolder, int csvRowIndex)
        {

            // m_currentCsvHolder
            {
                this.m_currentCsvHolder = csvHolder;
            }

            // m_lockGoingToNext
            {
                this.m_lockGoingToNext = false;
            }

            // setStateForStartOperation
            {

                PoetReduxManager prm = PoetReduxManager.Instance as PoetReduxManager;
                var psState = prm.PoetSequenceStateWatcher.state();

                psState.setStateForStartOperation(
                    prm.PoetSequenceStateWatcher,
                    this.m_currentCsvHolder.csvName()
                    );

            }

            // m_eventAtPoetStart
            {
                this.m_eventAtPoetStart.Invoke();
            }

            // goToSequence
            {
                this.goToSequence(csvRowIndex);
            }

        }

        /// <summary>
        /// Set current csv holder and send sequence
        /// </summary>
        /// <param name="csvHolder">target PoetCsvHolder</param>
        /// <param name="identifier">identifier</param>
        // -----------------------------------------------------------------------------------------------
        public void setCurrentCsvHolderAndGoToSequence(PoetCsvHolder csvHolder, string identifier)
        {

            this.setCurrentCsvHolderAndGoToSequence(
                csvHolder,
                (csvHolder) ? csvHolder.getCsvRowIndex(identifier) : -1
                );

        }

        /// <summary>
        /// Update already read range
        /// </summary>
        // -----------------------------------------------------------------------------------------------
        protected virtual void updateCurrentAlreadyReadRange()
        {

            PoetSceneChangeManager pscm = PoetSceneChangeManager.Instance as PoetSceneChangeManager;

            if (!pscm.isLoadingAlbumMemoryScene())
            {

                if (this.m_currentCsvHolder)
                {
                    SystemManager.Instance.updateAlreadyReadRange(
                        this.m_currentCsvHolder.csvName(),
                        this.m_currentCsvHolder.getReadRange(this.m_currentCsvRowIndex)
                        );
                }

            }

        }

        /// <summary>
        /// Get current CsvReadRange
        /// </summary>
        /// <returns></returns>
        // -----------------------------------------------------------------------------------------------
        public CsvReadRange currentReadRange()
        {

            if(this.m_currentCsvHolder)
            {
                return this.m_currentCsvHolder.getReadRange(this.m_currentCsvRowIndex);
            }

            return null;

        }

        /// <summary>
        /// Is current sequence already read
        /// </summary>
        /// <returns>already read</returns>
        // -----------------------------------------------------------------------------------------------
        public virtual bool isCurrentSequenceAlreadyRead()
        {

            PoetSceneChangeManager pscm = PoetSceneChangeManager.Instance as PoetSceneChangeManager;

            if (pscm.isLoadingAlbumMemoryScene())
            {
                return true;
            }

            return this.m_currentAlreadyRead;

        }

        /// <summary>
        /// Is already read
        /// </summary>
        /// <returns>already read</returns>
        // -----------------------------------------------------------------------------------------------
        public virtual bool isAlreadyRead(string csvRowIdentifier)
        {

            if (this.m_currentCsvHolder)
            {
                return SystemManager.Instance.isAlreadyRead(
                    this.m_currentCsvHolder.csvName(),
                    csvRowIdentifier
                    );
            }

            return false;

        }

        /// <summary>
        /// Should skip
        /// </summary>
        /// <returns>should</returns>
        // -----------------------------------------------------------------------------------------------
        public virtual bool shouldSkip()
        {

            PoetSceneChangeManager pscm = PoetSceneChangeManager.Instance as PoetSceneChangeManager;

            if (pscm.isLoadingAlbumMemoryScene())
            {
                return SystemManager.Instance.isSkip;
            }

            else
            {

                if (SystemManager.Instance.isSkip)
                {

                    PoetConfigDataSO conf = SystemManager.Instance.configDataSO<PoetConfigDataSO>();

                    if (conf.skipMode == PoetConfigDataSO.SkipMode.All)
                    {
                        return true;
                    }

                    else if (conf.skipMode == PoetConfigDataSO.SkipMode.AlreadyRead)
                    {
                        return this.m_currentAlreadyRead;
                    }

#if UNITY_EDITOR

                    else
                    {
                        Debug.LogError("(#if UNITY_EDITOR) Unknown skip mode");
                    }

#endif

                }

            }

            return false;

        }

        /// <summary>
        /// Set next index
        /// </summary>
        /// <param name="index">csv row index</param>
        // -----------------------------------------------------------------------------------------------
        public void setNextCsvRowIndex(int index)
        {
            this.m_nextCsvRowIndex = index;
        }

        /// <summary>
        /// Set next index
        /// </summary>
        /// <param name="index">csv row identifier</param>
        // -----------------------------------------------------------------------------------------------
        public void setNextCsvRowIndex(string csvRowIdentifier)
        {

            if (this.m_currentCsvHolder)
            {
                this.m_nextCsvRowIndex = this.m_currentCsvHolder.getCsvRowIndex(csvRowIdentifier);
            }

            else
            {
                Debug.LogWarning("m_currentCsvHolder is null");
            }

        }

    }

}
