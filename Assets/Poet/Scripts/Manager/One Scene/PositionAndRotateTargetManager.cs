﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poet
{

    /// <summary>
    /// Position and rotate target manager
    /// </summary>
    public class PositionAndRotateTargetManager : SSC.SingletonMonoBehaviour<PositionAndRotateTargetManager>
    {

        /// <summary>
        /// IdAndPosition target list
        /// </summary>
        [SerializeField]
        [Tooltip("IdAndPosition target list")]
        protected List<IdAndTransform> m_idAndTransformList = new List<IdAndTransform>();

        /// <summary>
        /// IdAndPosition for local position list
        /// </summary>
        [SerializeField]
        [Tooltip("IdAndPosition for local position list")]
        protected List<IdAndPosition> m_idAndLocalPositionList = new List<IdAndPosition>();

        /// <summary>
        /// IdAndPosition target dictionary
        /// </summary>
        protected Dictionary<string, IdAndTransform> m_idAndTransformDict = new Dictionary<string, IdAndTransform>();

        /// <summary>
        /// Dictionary for m_idAndLocalPositionList
        /// </summary>
        Dictionary<string, IdAndPosition> m_idAndLocalPositionDict = new Dictionary<string, IdAndPosition>();


#if UNITY_EDITOR

        public List<IdAndPosition> idAndLocalPositionList { get { return this.m_idAndLocalPositionList; } }

        public Vector3 getLocalPositionEditorOnly(string identifier)
        {

            foreach (var val in this.m_idAndLocalPositionList)
            {

                if (val.identifier == identifier)
                {
                    return val.position;
                }

            }

            return Vector3.zero;

        }

#endif

        /// <summary>
        /// Called in Awake
        /// </summary>
        // -------------------------------------------------------------------------------------
        protected override void initOnAwake()
        {

            this.m_idAndTransformDict = Funcs.listToDictionary<IdAndTransform>(this.m_idAndTransformList);
            this.m_idAndLocalPositionDict = Funcs.listToDictionary<IdAndPosition>(this.m_idAndLocalPositionList);

#if UNITY_EDITOR

            foreach (var kv in this.m_idAndTransformDict)
            {

                if (!kv.Value.transform)
                {
                    Debug.LogWarning("(#if UNITY_EDITOR) transform is null : PositionAndRotateTargetManager : " + kv.Key);
                }

            }
#endif

        }

        /// <summary>
        /// Add new IdAndTransform. You should not call this function at Awake
        /// </summary>
        /// <param name="identifier">identifier</param>
        /// <param name="trans">Transform</param>
        // -------------------------------------------------------------------------------------
        public void addNewIdAndTransform(string identifier, Transform trans)
        {

            if (string.IsNullOrEmpty(identifier))
            {

#if UNITY_EDITOR
                Debug.LogWarning("(#if UNITY_EDITOR) identifier is empty : PositionAndRotateTargetManager.addNewIdAndTransform");
#endif
                return;

            }

            else if (this.m_idAndTransformDict.ContainsKey(identifier))
            {

#if UNITY_EDITOR
                Debug.LogWarning("(#if UNITY_EDITOR) m_idAndTransformDict already contains a key : " + identifier);
#endif
                return;

            }

            else if (!trans)
            {

#if UNITY_EDITOR
                Debug.LogWarning("(#if UNITY_EDITOR) trans is null " + identifier);
#endif
                return;

            }

            // -------------------------------------------

            IdAndTransform val = new IdAndTransform();

            val.identifier = identifier;
            val.transform = trans;

            this.m_idAndTransformDict.Add(identifier, val);

#if UNITY_EDITOR
            this.m_idAndTransformList.Add(val);
#endif

        }

        /// <summary>
        /// Get target transform
        /// </summary>
        /// <param name="identifier">identifier</param>
        /// <returns>transform</returns>
        // -------------------------------------------------------------------------------------
        public virtual Transform getTargetTransform(string identifier)
        {

            if (this.m_idAndTransformDict.ContainsKey(identifier))
            {
                return this.m_idAndTransformDict[identifier].transform;
            }


#if UNITY_EDITOR

            else
            {
                Debug.LogWarning("(#if UNITY_EDITOR) m_idAndTransformDict does not contain a key : " + identifier);
            }

#endif

            return null;

        }

        /// <summary>
        /// Get target transform position
        /// </summary>
        /// <param name="identifier">identifier</param>
        /// <returns>position</returns>
        // -------------------------------------------------------------------------------------
        public virtual Vector3 getTargetTransformPosition(string identifier)
        {

            if (this.m_idAndTransformDict.ContainsKey(identifier))
            {

                if (this.m_idAndTransformDict[identifier].transform)
                {
                    return this.m_idAndTransformDict[identifier].transform.position;
                }

            }


#if UNITY_EDITOR

            else
            {
                Debug.LogWarning("(#if UNITY_EDITOR) m_idAndTransformDict does not contain a key : " + identifier);
            }

#endif

            return Vector3.zero;

        }

        /// <summary>
        /// Get local position by identifier
        /// </summary>
        /// <param name="identifier">identifier</param>
        /// <returns>position</returns>
        // -------------------------------------------------------------------------------------
        public virtual Vector3 getLocalPosition(string identifier)
        {

            if (this.m_idAndLocalPositionDict.ContainsKey(identifier))
            {
                return this.m_idAndLocalPositionDict[identifier].position;
            }

            else
            {

#if UNITY_EDITOR

                Debug.LogWarning("(#if UNITY_EDITOR) m_idAndLocalPositionDict does not contain a key : " + identifier);

#endif

            }

            return Vector3.zero;

        }

    }

}
