﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Poet
{

    [CustomEditor(typeof(PoetCsvHolder), true)]
    public class PoetCsvHolderInspector : Editor
    {

        protected SerializedProperty m_receiverMask;

        protected SerializedProperty m_csvHeaderForIdentifier;
        protected SerializedProperty m_loadingMode;
        protected SerializedProperty m_setThisAtSceneStart;
        protected SerializedProperty m_setThisAtSceneStartIdentifier;

        protected SerializedProperty m_csv;
        protected SerializedProperty m_csvEditorOnly;

        protected SerializedProperty m_assetBundleName;
        protected SerializedProperty m_assetBundleVariant;
        protected SerializedProperty m_assetNameInAssetBundle;

        protected UnityEngine.Object m_previousCsvEditorOnly = null;

        /// <summary>
        /// OnEnable
        /// </summary>
        // ----------------------------------------------------------------------------------------
        protected virtual void OnEnable()
        {

            if(!target)
            {
                return;
            }

            this.m_receiverMask = serializedObject.FindProperty("m_receiverMask");

            this.m_csvHeaderForIdentifier = serializedObject.FindProperty("m_csvHeaderForIdentifier");
            this.m_loadingMode = serializedObject.FindProperty("m_loadingMode");
            this.m_setThisAtSceneStart = serializedObject.FindProperty("m_setThisAtSceneStart");
            this.m_setThisAtSceneStartIdentifier = serializedObject.FindProperty("m_setThisAtSceneStartIdentifier");

            this.m_csv = serializedObject.FindProperty("m_csv");
            this.m_csvEditorOnly = serializedObject.FindProperty("m_csvEditorOnly");

            this.m_assetBundleName = serializedObject.FindProperty("m_assetBundleName");
            this.m_assetBundleVariant = serializedObject.FindProperty("m_assetBundleVariant");
            this.m_assetNameInAssetBundle = serializedObject.FindProperty("m_assetNameInAssetBundle");

        }

        /// <summary>
        /// OnInspectorGUI
        /// </summary>
        // ----------------------------------------------------------------------------------------
        public override void OnInspectorGUI()
        {

            serializedObject.Update();

            //
            {

                GUILayout.Space(20f);

                if (GUILayout.Button("Reimport\nAssetBundle Names", GUILayout.MinHeight(30)))
                {
                    this.reimportAssetBundleParameters(true);
                }

                GUILayout.Space(20f);
            }

            PoetCsvHolder script = target as PoetCsvHolder;

            GUI.enabled = false;
            EditorGUILayout.ObjectField("Script", MonoScript.FromMonoBehaviour(script), typeof(MonoScript), false);
            GUI.enabled = true;

            EditorGUILayout.PropertyField(this.m_receiverMask);
            EditorGUILayout.PropertyField(this.m_csvHeaderForIdentifier);
            EditorGUILayout.PropertyField(this.m_loadingMode);
            EditorGUILayout.PropertyField(this.m_setThisAtSceneStart);

            GUI.enabled = (this.m_setThisAtSceneStart.boolValue);
            EditorGUILayout.PropertyField(this.m_setThisAtSceneStartIdentifier);
            GUI.enabled = true;

            EditorGUILayout.PropertyField(this.m_csvEditorOnly);

            // m_csv
            {

                if (this.m_loadingMode.enumValueIndex == (int)LoadingMode.Normal)
                {
                    this.m_csv.objectReferenceValue = this.m_csvEditorOnly.objectReferenceValue;
                }

                else if (this.m_loadingMode.enumValueIndex == (int)LoadingMode.AssetBundle)
                {
                    this.m_csv.objectReferenceValue = null;
                }

                GUI.enabled = false;
                EditorGUILayout.PropertyField(this.m_csv);
                GUI.enabled = true;

            }

            GUI.enabled = (this.m_loadingMode.enumValueIndex == (int)LoadingMode.AssetBundle);
            EditorGUILayout.PropertyField(this.m_assetBundleName);
            EditorGUILayout.PropertyField(this.m_assetBundleVariant);
            EditorGUILayout.PropertyField(this.m_assetNameInAssetBundle);
            GUI.enabled = true;

            // m_previousCsvEditorOnly
            {

                if (this.m_previousCsvEditorOnly != this.m_csvEditorOnly.objectReferenceValue && this.m_csvEditorOnly.objectReferenceValue)
                {
                    this.reimportAssetBundleParameters(false);
                }

                this.m_previousCsvEditorOnly = this.m_csvEditorOnly.objectReferenceValue;

            }

            serializedObject.ApplyModifiedProperties();

        }

        /// <summary>
        /// Reimport AssetBundle parameters
        /// </summary>
        /// <param name="showDoneDialog">show dialog at finish</param>
        // ----------------------------------------------------------------------------------------
        protected void reimportAssetBundleParameters(bool showDoneDialog)
        {

            AssetImporter ai = AssetImporter.GetAtPath(AssetDatabase.GetAssetPath(this.m_csvEditorOnly.objectReferenceValue));

            this.m_assetBundleName.stringValue = (ai != null) ? ai.assetBundleName : "";
            this.m_assetBundleVariant.stringValue = (ai != null) ? ai.assetBundleVariant : "";
            this.m_assetNameInAssetBundle.stringValue = (ai != null) ? ai.assetPath.ToLower() : "";

            if(showDoneDialog)
            {
                EditorUtility.DisplayDialog("Confirmation", "Done.", "OK");
            }

        }

    }

}
