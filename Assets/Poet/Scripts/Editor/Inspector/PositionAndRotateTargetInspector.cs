﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Poet
{

    [CustomEditor(typeof(PositionAndRotateTargetManager))]
    public class PositionAndRotateTargetInspector : Editor
    {

        /// <summary>
        /// OnInspectorGUI
        /// </summary>
        // ----------------------------------------------------------------------------------------
        public override void OnInspectorGUI()
        {

            PositionAndRotateTargetManager script = target as PositionAndRotateTargetManager;

            GUI.enabled = false;
            EditorGUILayout.ObjectField("Script", MonoScript.FromMonoBehaviour(script), typeof(MonoScript), false);
            GUI.enabled = true;

            serializedObject.Update();

            //
            {

                EditorGUILayout.PropertyField(this.serializedObject.FindProperty("m_idAndTransformList"), true);

                GUILayout.Space(10.0f);

                // auto complete
                {

                    Transform trans = null;

                    EditorGUILayout.LabelField("Auto Complete Field for [Id And Transform List]");
                    trans = EditorGUILayout.ObjectField("Add New Transform", trans, typeof(Transform), true) as Transform;

                    if (trans)
                    {

                        SerializedProperty temp = serializedObject.FindProperty("m_idAndTransformList");
                        temp.InsertArrayElementAtIndex(temp.arraySize);

                        temp = temp.GetArrayElementAtIndex(temp.arraySize - 1);

                        temp.FindPropertyRelative("identifier").stringValue = trans.name;
                        temp.FindPropertyRelative("transform").objectReferenceValue = trans;

                    }

                }

            }

            GUILayout.Space(30.0f);

            //
            {

                // m_idAndLocalPositionList
                {
                    EditorGUILayout.PropertyField(this.serializedObject.FindProperty("m_idAndLocalPositionList"), true);
                }

                GUILayout.Space(10.0f);

                // auto complete
                {

                    Transform trans = null;

                    EditorGUILayout.LabelField("Auto Complete Field for [Id And Local Position List]");
                    trans = EditorGUILayout.ObjectField("Add New Local Pos", trans, typeof(Transform), true) as Transform;

                    if (trans)
                    {

                        SerializedProperty temp = serializedObject.FindProperty("m_idAndLocalPositionList");
                        temp.InsertArrayElementAtIndex(temp.arraySize);

                        temp = temp.GetArrayElementAtIndex(temp.arraySize - 1);

                        temp.FindPropertyRelative("identifier").stringValue = "New";
                        temp.FindPropertyRelative("position").vector3Value = trans.localPosition;

                    }

                }

            }

            serializedObject.ApplyModifiedProperties();

        }

    }

}
