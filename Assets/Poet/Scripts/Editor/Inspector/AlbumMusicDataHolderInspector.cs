﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Poet
{

    [CustomEditor(typeof(AlbumMusicDataHolder), true)]
    public class AlbumMusicDataHolderInspector : Editor
    {

        /// <summary>
        /// OnInspectorGUI
        /// </summary>
        // ----------------------------------------------------------------------------------------
        public override void OnInspectorGUI()
        {

            serializedObject.Update();

            GUILayout.Space(20f);

            if (GUILayout.Button("Reimport All \nAssetBundle Names", GUILayout.MinHeight(30)))
            {
                if (EditorUtility.DisplayDialog("Confirmation", "Reimport All AssetBundle Names ?", "Yes", "Cancel"))
                {
                    this.reimportAllAssetImporters();
                    EditorUtility.DisplayDialog("Confirmation", "Done.", "OK");
                    serializedObject.ApplyModifiedProperties();
                    return;
                }
            }

            GUILayout.Space(20f);

            DrawDefaultInspector();

            serializedObject.ApplyModifiedProperties();

        }

        /// <summary>
        /// Reimport all AssetBundle parameters
        /// </summary>
        // ----------------------------------------------------------------------------------------
        void reimportAllAssetImporters()
        {

            SerializedProperty dataList = this.serializedObject.FindProperty("m_dataList");

            SerializedProperty dataList_i = null;

            AssetImporter ai = null;

            string filePath = "";

            for (int i = 0; i < dataList.arraySize; i++)
            {

                dataList_i = dataList.GetArrayElementAtIndex(i);

                if (dataList_i.FindPropertyRelative("audioClipEditorOnly").objectReferenceValue)
                {

                    filePath = AssetDatabase.GetAssetPath(dataList_i.FindPropertyRelative("audioClipEditorOnly").objectReferenceValue);

                    ai = AssetImporter.GetAtPath(filePath);

                    dataList_i.FindPropertyRelative("assetBundleName").stringValue = (ai) ? ai.assetBundleName : "";
                    dataList_i.FindPropertyRelative("assetBundleVariant").stringValue = (ai) ? ai.assetBundleVariant : "";
                    dataList_i.FindPropertyRelative("assetNameInAssetBundle").stringValue = (ai) ? filePath.ToLower() : "";

                }

            }

        }

    }

}
