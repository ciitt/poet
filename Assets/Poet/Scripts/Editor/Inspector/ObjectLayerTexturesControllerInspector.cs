﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

namespace Poet
{

    [CustomEditor(typeof(ObjectLayerTexturesControllerScript), true)]
    public class ObjectLayerTexturesControllerInspector : Editor
    {

        bool m_init = false;
        LoadingMode m_previousLoadingMode = LoadingMode.Normal;

        /// <summary>
        /// OnInspectorGUI
        /// </summary>
        // ----------------------------------------------------------------------------------------
        public override void OnInspectorGUI()
        {

            serializedObject.Update();

            ObjectLayerTexturesControllerScript script = target as ObjectLayerTexturesControllerScript;

            GUILayout.Space(20f);

            if (GUILayout.Button("Show Custom Editor", GUILayout.MinHeight(30)))
            {
                ObjectLayerTexturesEditorWindow window = EditorWindow.GetWindow(typeof(ObjectLayerTexturesEditorWindow)) as ObjectLayerTexturesEditorWindow;
                window.setInstance(script);
                window.Show();
            }

            if (GUILayout.Button("Reimport All \nAssetBundle Names", GUILayout.MinHeight(30)))
            {
                if (EditorUtility.DisplayDialog("Confirmation", "Reimport All AssetBundle Names ?", "Yes", "Cancel"))
                {
                    this.reimportAllAssetImporters();
                    EditorUtility.DisplayDialog("Confirmation", "Done.", "OK");
                    serializedObject.ApplyModifiedProperties();
                    return;
                }
            }

            GUILayout.Space(20f);

            DrawDefaultInspector();

            //
            {
                this.resetTexturesIfNeeded();
            }

            serializedObject.ApplyModifiedProperties();

        }

        // ----------------------------------------------------------------------------------------
        void resetTextures(
            SerializedProperty texture,
            SerializedProperty textureEditorOnly,
            SerializedProperty spriteAtlas,
            SerializedProperty spriteAtlasEditorOnly,
            LoadingMode loadingMode
            )
        {

            if (loadingMode == LoadingMode.Normal)
            {
                texture.objectReferenceValue = textureEditorOnly.objectReferenceValue;
                spriteAtlas.objectReferenceValue = spriteAtlasEditorOnly.objectReferenceValue;
            }

            else if (loadingMode == LoadingMode.AssetBundle)
            {
                texture.objectReferenceValue = null;
                spriteAtlas.objectReferenceValue = null;
            }

        }

        /// <summary>
        /// Reset textures if needed
        /// </summary>
        // ----------------------------------------------------------------------------------------
        void resetTexturesIfNeeded()
        {

            LoadingMode loadingMode = (LoadingMode)serializedObject.FindProperty("m_loadingMode").enumValueIndex;

            if (!this.m_init)
            {
                this.m_init = true;
                this.m_previousLoadingMode = loadingMode;
                return;
            }

            if (this.m_previousLoadingMode == loadingMode)
            {
                return;
            }

            // --------------------------

            SerializedProperty layerTextures = this.serializedObject.FindProperty("m_layerTextures");

            SerializedProperty layer0 = null;
            SerializedProperty layerXs = null;
            SerializedProperty layerX = null;
            SerializedProperty layerTextures_i = null;
            SerializedProperty texture = null;
            SerializedProperty textureEditorOnly = null;
            SerializedProperty spriteAtlas = null;
            SerializedProperty spriteAtlasEditorOnly = null;

            for (int i = 0; i < layerTextures.arraySize; i++)
            {

                layerTextures_i = layerTextures.GetArrayElementAtIndex(i);

                // layer0
                {

                    layer0 = layerTextures_i.FindPropertyRelative("layer0");

                    texture = layer0.FindPropertyRelative("texture");
                    textureEditorOnly = layer0.FindPropertyRelative("textureEditorOnly");
                    spriteAtlas = layer0.FindPropertyRelative("spriteAtlas");
                    spriteAtlasEditorOnly = layer0.FindPropertyRelative("spriteAtlasEditorOnly");

                    this.resetTextures(texture, textureEditorOnly, spriteAtlas, spriteAtlasEditorOnly, loadingMode);

                }

                //
                {
                    List<string> list = new List<string>() {
                        "layer1s",
                        "layer2s",
                        "layer3s",
                        "layer4s",
                        "layer5s",
                        "overlays",
                    };

                    foreach(var val in list)
                    {

                        layerXs = layerTextures_i.FindPropertyRelative(val);

                        for (int j = 0; j < layerXs.arraySize; j++)
                        {

                            layerX = layerXs.GetArrayElementAtIndex(j);

                            texture = layerX.FindPropertyRelative("texture");
                            textureEditorOnly = layerX.FindPropertyRelative("textureEditorOnly");
                            spriteAtlas = layerX.FindPropertyRelative("spriteAtlas");
                            spriteAtlasEditorOnly = layerX.FindPropertyRelative("spriteAtlasEditorOnly");

                            this.resetTextures(texture, textureEditorOnly, spriteAtlas, spriteAtlasEditorOnly, loadingMode);

                        }

                    }

                }

            } // for (int i = 0; i < layerTextures.arraySize; i++)

            this.m_previousLoadingMode = loadingMode;

        }

        /// <summary>
        /// Reimport AssetImporter
        /// </summary>
        /// <param name="sp">SerializedProperty</param>
        // -----------------------------------------------------------------------------------------
        void reimportAssetImporter(SerializedProperty sp)
        {

            string filePath = "";

            // filePath
            {

                if (sp.FindPropertyRelative("spriteAtlasEditorOnly").objectReferenceValue)
                {
                    filePath = AssetDatabase.GetAssetPath(sp.FindPropertyRelative("spriteAtlasEditorOnly").objectReferenceValue);
                }

                else if (sp.FindPropertyRelative("textureEditorOnly").objectReferenceValue)
                {
                    filePath = AssetDatabase.GetAssetPath(sp.FindPropertyRelative("textureEditorOnly").objectReferenceValue);
                }

            }


            AssetImporter ai = AssetImporter.GetAtPath(filePath);

            sp.FindPropertyRelative("assetBundleName").stringValue = (ai) ? ai.assetBundleName : "";
            sp.FindPropertyRelative("assetBundleVariant").stringValue = (ai) ? ai.assetBundleVariant : "";
            sp.FindPropertyRelative("assetNameInAssetBundle").stringValue = (ai) ? filePath.ToLower() : "";

        }

        /// <summary>
        /// Reimport all AssetImporters foreach
        /// </summary>
        /// <param name="sp">SerializedProperty</param>
        // -----------------------------------------------------------------------------------------
        void reimportAllAssetImportersInternalForeach(SerializedProperty sp)
        {

            for (int i = 0; i < sp.arraySize; i++)
            {
                this.reimportAssetImporter(sp.GetArrayElementAtIndex(i));
            }

        }

        /// <summary>
        /// Reimport all AssetBundle parameters
        /// </summary>
        /// <param name="so">SerializedObject</param>
        // ----------------------------------------------------------------------------------------
        public void reimportAllAssetImporters()
        {

            if(serializedObject == null)
            {
                return;
            }

            // ----------------

            SerializedProperty temp = null;

            SerializedProperty layerTextures = serializedObject.FindProperty("m_layerTextures");

            for (int i = 0; i < layerTextures.arraySize; i++)
            {

                // layer0
                {
                    this.reimportAssetImporter(layerTextures.GetArrayElementAtIndex(i).FindPropertyRelative("layer0"));
                }

                // layerXs
                {

                    temp = layerTextures.GetArrayElementAtIndex(i);

                    this.reimportAllAssetImportersInternalForeach(temp.FindPropertyRelative("layer1s"));
                    this.reimportAllAssetImportersInternalForeach(temp.FindPropertyRelative("layer2s"));
                    this.reimportAllAssetImportersInternalForeach(temp.FindPropertyRelative("layer3s"));
                    this.reimportAllAssetImportersInternalForeach(temp.FindPropertyRelative("layer4s"));
                    this.reimportAllAssetImportersInternalForeach(temp.FindPropertyRelative("layer5s"));
                    this.reimportAllAssetImportersInternalForeach(temp.FindPropertyRelative("overlays"));

                }

            }

        }

    }

}
