﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Poet
{

    [CustomEditor(typeof(AlbumGraphicDataHolder), true)]
    public class AlbumGraphicDataHolderInspector : Editor
    {

        bool m_init = false;
        LoadingMode m_previousLoadingMode = LoadingMode.Normal;

        /// <summary>
        /// OnInspectorGUI
        /// </summary>
        // ----------------------------------------------------------------------------------------
        public override void OnInspectorGUI()
        {

            serializedObject.Update();

            AlbumGraphicDataHolder script = target as AlbumGraphicDataHolder;

            GUILayout.Space(20f);

            if (GUILayout.Button("Show Custom Editor", GUILayout.MinHeight(30)))
            {
                AlbumGraphicDataHolderWindow window = EditorWindow.GetWindow(typeof(AlbumGraphicDataHolderWindow)) as AlbumGraphicDataHolderWindow;
                window.setInstance(script);
                window.Show();
            }

            if (GUILayout.Button("Reimport All \nAssetBundle Names", GUILayout.MinHeight(30)))
            {
                if (EditorUtility.DisplayDialog("Confirmation", "Reimport All AssetBundle Names ?", "Yes", "Cancel"))
                {
                    this.reimportAllAssetImporters();
                    EditorUtility.DisplayDialog("Confirmation", "Done.", "OK");
                    serializedObject.ApplyModifiedProperties();
                    return;
                }
            }

            GUILayout.Space(20f);

            DrawDefaultInspector();

            this.resetTexturesIfNeeded();

            serializedObject.ApplyModifiedProperties();

        }

        /// <summary>
        /// Reset textures if needed
        /// </summary>
        // ----------------------------------------------------------------------------------------
        void resetTexturesIfNeeded()
        {

            LoadingMode loadingMode = (LoadingMode)serializedObject.FindProperty("m_loadingModeForThumbnail").enumValueIndex;

            if (!this.m_init)
            {
                this.m_init = true;
                this.m_previousLoadingMode = loadingMode;
                return;
            }

            if (this.m_previousLoadingMode == loadingMode)
            {
                return;
            }

            // --------------------------

            SerializedProperty dataList = this.serializedObject.FindProperty("m_dataList");

            SerializedProperty dataList_i = null;

            for (int i = 0; i < dataList.arraySize; i++)
            {

                dataList_i = dataList.GetArrayElementAtIndex(i);

                if(loadingMode == LoadingMode.Normal)
                {
                    dataList_i.FindPropertyRelative("thumbnail").objectReferenceValue =
                        dataList_i.FindPropertyRelative("thumbnailEditorOnly").objectReferenceValue;
                }

                else if(loadingMode == LoadingMode.AssetBundle)
                {
                    dataList_i.FindPropertyRelative("thumbnail").objectReferenceValue = null;
                }

            }

            this.m_previousLoadingMode = loadingMode;

        }

        /// <summary>
        /// Reimport all AssetBundle parameters
        /// </summary>
        // ----------------------------------------------------------------------------------------
        void reimportAllAssetImporters()
        {

            SerializedProperty dataList = this.serializedObject.FindProperty("m_dataList");

            SerializedProperty dataList_i = null;

            AssetImporter ai = null;

            string filePath = "";

            for (int i = 0; i < dataList.arraySize; i++)
            {

                dataList_i = dataList.GetArrayElementAtIndex(i);

                if(dataList_i.FindPropertyRelative("thumbnailEditorOnly").objectReferenceValue)
                {

                    filePath = AssetDatabase.GetAssetPath(dataList_i.FindPropertyRelative("thumbnailEditorOnly").objectReferenceValue);

                    ai = AssetImporter.GetAtPath(filePath);

                    dataList_i.FindPropertyRelative("assetBundleName").stringValue = (ai) ? ai.assetBundleName : "";
                    dataList_i.FindPropertyRelative("assetBundleVariant").stringValue = (ai) ? ai.assetBundleVariant : "";
                    dataList_i.FindPropertyRelative("assetNameInAssetBundle").stringValue = (ai) ? filePath.ToLower() : "";

                }

            }

        }

    }

}
