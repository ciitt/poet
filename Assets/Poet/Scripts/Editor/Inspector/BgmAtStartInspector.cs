﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Poet
{

    [CustomEditor(typeof(BgmAtStartScript))]
    public class BgmAtStartInspector : Editor
    {

        UnityEngine.Object m_temp = null;

        /// <summary>
        /// OnInspectorGUI
        /// </summary>
        // ----------------------------------------------------------------------------------------
        public override void OnInspectorGUI()
        {

            BgmAtStartScript script = target as BgmAtStartScript;

            GUI.enabled = false;
            EditorGUILayout.ObjectField("Script", MonoScript.FromMonoBehaviour(script), typeof(MonoScript), false);
            GUI.enabled = true;

            this.serializedObject.Update();

            //
            {

                BgmAtStartScript.LoadingAudioType ltype =
                    (BgmAtStartScript.LoadingAudioType)this.serializedObject.FindProperty("m_loadingAudioType").enumValueIndex;

                EditorGUILayout.PropertyField(this.serializedObject.FindProperty("m_loadingAudioType"));

                if (ltype == BgmAtStartScript.LoadingAudioType.Preload)
                {
                    EditorGUILayout.PropertyField(this.serializedObject.FindProperty("m_preloadedId"));
                }

                else if (ltype == BgmAtStartScript.LoadingAudioType.Resource)
                {
                    EditorGUILayout.PropertyField(this.serializedObject.FindProperty("m_resourcePath"));
                }

                else if (ltype == BgmAtStartScript.LoadingAudioType.AssetBundle)
                {

                    //
                    {

                        EditorGUILayout.PropertyField(this.serializedObject.FindProperty("m_assetBundleName"));
                        EditorGUILayout.PropertyField(this.serializedObject.FindProperty("m_assetBundleVariant"));
                        EditorGUILayout.PropertyField(this.serializedObject.FindProperty("m_assetNameInAssetBundle"));

                    }

                    //
                    {

                        GUILayout.Space(30.0f);

                        EditorGUILayout.LabelField("Auto Complete Field for AssetBundle Parameters", EditorStyles.boldLabel);
                        UnityEngine.Object obj = EditorGUILayout.ObjectField(this.m_temp, typeof(UnityEngine.Object), false);

                        if (obj)
                        {

                            string filePath = AssetDatabase.GetAssetPath(obj);
                            AssetImporter ai = AssetImporter.GetAtPath(filePath);

                            this.serializedObject.FindProperty("m_assetBundleName").stringValue = (ai) ? ai.assetBundleName : "";
                            this.serializedObject.FindProperty("m_assetBundleVariant").stringValue = (ai) ? ai.assetBundleVariant : "";
                            this.serializedObject.FindProperty("m_assetNameInAssetBundle").stringValue = (ai) ? filePath.ToLower() : "";

                        }

                    }

                }

            }

            this.serializedObject.ApplyModifiedProperties();

        }

    }

}
