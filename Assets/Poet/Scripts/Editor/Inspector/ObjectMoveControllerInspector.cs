﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

namespace Poet
{

    [CanEditMultipleObjects]
    [CustomEditor(typeof(ObjectMoveControllerScript), true)]
    public class ObjectMoveControllerInspector : Editor
    {

        PositionAndRotateTargetManager m_refPositionAndRotateTargetManager = null;

        /// <summary>
        /// OnInspectorGUI
        /// </summary>
        // ----------------------------------------------------------------------------------------
        public override void OnInspectorGUI()
        {

            ObjectMoveControllerScript script = target as ObjectMoveControllerScript;

            DrawDefaultInspector();

            GUILayout.Space(30f);

            if (serializedObject.FindProperty("m_localMovement").boolValue)
            {

                GUILayout.Label("Debug for Local Position", EditorStyles.boldLabel);

                if (this.m_refPositionAndRotateTargetManager)
                {

                    var list = this.m_refPositionAndRotateTargetManager.idAndLocalPositionList;

                    foreach (var val in list)
                    {

                        if (GUILayout.Button(val.identifier))
                        {
                            GUI.FocusControl("");
                            script.transform.localPosition = val.position;
                        }

                    }

                }

                else
                {

                    if (GUILayout.Button("Find PositionAndRotateTargetManager", GUILayout.MinHeight(30)))
                    {

                        this.m_refPositionAndRotateTargetManager = GameObject.FindObjectOfType<PositionAndRotateTargetManager>() as PositionAndRotateTargetManager;

                        if(!this.m_refPositionAndRotateTargetManager)
                        {
                            UnityEditor.EditorUtility.DisplayDialog("Not Found", "PositionAndRotateTargetManager not found", "Ok");
                        }

                    }

                }

            }

        }

    }

}
