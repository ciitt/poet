﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.UI;
using UnityEngine;

namespace Poet
{

    [CustomEditor(typeof(CharacterVoiceSlider))]
    public class CharacterVoiceSliderInspector : SliderEditor
    {

        SerializedProperty characterVoiceId;
        SerializedProperty onCharacterVoiceValueChanged;

        /// <summary>
        /// OnEnable
        /// </summary>
        // ----------------------------------------------------------------------------------------
        protected override void OnEnable()
        {

            base.OnEnable();

            this.characterVoiceId = serializedObject.FindProperty("characterVoiceId");
            this.onCharacterVoiceValueChanged = serializedObject.FindProperty("onCharacterVoiceValueChanged");

        }


        /// <summary>
        /// OnInspectorGUI
        /// </summary>
        // ----------------------------------------------------------------------------------------
        public override void OnInspectorGUI()
        {

            CharacterVoiceSlider script = (CharacterVoiceSlider)target;

            GUI.enabled = false;
            EditorGUILayout.ObjectField("Script", MonoScript.FromMonoBehaviour(script), typeof(MonoScript), false);
            GUI.enabled = true;

            base.OnInspectorGUI();

            serializedObject.Update();

            Color temp = GUI.backgroundColor;

            GUI.backgroundColor = Color.yellow;
            EditorGUILayout.PropertyField(this.characterVoiceId);
            EditorGUILayout.PropertyField(this.onCharacterVoiceValueChanged);
            GUI.backgroundColor = temp;

            serializedObject.ApplyModifiedProperties();

        }

    }

}
