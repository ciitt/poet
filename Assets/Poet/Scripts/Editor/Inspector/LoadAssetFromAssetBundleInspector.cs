﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Poet
{

    [CustomEditor(typeof(LoadAssetFromAssetBundleScript), true)]
    public class LoadAssetFromAssetBundleInspector : Editor
    {

        UnityEngine.Object m_temp = null;

        /// <summary>
        /// OnInspectorGUI
        /// </summary>
        // ----------------------------------------------------------------------------------------
        public override void OnInspectorGUI()
        {

            DrawDefaultInspector();

            GUILayout.Space(30.0f);

            this.serializedObject.Update();

            //
            {

                EditorGUILayout.LabelField("Auto Complete Field for AssetBundle Parameters", EditorStyles.boldLabel);
                UnityEngine.Object obj = EditorGUILayout.ObjectField(this.m_temp, typeof(UnityEngine.Object), false);

                if (obj)
                {
                    
                    string filePath = AssetDatabase.GetAssetPath(obj);
                    AssetImporter ai = AssetImporter.GetAtPath(filePath);

                    this.serializedObject.FindProperty("m_assetBundleName").stringValue = (ai) ? ai.assetBundleName : "";
                    this.serializedObject.FindProperty("m_assetBundleVariant").stringValue = (ai) ? ai.assetBundleVariant : "";
                    this.serializedObject.FindProperty("m_assetNameInAssetBundle").stringValue = (ai) ? filePath.ToLower() : "";

                }

            }

            this.serializedObject.ApplyModifiedProperties();

        }

    }

}
