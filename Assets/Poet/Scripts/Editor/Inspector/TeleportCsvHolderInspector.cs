﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Poet
{

    [CustomEditor(typeof(TeleportCsvHolder))]
    public class TeleportCsvHolderInspector : PoetCsvHolderInspector
    {

        /// <summary>
        /// OnInspectorGUI
        /// </summary>
        // ----------------------------------------------------------------------------------------
        public override void OnInspectorGUI()
        {

            serializedObject.Update();

            //
            {

                GUILayout.Space(20f);

                if (GUILayout.Button("Reimport\nAssetBundle Names", GUILayout.MinHeight(30)))
                {
                    this.reimportAssetBundleParameters(true);
                }

                GUILayout.Space(20f);

            }

            TeleportCsvHolder script = target as TeleportCsvHolder;

            GUI.enabled = false;
            EditorGUILayout.ObjectField("Script", MonoScript.FromMonoBehaviour(script), typeof(MonoScript), false);
            GUI.enabled = true;

            EditorGUILayout.PropertyField(this.m_csvHeaderForIdentifier);
            EditorGUILayout.PropertyField(this.m_loadingMode);
            EditorGUILayout.PropertyField(this.m_csvEditorOnly);

            // m_csv
            {

                if (this.m_loadingMode.enumValueIndex == (int)LoadingMode.Normal)
                {
                    this.m_csv.objectReferenceValue = this.m_csvEditorOnly.objectReferenceValue;
                }

                else if (this.m_loadingMode.enumValueIndex == (int)LoadingMode.AssetBundle)
                {
                    this.m_csv.objectReferenceValue = null;
                }

                GUI.enabled = false;
                EditorGUILayout.PropertyField(this.m_csv);
                GUI.enabled = true;

            }

            GUI.enabled = (this.m_loadingMode.enumValueIndex == (int)LoadingMode.AssetBundle);
            EditorGUILayout.PropertyField(this.m_assetBundleName);
            EditorGUILayout.PropertyField(this.m_assetBundleVariant);
            EditorGUILayout.PropertyField(this.m_assetNameInAssetBundle);
            GUI.enabled = true;

            // m_previousCsvEditorOnly
            {

                if (this.m_previousCsvEditorOnly != this.m_csvEditorOnly.objectReferenceValue && this.m_csvEditorOnly.objectReferenceValue)
                {
                    this.reimportAssetBundleParameters(false);
                }

                this.m_previousCsvEditorOnly = this.m_csvEditorOnly.objectReferenceValue;

            }

            serializedObject.ApplyModifiedProperties();

        }

    }

}
