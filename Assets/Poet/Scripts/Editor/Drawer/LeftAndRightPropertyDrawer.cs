﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Poet
{

    public class LeftAndRightPropertyDrawer : PropertyDrawer
    {

        public virtual void OnGUILeftAndRight(
            float leftPercentage01,
            string leftRelativePropertyPath,
            string rightRelativePropertyPath,
            Rect position,
            SerializedProperty property,
            GUIContent label
            )
        {

            EditorGUI.BeginProperty(position, label, property);

            EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), new GUIContent("*"));

            position.xMin += 20.0f;

            float leftX = position.xMin;
            float leftWidth = position.width * leftPercentage01;
            float rightX = leftX + leftWidth;
            float rightWidth = position.width - leftWidth;

            var leftRect = new Rect(leftX, position.y, leftWidth, position.height);
            var rightRect = new Rect(rightX, position.y, rightWidth, position.height);

            EditorGUI.PropertyField(leftRect, property.FindPropertyRelative(leftRelativePropertyPath), GUIContent.none);
            EditorGUI.PropertyField(rightRect, property.FindPropertyRelative(rightRelativePropertyPath), GUIContent.none);

            EditorGUI.EndProperty();

        }

    }

}
