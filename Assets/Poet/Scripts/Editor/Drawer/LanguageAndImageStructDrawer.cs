﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Poet
{

    [CustomPropertyDrawer(typeof(LanguageAndImageStruct))]
    public class LanguageAndImageStructDrawer : LeftAndRightPropertyDrawer
    {

        /// <summary>
        /// OnGUI
        /// </summary>
        /// <param name="position">Rect</param>
        /// <param name="property">SerializedProperty</param>
        /// <param name="label">GUIContent</param>
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            this.OnGUILeftAndRight(0.5f, "systemLanguage", "sprite", position, property, label);
        }

    }

}
