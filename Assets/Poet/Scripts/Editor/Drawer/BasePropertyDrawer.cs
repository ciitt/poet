﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Poet
{

    /// <summary>
    /// PropertyDrawer with common functions
    /// </summary>
    public class BasePropertyDrawer : PropertyDrawer
    {

        /// <summary>
        /// GetPropertyHeight
        /// </summary>
        /// <param name="property">SerializedProperty</param>
        /// <param name="label">GUIContent</param>
        /// <returns>height</returns>
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return EditorGUI.GetPropertyHeight(property);
        }

        /// <summary>
        /// Calc Rect
        /// </summary>
        /// <param name="position">position</param>
        /// <param name="index">Y index</param>
        /// <param name="heightLevel">height level</param>
        /// <returns></returns>
        protected virtual Rect calcRect(Rect position, int index, int heightLevel)
        {

            Rect ret = new Rect(position);

            ret.y += 18 * index;
            ret.height = 18 * heightLevel;

            return ret;

        }

        /// <summary>
        /// Calc Rect
        /// </summary>
        /// <param name="position">position</param>
        /// <param name="index">Y index</param>
        /// <param name="property">property</param>
        /// <returns>Rect</returns>
        protected virtual Rect calcRect(Rect position, int index, SerializedProperty property)
        {

            Rect ret = new Rect(position);

            ret.y += 18 * index;
            ret.height = EditorGUI.GetPropertyHeight(property);

            return ret;

        }

    }

}
