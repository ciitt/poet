﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace Poet
{

    [CustomPropertyDrawer(typeof(IdAndTextureDetail))]
    public class IdAndTextureDetailDrawer : BasePropertyDrawer
    {

        /// <summary>
        /// OnGUI
        /// </summary>
        /// <param name="position">Rect</param>
        /// <param name="property">SerializedProperty</param>
        /// <param name="label">GUIContent</param>
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            
            label = EditorGUI.BeginProperty(position, label, property);

            EditorGUI.PropertyField(this.calcRect(position, 0, 1), property, label, false);

            var script = property.serializedObject.targetObject as LayerTexturesControllerInterface;

            LoadingMode loadingMode = (script != null) ? script.loadingMode() : LoadingMode.Normal;

            SerializedProperty temp = null;

            if(script == null)
            {
                Debug.LogWarning("script doesn't have LayerTexturesControllerInterface");
            }

            if (property.isExpanded)
            {

                int index = 1;

                // identifier
                {
                    temp = property.FindPropertyRelative("identifier");
                    EditorGUI.PropertyField(this.calcRect(position, index, temp), temp);
                    index += 1;
                }

                // textureEditorOnly
                {
                    temp = property.FindPropertyRelative("textureEditorOnly");
                    EditorGUI.PropertyField(this.calcRect(position, index, temp), temp);
                    index += 1;
                }

                // texture
                {

                    property.FindPropertyRelative("texture").objectReferenceValue =
                        (loadingMode == LoadingMode.Normal) ?
                        property.FindPropertyRelative("textureEditorOnly").objectReferenceValue :
                        null
                        ;

                    GUI.enabled = false;
                    temp = property.FindPropertyRelative("texture");
                    EditorGUI.PropertyField(this.calcRect(position, index, temp), temp);
                    index += 1;
                    GUI.enabled = true;

                }

                // tilling
                {
                    temp = property.FindPropertyRelative("tilling");
                    EditorGUI.PropertyField(this.calcRect(position, index, temp), temp);
                    index += 1;
                }

                // offset
                {
                    temp = property.FindPropertyRelative("offset");
                    EditorGUI.PropertyField(this.calcRect(position, index, temp), temp);
                    index += 1;
                }

                // spriteAtlasEditorOnly
                {
                    temp = property.FindPropertyRelative("spriteAtlasEditorOnly");
                    EditorGUI.PropertyField(this.calcRect(position, index, temp), temp);
                    index += 1;
                }

                // spriteAtlas
                {

                    property.FindPropertyRelative("spriteAtlas").objectReferenceValue =
                        (loadingMode == LoadingMode.Normal) ?
                        property.FindPropertyRelative("spriteAtlasEditorOnly").objectReferenceValue :
                        null
                        ;

                    GUI.enabled = false;
                    temp = property.FindPropertyRelative("spriteAtlas");
                    EditorGUI.PropertyField(this.calcRect(position, index, temp), temp);
                    index += 1;
                    GUI.enabled = true;

                }

                // assetNameInSpriteAtlas
                {
                    temp = property.FindPropertyRelative("assetNameInSpriteAtlas");
                    EditorGUI.PropertyField(this.calcRect(position, index, temp), temp);
                    index += 1;
                }

                // assetBundleName
                {
                    GUI.enabled = (loadingMode == LoadingMode.AssetBundle);
                    temp = property.FindPropertyRelative("assetBundleName");
                    EditorGUI.PropertyField(this.calcRect(position, index, temp), temp);
                    index += 1;
                    GUI.enabled = true;
                }

                // assetBundleVariant
                {
                    GUI.enabled = (loadingMode == LoadingMode.AssetBundle);
                    temp = property.FindPropertyRelative("assetBundleVariant");
                    EditorGUI.PropertyField(this.calcRect(position, index, temp), temp);
                    index += 1;
                    GUI.enabled = true;
                }

                // assetNameInAssetBundle
                {
                    GUI.enabled = (loadingMode == LoadingMode.AssetBundle);
                    temp = property.FindPropertyRelative("assetNameInAssetBundle");
                    EditorGUI.PropertyField(this.calcRect(position, index, temp), temp);
                    index += 1;
                    GUI.enabled = true;
                }

            }

            EditorGUI.EndProperty();

        }

    }

}
