﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Poet
{

    [CustomPropertyDrawer(typeof(AlbumMemoryData))]
    public class AlbumMemoryDataDrawer : BasePropertyDrawer
    {

        /// <summary>
        /// OnGUI
        /// </summary>
        /// <param name="position">Rect</param>
        /// <param name="property">SerializedProperty</param>
        /// <param name="label">GUIContent</param>
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {

            label = EditorGUI.BeginProperty(position, label, property);

            EditorGUI.PropertyField(this.calcRect(position, 0, 1), property, label, false);

            var script = property.serializedObject.targetObject as AlbumMemoryDataHolder;

            LoadingMode loadingMode = (script) ? script.loadingMode : LoadingMode.Normal;

            if (!script)
            {
                Debug.LogError("Invalid usage : AlbumMemoryDataDrawer");
            }

            if (property.isExpanded)
            {

                int index = 1;

                SerializedProperty temp = null;

                // sceneName
                {
                    temp = property.FindPropertyRelative("sceneName");
                    EditorGUI.PropertyField(this.calcRect(position, index, temp), temp);
                    index += 1;
                }

                // csvName
                {
                    temp = property.FindPropertyRelative("csvName");
                    EditorGUI.PropertyField(this.calcRect(position, index, temp), temp);
                    index += 1;
                }

                // startCsvRowIdentifier
                {
                    temp = property.FindPropertyRelative("startCsvRowIdentifier");
                    EditorGUI.PropertyField(this.calcRect(position, index, temp), temp);
                    index += 1;
                }

                // endCsvRowIdentifier
                {
                    temp = property.FindPropertyRelative("endCsvRowIdentifier");
                    EditorGUI.PropertyField(this.calcRect(position, index, temp), temp);
                    index += 1;
                }

                // thumbnailEditorOnly
                {
                    temp = property.FindPropertyRelative("thumbnailEditorOnly");
                    EditorGUI.PropertyField(this.calcRect(position, index, temp), temp);
                    index += 1;
                }

                // thumbnail
                {
                    GUI.enabled = false;
                    temp = property.FindPropertyRelative("thumbnail");
                    EditorGUI.PropertyField(this.calcRect(position, index, temp), temp);
                    index += 1;
                    GUI.enabled = true;
                }

                // assetBundleName
                {
                    GUI.enabled = (loadingMode == LoadingMode.AssetBundle);
                    temp = property.FindPropertyRelative("assetBundleName");
                    EditorGUI.PropertyField(this.calcRect(position, index, temp), temp);
                    index += 1;
                    GUI.enabled = true;
                }

                // assetBundleVariant
                {
                    GUI.enabled = (loadingMode == LoadingMode.AssetBundle);
                    temp = property.FindPropertyRelative("assetBundleVariant");
                    EditorGUI.PropertyField(this.calcRect(position, index, temp), temp);
                    index += 1;
                    GUI.enabled = true;
                }

                // assetNameInAssetBundle
                {
                    GUI.enabled = (loadingMode == LoadingMode.AssetBundle);
                    temp = property.FindPropertyRelative("assetNameInAssetBundle");
                    EditorGUI.PropertyField(this.calcRect(position, index, temp), temp);
                    index += 1;
                    GUI.enabled = true;
                }

                // unlock
                {

                    temp = property.FindPropertyRelative("unlock");

                    if (temp.isExpanded)
                    {
                        EditorGUI.PropertyField(this.calcRect(position, index, temp), temp, true);
                        index += 4;
                    }

                    else
                    {
                        EditorGUI.PropertyField(this.calcRect(position, index, temp), temp, true);
                        index += 1;
                    }

                }

            }

            EditorGUI.EndProperty();

        }

    }

}
