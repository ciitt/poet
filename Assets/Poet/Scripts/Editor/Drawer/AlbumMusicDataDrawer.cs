﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Poet
{

    [CustomPropertyDrawer(typeof(AlbumMusicData))]
    public class AlbumMusicDataDrawer : BasePropertyDrawer
    {

        //UnityEngine.Object m_previousAudioClipEditorOnly = null;

        /// <summary>
        /// OnGUI
        /// </summary>
        /// <param name="position">Rect</param>
        /// <param name="property">SerializedProperty</param>
        /// <param name="label">GUIContent</param>
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {

            label = EditorGUI.BeginProperty(position, label, property);

            EditorGUI.PropertyField(this.calcRect(position, 0, 1), property, label, false);

            if (property.isExpanded)
            {

                int index = 1;

                SerializedProperty temp = null;
                SerializedProperty audioClipEditorOnly = null;

                bool loadingModeIsAssetBundle = false;

                // loadingMode
                {
                    temp = property.FindPropertyRelative("loadingMode");
                    EditorGUI.PropertyField(this.calcRect(position, index, temp), temp);
                    index += 1;

                    loadingModeIsAssetBundle = temp.enumValueIndex == (int)LoadingMode.AssetBundle;

                }

                // audioTitle
                {
                    temp = property.FindPropertyRelative("audioTitle");
                    EditorGUI.PropertyField(this.calcRect(position, index, temp), temp);
                    index += 1;
                }

                // audioClipEditorOnly
                {

                    temp = property.FindPropertyRelative("audioClipEditorOnly");
                    EditorGUI.PropertyField(this.calcRect(position, index, temp), temp);
                    index += 1;

                    audioClipEditorOnly = property.FindPropertyRelative("audioClipEditorOnly");

                }

                // audioClip
                {
                    GUI.enabled = false;
                    temp = property.FindPropertyRelative("audioClip");
                    temp.objectReferenceValue = loadingModeIsAssetBundle ? null : audioClipEditorOnly.objectReferenceValue;
                    EditorGUI.PropertyField(this.calcRect(position, index, temp), temp);
                    index += 1;
                    GUI.enabled = true;
                }

                // assetBundleName
                {
                    GUI.enabled = loadingModeIsAssetBundle;
                    temp = property.FindPropertyRelative("assetBundleName");
                    EditorGUI.PropertyField(this.calcRect(position, index, temp), temp);
                    index += 1;
                    GUI.enabled = true;
                }

                // assetBundleVariant
                {
                    GUI.enabled = loadingModeIsAssetBundle;
                    temp = property.FindPropertyRelative("assetBundleVariant");
                    EditorGUI.PropertyField(this.calcRect(position, index, temp), temp);
                    index += 1;
                    GUI.enabled = true;
                }

                // assetNameInAssetBundle
                {
                    GUI.enabled = loadingModeIsAssetBundle;
                    temp = property.FindPropertyRelative("assetNameInAssetBundle");
                    EditorGUI.PropertyField(this.calcRect(position, index, temp), temp);
                    index += 1;
                    GUI.enabled = true;
                }

            }

            EditorGUI.EndProperty();

        }

    }

}
