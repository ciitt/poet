﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

/// <summary>
/// Menus for Poet
/// </summary>
public class PoetEditorMenus
{

    /// <summary>
    /// Show CreateSpriteCharacterWindow
    /// </summary>
    [MenuItem("Tools/Poet/Create New Sprite Character Prefab", false, 0)]
    static void ShowCreateSpriteCharacterWindow()
    {

        Poet.CreateSpriteCharacterWindow window =
            EditorWindow.GetWindow(typeof(Poet.CreateSpriteCharacterWindow)) as Poet.CreateSpriteCharacterWindow;

        window.init();
        window.Show();

    }

    /// <summary>
    /// Show AnimatorControllerParamsWindow
    /// </summary>
    [MenuItem("Tools/Poet/Show AnimatorController Parameter Names", false, 0)]
    static void ShowAnimatorControllerParamsWindow()
    {
        (EditorWindow.GetWindow(typeof(Poet.AnimatorControllerParamsWindow)) as Poet.AnimatorControllerParamsWindow).Show();
    }

    /// <summary>
    /// Show SetNavigationExplicitWindow
    /// </summary>
    [MenuItem("Tools/Poet/Convert Automatic Navigation to Explicit", false, 0)]
    static void ShowSetNavigationExplicitWindow()
    {
        (EditorWindow.GetWindow(typeof(Poet.SetNavigationExplicitWindow)) as Poet.SetNavigationExplicitWindow).Show();
    }

}