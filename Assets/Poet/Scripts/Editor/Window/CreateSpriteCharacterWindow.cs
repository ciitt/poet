﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;

namespace Poet
{

    /// <summary>
    /// Create new sprite character prefab
    /// </summary>
    public class CreateSpriteCharacterWindow : EditorWindow
    {

        enum SpritePrefabType
        {
            SpriteRendererNormal,
            SpriteRendererSpriteAtlas,
            SpriteRendererNormal_Mobile,
            SpriteRendererSpriteAtlas_Mobile,
            MeshRendererNormal,
            MeshRendererSpriteAtlas,
        }

        class PrefabAndMaterialRef
        {

            public GameObject _prefab = null;
            public Material _material = null;

            public string setRef(string prefabFilePath, string materialFilePath)
            {

                this._prefab = AssetDatabase.LoadAssetAtPath<GameObject>(prefabFilePath);
                this._material = AssetDatabase.LoadAssetAtPath<Material>(materialFilePath);

                string ret = "";

                if(!this._prefab)
                {
                    ret += prefabFilePath + "\n";
                }

                if (!this._material)
                {
                    ret += materialFilePath + "\n";
                }

                return ret;

            }

        }

        /// <summary>
        /// Scroll pos
        /// </summary>
        Vector2 m_scrollPos = Vector2.zero;

        /// <summary>
        /// Reference to sprite  
        /// </summary>
        PrefabAndMaterialRef m_refSprite = new PrefabAndMaterialRef();

        /// <summary>
        /// Reference to sprite SA 
        /// </summary>
        PrefabAndMaterialRef m_refSpriteSA = new PrefabAndMaterialRef();

        /// <summary>
        /// Reference to mobile
        /// </summary>
        PrefabAndMaterialRef m_refMobile = new PrefabAndMaterialRef();

        /// <summary>
        /// Reference to mobile SA
        /// </summary>
        PrefabAndMaterialRef m_refMobileSA = new PrefabAndMaterialRef();

        /// <summary>
        /// Reference to quad
        /// </summary>
        PrefabAndMaterialRef m_refQuad = new PrefabAndMaterialRef();

        /// <summary>
        /// Reference to quad SA
        /// </summary>
        PrefabAndMaterialRef m_refQuadSA = new PrefabAndMaterialRef();

        /// <summary>
        /// SpritePrefabType
        /// </summary>
        SpritePrefabType m_prefabType = SpritePrefabType.SpriteRendererNormal;

        /// <summary>
        /// File path for new prefab
        /// </summary>
        string m_newPrefabFilePath = "Assets/New Character.prefab";

        /// <summary>
        /// File path for new material
        /// </summary>
        string m_newMaterialFilePath = "Assets/New Character Material.mat";

        /// <summary>
        /// Error
        /// </summary>
        string m_error = "";

        /// <summary>
        /// Current folder path
        /// </summary>
        string m_currentFolderPath = "Assets/";

        /// <summary>
        /// Init
        /// </summary>
        // -----------------------------------------------------------------------------------------
        public void init()
        {
            
            this.m_error = "";

            string sprite = "Assets/Poet/Prefabs/Sprite/Sprite Character Base.prefab";
            string spriteMat = "Assets/Poet/Prefabs/Sprite/Sprite Character Base Material.mat";

            string spriteSA = "Assets/Poet/Prefabs/Sprite/Sprite SA Character Base.prefab";
            string spriteSAMat = "Assets/Poet/Prefabs/Sprite/Sprite SA Character Base Material.mat";

            string mobile = "Assets/Poet/Prefabs/Sprite/Mobile Character Base.prefab";
            string mobileMat = "Assets/Poet/Prefabs/Sprite/Mobile Character Base Material.mat";

            string mobileSA = "Assets/Poet/Prefabs/Sprite/Mobile SA Character Base.prefab";
            string mobileSAMat = "Assets/Poet/Prefabs/Sprite/Mobile SA Character Base Material.mat";

            string quad = "Assets/Poet/Prefabs/Sprite/Quad Character Base.prefab";
            string quadMat = "Assets/Poet/Prefabs/Sprite/Quad Character Base Material.mat";

            string quadSA = "Assets/Poet/Prefabs/Sprite/Quad SA Character Base.prefab";
            string quadSAMat = "Assets/Poet/Prefabs/Sprite/Quad SA Character Base Material.mat";

            this.m_error += this.m_refSprite.setRef(sprite, spriteMat);
            this.m_error += this.m_refSpriteSA.setRef(spriteSA, spriteSAMat);
            this.m_error += this.m_refMobile.setRef(mobile, mobileMat);
            this.m_error += this.m_refMobileSA.setRef(mobileSA, mobileSAMat);
            this.m_error += this.m_refQuad.setRef(quad, quadMat);
            this.m_error += this.m_refQuadSA.setRef(quadSA, quadSAMat);

            if (!string.IsNullOrEmpty(this.m_error))
            {
                EditorUtility.DisplayDialog("Not found", this.m_error, "OK");
            }

        }

        /// <summary>
        /// Is available
        /// </summary>
        /// <returns>available</returns>
        // -----------------------------------------------------------------------------------------
        bool isAvailable()
        {

            return
                string.IsNullOrEmpty(this.m_error) &&
                !string.IsNullOrEmpty(this.m_newPrefabFilePath) &&
                !string.IsNullOrEmpty(this.m_newMaterialFilePath) &&
                this.m_newPrefabFilePath.StartsWith("Assets/") &&
                this.m_newMaterialFilePath.StartsWith("Assets/") &&
                Path.GetFileNameWithoutExtension(this.m_newPrefabFilePath).Length > 0 &&
                Path.GetFileNameWithoutExtension(this.m_newMaterialFilePath).Length > 0
                ;

        }

        /// <summary>
        /// Get base PrefabAndMaterialRef
        /// </summary>
        /// <returns>PrefabAndMaterialRef</returns>
        // -----------------------------------------------------------------------------------------
        PrefabAndMaterialRef basePrefabAndMat()
        {
            
            if(this.m_prefabType == SpritePrefabType.SpriteRendererNormal)
            {
                return this.m_refSprite;
            }

            else if (this.m_prefabType == SpritePrefabType.SpriteRendererSpriteAtlas)
            {
                return this.m_refSpriteSA;
            }

            else if (this.m_prefabType == SpritePrefabType.SpriteRendererNormal_Mobile)
            {
                return this.m_refMobile;
            }

            else if (this.m_prefabType == SpritePrefabType.SpriteRendererSpriteAtlas_Mobile)
            {
                return this.m_refMobileSA;
            }

            else if (this.m_prefabType == SpritePrefabType.MeshRendererNormal)
            {
                return this.m_refQuad;
            }

            else if (this.m_prefabType == SpritePrefabType.MeshRendererSpriteAtlas)
            {
                return this.m_refQuadSA;
            }

            return null;

        }

        /// <summary>
        /// Create
        /// </summary>
        // -----------------------------------------------------------------------------------------
        void create()
        {

            PrefabAndMaterialRef prefabAndMat = this.basePrefabAndMat();

            if (!this.isAvailable())
            {
                return;
            }

            if (!prefabAndMat._prefab || !prefabAndMat._material)
            {
                EditorUtility.DisplayDialog("Invalid Usage", "Invalid Usage.", "OK");
                return;
            }

            string existPaths = "";

            //
            {

                if (File.Exists(this.m_newPrefabFilePath))
                {
                    existPaths += this.m_newPrefabFilePath + "\n";
                }

                if (File.Exists(this.m_newMaterialFilePath))
                {
                    existPaths += this.m_newMaterialFilePath + "\n";
                }

            }

            if (!string.IsNullOrEmpty(existPaths))
            {

                if (
                    !EditorUtility.DisplayDialog(
                    "Confirmation",
                    "Overwrite ?\n\n" + existPaths,
                    "Yes",
                    "Cancel"
                    ))
                {
                    return;
                }

            }

            else
            {
                if (!EditorUtility.DisplayDialog("Confirmation", "Create ?", "Yes", "Cancel"))
                {
                    return;
                }
            }

            // -----------------------------

            // CopyAsset
            {
                AssetDatabase.CopyAsset(AssetDatabase.GetAssetPath(prefabAndMat._prefab), this.m_newPrefabFilePath);
                AssetDatabase.CopyAsset(AssetDatabase.GetAssetPath(prefabAndMat._material), this.m_newMaterialFilePath);
                AssetDatabase.Refresh();
            }

            //
            {
                GameObject newPrefab = AssetDatabase.LoadAssetAtPath<GameObject>(this.m_newPrefabFilePath);

                if (newPrefab)
                {

                    Renderer renderer = newPrefab.GetComponentInChildren(typeof(Renderer), true) as Renderer;

                    if (renderer)
                    {
                        renderer.sharedMaterial = AssetDatabase.LoadAssetAtPath<Material>(this.m_newMaterialFilePath);
                        Selection.activeObject = newPrefab;
                    }

                }

            }

            EditorUtility.DisplayDialog("Confirmation", "Done", "OK");

        }

        /// <summary>
        /// OnGUI
        /// </summary>
        // -----------------------------------------------------------------------------------------
        void OnGUI()
        {

            this.m_scrollPos = EditorGUILayout.BeginScrollView(this.m_scrollPos);

            //
            {

                GUILayout.Space(10.0f);
                this.m_prefabType = (SpritePrefabType)EditorGUILayout.EnumPopup("Type", this.m_prefabType);

            }

            GUILayout.Space(30.0f);
            EditorGUILayout.LabelField("* References to copy (unchangeable)", EditorStyles.boldLabel);

            //
            {

                GUI.enabled = false;

                var temp = this.basePrefabAndMat();

                if(temp != null)
                {
                    temp._prefab = EditorGUILayout.ObjectField("Prefab", temp._prefab, typeof(GameObject), true) as GameObject;
                    temp._material = EditorGUILayout.ObjectField("Material", temp._material, typeof(Material), true) as Material;
                }

                GUI.enabled = true;

            }

            GUILayout.Space(20.0f);
            EditorGUILayout.LabelField("* Paths to save", EditorStyles.boldLabel);

            //
            {

                EditorGUILayout.BeginHorizontal();

                this.m_newPrefabFilePath = EditorGUILayout.TextField("Prefab", this.m_newPrefabFilePath);

                if (GUILayout.Button("...", GUILayout.MaxWidth(30)))
                {

                    GUI.FocusControl("");

                    string path = EditorUtility.SaveFilePanelInProject(
                        "Save",
                        Path.GetFileNameWithoutExtension(this.m_newPrefabFilePath),
                        "prefab",
                        "Please enter a file name to save the prefab to",
                        this.m_currentFolderPath
                        );

                    if (!string.IsNullOrEmpty(path))
                    {
                        this.m_newPrefabFilePath = path;
                        this.m_currentFolderPath = Path.GetDirectoryName(path);
                    }

                }

                if (!this.m_newPrefabFilePath.EndsWith(".prefab"))
                {
                    this.m_newPrefabFilePath += ".prefab";
                }

                EditorGUILayout.EndHorizontal();

            }

            //
            {

                EditorGUILayout.BeginHorizontal();

                this.m_newMaterialFilePath = EditorGUILayout.TextField("Material", this.m_newMaterialFilePath);

                if (GUILayout.Button("...", GUILayout.MaxWidth(30)))
                {

                    GUI.FocusControl("");

                    string path = EditorUtility.SaveFilePanelInProject(
                        "Save",
                        Path.GetFileNameWithoutExtension(this.m_newMaterialFilePath),
                        "mat",
                        "Please enter a file name to save the material to",
                        this.m_currentFolderPath
                        );

                    if (!string.IsNullOrEmpty(path))
                    {
                        this.m_newMaterialFilePath = path;
                        this.m_currentFolderPath = Path.GetDirectoryName(path);
                    }

                }

                if (!this.m_newMaterialFilePath.EndsWith(".mat"))
                {
                    this.m_newMaterialFilePath += ".mat";
                }

                EditorGUILayout.EndHorizontal();

            }

            GUILayout.FlexibleSpace();

            //
            {

                GUI.enabled = this.isAvailable();

                if (GUILayout.Button("Create Prefab", GUILayout.MinHeight(30)))
                {
                    this.create();
                }

                GUI.enabled = true;

            }

            EditorGUILayout.EndScrollView();

        }

    }

}
