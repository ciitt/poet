﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace Poet
{

    /// <summary>
    /// Set navigation explicit
    /// </summary>
    public class SetNavigationExplicitWindow : EditorWindow
    {

        /// <summary>
        /// Scroll pos
        /// </summary>
        Vector2 m_scrollPos = Vector2.zero;

        /// <summary>
        /// Reference to target parent Transform
        /// </summary>
        Transform m_refParent = null;

        /// <summary>
        /// Selectable array
        /// </summary>
        Selectable[] m_selectables = null;

        /// <summary>
        /// OnGUI
        /// </summary>
        // -----------------------------------------------------------------------------------------
        void OnGUI()
        {

            this.m_scrollPos = EditorGUILayout.BeginScrollView(this.m_scrollPos);

            // HelpBox
            {
                EditorGUILayout.HelpBox(
                    "Convert [Automatic] navigation to [Explicit]",
                    MessageType.Info
                    );
            }

            //
            {
                this.m_refParent = EditorGUILayout.ObjectField("Select Parent", this.m_refParent, typeof(Transform), true) as Transform;
            }

            GUILayout.Space(30.0f);

            //
            {

                if (!this.m_refParent)
                {
                    this.m_selectables = null;
                }

                else
                {

                    if (GUILayout.Button("GetComponentsInChildren<Selectable>()", GUILayout.MinHeight(30)))
                    {

                        this.m_selectables = this.m_refParent.GetComponentsInChildren<Selectable>();

                        if(this.m_selectables.Length <= 0)
                        {
                            EditorUtility.DisplayDialog("Confirmation", "Selectable not found in children", "OK");
                        }

                    }

                    GUILayout.Space(10.0f);

                    GUI.enabled = false;

                    if(this.m_selectables != null && this.m_selectables.Length > 0)
                    {

                        for (int i = 0; i < this.m_selectables.Length; i++)
                        {
                            this.m_selectables[i] =
                                EditorGUILayout.ObjectField(i.ToString("00"), this.m_selectables[i], typeof(Selectable), true) as Selectable;
                        }

                    }

                    GUI.enabled = true;

                }

            }

            GUILayout.Space(30.0f);

            //
            {

                if (this.m_selectables != null && this.m_selectables.Length > 0)
                {

                    if (GUILayout.Button("Convert", GUILayout.MinHeight(30)))
                    {

                        Navigation nav = new Navigation();

                        foreach (var val in this.m_selectables)
                        {

                            if(val.navigation.mode != Navigation.Mode.Automatic)
                            {
                                continue;
                            }

                            // --------------

                            nav.mode = Navigation.Mode.Explicit;
                            nav.selectOnDown = val.FindSelectableOnDown();
                            nav.selectOnLeft = val.FindSelectableOnLeft();
                            nav.selectOnRight = val.FindSelectableOnRight();
                            nav.selectOnUp = val.FindSelectableOnUp();

                            val.navigation = nav;

                            nav = new Navigation();

                        }

                        EditorUtility.DisplayDialog("Confirmation", "Done.", "OK");

                    }

                }

            }

            EditorGUILayout.EndScrollView();

        }

    }

}
