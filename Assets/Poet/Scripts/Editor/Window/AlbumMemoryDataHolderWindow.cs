﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

namespace Poet
{

    /// <summary>
    /// EditorWindow for AlbumMemoryDataHolder
    /// </summary>
    public class AlbumMemoryDataHolderWindow : EditorWindow
    {

        /// <summary>
        /// SerializedObject
        /// </summary>
        SerializedObject m_serializedObject = null;

        /// <summary>
        /// Scroll pos
        /// </summary>
        Vector2 m_scrollPos = Vector2.zero;

        /// <summary>
        /// SerializedProperty for m_dataList
        /// </summary>
        SerializedProperty m_dataList = null;

        /// <summary>
        /// Delete target index
        /// </summary>
        int m_deleteTarget = -1;

        /// <summary>
        /// Set instance
        /// </summary>
        /// <param name="holder">AlbumMemoryDataHolder</param>
        // -----------------------------------------------------------------------------------------
        public void setInstance(AlbumMemoryDataHolder holder)
        {

            this.m_serializedObject = new SerializedObject(holder);

            this.m_dataList = this.m_serializedObject.FindProperty("m_dataList");

        }

        /// <summary>
        /// LoadingMode
        /// </summary>
        /// <returns>LoadingMode</returns>
        // -----------------------------------------------------------------------------------------
        LoadingMode loadingMode()
        {
            return (LoadingMode)this.m_serializedObject.FindProperty("m_loadingModeForThumbnail").enumValueIndex;
        }

        /// <summary>
        /// Draw headers
        /// </summary>
        // -----------------------------------------------------------------------------------------
        void drawHeaders()
        {

            GUILayout.Space(20f);

            EditorGUILayout.BeginHorizontal(GUI.skin.box, GUILayout.MinHeight(22));

            GUILayout.Space(20f);

            float baseWidth = 200.0f;

            EditorGUILayout.LabelField("| Scene Name to Load", GUILayout.MinWidth(baseWidth), GUILayout.MaxWidth(baseWidth));
            EditorGUILayout.LabelField("| Csv Name to Load", GUILayout.MinWidth(baseWidth), GUILayout.MaxWidth(baseWidth));
            EditorGUILayout.LabelField("| Csv Row Identifier to Start", GUILayout.MinWidth(baseWidth), GUILayout.MaxWidth(baseWidth));
            EditorGUILayout.LabelField("| Csv Row Identifier to End", GUILayout.MinWidth(baseWidth), GUILayout.MaxWidth(baseWidth));
            EditorGUILayout.LabelField("| Thumbnail", GUILayout.MinWidth(baseWidth), GUILayout.MaxWidth(baseWidth));
            EditorGUILayout.LabelField("| AssetBundle Name", GUILayout.MinWidth(baseWidth), GUILayout.MaxWidth(baseWidth));
            EditorGUILayout.LabelField("| AssetBundle Variant", GUILayout.MinWidth(baseWidth), GUILayout.MaxWidth(baseWidth));
            EditorGUILayout.LabelField("| Asset Name in AssetBundle", GUILayout.MinWidth(baseWidth), GUILayout.MaxWidth(baseWidth));
            EditorGUILayout.LabelField("| Csv Name to Unlock", GUILayout.MinWidth(baseWidth), GUILayout.MaxWidth(baseWidth));
            EditorGUILayout.LabelField("| Csv Row Identifier to Unlock", GUILayout.MinWidth(baseWidth), GUILayout.MaxWidth(baseWidth));

            EditorGUILayout.EndHorizontal();

            GUILayout.Space(20f);

        }

        /// <summary>
        /// Draw one line
        /// </summary>
        /// <param name="sp">SerializedProperty</param>
        /// <param name="index">array index</param>
        /// <returns>clicked delete button</returns>
        // -----------------------------------------------------------------------------------------
        void drawOneLine(SerializedProperty sp, int index)
        {

            if (sp == null)
            {
                return;
            }

            // -----------------------

            float baseWidth = 200.0f;

            var textFieldStyle = EditorStyles.textField;
            textFieldStyle.wordWrap = true;

            // -----------------------

            EditorGUILayout.BeginHorizontal();

            //
            {

                SerializedProperty tempSP = null;

                //
                {

                    EditorGUILayout.BeginVertical(GUILayout.MaxWidth(22));

                    if (GUILayout.Button("-", GUILayout.MinWidth(22), GUILayout.MinHeight(22), GUILayout.MaxWidth(22), GUILayout.MaxHeight(22)))
                    {

                        GUI.FocusControl("");

                        if (EditorUtility.DisplayDialog("Confirmation", "Delete this element ?", "Yes", "Cancel"))
                        {
                            this.m_deleteTarget = index;
                        }

                    }

                    EditorGUILayout.LabelField(index.ToString(), GUILayout.MinWidth(22), GUILayout.MinHeight(22), GUILayout.MaxWidth(22), GUILayout.MaxHeight(22));

                    EditorGUILayout.EndVertical();

                }

                // sceneName
                {

                    tempSP = sp.FindPropertyRelative("sceneName");

                    tempSP.stringValue =
                        EditorGUILayout.TextArea(
                            tempSP.stringValue,
                            textFieldStyle, GUILayout.MinHeight(64.0f),
                            GUILayout.MinWidth(baseWidth),
                            GUILayout.MaxWidth(baseWidth)
                            );
                }

                // csvName
                {

                    EditorGUILayout.BeginVertical();

                    tempSP = sp.FindPropertyRelative("csvName");

                    tempSP.stringValue =
                        EditorGUILayout.TextArea(
                            tempSP.stringValue,
                            textFieldStyle, GUILayout.MinHeight(64.0f),
                            GUILayout.MinWidth(baseWidth),
                            GUILayout.MaxWidth(baseWidth)
                            );

                    // autoComplete
                    {

                        //
                        {

                            TextAsset autoComplete = null;

                            autoComplete =
                                EditorGUILayout.ObjectField(autoComplete,
                                typeof(TextAsset),
                                false,
                                GUILayout.MinWidth(baseWidth),
                                GUILayout.MaxWidth(baseWidth)
                                ) as TextAsset;

                            if (autoComplete)
                            {
                                tempSP.stringValue = autoComplete.name;
                            }

                        }

                        //
                        {
                            EditorGUILayout.LabelField("(Auto complete field)");
                        }

                    }

                    EditorGUILayout.EndVertical();

                }

                // startCsvRowIdentifier
                {

                    tempSP = sp.FindPropertyRelative("startCsvRowIdentifier");

                    tempSP.stringValue =
                        EditorGUILayout.TextArea(
                            tempSP.stringValue,
                            textFieldStyle, GUILayout.MinHeight(64.0f),
                            GUILayout.MinWidth(baseWidth),
                            GUILayout.MaxWidth(baseWidth)
                            );
                }

                // endCsvRowIdentifier
                {

                    tempSP = sp.FindPropertyRelative("endCsvRowIdentifier");

                    tempSP.stringValue =
                        EditorGUILayout.TextArea(
                            tempSP.stringValue,
                            textFieldStyle, GUILayout.MinHeight(64.0f),
                            GUILayout.MinWidth(baseWidth),
                            GUILayout.MaxWidth(baseWidth)
                            );
                }


                // thumbnailEditorOnly
                {

                    tempSP = sp.FindPropertyRelative("thumbnailEditorOnly");

                    tempSP.objectReferenceValue =
                        EditorGUILayout.ObjectField(tempSP.objectReferenceValue,
                        typeof(Sprite),
                        false,
                        GUILayout.MinWidth(baseWidth),
                        GUILayout.MaxWidth(baseWidth)
                        ) as Sprite;

                    // thumbnail
                    {
                        sp.FindPropertyRelative("thumbnail").objectReferenceValue =
                            (this.loadingMode() == LoadingMode.AssetBundle) ?
                            null :
                            tempSP.objectReferenceValue
                            ;
                    }

                }

                // assetBundleName
                {

                    GUI.enabled = this.loadingMode() == LoadingMode.AssetBundle;

                    sp.FindPropertyRelative("assetBundleName").stringValue =
                        EditorGUILayout.TextArea(
                            sp.FindPropertyRelative("assetBundleName").stringValue,
                            textFieldStyle,
                            GUILayout.MinHeight(64.0f),
                            GUILayout.MinWidth(baseWidth),
                            GUILayout.MaxWidth(baseWidth)
                            );

                    sp.FindPropertyRelative("assetBundleVariant").stringValue =
                        EditorGUILayout.TextField(
                            sp.FindPropertyRelative("assetBundleVariant").stringValue,
                            GUILayout.MinWidth(baseWidth),
                            GUILayout.MaxWidth(baseWidth)
                            );

                    sp.FindPropertyRelative("assetNameInAssetBundle").stringValue =
                        EditorGUILayout.TextArea(
                            sp.FindPropertyRelative("assetNameInAssetBundle").stringValue,
                            textFieldStyle,
                            GUILayout.MinHeight(64.0f),
                            GUILayout.MinWidth(baseWidth),
                            GUILayout.MaxWidth(baseWidth)
                            );

                    GUI.enabled = true;

                }

                // unlock
                {

                    tempSP = sp.FindPropertyRelative("unlock");

                    // csvName
                    {

                        EditorGUILayout.BeginVertical();

                        //
                        {
                            tempSP.FindPropertyRelative("csvName").stringValue =
                                EditorGUILayout.TextArea(
                                    tempSP.FindPropertyRelative("csvName").stringValue,
                                    textFieldStyle,
                                    GUILayout.MinHeight(64.0f),
                                    GUILayout.MinWidth(baseWidth),
                                    GUILayout.MaxWidth(baseWidth)
                                    );
                        }

                        // autoComplete
                        {

                            //
                            {

                                TextAsset autoComplete = null;

                                autoComplete =
                                    EditorGUILayout.ObjectField(autoComplete,
                                    typeof(TextAsset),
                                    false,
                                    GUILayout.MinWidth(baseWidth),
                                    GUILayout.MaxWidth(baseWidth)
                                    ) as TextAsset;

                                if (autoComplete)
                                {
                                    tempSP.FindPropertyRelative("csvName").stringValue = autoComplete.name;
                                }

                            }

                            //
                            {
                                EditorGUILayout.LabelField("(Auto complete field)");
                            }

                        }

                        EditorGUILayout.EndVertical();

                    }

                    // csvRowIdentifier
                    {

                        tempSP.FindPropertyRelative("csvRowIdentifier").stringValue =
                        EditorGUILayout.TextArea(
                            tempSP.FindPropertyRelative("csvRowIdentifier").stringValue,
                            textFieldStyle,
                            GUILayout.MinHeight(64.0f),
                            GUILayout.MinWidth(baseWidth),
                            GUILayout.MaxWidth(baseWidth)
                            );

                    }

                }

            }

            GUILayout.FlexibleSpace();

            EditorGUILayout.EndHorizontal();

            GUILayout.Space(10.0f);

        }

        /// <summary>
        /// OnGUI
        /// </summary>
        // -----------------------------------------------------------------------------------------
        void OnGUI()
        {

            if (this.m_serializedObject == null)
            {
                return;
            }

            EditorGUI.BeginChangeCheck();
            this.m_serializedObject.Update();

            this.m_scrollPos = EditorGUILayout.BeginScrollView(this.m_scrollPos);

            //
            {

                if (this.m_deleteTarget >= 0)
                {
                    this.m_dataList.DeleteArrayElementAtIndex(this.m_deleteTarget);
                    this.m_serializedObject.ApplyModifiedProperties();
                    this.m_deleteTarget = -1;
                }

            }

            //
            {

                // drawHeaders
                {
                    this.drawHeaders();
                }

                //
                {

                    for (int i = 0; i < this.m_dataList.arraySize; i++)
                    {
                        this.drawOneLine(this.m_dataList.GetArrayElementAtIndex(i), i);
                    }

                }

                //
                {
                    if (GUILayout.Button("Add new element", GUILayout.MaxWidth(200), GUILayout.MinHeight(22)))
                    {

                        GUI.FocusControl("");
                        this.m_dataList.InsertArrayElementAtIndex(this.m_dataList.arraySize);

                    }

                }

            }

            EditorGUILayout.EndScrollView();

            if (EditorGUI.EndChangeCheck())
            {
                EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());
            }

            this.m_serializedObject.ApplyModifiedProperties();

        }

    }

}
