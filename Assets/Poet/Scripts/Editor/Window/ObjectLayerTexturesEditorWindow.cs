﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.U2D;

namespace Poet
{

    /// <summary>
    /// EditorWindow for ObjectLayerTexturesControllerScript
    /// </summary>
    public class ObjectLayerTexturesEditorWindow : EditorWindow
    {

        protected class DeleteTargetPose
        {

            public int poseIndex = -1;

            public DeleteTargetPose(int _poseIndex)
            {
                this.poseIndex = _poseIndex;
            }

        }

        protected class DeleteTargetElem : DeleteTargetPose
        {

            public string layerName = "";
            public int indexInLayer = -1;

            public DeleteTargetElem(int _poseIndex, string _layerName, int _indexInLayer) : base(_poseIndex)
            {
                this.layerName = _layerName;
                this.indexInLayer = _indexInLayer;
            }

        }

        /// <summary>
        /// Reference to ObjectLayerTexturesControllerScript
        /// </summary>
        ObjectLayerTexturesControllerScript m_refOltcs = null;

        /// <summary>
        /// SerializedObject
        /// </summary>
        SerializedObject m_serializedObject = null;

        /// <summary>
        /// SerializedProperty
        /// </summary>
        SerializedProperty m_layerTextures = null;

        /// <summary>
        /// Scroll pos for left
        /// </summary>
        Vector2 m_scrollPosLeft = Vector2.zero;

        /// <summary>
        /// Scroll pos for right
        /// </summary>
        Vector2 m_scrollPosRight = Vector2.zero;

        /// <summary>
        /// Selected pose index
        /// </summary>
        int m_selectedPoseIndex = -1;

        /// <summary>
        /// Delete target
        /// </summary>
        DeleteTargetPose m_deleteTarget = null;

        /// <summary>
        /// Current updated target
        /// </summary>
        SerializedProperty m_currentUpdated = null;

        GUIStyle m_boxAlignCenterStyle = null;
        GUIStyle m_boxAlignLeftStyle = null;
        GUIStyle m_buttonAlignCenterStyle = null;
        GUIStyle m_buttonAlignLeftStyle = null;

        /// <summary>
        /// Set instance
        /// </summary>
        /// <param name="oltcs">ObjectLayerTexturesControllerScript</param>
        // -----------------------------------------------------------------------------------------
        public void setInstance(ObjectLayerTexturesControllerScript oltcs)
        {

            this.m_refOltcs = oltcs;

            this.m_serializedObject = new SerializedObject(oltcs);

            this.m_layerTextures = this.m_serializedObject.FindProperty("m_layerTextures");

        }

        /// <summary>
        /// LoadingMode
        /// </summary>
        /// <returns>LoadingMode</returns>
        // -----------------------------------------------------------------------------------------
        LoadingMode loadingMode()
        {
            return (LoadingMode)this.m_serializedObject.FindProperty("m_loadingMode").enumValueIndex;
        }

        /// <summary>
        /// Init styles
        /// </summary>
        // -----------------------------------------------------------------------------------------
        void initStyles()
        {

            if (this.m_boxAlignCenterStyle == null)
            {

                this.m_boxAlignCenterStyle = new GUIStyle(GUI.skin.box);
                this.m_boxAlignLeftStyle = new GUIStyle(GUI.skin.box);
                this.m_buttonAlignCenterStyle = new GUIStyle(GUI.skin.button);
                this.m_buttonAlignLeftStyle = new GUIStyle(GUI.skin.button);

                this.m_boxAlignCenterStyle.alignment = TextAnchor.MiddleCenter;
                this.m_boxAlignLeftStyle.alignment = TextAnchor.MiddleLeft;
                this.m_buttonAlignCenterStyle.alignment = TextAnchor.MiddleCenter;
                this.m_buttonAlignLeftStyle.alignment = TextAnchor.MiddleLeft;

            }

        }

        /// <summary>
        /// Draw lines
        /// </summary>
        /// <param name="sp">SerializedProperty</param>
        /// <param name="poseIndex">pose index</param>
        /// <param name="layerName">layer name</param>
        // -----------------------------------------------------------------------------------------
        void drawOneLines(SerializedProperty sp, int poseIndex, string layerName)
        {

            for (int i = 0; i < sp.arraySize; i++)
            {
                this.drawOneLine(sp.GetArrayElementAtIndex(i), poseIndex, layerName, i);
            }

            GUILayout.Space(20f);

            if (GUILayout.Button(string.Format("Add new item to {0}", layerName), GUILayout.MaxWidth(200), GUILayout.MinHeight(22)))
            {

                GUI.FocusControl("");
                sp.InsertArrayElementAtIndex(sp.arraySize);

                //
                {

                    var temp = sp.GetArrayElementAtIndex(sp.arraySize - 1);

                    temp.FindPropertyRelative("identifier").stringValue = "";
                    temp.FindPropertyRelative("textureEditorOnly").objectReferenceValue = null;
                    temp.FindPropertyRelative("texture").objectReferenceValue = null;
                    temp.FindPropertyRelative("offset").vector2Value = Vector2.zero;
                    temp.FindPropertyRelative("tilling").vector2Value = Vector2.one;
                    temp.FindPropertyRelative("assetBundleName").stringValue = "";
                    temp.FindPropertyRelative("assetBundleVariant").stringValue = "";
                    temp.FindPropertyRelative("assetNameInAssetBundle").stringValue = "";
                    temp.FindPropertyRelative("keepTillingAspect").boolValue = false;

                }

            }

            //
            {
                this.drawDragAndDropBox(sp);
            }

        }

        /// <summary>
        /// Draw drag and drop
        /// </summary>
        /// <param name="sp">SerializedProperty</param>
        // -----------------------------------------------------------------------------------------
        void drawDragAndDropBox(SerializedProperty sp)
        {

            Event currentEvent = Event.current;

            var dropArea = GUILayoutUtility.GetRect(200.0f, 50.0f, GUILayout.MaxWidth(200.0f));
            GUI.Box(dropArea, "Drag & Drop Textures Here", this.m_boxAlignCenterStyle);

            Texture2D tex = null;
            AssetImporter ai = null;

            if (dropArea.Contains(currentEvent.mousePosition))
            {

                DragAndDrop.visualMode = DragAndDropVisualMode.Copy;

                if (currentEvent.type == EventType.DragPerform)
                {

                    foreach (string path in DragAndDrop.paths)
                    {

                        string fileName = Path.GetFileNameWithoutExtension(path);

                        string[] fileNameSplitted = fileName.Split('@');

                        tex = (Texture2D)AssetDatabase.LoadAssetAtPath(path, typeof(Texture2D));

                        ai = AssetImporter.GetAtPath(path);

                        if (tex)
                        {

                            sp.InsertArrayElementAtIndex(sp.arraySize);

                            //
                            {

                                var temp = sp.GetArrayElementAtIndex(sp.arraySize - 1);

                                temp.FindPropertyRelative("identifier").stringValue = fileNameSplitted[fileNameSplitted.Length - 1];
                                temp.FindPropertyRelative("textureEditorOnly").objectReferenceValue = tex;
                                temp.FindPropertyRelative("texture").objectReferenceValue = (this.loadingMode() == LoadingMode.Normal) ? tex : null;

                                temp.FindPropertyRelative("tilling").vector2Value = Vector2.one;
                                temp.FindPropertyRelative("offset").vector2Value = Vector2.zero;

                                temp.FindPropertyRelative("spriteAtlasEditorOnly").objectReferenceValue = null;
                                temp.FindPropertyRelative("spriteAtlas").objectReferenceValue = null;
                                temp.FindPropertyRelative("assetNameInSpriteAtlas").stringValue = "";
                                temp.FindPropertyRelative("posAndSizeForSpriteAtlas").vector4Value = Vector4.one;

                                temp.FindPropertyRelative("assetBundleName").stringValue = (ai) ? ai.assetBundleName : "";
                                temp.FindPropertyRelative("assetBundleVariant").stringValue = (ai) ? ai.assetBundleVariant : "";
                                temp.FindPropertyRelative("assetNameInAssetBundle").stringValue = (ai) ? ai.assetPath.ToLower() : "";

                                temp.FindPropertyRelative("keepTillingAspect").boolValue = false;
                                temp.FindPropertyRelative("usePixelOffset").boolValue = false;
                                temp.FindPropertyRelative("pixelOffset").vector2Value = Vector2.zero;

                            }

                        }

                    }

                    currentEvent.Use();

                }

            }

        }

        /// <summary>
        /// Draw one line
        /// </summary>
        /// <param name="sp">SerializedProperty</param>
        /// <param name="poseIndex">pose index</param>
        /// <param name="layerName">layer name</param>
        /// <param name="indexInLayer">index in layer</param>
        // -----------------------------------------------------------------------------------------
        void drawOneLine(SerializedProperty sp, int poseIndex, string layerName, int indexInLayer)
        {

            bool shouldUpdate = (Event.current.commandName == "UndoRedoPerformed");

            Color oriColor = GUI.backgroundColor;

            if (sp == null)
            {
                return;
            }

            // -----------------

            bool thisIsLayer0 = (layerName == "Layer0");

            SerializedProperty identifier = sp.FindPropertyRelative("identifier");
            SerializedProperty textureEditorOnly = sp.FindPropertyRelative("textureEditorOnly");
            SerializedProperty texture = sp.FindPropertyRelative("texture");

            SerializedProperty tilling = sp.FindPropertyRelative("tilling");
            SerializedProperty offset = sp.FindPropertyRelative("offset");

            SerializedProperty spriteAtlasEditorOnly = sp.FindPropertyRelative("spriteAtlasEditorOnly");
            SerializedProperty spriteAtlas = sp.FindPropertyRelative("spriteAtlas");
            SerializedProperty assetNameInSpriteAtlas = sp.FindPropertyRelative("assetNameInSpriteAtlas");
            SerializedProperty posAndSizeForSpriteAtlas = sp.FindPropertyRelative("posAndSizeForSpriteAtlas");

            SerializedProperty assetBundleName = sp.FindPropertyRelative("assetBundleName");
            SerializedProperty assetBundleVariant = sp.FindPropertyRelative("assetBundleVariant");
            SerializedProperty assetNameInAssetBundle = sp.FindPropertyRelative("assetNameInAssetBundle");

            SerializedProperty keepTillingAspect = sp.FindPropertyRelative("keepTillingAspect");
            SerializedProperty usePixelOffset = sp.FindPropertyRelative("usePixelOffset");
            SerializedProperty pixelOffset = sp.FindPropertyRelative("pixelOffset");

            // -----------------

            if (SerializedProperty.EqualContents(this.m_currentUpdated, sp))
            {
                GUI.backgroundColor = Color.green;
            }

            EditorGUILayout.BeginHorizontal();

            if (thisIsLayer0)
            {
                GUILayout.Space(30f);
            }

            else
            {

                if (GUILayout.Button("-", GUILayout.MinWidth(22), GUILayout.MinHeight(22), GUILayout.MaxWidth(22), GUILayout.MaxHeight(22)))
                {

                    GUI.FocusControl("");

                    if (EditorUtility.DisplayDialog("Confirmation", "Delete this item ?", "Yes", "Cancel"))
                    {
                        this.m_deleteTarget = new DeleteTargetElem(poseIndex, layerName, indexInLayer);
                    }

                }

            }

            //
            {

                if (GUILayout.Button(
                    new GUIContent(textureEditorOnly.objectReferenceValue as Texture2D),
                    this.m_buttonAlignCenterStyle,
                    GUILayout.MinWidth(64),
                    GUILayout.MinHeight(64),
                    GUILayout.MaxWidth(64),
                    GUILayout.MaxHeight(64)
                    ))
                {
                    shouldUpdate = true;
                }

            }

            float baseWidth = 200.0f;

            var textFieldStyle = EditorStyles.textField;
            textFieldStyle.wordWrap = true;

            SerializedProperty layer0 = this.m_layerTextures.GetArrayElementAtIndex(poseIndex).FindPropertyRelative("layer0");
            SerializedProperty layer0TextureEditorOnly = layer0.FindPropertyRelative("textureEditorOnly");

            bool useSpriteAtlas = this.m_refOltcs.isSpriteAtlasShaderForEditor();

            // identifier
            {

                //
                {

                    if (textureEditorOnly.objectReferenceValue && string.IsNullOrEmpty(identifier.stringValue))
                    {
                        identifier.stringValue = textureEditorOnly.objectReferenceValue.name;
                    }

                }

                identifier.stringValue =
                    EditorGUILayout.TextArea(
                        identifier.stringValue,
                        textFieldStyle, GUILayout.MinHeight(64.0f),
                        GUILayout.MinWidth(baseWidth),
                        GUILayout.MaxWidth(baseWidth)
                        );
            }

            // tilling
            {

                Vector2 previousTilling = tilling.vector2Value;
                Texture2D textureEditorOnlyTex = textureEditorOnly.objectReferenceValue as Texture2D;

                EditorGUILayout.BeginVertical();

                tilling.vector2Value =
                    EditorGUILayout.Vector2Field("", tilling.vector2Value,
                    GUILayout.MinWidth(baseWidth),
                    GUILayout.MaxWidth(baseWidth)
                    );

                keepTillingAspect.boolValue = EditorGUILayout.ToggleLeft("Keep Aspect", keepTillingAspect.boolValue);

                if (previousTilling != tilling.vector2Value)
                {
                    shouldUpdate = true;
                }

                if (keepTillingAspect.boolValue && textureEditorOnlyTex)
                {

                    Vector2 temp = tilling.vector2Value;
                    float aspect = (float)textureEditorOnlyTex.width / (float)textureEditorOnlyTex.height;

                    if (
                        useSpriteAtlas &&
                        posAndSizeForSpriteAtlas.vector4Value.y > 0 &&
                        posAndSizeForSpriteAtlas.vector4Value.w > 0
                        )
                    {
                        aspect *= posAndSizeForSpriteAtlas.vector4Value.y / posAndSizeForSpriteAtlas.vector4Value.w;
                    }

                    if (previousTilling.x != temp.x)
                    {
                        temp.y = temp.x * aspect;
                        shouldUpdate = true;
                    }

                    else if (previousTilling.y != temp.y && aspect > 0.0f)
                    {
                        temp.x = temp.y / aspect;
                        shouldUpdate = true;
                    }

                    tilling.vector2Value = temp;

                }

                if (layerName != "Layer0" && textureEditorOnlyTex && layer0TextureEditorOnly.objectReferenceValue)
                {

                    if (GUILayout.Button("Set Relative Size", GUILayout.MinHeight(20)))
                    {

                        Texture2D layer0Tex = layer0TextureEditorOnly.objectReferenceValue as Texture2D;
                        Vector2 layer0Tilling = layer0.FindPropertyRelative("tilling").vector2Value;

                        Vector2 temp = Vector2.zero;

                        GUI.FocusControl("");

                        temp.x = (float)layer0Tex.width / (float)textureEditorOnlyTex.width;
                        temp.y = (float)layer0Tex.height / (float)textureEditorOnlyTex.height;
                        temp.x *= layer0Tilling.x;
                        temp.y *= layer0Tilling.y;

                        if (
                            useSpriteAtlas &&
                            posAndSizeForSpriteAtlas.vector4Value.y > 0 &&
                            posAndSizeForSpriteAtlas.vector4Value.w > 0
                            )
                        {
                            temp.x /= posAndSizeForSpriteAtlas.vector4Value.y;
                            temp.y /= posAndSizeForSpriteAtlas.vector4Value.w;
                        }

                        tilling.vector2Value = temp;

                        shouldUpdate = true;

                    }

                }

                EditorGUILayout.EndVertical();

            }

            // pixelOffset offset
            {

                Texture2D layer0Texture = layer0TextureEditorOnly.objectReferenceValue as Texture2D;

                EditorGUILayout.BeginVertical();

                if (usePixelOffset.boolValue && layer0Texture)
                {

                    Vector2 previousPixelOffset = pixelOffset.vector2Value;

                    pixelOffset.vector2Value =
                        EditorGUILayout.Vector2Field(
                            "",
                            pixelOffset.vector2Value,
                            GUILayout.MinWidth(baseWidth),
                            GUILayout.MaxWidth(baseWidth)
                            );

                    offset.vector2Value =
                        new Vector2(
                            0.5f - (pixelOffset.vector2Value.x / layer0Texture.width),
                            (pixelOffset.vector2Value.y / layer0Texture.height) - 0.5f
                            );

                    if (previousPixelOffset != pixelOffset.vector2Value)
                    {
                        shouldUpdate = true;
                    }

                }

                // offset
                else
                {

                    Vector2 previousOffset = offset.vector2Value;

                    offset.vector2Value =
                        EditorGUILayout.Vector2Field(
                            "",
                            offset.vector2Value,
                            GUILayout.MinWidth(baseWidth),
                            GUILayout.MaxWidth(baseWidth)
                            );

                    if (layer0Texture)
                    {
                        pixelOffset.vector2Value =
                            new Vector2(
                                (0.5f - offset.vector2Value.x) * layer0Texture.width,
                                (0.5f + offset.vector2Value.y) * layer0Texture.height
                                );
                    }

                    if (previousOffset != offset.vector2Value)
                    {
                        shouldUpdate = true;
                    }

                }

                // usePixelOffset
                {

                    if (layer0Texture)
                    {
                        usePixelOffset.boolValue = EditorGUILayout.ToggleLeft("Use Pixel Offset", usePixelOffset.boolValue);
                    }

                    else
                    {
                        usePixelOffset.boolValue = false;
                    }

                }

                EditorGUILayout.EndVertical();

            }

            // textureEditorOnly
            {

                EditorGUILayout.BeginVertical();

                Texture2D previous = textureEditorOnly.objectReferenceValue as Texture2D;

                textureEditorOnly.objectReferenceValue =
                    EditorGUILayout.ObjectField(textureEditorOnly.objectReferenceValue,
                    typeof(Texture2D),
                    false,
                    GUILayout.MinWidth(baseWidth),
                    GUILayout.MaxWidth(baseWidth)
                    ) as Texture2D;

                if (previous != textureEditorOnly.objectReferenceValue)
                {
                    shouldUpdate = true;
                }

                // texture
                {

                    if(spriteAtlasEditorOnly.objectReferenceValue)
                    {
                        texture.objectReferenceValue = null;
                    }

                    else
                    {

                        texture.objectReferenceValue =
                            (this.loadingMode() == LoadingMode.AssetBundle) ?
                            null :
                            textureEditorOnly.objectReferenceValue
                            ;

                    }

                }


                //
                {

                    GUI.enabled = useSpriteAtlas;

                    if (useSpriteAtlas && textureEditorOnly.objectReferenceValue && spriteAtlasEditorOnly.objectReferenceValue)
                    {

                        if (GUILayout.Button("Calculate Pos and Size", GUILayout.MinHeight(20)))
                        {

                            Vector4 posAndSize = Vector4.zero;

                            if (
                                this.calcPosAndSize(
                                    textureEditorOnly.objectReferenceValue as Texture2D,
                                    ref posAndSize
                                    )
                                )
                            {
                                posAndSizeForSpriteAtlas.vector4Value = posAndSize;
                                shouldUpdate = true;
                            }

                        }

                        // posAndSizeForSpriteAtlas
                        {
                            posAndSizeForSpriteAtlas.vector4Value = EditorGUILayout.Vector4Field("", posAndSizeForSpriteAtlas.vector4Value);
                        }

                    }

                    else
                    {
                        posAndSizeForSpriteAtlas.vector4Value = IdAndTextureDetail.defaultPosAndSizeForSpriteAtlas;
                    }

                    GUI.enabled = true;

                }

                EditorGUILayout.EndVertical();

            }

            // spriteAtlas
            {

                EditorGUILayout.BeginVertical();

                GUI.enabled = useSpriteAtlas;

                spriteAtlasEditorOnly.objectReferenceValue =
                    EditorGUILayout.ObjectField(spriteAtlasEditorOnly.objectReferenceValue,
                    typeof(SpriteAtlas),
                    false,
                    GUILayout.MinWidth(baseWidth),
                    GUILayout.MaxWidth(baseWidth)
                    ) as SpriteAtlas;

                // spriteAtlas
                {
                    spriteAtlas.objectReferenceValue =
                        (this.loadingMode() == LoadingMode.AssetBundle) ?
                        null :
                        spriteAtlasEditorOnly.objectReferenceValue
                        ;
                }

                // assetNameInSpriteAtlas
                {

                    if (textureEditorOnly.objectReferenceValue)
                    {
                        assetNameInSpriteAtlas.stringValue = textureEditorOnly.objectReferenceValue.name;
                    }

                }

                GUI.enabled = true;

                EditorGUILayout.EndVertical();

            }

            // assetBundleName
            {

                GUI.enabled = this.loadingMode() == LoadingMode.AssetBundle;

                assetBundleName.stringValue =
                    EditorGUILayout.TextArea(
                        assetBundleName.stringValue,
                        textFieldStyle,
                        GUILayout.MinHeight(64.0f),
                        GUILayout.MinWidth(baseWidth),
                        GUILayout.MaxWidth(baseWidth)
                        );

                assetBundleVariant.stringValue =
                    EditorGUILayout.TextField(
                        assetBundleVariant.stringValue,
                        GUILayout.MinWidth(baseWidth),
                        GUILayout.MaxWidth(baseWidth)
                        );

                assetNameInAssetBundle.stringValue =
                    EditorGUILayout.TextArea(
                        assetNameInAssetBundle.stringValue,
                        textFieldStyle,
                        GUILayout.MinHeight(64.0f),
                        GUILayout.MinWidth(baseWidth),
                        GUILayout.MaxWidth(baseWidth)
                        );

                GUI.enabled = true;

            }

            GUILayout.FlexibleSpace();

            EditorGUILayout.EndHorizontal();

            // shouldUpdate
            {

                if (shouldUpdate)
                {

                    this.m_currentUpdated = sp;

                    this.m_refOltcs.applyTextureForEditor(
                        layerName,
                        tilling.vector2Value,
                        offset.vector2Value,
                        textureEditorOnly.objectReferenceValue as Texture2D,
                        posAndSizeForSpriteAtlas.vector4Value
                        );

                }

            }

            // backgroundColor
            {
                GUI.backgroundColor = oriColor;
            }

        }

        /// <summary>
        /// Draw headers
        /// </summary>
        // -----------------------------------------------------------------------------------------
        void drawHeaders()
        {

            GUILayout.Space(20f);

            EditorGUILayout.BeginHorizontal(GUI.skin.box, GUILayout.MinHeight(22));

            GUILayout.Space(100f);

            float baseWidth = 200.0f;

            EditorGUILayout.LabelField("| Identifier", GUILayout.MinWidth(baseWidth), GUILayout.MaxWidth(baseWidth));
            EditorGUILayout.LabelField("| Tilling", GUILayout.MinWidth(baseWidth), GUILayout.MaxWidth(baseWidth));
            EditorGUILayout.LabelField("| Offset", GUILayout.MinWidth(baseWidth), GUILayout.MaxWidth(baseWidth));
            EditorGUILayout.LabelField("| Texture", GUILayout.MinWidth(baseWidth), GUILayout.MaxWidth(baseWidth));
            EditorGUILayout.LabelField("| SpriteAtlas", GUILayout.MinWidth(baseWidth), GUILayout.MaxWidth(baseWidth));
            EditorGUILayout.LabelField("| AssetBundle Name", GUILayout.MinWidth(baseWidth), GUILayout.MaxWidth(baseWidth));
            EditorGUILayout.LabelField("| AssetBundle Variant", GUILayout.MinWidth(baseWidth), GUILayout.MaxWidth(baseWidth));
            EditorGUILayout.LabelField("| Asset Name in AssetBundle", GUILayout.MinWidth(baseWidth), GUILayout.MaxWidth(baseWidth));

            EditorGUILayout.EndHorizontal();

            GUILayout.Space(20f);

        }

        /// <summary>
        /// Pose name
        /// </summary>
        /// <param name="index"></param>
        /// <returns>pose name</returns>
        // -----------------------------------------------------------------------------------------
        string poseName(int index)
        {

            if (index >= this.m_layerTextures.arraySize)
            {
                return "";
            }

            // ---------------------

            var temp = this.m_layerTextures.GetArrayElementAtIndex(index).FindPropertyRelative("layer0");

            string identifier = temp.FindPropertyRelative("identifier").stringValue;

            if (!string.IsNullOrEmpty(identifier))
            {
                return string.Format("Pose {0} -> ", index) + identifier;
            }

            else
            {
                return string.Format("Pose {0}", index);
            }

        }

        /// <summary>
        /// Delete object
        /// </summary>
        // -----------------------------------------------------------------------------------------
        void deleteObjectIfNeeded()
        {

            if (this.m_deleteTarget == null)
            {
                return;
            }

            // --------------------

            if (this.m_deleteTarget is DeleteTargetElem)
            {

                SerializedProperty sp = null;
                var temp = this.m_deleteTarget as DeleteTargetElem;

                if (temp.layerName == "Layer1")
                {
                    sp = this.m_layerTextures.GetArrayElementAtIndex(temp.poseIndex).FindPropertyRelative("layer1s");
                }

                else if (temp.layerName == "Layer2")
                {
                    sp = this.m_layerTextures.GetArrayElementAtIndex(temp.poseIndex).FindPropertyRelative("layer2s");
                }

                else if (temp.layerName == "Layer3")
                {
                    sp = this.m_layerTextures.GetArrayElementAtIndex(temp.poseIndex).FindPropertyRelative("layer3s");
                }

                else if (temp.layerName == "Layer4")
                {
                    sp = this.m_layerTextures.GetArrayElementAtIndex(temp.poseIndex).FindPropertyRelative("layer4s");
                }

                else if (temp.layerName == "Layer5")
                {
                    sp = this.m_layerTextures.GetArrayElementAtIndex(temp.poseIndex).FindPropertyRelative("layer5s");
                }

                else if (temp.layerName == "Overlay")
                {
                    sp = this.m_layerTextures.GetArrayElementAtIndex(temp.poseIndex).FindPropertyRelative("overlays");
                }

                // ----------------

                if (sp != null && temp.indexInLayer < sp.arraySize)
                {
                    sp.DeleteArrayElementAtIndex(temp.indexInLayer);
                    this.m_currentUpdated = null;
                }

            }

            else
            {
                if (this.m_deleteTarget.poseIndex < this.m_layerTextures.arraySize)
                {
                    this.m_layerTextures.DeleteArrayElementAtIndex(this.m_deleteTarget.poseIndex);
                }
            }

            this.m_serializedObject.ApplyModifiedProperties();

            this.m_deleteTarget = null;

        }

        /// <summary>
        /// Draw list
        /// </summary>
        /// <param name="sp">SerializedProperty</param>
        /// <param name="poseIndex">pose index</param>
        /// <param name="layerName">layer name</param>
        // -----------------------------------------------------------------------------------------
        void drawLayerList(SerializedProperty sp, int poseIndex, string layerName)
        {

            GUILayout.Space(100f);

            GUI.backgroundColor = Color.yellow;
            EditorGUILayout.LabelField(string.Format("{0} List", layerName), this.m_boxAlignLeftStyle, GUILayout.ExpandWidth(true));
            GUI.backgroundColor = Color.white;
            this.drawOneLines(sp, poseIndex, layerName);

        }

        /// <summary>
        /// Clear material textures
        /// </summary>
        // -----------------------------------------------------------------------------------------
        void clearMaterialTextures()
        {
            this.m_refOltcs.clearMaterialTextures();
        }

        /// <summary>
        /// Calculate pos and size for SpriteAtlas
        /// </summary>
        /// <param name="tex">Texture2D</param>
        /// <param name="ret">Vector4</param>
        /// <returns>success</returns>
        // -----------------------------------------------------------------------------------------
        bool calcPosAndSize(Texture2D tex, ref Vector4 ret)
        {

            if (!tex)
            {
                return false;
            }

            // ------------------

            string path = AssetDatabase.GetAssetPath(tex);


            TextureImporter ti = AssetImporter.GetAtPath(path) as TextureImporter;

            bool oriReadable = ti.isReadable;

            if (!ti.isReadable)
            {
                ti.isReadable = true;
                AssetDatabase.ImportAsset(path, ImportAssetOptions.ForceSynchronousImport);
            }

            Color[] colors = tex.GetPixels();

            int left = tex.width;
            int right = 0;
            int up = 0;
            int bottom = tex.height;

            Color color = Color.white;

            for (int h = 0; h < tex.height; h++)
            {

                for (int w = 0; w < tex.width; w++)
                {

                    color = colors[(h * tex.width) + w];

                    if (color.a > 0.0f)
                    {

                        if (w < left) { left = w; }
                        if (w > right) { right = w; }
                        if (h < bottom) { bottom = h; }
                        if (h > up) { up = h; }

                    }

                }

            }

            if (left < right && bottom < up)
            {

                //ret.x = left / (float)tex.width;
                //ret.y = (right - left + 1) / (float)tex.width;
                //ret.z = bottom / (float)tex.height;
                //ret.w = (up - bottom + 1) / (float)tex.height;

                ret.x = Mathf.Max(0.0f, (left - 2) / (float)tex.width);
                ret.y = Mathf.Min(1.0f, (right - left + 5) / (float)tex.width);
                ret.z = Mathf.Max(0.0f, (bottom - 2) / (float)tex.height);
                ret.w = Mathf.Min(1.0f, (up - bottom + 5) / (float)tex.height);

            }

            if (oriReadable != ti.isReadable)
            {
                ti.isReadable = oriReadable;
                AssetDatabase.ImportAsset(path, ImportAssetOptions.ForceSynchronousImport);
            }

            return true;

        }

        /// <summary>
        /// OnGUI
        /// </summary>
        // -----------------------------------------------------------------------------------------
        void OnGUI()
        {

            this.initStyles();

            if (!this.m_refOltcs || this.m_serializedObject == null)
            {
                return;
            }

            EditorGUI.BeginChangeCheck();
            this.m_serializedObject.Update();

            // deleteObjectIfNeeded
            {
                this.deleteObjectIfNeeded();
            }

            // ----------------------------

            // main
            {

                int texturesListSize = this.m_layerTextures.arraySize;

                Color originalBackgroundColor = GUI.backgroundColor;

                EditorGUILayout.BeginHorizontal();

                // left
                {

                    this.m_scrollPosLeft = EditorGUILayout.BeginScrollView(this.m_scrollPosLeft, GUILayout.MinWidth(200));

                    EditorGUILayout.BeginVertical(GUI.skin.box);

                    // add
                    {
                        if (GUILayout.Button("Add New Pose", GUILayout.MinHeight(30)))
                        {

                            this.m_layerTextures.InsertArrayElementAtIndex(this.m_layerTextures.arraySize);

                            // layer0
                            {

                                var temp = this.m_layerTextures.GetArrayElementAtIndex(this.m_layerTextures.arraySize - 1).FindPropertyRelative("layer0");

                                temp.FindPropertyRelative("identifier").stringValue = "";
                                temp.FindPropertyRelative("textureEditorOnly").objectReferenceValue = null;
                                temp.FindPropertyRelative("texture").objectReferenceValue = null;

                                temp.FindPropertyRelative("tilling").vector2Value = Vector2.one;
                                temp.FindPropertyRelative("offset").vector2Value = Vector2.zero;

                                temp.FindPropertyRelative("spriteAtlasEditorOnly").objectReferenceValue = null;
                                temp.FindPropertyRelative("spriteAtlas").objectReferenceValue = null;
                                temp.FindPropertyRelative("assetNameInSpriteAtlas").stringValue = "";
                                temp.FindPropertyRelative("posAndSizeForSpriteAtlas").vector4Value = Vector4.one;

                                temp.FindPropertyRelative("assetBundleName").stringValue = "";
                                temp.FindPropertyRelative("assetBundleVariant").stringValue = "";
                                temp.FindPropertyRelative("assetNameInAssetBundle").stringValue = "";

                                temp.FindPropertyRelative("keepTillingAspect").boolValue = false;
                                temp.FindPropertyRelative("usePixelOffset").boolValue = false;
                                temp.FindPropertyRelative("pixelOffset").vector2Value = Vector2.zero;

                            }

                            //
                            {

                                var temp = this.m_layerTextures.GetArrayElementAtIndex(this.m_layerTextures.arraySize - 1);

                                temp.FindPropertyRelative("layer1s").ClearArray();
                                temp.FindPropertyRelative("layer2s").ClearArray();
                                temp.FindPropertyRelative("layer3s").ClearArray();
                                temp.FindPropertyRelative("layer4s").ClearArray();
                                temp.FindPropertyRelative("layer5s").ClearArray();
                                temp.FindPropertyRelative("overlays").ClearArray();

                            }

                        }

                    }

                    GUILayout.Space(20f);

                    // pose list
                    {

                        for (int i = 0; i < texturesListSize; i++)
                        {

                            EditorGUILayout.BeginHorizontal();

                            // delete button
                            {

                                if (GUILayout.Button("-", GUILayout.MaxWidth(20), GUILayout.MinHeight(22)))
                                {

                                    GUI.FocusControl("");

                                    if (EditorUtility.DisplayDialog("Confirmation", "Delete this pose ?", "Yes", "Cancel"))
                                    {
                                        this.m_deleteTarget = new DeleteTargetPose(i);
                                    }

                                }

                            }

                            // pose button
                            {

                                if (this.m_selectedPoseIndex == i)
                                {
                                    GUI.backgroundColor = Color.yellow;
                                }

                                if (GUILayout.Button(this.poseName(i), this.m_buttonAlignLeftStyle, GUILayout.MinHeight(22)))
                                {
                                    GUI.FocusControl("");
                                    this.m_selectedPoseIndex = i;
                                }

                                GUI.backgroundColor = originalBackgroundColor;

                            }

                            EditorGUILayout.EndHorizontal();

                        }

                    }

                    GUILayout.Space(20f);

                    // Clear Material Textures
                    {

                        if (GUILayout.Button("Clear\nMaterial Textures", GUILayout.MinHeight(30)))
                        {
                            this.clearMaterialTextures();
                        }

                    }

                    GUILayout.FlexibleSpace();

                    EditorGUILayout.EndVertical();

                    EditorGUILayout.EndScrollView();

                }

                // right
                {

                    this.m_scrollPosRight = EditorGUILayout.BeginScrollView(this.m_scrollPosRight);

                    // HelpBox
                    {
                        EditorGUILayout.HelpBox(
                            "Other parameters are in Inspector.\n" +
                            "Click image icon to apply a texture.\n",
                            MessageType.Info
                            );
                    }

                    EditorGUILayout.BeginVertical();

                    GUILayout.Space(20f);

                    this.drawHeaders();

                    if (0 <= this.m_selectedPoseIndex && this.m_selectedPoseIndex < texturesListSize)
                    {

                        // layer0
                        {
                            GUILayout.Space(20f);
                            GUI.backgroundColor = Color.yellow;
                            EditorGUILayout.LabelField("Layer 0", this.m_boxAlignLeftStyle, GUILayout.ExpandWidth(true));
                            GUI.backgroundColor = Color.white;
                            this.drawOneLine(this.m_layerTextures.GetArrayElementAtIndex(this.m_selectedPoseIndex).FindPropertyRelative("layer0"), this.m_selectedPoseIndex, "Layer0", -1);
                        }

                        this.drawLayerList(this.m_layerTextures.GetArrayElementAtIndex(this.m_selectedPoseIndex).FindPropertyRelative("layer1s"), this.m_selectedPoseIndex, "Layer1");

                        if (!this.m_refOltcs.isMobileShaderForEditor())
                        {
                            this.drawLayerList(this.m_layerTextures.GetArrayElementAtIndex(this.m_selectedPoseIndex).FindPropertyRelative("layer2s"), this.m_selectedPoseIndex, "Layer2");
                            this.drawLayerList(this.m_layerTextures.GetArrayElementAtIndex(this.m_selectedPoseIndex).FindPropertyRelative("layer3s"), this.m_selectedPoseIndex, "Layer3");
                            this.drawLayerList(this.m_layerTextures.GetArrayElementAtIndex(this.m_selectedPoseIndex).FindPropertyRelative("layer4s"), this.m_selectedPoseIndex, "Layer4");
                            this.drawLayerList(this.m_layerTextures.GetArrayElementAtIndex(this.m_selectedPoseIndex).FindPropertyRelative("layer5s"), this.m_selectedPoseIndex, "Layer5");
                            this.drawLayerList(this.m_layerTextures.GetArrayElementAtIndex(this.m_selectedPoseIndex).FindPropertyRelative("overlays"), this.m_selectedPoseIndex, "Overlay");
                        }

                    }

                    else
                    {

                        GUILayout.Space(20f);

                        EditorGUILayout.LabelField("Choose a pose.");
                    }

                    GUILayout.Space(200f);

                    EditorGUILayout.EndScrollView();

                    EditorGUILayout.EndVertical();


                }

                EditorGUILayout.EndHorizontal();

            } // main

            if (EditorGUI.EndChangeCheck())
            {
                EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());
            }

            this.m_serializedObject.ApplyModifiedProperties();

        }

    }

}
