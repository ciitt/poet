﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEditor;
using UnityEditor.Animations;
using UnityEngine;
using UnityEngine.UI;

namespace Poet
{

    /// <summary>
    /// Show AnimatorController parameters
    /// </summary>
    public class AnimatorControllerParamsWindow : EditorWindow
    {

        /// <summary>
        /// Scroll pos
        /// </summary>
        Vector2 m_scrollPos = Vector2.zero;

        AnimatorController m_controller = null;

        StringBuilder m_stringBuilderForText = new StringBuilder();

        StringBuilder m_stringBuilderForSet = new StringBuilder();

        string m_text = "";

        bool m_set = false;

        string m_textForSet = "";

        /// <summary>
        /// OnGUI
        /// </summary>
        // -----------------------------------------------------------------------------------------
        void OnGUI()
        {

            this.m_scrollPos = EditorGUILayout.BeginScrollView(this.m_scrollPos);

            // HelpBox
            {
                EditorGUILayout.HelpBox(
                    "Show AnimatorController Parameter Names",
                    MessageType.Info
                    );
            }

            GUILayout.Space(30.0f);

            //
            {

                AnimatorController previous = this.m_controller;

                this.m_controller = EditorGUILayout.ObjectField("AnimatorController", this.m_controller, typeof(AnimatorController), true) as AnimatorController;

                this.m_set = EditorGUILayout.ToggleLeft("Set****()", this.m_set);

                if (this.m_controller && this.m_controller != previous)
                {

                    this.m_text = "";
                    this.m_textForSet = "";
                    this.m_stringBuilderForText.Length = 0;
                    this.m_stringBuilderForSet.Length = 0;

                    foreach (var val in this.m_controller.parameters)
                    {
                        //
                        {

                            if(val.type == AnimatorControllerParameterType.Bool)
                            {
                                this.m_stringBuilderForSet.Append("SetBool(" + val.name + ")\n");
                            }

                            else if (val.type == AnimatorControllerParameterType.Float)
                            {
                                this.m_stringBuilderForSet.Append("SetFloat(" + val.name + ")\n");
                            }

                            else if (val.type == AnimatorControllerParameterType.Int)
                            {
                                this.m_stringBuilderForSet.Append("SetInteger(" + val.name + ")\n");
                            }

                            else if (val.type == AnimatorControllerParameterType.Trigger)
                            {
                                this.m_stringBuilderForSet.Append("SetTrigger(" + val.name + ")\n");
                            }

                        }

                        //
                        {
                            this.m_stringBuilderForText.Append(val.name + "\n");
                        }

                    }

                    this.m_text = this.m_stringBuilderForText.ToString();
                    this.m_textForSet = this.m_stringBuilderForSet.ToString();

                }

            }

            // TextArea
            {
                EditorGUILayout.TextArea(this.m_set ? this.m_textForSet : this.m_text);
            }

            EditorGUILayout.EndScrollView();

        }

    }

}
