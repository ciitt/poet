﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;

namespace Poet
{

    public class AtlasLoaderDummy : MonoBehaviour
    {

        void OnEnable()
        {
            SpriteAtlasManager.atlasRequested += RequestAtlas;
        }

        void OnDisable()
        {
            SpriteAtlasManager.atlasRequested -= RequestAtlas;
        }

        void RequestAtlas(string tag, System.Action<SpriteAtlas> callback)
        {
            //var sa = Resources.Load<SpriteAtlas>(tag);
            //callback(sa);
        }

    }

}
