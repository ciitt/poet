﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poet
{

    public class SampleForPoetEventScript : MonoBehaviour
    {

#if UNITY_EDITOR

        /// <summary>
        /// User progress data
        /// </summary>
        [Serializable]
        protected class UserProgressData
        {

            public string text = "";
            public Vector3 vec3 = Vector3.zero;

        }

        /// <summary>
        /// UserProgressData
        /// </summary>
        UserProgressData m_userProgressData = new UserProgressData();

        /// <summary>
        /// PoetSequenceState receiver
        /// </summary>
        /// <param name="psState">PoetSequenceState</param>
        // ------------------------------------------------------------------------------------------
        void onPoetSequenceState(PoetSequenceState psState)
        {
            print("psState.stateEnum : " + psState.stateEnum);
        }

        /// <summary>
        /// PoetInitState receiver
        /// </summary>
        /// <param name="piState">PoetInitState</param>
        // ------------------------------------------------------------------------------------------
        void onPoetInitState(PoetInitState piState)
        {
            //
        }

        /// <summary>
        /// TextShowingState receiver
        /// </summary>
        /// <param name="tsState">TextShowingState</param>
        // ------------------------------------------------------------------------------------------
        void onTextShowingState(TextShowingState tsState)
        {
            print("tsState.stateEnum : " + tsState.stateEnum);
        }

        /// <summary>
        /// UserProgressDataSignal receiver
        /// </summary>
        /// <param name="updSignal">UserProgressDataSignal</param>
        // ------------------------------------------------------------------------------------------
        void onUserProgressDataSignal(UserProgressDataSignal updSignal)
        {

            print("updSignal");

            this.m_userProgressData.text = Time.frameCount.ToString();
            this.m_userProgressData.vec3 = this.transform.position;

            updSignal.addDataAction(this.transform, this, JsonUtility.ToJson(this.m_userProgressData));

        }

        /// <summary>
        /// SceneChangeState receiver
        /// </summary>
        /// <param name="scState">SceneChangeState</param>
        // ------------------------------------------------------------------------------------------
        void onSceneChangeStateReceiver(SSC.SceneChangeState scState)
        {

            print("scState.stateEnum : " + scState.stateEnum);

            if (scState.stateEnum == SSC.SceneChangeState.StateEnum.AllStartupsDone)
            {

                PoetSceneChangeManager pscm = PoetSceneChangeManager.Instance as PoetSceneChangeManager;

                if (pscm.isLoadingCurrentSceneWithUserProgressData())
                {

                    this.m_userProgressData = pscm.getDataFromCurrentUserProgressData<UserProgressData>(this.transform, this);

                    print("m_userProgressData.text : " + this.m_userProgressData.text);

                }

            }

        }

        /// <summary>
        /// PauseState receiver
        /// </summary>
        /// <param name="pState">PauseState</param>
        // ------------------------------------------------------------------------------------------
        void onPauseStateReceiver(SSC.PauseState pState)
        {
            print("pState.pause : " + pState.pause);
        }

        /// <summary>
        /// UiState receiver
        /// </summary>
        /// <param name="uiState">UiState</param>
        // -----------------------------------------------------------------------------------------------
        void onUiStateReceiver(UiState uiState)
        {
            print("uiState.stateEnum : " + uiState.stateEnum);
        }

        /// <summary>
        /// ChangeLanguageSignal receiver
        /// </summary>
        /// <param name="clSignal">ChangeLanguageSignal</param>
        // -----------------------------------------------------------------------------------------------
        void onChangeLanguageSignalReceiver(ChangeLanguageSignal clSignal)
        {
            print("clSignal : " + SystemManager.Instance.systemLanguage);
        }

        // ------------------------------------------------------------------------------------------
        void Start()
        {

            PoetReduxManager prm = PoetReduxManager.Instance as PoetReduxManager;

            prm.addPoetSequenceStateReceiver(this.onPoetSequenceState);
            prm.addPoetInitStateReceiver(this.onPoetInitState);
            prm.addTextFinishedStateReceiver(this.onTextShowingState);
            prm.addUserProgressDataSignalReceiver(this.onUserProgressDataSignal);
            prm.addUiStateReceiver(this.onUiStateReceiver);
            prm.addChangeLanguageSignalReceiver(this.onChangeLanguageSignalReceiver);
            prm.addSceneChangeStateReceiver(this.onSceneChangeStateReceiver);
            prm.addPauseStateReceiver(this.onPauseStateReceiver);

        }

        //// ------------------------------------------------------------------------------------------
        //void Update()
        //{

        //    // How to get current state

        //    PoetReduxManager prm = PoetReduxManager.Instance as PoetReduxManager;

        //    // var psState = prm.PoetSequenceStateWatcher.state(); // not have much meaning
        //    // var piState = prm.PoetInitStateWatcher.state(); // not have much meaning
        //    var tsState = prm.TextFinishedStateWatcher.state();
        //    // var updSignal = prm.UserProgressDataSignalWatcher.state(); // not have much meaning
        //    var uiState = prm.UiStateWatcher.state();
        //    // var clSignal = prm.ChangeLanguageSignalWatcher.state(); // not have much meaning
        //    var scState = prm.SceneChangeStateWatcher.state();
        //    var pauseState = prm.PauseStateWatcher.state();

        //}

#endif

    }

}
