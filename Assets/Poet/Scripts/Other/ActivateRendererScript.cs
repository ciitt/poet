﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poet
{

    /// <summary>
    /// Activate renderer
    /// </summary>
    public class ActivateRendererScript : MonoBehaviour
    {

        /// <summary>
        /// User progress data
        /// </summary>
        [Serializable]
        protected class UserProgressData
        {
            public bool active = false;
        }

        /// <summary>
        /// Hide at scene start
        /// </summary>
        [SerializeField]
        [Tooltip("Hide at scene start")]
        protected bool m_hideAtSceneStart = true;

        /// <summary>
        /// UserProgressData
        /// </summary>
        protected UserProgressData m_userProgressData = new UserProgressData();

        /// <summary>
        /// Renderer list
        /// </summary>
        protected Renderer[] m_rendererList = null;

        /// <summary>
        /// Start
        /// </summary>
        // -------------------------------------------------------------------------------------
        protected virtual void Start()
        {

            // prm
            {

                PoetReduxManager prm = PoetReduxManager.Instance as PoetReduxManager;

                prm.addUserProgressDataSignalReceiver(this.onUserProgressDataSignal);
                prm.addSceneChangeStateReceiver(this.onSceneChangeStateReceiver);

            }

        }

        /// <summary>
        /// UserProgressDataSignal receiver
        /// </summary>
        /// <param name="updSignal">UserProgressDataSignal</param>
        // ------------------------------------------------------------------------------------------
        protected virtual void onUserProgressDataSignal(UserProgressDataSignal updSignal)
        {
            updSignal.addDataAction(this.transform, this, JsonUtility.ToJson(this.m_userProgressData));
        }

        /// <summary>
        /// SceneChangeState receiver
        /// </summary>
        /// <param name="scState">SceneChangeState</param>
        // ------------------------------------------------------------------------------------------
        protected virtual void onSceneChangeStateReceiver(SSC.SceneChangeState scState)
        {

            if (scState.stateEnum == SSC.SceneChangeState.StateEnum.AllStartupsDone)
            {

                // m_rendererList
                {
                    this.m_rendererList = this.GetComponentsInChildren<Renderer>();
                }

                PoetSceneChangeManager pscm = PoetSceneChangeManager.Instance as PoetSceneChangeManager;

                if (pscm.isLoadingCurrentSceneWithUserProgressData())
                {

                    this.m_userProgressData = pscm.getDataFromCurrentUserProgressData<UserProgressData>(this.transform, this);

                    if(!this.m_userProgressData.active)
                    {
                        this.setAllRenderersActive(false);
                    }

                }

                else
                {

                    if (this.m_hideAtSceneStart)
                    {
                        this.setAllRenderersActive(false);
                    }

                }

            }

        }

        /// <summary>
        /// Set active for all renderers
        /// </summary>
        /// <param name="active">active</param>
        // -------------------------------------------------------------------------------------
        public void setAllRenderersActive(bool active)
        {

            // enabled
            {

                foreach (var val in this.m_rendererList)
                {
                    val.enabled = active;
                }

            }

            // m_userProgressData
            {
                this.m_userProgressData.active = active;
            }

        }

    }

}
