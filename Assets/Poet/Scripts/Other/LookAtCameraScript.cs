﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poet
{

    /// <summary>
    /// LookAt
    /// </summary>
    [ExecuteInEditMode]
    public class LookAtCameraScript : MonoBehaviour
    {

        /// <summary>
        /// Only axis Y
        /// </summary>
        [SerializeField]
        [Tooltip("Only axis Y")]
        protected bool m_onlyAxisY = false;

        /// <summary>
        /// If null, Camera.main
        /// </summary>
        [SerializeField]
        [Tooltip("If null, Camera.main")]
        protected Camera m_refCamera = null;

        /// <summary>
        /// OnEnable
        /// </summary>
        // ------------------------------------------------------------------------------------
        private void OnEnable()
        {
            
            if(!this.m_refCamera)
            {
                this.m_refCamera = Camera.main;
            }

        }

        /// <summary>
        /// Update
        /// </summary>
        // ------------------------------------------------------------------------------------
        private void Update()
        {
            
            if(!this.m_refCamera)
            {
                return;
            }

            // ------------------

            if(this.m_onlyAxisY)
            {

                this.transform.LookAt(new Vector3(
                    this.m_refCamera.transform.position.x,
                    this.transform.position.y,
                    this.m_refCamera.transform.position.z
                    ));

            }

            else
            {
                this.transform.LookAt(this.m_refCamera.transform);
            }

        }

    }

}
