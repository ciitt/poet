﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Poet
{

    /// <summary>
    /// UserProgressDataSignal class
    /// </summary>
    public class UserProgressDataSignal : SSC.IResetStateOnSceneLoaded
    {

        /// <summary>
        /// Add data function
        /// </summary>
        public Action<Transform, MonoBehaviour, string> addDataAction = null;

        /// <summary>
        /// Callback action
        /// </summary>
        public Func<Transform, MonoBehaviour, string> getDataFunc = null;

        /// <summary>
        /// Send create signal
        /// </summary>
        /// <param name="watcher">watcher</param>
        /// <param name="_addDataAction">addDataAction</param>
        // ---------------------------------------------------------------------------------------------------------------
        public void sendCreateSignal(SSC.StateWatcher<UserProgressDataSignal> watcher, Action<Transform, MonoBehaviour, string> _addDataAction)
        {

            this.addDataAction = _addDataAction;
            this.getDataFunc = null;

            watcher.sendState();

        }

        /// <summary>
        /// Reset
        /// </summary>
        // ---------------------------------------------------------------------------------------------------------------
        public void resetOnSceneLevelLoaded()
        {
            this.addDataAction = null;
            this.getDataFunc = null;
        }

    }

}