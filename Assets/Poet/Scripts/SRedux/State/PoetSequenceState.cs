﻿using System;
using System.Collections.Generic;

namespace Poet
{

    /// <summary>
    /// PoetSequenceState class
    /// </summary>
    public class PoetSequenceState : SSC.IResetStateOnSceneLoaded
    {

        /// <summary>
        /// enum state
        /// </summary>
        public enum StateEnum
        {

            Neutral,
            PreProcess,
            MainProcess,
            StartOperation,
            FinishOperation,

        }

        /// <summary>
        /// Current state
        /// </summary>
        public StateEnum stateEnum = StateEnum.Neutral;

        /// <summary>
        /// TextAssetName
        /// </summary>
        public string textAssetName = "";

        /// <summary>
        /// Csv row index
        /// </summary>
        public int index = -1;

        /// <summary>
        /// Header and sequence dictionary
        /// </summary>
        public Dictionary<string, string> headerAndCells = new Dictionary<string, string>();

        /// <summary>
        /// Header and sequence dictionary
        /// </summary>
        public List<string> finishOperationParameters = new List<string>();

        /// <summary>
        /// Set state
        /// </summary>
        /// <param name="watcher">watcher</param>
        /// <param name="_stateEnum">stateEnum</param>
        /// <param name="_textAssetName">textAssetName</param>
        /// <param name="_index">index</param>
        /// <param name="_headersAndCells">headersAndCells</param>
        // ---------------------------------------------------------------------------------------------------------------
        public void setState(
            SSC.StateWatcher<PoetSequenceState> watcher,
            StateEnum _stateEnum,
            string _textAssetName,
            int _index,
            Dictionary<string, string> _headerAndCells
            )
        {

            this.stateEnum = _stateEnum;

            this.textAssetName = _textAssetName;

            this.index = _index;

            this.headerAndCells.Clear();
            foreach (var kv in _headerAndCells)
            {
                this.headerAndCells.Add(kv.Key, kv.Value);
            }

            this.finishOperationParameters.Clear();

            watcher.sendState();

        }

        /// <summary>
        /// Set state
        /// </summary>
        /// <param name="watcher">watcher</param>
        /// <param name="_textAssetName">textAssetName</param>
        /// <param name="_index">index</param>
        /// <param name="_headersAndCells">headersAndCells</param>
        // ---------------------------------------------------------------------------------------------------------------
        public void setStateForStartOperation(
            SSC.StateWatcher<PoetSequenceState> watcher,
            string _textAssetName
            )
        {

            this.stateEnum = StateEnum.StartOperation;

            this.textAssetName = _textAssetName;

            this.index = -1;

            this.headerAndCells.Clear();
    
            this.finishOperationParameters.Clear();

            watcher.sendState();

        }

        /// <summary>
        /// Set state
        /// </summary>
        /// <param name="watcher">watcher</param>
        /// <param name="_textAssetName">textAssetName</param>
        /// <param name="_index">index</param>
        /// <param name="_headersAndCells">headersAndCells</param>
        // ---------------------------------------------------------------------------------------------------------------
        public void setStateForFinishOperation(
            SSC.StateWatcher<PoetSequenceState> watcher,
            string _textAssetName,
            int _index,
            Dictionary<string, string> _headerAndCells,
            List<string> _finishOperationParameters
            )
        {

            this.stateEnum = StateEnum.FinishOperation;

            this.textAssetName = _textAssetName;

            this.index = _index;

            this.headerAndCells.Clear();
            foreach (var kv in _headerAndCells)
            {
                this.headerAndCells.Add(kv.Key, kv.Value);
            }

            this.finishOperationParameters.Clear();
            this.finishOperationParameters.AddRange(_finishOperationParameters);

            watcher.sendState();

        }


        /// <summary>
        /// Reset
        /// </summary>
        // ---------------------------------------------------------------------------------------------------------------
        public void resetOnSceneLevelLoaded()
        {
            this.stateEnum = StateEnum.Neutral;
            this.textAssetName = "";
            this.index = -1;
            this.headerAndCells.Clear();
            this.finishOperationParameters.Clear();
        }

    }

}