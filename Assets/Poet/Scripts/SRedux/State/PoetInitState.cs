﻿using System;
using System.Collections.Generic;

namespace Poet
{

    /// <summary>
    /// PoetInitState class
    /// </summary>
    public class PoetInitState : SSC.IResetStateOnSceneLoaded
    {

        /// <summary>
        /// TextAssetName
        /// </summary>
        public string textAssetName = "";

        /// <summary>
        /// Csv row index
        /// </summary>
        public int index = 0;

        /// <summary>
        /// Header and sequence dictionary
        /// </summary>
        public Dictionary<string, string> headerAndCells = new Dictionary<string, string>();

        /// <summary>
        /// Set state
        /// </summary>
        /// <param name="watcher">watcher</param>
        /// <param name="_stateEnum">stateEnum</param>
        // ---------------------------------------------------------------------------------------------------------------
        public void setState(SSC.StateWatcher<PoetInitState> watcher, string _textAssetName, int _index, Dictionary<string, string> _headerAndCells)
        {

            this.textAssetName = _textAssetName;

            this.index = _index;

            this.headerAndCells.Clear();
            foreach (var kv in _headerAndCells)
            {
                this.headerAndCells.Add(kv.Key, kv.Value);
            }

            watcher.sendState();

        }

        /// <summary>
        /// Reset
        /// </summary>
        // ---------------------------------------------------------------------------------------------------------------
        public void resetOnSceneLevelLoaded()
        {
            this.textAssetName = "";
            this.index = 0;
            this.headerAndCells.Clear();
        }

    }

}