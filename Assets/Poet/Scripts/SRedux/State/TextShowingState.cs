﻿using System;
using System.Collections.Generic;

namespace Poet
{

    /// <summary>
    /// TextShowingState class
    /// </summary>
    public class TextShowingState : SSC.IResetStateOnSceneLoaded
    {

        /// <summary>
        /// enum state
        /// </summary>
        public enum StateEnum
        {

            ShowingTextStarted,
            ShowingTextFinished,

        }

        /// <summary>
        /// Current state
        /// </summary>
        public StateEnum stateEnum = StateEnum.ShowingTextFinished;

        /// <summary>
        /// Current text
        /// </summary>
        public string currentRawText = "";

        /// <summary>
        /// Set state
        /// </summary>
        /// <param name="watcher">watcher</param>
        /// <param name="_stateEnum">stateEnum</param>
        /// <param name="_currentText">currentText</param>
        // ---------------------------------------------------------------------------------------------------------------
        public void setState(SSC.StateWatcher<TextShowingState> watcher, StateEnum _stateEnum, string _currentRawText)
        {

            this.stateEnum = _stateEnum;
            this.currentRawText = _currentRawText;

            watcher.sendState();

        }

        /// <summary>
        /// Reset
        /// </summary>
        // ---------------------------------------------------------------------------------------------------------------
        public void resetOnSceneLevelLoaded()
        {
            this.stateEnum = StateEnum.ShowingTextFinished;
            this.currentRawText = "";
        }

    }

}