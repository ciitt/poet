﻿using System;
using System.Collections.Generic;

namespace Poet
{

    /// <summary>
    /// UiState class
    /// </summary>
    public class UiState : SSC.IResetStateOnSceneLoaded
    {

        /// <summary>
        /// enum state
        /// </summary>
        [System.Flags]
        public enum StateEnum
        {

            Normal = 1 << 0,
            AlbumMemory = 1 << 1,
            DeveloperDefine01 = 1 << 10,
            DeveloperDefine02 = 1 << 11,
            DeveloperDefine03 = 1 << 12,
            DeveloperDefine04 = 1 << 13,

        }

        /// <summary>
        /// Current state
        /// </summary>
        public StateEnum stateEnum = StateEnum.Normal;

        /// <summary>
        /// Set state
        /// </summary>
        /// <param name="watcher">watcher</param>
        /// <param name="_stateEnum">stateEnum</param>
        // ---------------------------------------------------------------------------------------------------------------
        public void setState(SSC.StateWatcher<UiState> watcher, StateEnum _stateEnum)
        {

            this.stateEnum = _stateEnum;

            watcher.sendState();

        }

        /// <summary>
        /// Reset
        /// </summary>
        // ---------------------------------------------------------------------------------------------------------------
        public void resetOnSceneLevelLoaded()
        {
            
        }

    }

}