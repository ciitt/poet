﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Poet
{

    /// <summary>
    /// ChangeLanguageSignal class
    /// </summary>
    public class ChangeLanguageSignal : SSC.IResetStateOnSceneLoaded
    {

#if UNITY_EDITOR

        /// <summary>
        /// Use SystemManager.Instance.systemLanguage
        /// </summary>
        [Obsolete("Use SystemManager.Instance.systemLanguage", true)]
        public SystemLanguage language = SystemLanguage.Unknown;

#endif

        /// <summary>
        /// Set state
        /// </summary>
        /// <param name="watcher">watcher</param>
        // ---------------------------------------------------------------------------------------------------------------
        public void setState(SSC.StateWatcher<ChangeLanguageSignal> watcher)
        {
            watcher.sendState();
        }

        /// <summary>
        /// Reset
        /// </summary>
        // ---------------------------------------------------------------------------------------------------------------
        public void resetOnSceneLevelLoaded()
        {

        }

    }

}