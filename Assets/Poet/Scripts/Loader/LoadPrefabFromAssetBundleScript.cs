﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poet
{

    public class LoadPrefabFromAssetBundleScript : LoadAssetFromAssetBundleScript
    {

        /// <summary>
        /// Load asset from AssetBundle
        /// </summary>
        /// <param name="objFromAssetBundle">asset from AssetBundle</param>
        // -------------------------------------------------------------------------------------------
        protected override void loadAssetFromAssetBundle(UnityEngine.Object objFromAssetBundle)
        {
            
            if(!objFromAssetBundle)
            {
                return;
            }

            // -------------------------

            GameObject gobj = Instantiate(objFromAssetBundle) as GameObject;

            if(gobj)
            {
                gobj.transform.SetParent(this.transform, false);
            }

        }

    }

}
