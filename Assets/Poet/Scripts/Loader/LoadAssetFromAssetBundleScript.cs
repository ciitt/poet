﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poet
{

    /// <summary>
    /// Load Asset from AssetBundle
    /// </summary>
    public abstract class LoadAssetFromAssetBundleScript : MonoBehaviour, AssetBundleNameInterface
    {

        /// <summary>
        /// AssetBundle name
        /// </summary>
        [SerializeField]
        [Tooltip("AssetBundle name")]
        protected string m_assetBundleName = "";

        /// <summary>
        /// AssetBundle variant
        /// </summary>
        [SerializeField]
        [Tooltip("AssetBundle variant")]
        protected string m_assetBundleVariant = "";

        /// <summary>
        /// Asset name in AssetBundle. Ignore if GetAllAssetNames().Count == 1
        /// </summary>
        [SerializeField]
        [Tooltip("Asset name in AssetBundle. This will be ignored if GetAllAssetNames().Count == 1")]
        protected string m_assetNameInAssetBundle = "";

        // -------------------------------------------------------------------------------------------

        /// <summary>
        /// Load asset from AssetBundle
        /// </summary>
        /// <param name="objFromAssetBundle">asset from AssetBundle</param>
        // -------------------------------------------------------------------------------------------
        protected abstract void loadAssetFromAssetBundle(UnityEngine.Object objFromAssetBundle);

        // -------------------------------------------------------------------------------------------

        /// <summary>
        /// AssetBundle name
        /// </summary>
        public string AssetBundleName { get { return this.m_assetBundleName; } set { this.m_assetBundleName = value; } }

        /// <summary>
        /// AssetBundle variant
        /// </summary>
        public string AssetBundleVariant { get { return this.m_assetBundleVariant; } set { this.m_assetBundleVariant = value; } }

        /// <summary>
        /// Asset name in AssetBundle
        /// </summary>
        public string AssetNameInAssetBundle { get { return this.m_assetNameInAssetBundle; } set { this.m_assetNameInAssetBundle = value; } }

        // -------------------------------------------------------------------------------------------

        /// <summary>
        /// Start
        /// </summary>
        // -------------------------------------------------------------------------------------------
        protected virtual void Start()
        {

#if UNITY_EDITOR

            if (string.IsNullOrEmpty(this.m_assetBundleName))
            {
                Debug.LogWarning("(#if UNITY_EDITOR) m_assetBundleName is empty : " + this.gameObject.name);
            }

#endif

            PoetAssetBundleStartupManager.Instance.addSceneStartupAssetBundle(
                this.m_assetBundleName,
                this.m_assetBundleVariant,
                this.onSuccessAssetBundleLoading,
                null,
                null,
                null
                );

        }

        /// <summary>
        /// Success loading AssetBundle function
        /// </summary>
        // -------------------------------------------------------------------------------------------
        protected void onSuccessAssetBundleLoading(AssetBundle ab, System.Object info, Action finishCallback)
        {
            StartCoroutine(this.onSuccessAssetBundleLoadingIE(ab, info, finishCallback));
        }

        /// <summary>
        /// Success loading AssetBundle function
        /// </summary>
        // -------------------------------------------------------------------------------------------
        protected virtual IEnumerator onSuccessAssetBundleLoadingIE(AssetBundle ab, System.Object info, Action finishCallback)
        {

            yield return null;

            if (ab && !ab.isStreamedSceneAssetBundle)
            {

                string[] names = ab.GetAllAssetNames();

                AssetBundleRequest abr = null;
                
                if (string.IsNullOrEmpty(this.m_assetNameInAssetBundle) && names.Length > 0)
                {
                    abr = ab.LoadAssetAsync(names[0]);
                }

                else
                {
                    abr = ab.LoadAssetAsync(this.m_assetNameInAssetBundle);
                }

                while (!abr.isDone)
                {
                    yield return null;
                }

                this.loadAssetFromAssetBundle(abr.asset);

            }

            finishCallback();

        }

    }

}
