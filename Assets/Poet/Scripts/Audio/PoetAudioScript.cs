﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poet
{

    /// <summary>
    /// Poet Audio
    /// </summary>
    [RequireComponent(typeof(AudioSource))]
    public abstract class PoetAudioScript : PoetBaseReceiverScript
    {

        protected class AssetBundleNameAndAssetName
        {

            public string assetBundleName = "";
            public string assetName = "";
            public string key = "";

            public AssetBundleNameAndAssetName(string _assetBundleName, string _assetName, string _key)
            {
                this.assetBundleName = _assetBundleName;
                this.assetName = _assetName;
                this.key = _key;
            }

        }

        /// <summary>
        /// User progress data
        /// </summary>
        [Serializable]
        protected class UserProgressData
        {

            public float invokeTime = 0.0f;

            public string preloadedKey = "";
            public string assetBundleKey = "";
            public string resourceKey = "";

            public void clear()
            {

                this.invokeTime = 0.0f;

                this.preloadedKey = "";
                this.assetBundleKey = "";
                this.resourceKey = "";

            }

            public void setPreloadedKey(string key)
            {
                this.preloadedKey = key;
                this.assetBundleKey = "";
                this.resourceKey = "";
            }

            public void setAssetBundleKey(string key)
            {
                this.preloadedKey = "";
                this.assetBundleKey = key;
                this.resourceKey = "";
            }

            public void setResourceKey(string key)
            {
                this.preloadedKey = "";
                this.assetBundleKey = "";
                this.resourceKey = key;
            }

        }

        /// <summary>
        /// Reference to AudioSource
        /// </summary>
        protected AudioSource m_refAudioSource;

        /// <summary>
        /// Default fadeout seconds
        /// </summary>
        [SerializeField]
        [Tooltip("Default fadeout seconds")]
        protected float m_defaultFadeOutSeconds = 0.0f;

        /// <summary>
        /// Default fadein seconds
        /// </summary>
        [SerializeField]
        [Tooltip("Default fadein seconds")]
        protected float m_defaultFadeInSeconds = 0.0f;

        /// <summary>
        /// Preloaded audio
        /// </summary>
        [SerializeField]
        [Tooltip("Preloaded audio")]
        protected List<IdAndAudioClip> m_preloadedAudioList = new List<IdAndAudioClip>();

        /// <summary>
        /// Preloaded audio dictionary
        /// </summary>
        protected Dictionary<string, IdAndAudioClip> m_preloadedAudioDict = new Dictionary<string, IdAndAudioClip>();

        /// <summary>
        /// AssetBundle audio dictionary
        /// </summary>
        protected Dictionary<string, AudioClip> m_assetBundleAudioDict = new Dictionary<string, AudioClip>();

        /// <summary>
        /// Resource audio dictionary
        /// </summary>
        protected Dictionary<string, AudioClip> m_resourceAudioDict = new Dictionary<string, AudioClip>();

        /// <summary>
        /// IEnumerator for playAudioIE
        /// </summary>
        protected IEnumerator m_playAudioIE = null;

        /// <summary>
        /// User progress data
        /// </summary>
        protected UserProgressData m_userProgressData = new UserProgressData();

        // ------------------------------------------------------------------------------------------

        /// <summary>
        /// Csv header
        /// </summary>
        /// <returns>header</returns>
        protected abstract string csvHeader();

        /// <summary>
        /// Reference to AudioSource
        /// </summary>
        public AudioSource refAudioSource { get { return this.m_refAudioSource; } }

        // ------------------------------------------------------------------------------------------

        /// <summary>
        /// initOnAwake() is called after Awake().
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected override void initOnAwake()
        {

            // m_refAudioSource
            {
                this.m_refAudioSource = this.GetComponent<AudioSource>();
            }

            // m_preloadedAudioDict
            {
                this.m_preloadedAudioDict = Funcs.listToDictionary<IdAndAudioClip>(this.m_preloadedAudioList);
            }

        }

        /// <summary>
        /// Get AudioClip from preloaded dictionary
        /// </summary>
        /// <param name="identifier">identifier</param>
        /// <returns>AudioClip</returns>
        // ------------------------------------------------------------------------------------------
        public AudioClip getPreloadedAudioClip(string identifier)
        {

            if(this.m_preloadedAudioDict.ContainsKey(identifier))
            {
                return this.m_preloadedAudioDict[identifier].audioClip;
            }

            return null;

        }

        /// <summary>
        /// Get AudioClip from AssetBundle dictionary
        /// </summary>
        /// <param name="identifier">identifier</param>
        /// <returns>AudioClip</returns>
        // ------------------------------------------------------------------------------------------
        public AudioClip getAssetBundleAudioClip(string identifier)
        {

            if (this.m_assetBundleAudioDict.ContainsKey(identifier))
            {
                return this.m_assetBundleAudioDict[identifier];
            }

            return null;

        }

        /// <summary>
        /// Get AudioClip from resource dictionary
        /// </summary>
        /// <param name="identifier">identifier</param>
        /// <returns>AudioClip</returns>
        // ------------------------------------------------------------------------------------------
        public AudioClip getResourceAudioClip(string identifier)
        {

            if (this.m_resourceAudioDict.ContainsKey(identifier))
            {
                return this.m_resourceAudioDict[identifier];
            }

            return null;

        }

        /// <summary>
        /// Solve functions
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected void solveFunctions(FuncNameAndParamsList fnapList)
        {

            if (fnapList == null)
            {
                return;
            }

            // -------------------

            string temp0 = "";
            string temp1 = "";

            foreach (var fnap in fnapList.list)
            {

                temp0 = fnap.getStringParameter(0);
                temp1 = fnap.getStringParameter(1);

                if (fnap.functionName == "Stop")
                {
                    this.stop(fnapList.invokeTime);
                    break;
                }

                else if (fnap.functionName == "PlayAB")
                {
                    this.playAssetBundleAudio(this.createKeyForAssetBundle(temp0, ""), fnapList.invokeTime, true);
                    break;
                }

                else if (fnap.functionName == "PlayABWithAssetName")
                {
                    this.playAssetBundleAudio(this.createKeyForAssetBundle(temp0, temp1), fnapList.invokeTime, true);
                    break;
                }

                else if (fnap.functionName == "PlayResource")
                {
                    this.playResourceAudio(temp0, fnapList.invokeTime, true);
                    break;
                }

                else if (fnap.functionName == "PlayID")
                {
                    this.playPreloadedAudio(temp0, fnapList.invokeTime, true);
                    break;
                }

            } // foreach

        }

        /// <summary>
        /// Stop audio
        /// </summary>
        /// <param name="invokeTime">invoke time</param>
        // -----------------------------------------------------------------------------------------------------------
        public void stop(float invokeTime)
        {
            this.m_userProgressData.clear();
            this.playAudio(null, invokeTime);
        }

        /// <summary>
        /// Stop audio
        /// </summary>
        /// <param name="invokeTime">invoke time</param>
        /// <param name="fadeOutSeconds">fade out seconds</param>
        // -----------------------------------------------------------------------------------------------------------
        public void stop(float invokeTime, float fadeOutSeconds)
        {
            this.m_userProgressData.clear();
            this.playAudio(null, fadeOutSeconds, 0.0f, invokeTime);
        }

        /// <summary>
        /// Play AssetBundle audio
        /// </summary>
        /// <param name="assetBundleKey">AssetBundle key</param>
        /// <param name="invokeTime">invoke time</param>
        /// <param name="saveToUserProgressData">save</param>
        // -----------------------------------------------------------------------------------------------------------
        public void playAssetBundleAudio(string assetBundleKey, float invokeTime, bool saveToUserProgressData)
        {
            this.playAssetBundleAudio(assetBundleKey, this.m_defaultFadeOutSeconds, this.m_defaultFadeInSeconds, invokeTime, saveToUserProgressData);
        }

        /// <summary>
        /// Play AssetBundle audio
        /// </summary>
        /// <param name="assetBundleKey">AssetBundle key</param>
        /// <param name="fadeOutSeconds">fade out seconds</param>
        /// <param name="fadeInSeconds">fade in seconds</param>
        /// <param name="invokeTime">invoke time</param>
        /// <param name="saveToUserProgressData">save</param>
        // -----------------------------------------------------------------------------------------------------------
        public void playAssetBundleAudio(string assetBundleKey, float fadeOutSeconds, float fadeInSeconds, float invokeTime, bool saveToUserProgressData)
        {

            if(saveToUserProgressData)
            {
                this.m_userProgressData.invokeTime = invokeTime;
                this.m_userProgressData.setAssetBundleKey(assetBundleKey);
            }

            AudioClip audioClip =
                this.m_assetBundleAudioDict.ContainsKey(assetBundleKey) ?
                this.m_assetBundleAudioDict[assetBundleKey] :
                null
                ;

            this.playAudio(audioClip, fadeOutSeconds, fadeInSeconds, invokeTime);

        }

        /// <summary>
        /// Play AssetBundle audio
        /// </summary>
        /// <param name="resourceKey">resource key</param>
        /// <param name="invokeTime">invoke time</param>
        /// <param name="saveToUserProgressData">save</param>
        // -----------------------------------------------------------------------------------------------------------
        public void playResourceAudio(string resourceKey, float invokeTime, bool saveToUserProgressData)
        {
            this.playResourceAudio(resourceKey, this.m_defaultFadeOutSeconds, this.m_defaultFadeInSeconds, invokeTime, saveToUserProgressData);
        }

        /// <summary>
        /// Play AssetBundle audio
        /// </summary>
        /// <param name="resourceKey">resource key</param>
        /// <param name="fadeOutSeconds">fade out seconds</param>
        /// <param name="fadeInSeconds">fade in seconds</param>
        /// <param name="invokeTime">invoke time</param>
        /// <param name="saveToUserProgressData">save</param>
        // -----------------------------------------------------------------------------------------------------------
        public void playResourceAudio(string resourceKey, float fadeOutSeconds, float fadeInSeconds, float invokeTime, bool saveToUserProgressData)
        {

            if (saveToUserProgressData)
            {
                this.m_userProgressData.invokeTime = invokeTime;
                this.m_userProgressData.setResourceKey(resourceKey);
            }

            AudioClip audioClip =
                this.m_resourceAudioDict.ContainsKey(resourceKey) ?
                this.m_resourceAudioDict[resourceKey] :
                null
                ;

            this.playAudio(audioClip, fadeOutSeconds, fadeInSeconds, invokeTime);

        }

        /// <summary>
        /// Play AssetBundle audio
        /// </summary>
        /// <param name="preloadedId">preloaded id</param>
        /// <param name="invokeTime">invoke time</param>
        /// <param name="saveToUserProgressData">save</param>
        // -----------------------------------------------------------------------------------------------------------
        public void playPreloadedAudio(string preloadedId, float invokeTime, bool saveToUserProgressData)
        {
            this.playPreloadedAudio(preloadedId, this.m_defaultFadeOutSeconds, this.m_defaultFadeInSeconds, invokeTime, saveToUserProgressData);
        }

        /// <summary>
        /// Play AssetBundle audio
        /// </summary>
        /// <param name="preloadedId">preloaded id</param>
        /// <param name="fadeOutSeconds">fade out seconds</param>
        /// <param name="fadeInSeconds">fade in seconds</param>
        /// <param name="invokeTime">invoke time</param>
        /// <param name="saveToUserProgressData">save</param>
        // -----------------------------------------------------------------------------------------------------------
        public void playPreloadedAudio(string preloadedId, float fadeOutSeconds, float fadeInSeconds, float invokeTime, bool saveToUserProgressData)
        {

            if (saveToUserProgressData)
            {
                this.m_userProgressData.invokeTime = invokeTime;
                this.m_userProgressData.setPreloadedKey(preloadedId);
            }

            AudioClip audioClip =
                this.m_preloadedAudioDict.ContainsKey(preloadedId) ?
                this.m_preloadedAudioDict[preloadedId].audioClip :
                null
                ;

            this.playAudio(audioClip, fadeOutSeconds, fadeInSeconds, invokeTime);

        }

        /// <summary>
        /// Play AudioClip
        /// </summary>
        /// <param name="audioClip">AudioClip</param>
        /// <param name="invokeTime">invoke time</param>
        // -----------------------------------------------------------------------------------------------------------
        public virtual void playAudio(AudioClip audioClip, float invokeTime)
        {
            this.playAudio(audioClip, this.m_defaultFadeOutSeconds, this.m_defaultFadeInSeconds, invokeTime);
        }

        /// <summary>
        /// Play AudioClip
        /// </summary>
        /// <param name="audioClip">AudioClip</param>
        /// <param name="fadeOutSeconds">fade out seconds</param>
        /// <param name="fadeInSeconds">fade in seconds</param>
        /// <param name="invokeTime">invoke time</param>
        /// <param name="resumeTime">resume time</param>
        // -----------------------------------------------------------------------------------------------------------
        public virtual void playAudio(AudioClip audioClip, float fadeOutSeconds, float fadeInSeconds, float invokeTime)
        {

            if (audioClip && audioClip == this.m_refAudioSource.clip && this.m_refAudioSource.loop)
            {
                return;
            }

            if (
                audioClip &&
                PoetOneSceneManager.isAvailable() &&
                PoetOneSceneManager.Instance.shouldSkip() &&
                !this.m_refAudioSource.loop &&
                PoetCommonUiManager.Instance.containsIdentifier(PoetUiIdentifiers.Common.Text)
                )
            {
                return;
            }

            // -------------

            // StopCoroutine
            {
                if (this.m_playAudioIE != null)
                {
                    StopCoroutine(this.m_playAudioIE);
                }
            }

            StartCoroutine(this.m_playAudioIE = this.playAudioIE(audioClip, fadeOutSeconds, fadeInSeconds, invokeTime));

        }

        /// <summary>
        /// Play audio IEnumerator
        /// </summary>
        /// <param name="audioClip">AudioClip</param>
        /// <param name="fadeOutSeconds">fade out seconds</param>
        /// <param name="fadeInSeconds"fade in seconds></param>
        /// <param name="invokeTime">invoke time</param>
        /// <returns></returns>
        // ------------------------------------------------------------------------------------------
        protected IEnumerator playAudioIE(
            AudioClip audioClip,
            float fadeOutSeconds,
            float fadeInSeconds,
            float invokeTime
            )
        {

            float timer = 0.0f;

            // wait for invoke
            {
                yield return this.waitInvokeTime(invokeTime, 0.0f, null);
            }

            // fade out
            {

                timer = 0.0f;

                float startVolume = this.m_refAudioSource.volume;

                if (this.m_refAudioSource.isPlaying)
                {

                    while (timer < fadeOutSeconds)
                    {

                        // break
                        {
                            if (PoetOneSceneManager.isAvailable() && PoetOneSceneManager.Instance.shouldSkip())
                            {
                                break;
                            }
                        }

                        // volume
                        {
                            this.m_refAudioSource.volume = Mathf.Lerp(startVolume, 0.0f, timer / fadeOutSeconds);
                        }

                        // timer
                        {
                            timer += Time.deltaTime;
                        }

                        yield return null;

                    }

                }

                // finish
                {
                    this.m_refAudioSource.volume = 0.0f;
                    this.m_refAudioSource.Stop();
                }

            }

            // set
            {
                yield return null;
                this.m_refAudioSource.clip = audioClip;
            }

            // fade in
            {

                timer = 0.0f;

                if(this.m_refAudioSource.clip)
                {

                    this.m_refAudioSource.Play();

                    while (timer < fadeInSeconds)
                    {

                        // break
                        {
                            if (PoetOneSceneManager.isAvailable() && PoetOneSceneManager.Instance.shouldSkip())
                            {
                                break;
                            }
                        }

                        // volume
                        {

                            this.m_refAudioSource.volume = Mathf.Lerp(
                                0.0f,
                                this.targetVolume(),
                                timer / fadeInSeconds
                                );

                        }

                        // timer
                        {
                            timer += Time.deltaTime;
                        }

                        yield return null;

                    }

                }

                this.m_refAudioSource.volume = this.targetVolume();

            }

            this.m_playAudioIE = null;

        }

        /// <summary>
        /// Get target volume
        /// </summary>
        /// <returns>volume</returns>
        // -----------------------------------------------------------------------------------------------------------
        protected virtual float targetVolume()
        {
            return PoetAudioManager.Instance.targetVolume(this);
        }

        /// <summary>
        /// Create dictionary key for AssetBundle
        /// </summary>
        /// <param name="assetBundleName">assetBundleName</param>
        /// <param name="aasetNameInAssetBundle">aasetNameInAssetBundle</param>
        /// <returns>key</returns>
        // ------------------------------------------------------------------------------------------
        protected string createKeyForAssetBundle(string assetBundleName, string aasetNameInAssetBundle)
        {
            return assetBundleName + aasetNameInAssetBundle;
        }

        /// <summary>
        /// PoetSequenceState receiver
        /// </summary>
        /// <param name="psState">PoetSequenceState</param>
        // ------------------------------------------------------------------------------------------
        protected override void onPoetSequenceState(PoetSequenceState psState)
        {

            if (psState.stateEnum == PoetSequenceState.StateEnum.PreProcess)
            {

                if (!this.m_refAudioSource.loop)
                {
                    this.m_userProgressData.clear();
                }

                if (psState.headerAndCells.ContainsKey(this.csvHeader()))
                {

                    FuncNameAndParamsList fnapList = CsvParser.parseFunctionsInCell(psState.headerAndCells[this.csvHeader()]);

                    if (fnapList.preProcess)
                    {
                        this.solveFunctions(fnapList);
                    }

                }

            }

            else if (psState.stateEnum == PoetSequenceState.StateEnum.MainProcess)
            {

                if (psState.headerAndCells.ContainsKey(this.csvHeader()))
                {

                    FuncNameAndParamsList fnapList = CsvParser.parseFunctionsInCell(psState.headerAndCells[this.csvHeader()]);

                    if (!fnapList.preProcess)
                    {
                        this.solveFunctions(fnapList);
                    }

                }
            }

        }

        /// <summary>
        /// PoetInitState receiver
        /// </summary>
        /// <param name="piState">PoetInitState</param>
        // ------------------------------------------------------------------------------------------
        protected override void onPoetInitState(PoetInitState piState)
        {

            this.initAssetBundleAndResouces(piState);

#if UNITY_EDITOR

            if (piState.headerAndCells.ContainsKey(this.csvHeader()))
            {

                FuncNameAndParamsList fnapList = CsvParser.parseFunctionsInCell(piState.headerAndCells[this.csvHeader()]);

                foreach (var val in fnapList.list)
                {

                    if (
                        val.functionName != "PlayAB" &&
                        val.functionName != "PlayABWithAssetName" &&
                        val.functionName != "PlayResource" &&
                        val.functionName != "PlayID" &&
                        val.functionName != "Stop"
                        )
                    {

                        Debug.LogWarning(
                            "Unknown function name : " +
                            val.functionName + " : " +
                            this.csvHeader() + " : " +
                            piState.textAssetName + " : " +
                            piState.index
                            );

                    }

                }

            }

#endif

        }

        /// <summary>
        /// TextShowingState receiver
        /// </summary>
        /// <param name="tsState">TextShowingState</param>
        // ------------------------------------------------------------------------------------------
        protected override void onTextShowingState(TextShowingState tsState)
        {

        }

        /// <summary>
        /// UserProgressDataSignal receiver
        /// </summary>
        /// <param name="updSignal">UserProgressDataSignal</param>
        // ------------------------------------------------------------------------------------------
        protected override void onUserProgressDataSignal(UserProgressDataSignal updSignal)
        {
            updSignal.addDataAction(this.transform, this, JsonUtility.ToJson(this.m_userProgressData));
        }

        /// <summary>
        /// SceneChangeState receiver
        /// </summary>
        /// <param name="scState">SceneChangeState</param>
        // ------------------------------------------------------------------------------------------
        protected override void onSceneChangeStateReceiver(SSC.SceneChangeState scState)
        {

            if (scState.stateEnum == SSC.SceneChangeState.StateEnum.NowLoadingMain)
            {
                this.clearAudio();
                this.m_userProgressData.clear();
            }

            else if (scState.stateEnum == SSC.SceneChangeState.StateEnum.AllStartupsDone)
            {

                PoetSceneChangeManager pscm = PoetSceneChangeManager.Instance as PoetSceneChangeManager;

                if (pscm.isLoadingCurrentSceneWithUserProgressData())
                {
                    this.m_userProgressData = pscm.getDataFromCurrentUserProgressData<UserProgressData>(this.transform, this);
                }

            }

            else if (scState.stateEnum == SSC.SceneChangeState.StateEnum.ScenePlaying)
            {

                PoetSceneChangeManager pscm = PoetSceneChangeManager.Instance as PoetSceneChangeManager;

                if (pscm.isLoadingCurrentSceneWithUserProgressData())
                {

                    // m_userProgressData
                    {

                        if (!string.IsNullOrEmpty(this.m_userProgressData.preloadedKey))
                        {
                            this.playPreloadedAudio(this.m_userProgressData.preloadedKey, this.m_userProgressData.invokeTime, false);
                        }

                        else if (!string.IsNullOrEmpty(this.m_userProgressData.assetBundleKey))
                        {
                            this.playAssetBundleAudio(this.m_userProgressData.assetBundleKey, this.m_userProgressData.invokeTime, false);
                        }

                        else if (!string.IsNullOrEmpty(this.m_userProgressData.resourceKey))
                        {
                            this.playResourceAudio(this.m_userProgressData.resourceKey, this.m_userProgressData.invokeTime, false);
                        }

                    }

                }

            }

        }

        /// <summary>
        /// PauseState receiver
        /// </summary>
        /// <param name="pState">PauseState</param>
        // ------------------------------------------------------------------------------------------
        protected override void onPauseStateReceiver(SSC.PauseState pState)
        {

        }

        /// <summary>
        /// Clear audio
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected void clearAudio()
        {

            foreach (var kv in this.m_assetBundleAudioDict)
            {
                if (kv.Value)
                {
                    kv.Value.UnloadAudioData();
                }
            }

            foreach (var kv in this.m_resourceAudioDict)
            {
                if (kv.Value)
                {
                    kv.Value.UnloadAudioData();
                }
            }

            this.m_assetBundleAudioDict.Clear();
            this.m_resourceAudioDict.Clear();

        }

        /// <summary>
        /// Init and add AssetBundle names
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected void initAssetBundleAndResouces(PoetInitState piState)
        {

            string header = this.csvHeader();

            if (piState.headerAndCells.ContainsKey(header))
            {

                string temp0 = "";
                string temp1 = "";

                var fnapList = CsvParser.parseFunctionsInCell(piState.headerAndCells[header]);

                foreach (var fnap in fnapList.list)
                {

                    if (fnap.functionName == "PlayAB")
                    {

                        temp0 = fnap.getStringParameter(0);

                        string key = this.createKeyForAssetBundle(temp0, "");

                        if (!string.IsNullOrEmpty(key) && !this.m_assetBundleAudioDict.ContainsKey(key))
                        {

                            this.m_assetBundleAudioDict.Add(key, null);

                            PoetAssetBundleStartupManager.Instance.addSceneStartupAssetBundle(
                                key,
                                "",
                                this.onSuccessPlayAB,
                                null,
                                null,
                                key
                                );

                        }

                    }

                    else if (fnap.functionName == "PlayABWithAssetName")
                    {

                        temp0 = fnap.getStringParameter(0);
                        temp1 = fnap.getStringParameter(1);

                        string key = this.createKeyForAssetBundle(temp0, temp1);

                        if (!string.IsNullOrEmpty(key) && !this.m_assetBundleAudioDict.ContainsKey(key))
                        {

                            this.m_assetBundleAudioDict.Add(key, null);

                            AssetBundleNameAndAssetName temp = new AssetBundleNameAndAssetName(temp0, temp1, key);

                            PoetAssetBundleStartupManager.Instance.addSceneStartupAssetBundle(
                                temp.assetBundleName,
                                "",
                                this.onSuccessPlayABWithAssetName,
                                null,
                                null,
                                temp
                                );

                        }

                    }

                    else if (fnap.functionName == "PlayResource")
                    {

                        temp0 = fnap.getStringParameter(0);

                        if (!this.m_resourceAudioDict.ContainsKey(temp0))
                        {
                            this.m_resourceAudioDict.Add(temp0, null);
                            PoetIEnumeratorStartupManager.Instance.addSceneStartupIEnumerator(this.loadResource(temp0), null, null, SSC.IEnumeratorStartupManager.BeforeAfter.Before);
                        }

                    }

                }

            }

        }

        /// <summary>
        /// Load resource audio
        /// </summary>
        /// <param name="param">path</param>
        /// <returns>IEnumerator</returns>
        // ------------------------------------------------------------------------------------------
        protected IEnumerator loadResource(string param)
        {

            var request = Resources.LoadAsync(param, typeof(AudioClip));

            if (request == null)
            {
                yield break;
            }

            // ------------------

            while (!request.isDone)
            {
                yield return null;
            }

            if (this.m_resourceAudioDict.ContainsKey(param))
            {

                if (request.asset is AudioClip)
                {
                    this.m_resourceAudioDict[param] = request.asset as AudioClip;
                }

#if UNITY_EDITOR

                else if(!request.asset)
                {
                    Debug.LogWarning("(#if UNITY_EDITOR) Resource not found : " + param);
                }

                else
                {
                    Debug.LogWarning("(#if UNITY_EDITOR) Resource is not AudioClip : " + param);
                }

#endif

            }

        }

        /// <summary>
        /// Success for PlayAB
        /// </summary>
        /// <param name="ab">AssetBundle</param>
        /// <param name="info">info</param>
        /// <param name="finishCallback">callback to finish</param>
        // ------------------------------------------------------------------------------------------
        protected void onSuccessPlayAB(AssetBundle ab, System.Object info, Action finishCallback)
        {
            StartCoroutine(this.onSuccessPlayABIE(ab, info, finishCallback));
        }

        /// <summary>
        /// Success for PlayAB
        /// </summary>
        /// <param name="ab"></param>
        /// <param name="info"></param>
        /// <param name="finishCallback">callback to finish</param>
        // ------------------------------------------------------------------------------------------
        protected IEnumerator onSuccessPlayABIE(AssetBundle ab, System.Object info, Action finishCallback)
        {

            yield return null;

            string key = info as string;

            if (ab && !string.IsNullOrEmpty(key) && this.m_assetBundleAudioDict.ContainsKey(key))
            {

                if (this.m_assetBundleAudioDict[key])
                {
                    Destroy(this.m_assetBundleAudioDict[key]);
                }

                // ----------------

                string[] temps = ab.GetAllAssetNames();

                if (temps.Length == 1)
                {

                    AssetBundleRequest request = ab.LoadAssetAsync(temps[0]);

                    while (!request.isDone)
                    {
                        yield return null;
                    }

                    this.m_assetBundleAudioDict[key] = request.asset as AudioClip;

                }

#if UNITY_EDITOR

                if (temps.Length >= 2)
                {
                    Debug.LogWarning("(#if UNITY_EDITOR) Multiple assets in one AssetBundle is not supported in PlayAB");
                }

                if (!this.m_assetBundleAudioDict[key])
                {
                    Debug.LogWarning("(#if UNITY_EDITOR) Loading AudioClip failed " + key);
                }

#endif

            }

            else
            {
                Debug.LogError("Implementation Error : onSuccessPlayABIE");
            }

            finishCallback();

        }

        /// <summary>
        /// Success for PlayABWithAssetName
        /// </summary>
        /// <param name="ab"></param>
        /// <param name="info"></param>
        /// <param name="finishCallback">callback to finish</param>
        // ------------------------------------------------------------------------------------------
        protected void onSuccessPlayABWithAssetName(AssetBundle ab, System.Object info, Action finishCallback)
        {
            StartCoroutine(this.onSuccessPlayABWithAssetNameIE(ab, info, finishCallback));
        }

        /// <summary>
        /// Success for PlayABWithAssetName
        /// </summary>
        /// <param name="ab"></param>
        /// <param name="info"></param>
        /// <param name="finishCallback">callback to finish</param>
        // ------------------------------------------------------------------------------------------
        protected IEnumerator onSuccessPlayABWithAssetNameIE(AssetBundle ab, System.Object info, Action finishCallback)
        {

            yield return null;

            AssetBundleNameAndAssetName abnaan = info as AssetBundleNameAndAssetName;

            string key = (abnaan != null) ? abnaan.key : "";

            if (ab && !string.IsNullOrEmpty(key) && this.m_assetBundleAudioDict.ContainsKey(key))
            {

                AssetBundleRequest request = ab.LoadAssetAsync(abnaan.assetName);

                if (request != null)
                {

                    while (!request.isDone)
                    {
                        yield return null;
                    }

                    this.m_assetBundleAudioDict[key] = request.asset as AudioClip;

#if UNITY_EDITOR

                    if (!this.m_assetBundleAudioDict[key])
                    {
                        Debug.LogWarning("(#if UNITY_EDITOR) Loading AudioClip failed " + key);
                    }

#endif


                }

            }

            else
            {
                Debug.LogError("Implementation Error : PAS.oSPABWAN");
            }

            finishCallback();

        }

        /// <summary>
        /// Set volume
        /// </summary>
        /// <param name="volume">volume</param>
        // -----------------------------------------------------------------------------------------------------------
        public void setVolume(float volume)
        {

            if(this.m_playAudioIE == null)
            {
                this.m_refAudioSource.volume = volume;
            }

        }

        /// <summary>
        /// Is playing
        /// </summary>
        /// <returns>playing</returns>
        // -----------------------------------------------------------------------------------------------------------
        public bool isPlaying()
        {
            return this.m_refAudioSource.isPlaying;
        }

    }

}
