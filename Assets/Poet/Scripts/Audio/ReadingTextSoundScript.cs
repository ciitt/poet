﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poet
{

    /// <summary>
    /// Reading text sound
    /// </summary>
    [RequireComponent(typeof(AudioSource))]
    public class ReadingTextSoundScript : MonoBehaviour
    {

        /// <summary>
        /// Reference to AudioSource
        /// </summary>
        protected AudioSource m_refAudioSource;

        /// <summary>
        /// Is this active
        /// </summary>
        [SerializeField]
        [Tooltip("Is this active")]
        protected bool m_isThisActive = false;

        /// <summary>
        /// Don't use this feature if WebGL
        /// </summary>
        [SerializeField]
        [Tooltip("Don't use this feature if WebGL")]
        protected bool m_dontUseIfWebGL = true;

        // ------------------------------------------------------------------------------------------

        /// <summary>
        /// Reference to AudioSource
        /// </summary>
        public AudioSource refAudioSource { get { return this.m_refAudioSource; } }


        /// <summary>
        /// Is this active
        /// </summary>
        public bool isThisActive { get { return this.m_isThisActive; } }

        // ------------------------------------------------------------------------------------------

        /// <summary>
        /// Awake
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected virtual void Awake()
        {

            // m_refAudioSource
            {
                this.m_refAudioSource = this.GetComponent<AudioSource>();
            }

            // addTextFinishedStateReceiver
            {
                PoetReduxManager prm = PoetReduxManager.Instance as PoetReduxManager;
                prm.addTextFinishedStateReceiver(this.onTextShowingState);
            }


#if UNITY_EDITOR

            if (!this.m_refAudioSource.clip)
            {
                Debug.LogWarning("AudioSource.clip is null : " + this.gameObject.name);
            }

#endif

#if UNITY_WEBGL
            
            if(this.m_dontUseIfWebGL)
            {
                this.m_isThisActive = false;
            }
            
#endif


        }

        /// <summary>
        /// Set loop
        /// </summary>
        /// <param name="value">value</param>
        // ------------------------------------------------------------------------------------------
        public void setLoop(bool value)
        {

            this.m_refAudioSource.loop = value;

            if(this.m_isThisActive && value && !this.m_refAudioSource.isPlaying)
            {
                this.m_refAudioSource.Play();
            }

        }

        /// <summary>
        /// Set active
        /// </summary>
        /// <param name="value">active</param>
        // ------------------------------------------------------------------------------------------
        public void setReadingTextSoundActive(bool value)
        {

#if UNITY_WEBGL
            
            if(this.m_dontUseIfWebGL)
            {
                this.m_isThisActive = false;
                return;
            }
            
#endif

            // --------------------------

            this.m_isThisActive = value;

            if (!value)
            {
                this.m_refAudioSource.Stop();
            }

        }

        /// <summary>
        /// TextShowingState receiver
        /// </summary>
        /// <param name="tsState">TextShowingState</param>
        // ------------------------------------------------------------------------------------------
        protected virtual void onTextShowingState(TextShowingState tsState)
        {

            if (tsState.stateEnum == TextShowingState.StateEnum.ShowingTextStarted)
            {

                if(PoetOneSceneManager.isAvailable() && !PoetOneSceneManager.Instance.shouldSkip())
                {

                    if (this.m_isThisActive && tsState.currentRawText.Length > 0)
                    {
                        this.m_refAudioSource.Play();
                    }

                }

            }

            else if(tsState.stateEnum == TextShowingState.StateEnum.ShowingTextFinished)
            {
                this.m_refAudioSource.loop = false;
            }

        }

    }

}
