﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poet
{

    /// <summary>
    /// Switch reading text sound
    /// </summary>
    public class SwitchReadingTextSoundScript : MonoBehaviour
    {

        /// <summary>
        /// Activate reading text sound at Start
        /// </summary>
        [SerializeField]
        [Tooltip("Activate or deactivate reading text sound at Start")]
        bool m_value = false;

        /// <summary>
        /// Original value
        /// </summary>
        bool m_oriValue = false;

        /// <summary>
        /// Start
        /// </summary>
        // --------------------------------------------------------------------------------------
        void Start()
        {
            this.m_oriValue = PoetAudioManager.Instance.isReadingTextSoundActive();
            PoetAudioManager.Instance.setReadingTextSoundActive(this.m_value);
        }

        /// <summary>
        /// OnDestroy
        /// </summary>
        // --------------------------------------------------------------------------------------
        private void OnDestroy()
        {
            
            if(PoetAudioManager.isAvailable())
            {
                PoetAudioManager.Instance.setReadingTextSoundActive(this.m_oriValue);
            }

        }

    }

}
