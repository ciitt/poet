﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poet
{

    /// <summary>
    /// Single PoetAudioScript
    /// </summary>
    public class SingleAudioScript : PoetAudioScript
    {

        /// <summary>
        /// Csv header for audio
        /// </summary>
        [SerializeField]
        [Tooltip("Csv header for audio")]
        [Space(30.0f)]
        protected string m_csvHeaderForAudio = "BGM";

        /// <summary>
        /// Get header
        /// </summary>
        /// <returns>header</returns>
        // ------------------------------------------------------------------------------------------
        protected override string csvHeader()
        {
            return this.m_csvHeaderForAudio;
        }

    }

}
