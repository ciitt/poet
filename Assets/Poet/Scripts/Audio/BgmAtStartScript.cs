﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poet
{

    /// <summary>
    /// Play BGM at scene start
    /// </summary>
    public class BgmAtStartScript : LoadAssetFromAssetBundleScript
    {
    
        public enum LoadingAudioType
        {
            Preload,
            Resource,
            AssetBundle
        }

        /// <summary>
        /// LoadingAudioType
        /// </summary>
        [SerializeField]
        [Tooltip("LoadingAudioType")]
        protected LoadingAudioType m_loadingAudioType = LoadingAudioType.Preload;

        /// <summary>
        /// Identifier for Preload
        /// </summary>
        [SerializeField]
        [Tooltip("Identifier for Preload")]
        protected string m_preloadedId = "";

        /// <summary>
        /// Resource path
        /// </summary>
        [SerializeField]
        [Tooltip("Resource path")]
        protected string m_resourcePath = "";

        /// <summary>
        /// Temp for AudioClip
        /// </summary>
        protected AudioClip m_tempAudio = null;

        /// <summary>
        /// Start
        /// </summary>
        // -------------------------------------------------------------------------------------------
        protected override void Start()
        {

            //
            {

                if (this.m_loadingAudioType == LoadingAudioType.Preload)
                {
                    //
                }

                else if (this.m_loadingAudioType == LoadingAudioType.Resource)
                {
                    PoetIEnumeratorStartupManager.Instance.addSceneStartupIEnumerator(
                        this.loadResource(this.m_resourcePath),
                        null,
                        null,
                        SSC.IEnumeratorStartupManager.BeforeAfter.Before
                        );
                }

                else if (this.m_loadingAudioType == LoadingAudioType.AssetBundle)
                {
                    base.Start();
                }

            }

            // addSceneChangeStateReceiver
            {
                PoetReduxManager prm = PoetReduxManager.Instance as PoetReduxManager;
                prm.addSceneChangeStateReceiver(this.onSceneChangeStateReceiver);
            }

        }

        /// <summary>
        /// SceneChangeState receiver
        /// </summary>
        /// <param name="scState">SceneChangeState</param>
        // -----------------------------------------------------------------------------------------------
        protected virtual void onSceneChangeStateReceiver(SSC.SceneChangeState scState)
        {

            if(scState.stateEnum == SSC.SceneChangeState.StateEnum.ScenePlaying)
            {

                PoetSceneChangeManager pscm = PoetSceneChangeManager.Instance as PoetSceneChangeManager;

                if (!pscm.isLoadingCurrentSceneWithUserProgressData())
                {

                    if (this.m_loadingAudioType == LoadingAudioType.Preload)
                    {

                        if (PoetAudioManager.Instance.refBGM)
                        {
                            PoetAudioManager.Instance.refBGM.playPreloadedAudio(this.m_preloadedId, 0.0f, true);
                        }

                    }

                    else if (
                        this.m_loadingAudioType == LoadingAudioType.Resource ||
                        this.m_loadingAudioType == LoadingAudioType.AssetBundle
                        )
                    {

                        if (this.m_tempAudio && PoetAudioManager.Instance.refBGM)
                        {
                            PoetAudioManager.Instance.refBGM.playAudio(this.m_tempAudio, 0.0f);
                        }

                    }

                }

            }

        }

        /// <summary>
        /// Load resource audio
        /// </summary>
        /// <param name="param">path</param>
        /// <returns>IEnumerator</returns>
        // ------------------------------------------------------------------------------------------
        protected IEnumerator loadResource(string param)
        {

            var request = Resources.LoadAsync(param, typeof(AudioClip));

            if (request == null)
            {
                yield break;
            }

            // ------------------

            while (!request.isDone)
            {
                yield return null;
            }

            this.m_tempAudio = request.asset as AudioClip;

        }

        /// <summary>
        /// Load asset from AssetBundle
        /// </summary>
        /// <param name="objFromAssetBundle">asset from AssetBundle</param>
        // -------------------------------------------------------------------------------------------
        protected override void loadAssetFromAssetBundle(UnityEngine.Object objFromAssetBundle)
        {

            if (!objFromAssetBundle)
            {
                return;
            }

            // -------------------------

            this.m_tempAudio = Instantiate(objFromAssetBundle) as AudioClip;

        }

    }

}
