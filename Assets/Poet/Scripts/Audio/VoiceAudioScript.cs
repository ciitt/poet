﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poet
{

    /// <summary>
    /// Audio script for voice
    /// </summary>
    public class VoiceAudioScript : MultiLanguageAudioScript
    {

        /// <summary>
        /// PoetSequenceState receiver
        /// </summary>
        /// <param name="state">PoetSequenceState</param>
        /// <param name="header">csv header</param>
        // ------------------------------------------------------------------------------------------
        protected override void onPoetSequenceState(PoetSequenceState psState)
        {

            base.onPoetSequenceState(psState);

            if (psState.stateEnum == PoetSequenceState.StateEnum.MainProcess)
            {

                if (psState.headerAndCells.ContainsKey(this.csvHeader()))
                {

                    FuncNameAndParamsList fnapList = CsvParser.parseFunctionsInCell(psState.headerAndCells[this.csvHeader()]);

                    if(TextLogManager.isAvailable())
                    {

                        string temp0 = "";
                        string temp1 = "";

                        foreach (var fnap in fnapList.list)
                        {

                            temp0 = fnap.getStringParameter(0);
                            temp1 = fnap.getStringParameter(1);

                            if (fnap.functionName == "PlayAB")
                            {
                                TextLogManager.Instance.updateAssetBundleVoiceId(this.createKeyForAssetBundle(temp0, ""));
                                break;
                            }

                            else if (fnap.functionName == "PlayABWithAssetName")
                            {
                                TextLogManager.Instance.updateAssetBundleVoiceId(this.createKeyForAssetBundle(temp0, temp1));
                                break;
                            }

                            else if (fnap.functionName == "PlayResource")
                            {
                                TextLogManager.Instance.updateResourceVoiceId(temp0);
                                break;
                            }

                            else if (fnap.functionName == "PlayID")
                            {
                                TextLogManager.Instance.updatePreloadedVoiceId(temp0);
                                break;
                            }

                        } // foreach

                    }

                }

            }

        }

    }

}
