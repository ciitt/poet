﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poet
{

    /// <summary>
    /// PoetAudioScript for voice
    /// </summary>
    public class MultiLanguageAudioScript : PoetAudioScript
    {

        /// <summary>
        /// Languaget and its header
        /// </summary>
        [SerializeField]
        [Tooltip("Languaget and its header")]
        protected LanguageAndHeaders m_languageAndHeaders = new LanguageAndHeaders("Voice");

        /// <summary>
        /// Called in Awake
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected override void initOnAwake()
        {

            base.initOnAwake();

            this.m_languageAndHeaders.initDictionary();

        }

        /// <summary>
        /// Get header
        /// </summary>
        /// <returns>header</returns>
        // ------------------------------------------------------------------------------------------
        protected override string csvHeader()
        {

            SystemLanguage sl = SystemManager.Instance.systemLanguage;

            if (this.m_languageAndHeaders.languageAndHeaderDictionary.ContainsKey(sl))
            {
                return this.m_languageAndHeaders.languageAndHeaderDictionary[sl];
            }

            return "";

        }

    }

}