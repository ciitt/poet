﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Poet
{

    [RequireComponent(typeof(Toggle))]
    public class SkipToggleButtonScript : MonoBehaviour
    {

        Toggle m_refToggle = null;

        void Awake()
        {

            this.m_refToggle = this.GetComponent<Toggle>();

        }

        void Update()
        {

            if (SystemManager.Instance.isSkip != this.m_refToggle.isOn)
            {
                this.m_refToggle.isOn = SystemManager.Instance.isSkip;
            }

        }

        public void onSkipValueChanged(bool isOn)
        {

            if(SystemManager.Instance.isSkip != isOn)
            {
                SystemManager.Instance.isSkip = isOn;
            }
            
        }

    }

}
