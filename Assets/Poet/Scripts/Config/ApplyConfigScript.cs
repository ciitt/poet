﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Poet
{

    /// <summary>
    /// Apply config
    /// </summary>
    public class ApplyConfigScript : MonoBehaviour
    {

        /// <summary>
        /// Reference to window mode window Toggle
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to window mode window Toggle")]
        protected Toggle m_windowModeWindowToggle = null;

        /// <summary>
        /// Reference to window mode window Toggle
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to window mode full screen Toggle")]
        protected Toggle m_windowModeFullScreenToggle = null;

        // --------------------------------------------------------------------------------

        /// <summary>
        /// Reference to skip mode all Toggle
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to skip mode all Toggle")]
        protected Toggle m_skipModeAllToggle = null;

        /// <summary>
        /// Reference to skip mode already read Toggle
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to skip mode already read Toggle")]
        protected Toggle m_skipModeAlreadyReadToggle = null;

        // --------------------------------------------------------------------------------

        /// <summary>
        /// Reference to change already read text color on Toggle
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to change already read text color on Toggle")]
        protected Toggle m_changeAlreadyReadTextColorOnToggle = null;

        /// <summary>
        /// Reference to change already read text color off Toggle
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to change already read text color off Toggle")]
        protected Toggle m_changeAlreadyReadTextColorOffToggle = null;

        // --------------------------------------------------------------------------------

        /// <summary>
        /// Reference to laguage Toggle
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to laguage Toggle")]
        protected List<CustomToggleButton> m_languageToggles = new List<CustomToggleButton>();

        // --------------------------------------------------------------------------------

        /// <summary>
        /// Reference to voice stop mode next text Toggle
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to voice stop mode next text Toggle")]
        protected Toggle m_voiceStopModeNextTextToggle = null;

        /// <summary>
        /// Reference to voice stop mode next voice Toggle
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to voice stop mode next voice Toggle")]
        protected Toggle m_voiceStopModeNextVoiceToggle = null;

        // --------------------------------------------------------------------------------
        /// <summary>
        /// Reference to text speed Slider
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to text speed Slider")]
        protected Slider m_textSpeedSlider = null;

        /// <summary>
        /// Reference to auto text speed Slider
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to auto text speed Slider")]
        protected Slider m_autoTextSpeedSlider = null;

        /// <summary>
        /// Reference to text opacity Slider
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to text window opacity Slider")]
        protected Slider m_textWindowOpacitySlider = null;

        // --------------------------------------------------------------------------------

        /// <summary>
        /// Reference to text window CanvasGroup
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to text window CanvasGroup")]
        protected CanvasGroup m_textWindowCanvasGroup = null;

        // --------------------------------------------------------------------------------


        /// <summary>
        /// Reference to master volume Slider
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to master volume Slider")]
        protected Slider m_masterVolumeSlider = null;

        /// <summary>
        /// Reference to bgm volume Slider
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to bgm volume Slider")]
        protected Slider m_bgmVolumeSlider = null;

        /// <summary>
        /// Reference to se volume Slider
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to se volume Slider")]
        protected Slider m_seVolumeSlider = null;

        /// <summary>
        /// Reference to voice volume Slider
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to voice volume Slider")]
        protected Slider m_voiceVolumeSlider = null;

        /// <summary>
        /// Reference to environment volume Slider
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to environment volume Slider")]
        protected Slider m_environmentVolumeSlider = null;

        /// <summary>
        /// Reference to extra volume Slider
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to extra volume Slider")]
        protected Slider m_extraVolumeSlider = null;

        /// <summary>
        /// Reference to character voice Slider list
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to character voice Slider list")]
        protected List<CharacterVoiceSlider> m_characterVoiceSliderList = new List<CharacterVoiceSlider>();

        /// <summary>
        /// Apply config data to UI
        /// </summary>
        // ---------------------------------------------------------------------------
        public virtual void applyConfigDataToUi()
        {
           
            PoetConfigDataSO conf = SystemManager.Instance.configDataSO<PoetConfigDataSO>();

            // m_windowModeWindowToggle m_windowModeFullScreenToggle
            {

                if (this.m_windowModeFullScreenToggle)
                {
                    this.m_windowModeFullScreenToggle.isOn = (conf.windowMode == PoetConfigDataSO.WindowMode.FullScreen);
                }

                else if(this.m_windowModeWindowToggle)
                {
                    this.m_windowModeWindowToggle.isOn = (conf.windowMode == PoetConfigDataSO.WindowMode.Window);
                }

            }

            // m_skipModeAllToggle m_skipModeAlreadyReadToggle
            {

                if (this.m_skipModeAllToggle)
                {
                    this.m_skipModeAllToggle.isOn = (conf.skipMode == PoetConfigDataSO.SkipMode.All);
                }

                else if (this.m_skipModeAlreadyReadToggle)
                {
                    this.m_skipModeAlreadyReadToggle.isOn = (conf.skipMode == PoetConfigDataSO.SkipMode.AlreadyRead);
                }

            }

            // m_changeAlreadyReadTextColorOffToggle m_changeAlreadyReadTextColorOnToggle
            {

                if (this.m_changeAlreadyReadTextColorOffToggle)
                {
                    this.m_changeAlreadyReadTextColorOffToggle.isOn = !conf.changeAlreadyReadTextColor;
                }

                else if(this.m_changeAlreadyReadTextColorOnToggle)
                {
                    this.m_changeAlreadyReadTextColorOnToggle.isOn = conf.changeAlreadyReadTextColor;
                }

            }

            // m_voiceStopModeNextVoiceToggle m_voiceStopModeNextTextToggle
            {

                if (this.m_voiceStopModeNextVoiceToggle)
                {
                    this.m_voiceStopModeNextVoiceToggle.isOn = (conf.voiceStopMode == PoetConfigDataSO.VoiceStopMode.NextVoice);
                }

                else if (this.m_voiceStopModeNextTextToggle)
                {
                    this.m_voiceStopModeNextTextToggle.isOn = (conf.voiceStopMode == PoetConfigDataSO.VoiceStopMode.NextText);
                }

            }

            //
            {

                if (this.m_textSpeedSlider)
                {
                    this.m_textSpeedSlider.normalizedValue = conf.textSpeed01;
                }

                if (this.m_autoTextSpeedSlider)
                {
                    this.m_autoTextSpeedSlider.normalizedValue = conf.autoModeSpeed01;
                }

                if (this.m_textWindowOpacitySlider)
                {
                    this.m_textWindowOpacitySlider.normalizedValue = conf.textWindowOpacity01;
                }

            }

            // m_languageToggles
            {

                string language = conf.systemLanguage.ToString();

                foreach (CustomToggleButton val in this.m_languageToggles)
                {

                    if(val.identifier == language)
                    {
                        val.isOn = true;
                        break;
                    }

#if UNITY_EDITOR

                    if(string.IsNullOrEmpty(val.identifier))
                    {
                        Debug.LogWarning("CustomToggleButton.identifier is empty : " + val.gameObject.name);
                    }
#endif

                }

            }

            //
            {

                if (this.m_masterVolumeSlider)
                {
                    this.m_masterVolumeSlider.normalizedValue = conf.masterVolume01;
                }

                if (this.m_bgmVolumeSlider)
                {
                    this.m_bgmVolumeSlider.normalizedValue = conf.bgmVolume01;
                }

                if (this.m_seVolumeSlider)
                {
                    this.m_seVolumeSlider.normalizedValue = conf.seVolume01;
                }

                if (this.m_voiceVolumeSlider)
                {
                    this.m_voiceVolumeSlider.normalizedValue = conf.voiceVolume01;
                }

                if (this.m_environmentVolumeSlider)
                {
                    this.m_environmentVolumeSlider.normalizedValue = conf.environmentVolume01;
                }

                if (this.m_extraVolumeSlider)
                {
                    this.m_extraVolumeSlider.normalizedValue = conf.extraVolume01;
                }

            }

            //
            {

#if UNITY_EDITOR

                List<string> debugList = new List<string>();

                foreach (var val in this.m_characterVoiceSliderList)
                {

                    if(debugList.Contains(val.characterVoiceId))
                    {
                        Debug.LogWarning("(#if UNITY_EDITOR) m_characterVoiceSliderList already contains a character voice id : " + val.characterVoiceId);
                        Debug.Log("Search [CharacterVoiceSlider] in hierarchy and check the result.");
                    }

                    else
                    {
                        debugList.Add(val.characterVoiceId);
                    }
                    
                }

#endif

                foreach (var val in this.m_characterVoiceSliderList)
                {

                    if(conf.characterVoiceIdAndVolumeDict.ContainsKey(val.characterVoiceId))
                    {
                        val.normalizedValue = conf.characterVoiceIdAndVolumeDict[val.characterVoiceId].volume01;
                    }

                    else
                    {

                        if(!string.IsNullOrEmpty(val.characterVoiceId))
                        {

                            CharacterVoiceIdAndVolume temp = new CharacterVoiceIdAndVolume();

                            temp.identifier = val.characterVoiceId;
                            temp.volume01 = 1.0f;

                            conf.characterVoiceIdAndVolumeDict.Add(val.characterVoiceId, temp);

                            val.normalizedValue = 1.0f;

                        }
                        
                    }

                }

            }

        }

        /// <summary>
        /// Set screen mode
        /// </summary>
        /// <param name="enumValue">enumValue</param>
        // ---------------------------------------------------------------------------
        public void setScreenMode(int enumValue)
        {

#if !UNITY_STANDALONE
            return;
#endif

            // -------------------

            PoetConfigDataSO conf = SystemManager.Instance.configDataSO<PoetConfigDataSO>();

            if (enumValue == (int)PoetConfigDataSO.WindowMode.Window && conf.windowMode != PoetConfigDataSO.WindowMode.Window)
            {

                conf.windowMode = PoetConfigDataSO.WindowMode.Window;

                Screen.SetResolution(
                    (int)SystemManager.Instance.screenWindowSize.x,
                    (int)SystemManager.Instance.screenWindowSize.y,
                    false
                    );

            }

            else if(enumValue == (int)PoetConfigDataSO.WindowMode.FullScreen && conf.windowMode != PoetConfigDataSO.WindowMode.FullScreen)
            {

                conf.windowMode = PoetConfigDataSO.WindowMode.FullScreen;

                Screen.SetResolution(
                    (int)SystemManager.Instance.screenFullScreenSize.x,
                    (int)SystemManager.Instance.screenFullScreenSize.y,
                    true
                    );

            }

        }

        /// <summary>
        /// Set skip mode
        /// </summary>
        /// <param name="enumValue">enumValue</param>
        // ---------------------------------------------------------------------------
        public void setSkipMode(int enumValue)
        {

            PoetConfigDataSO conf = SystemManager.Instance.configDataSO<PoetConfigDataSO>();

            if (enumValue == (int)PoetConfigDataSO.SkipMode.All)
            {
                conf.skipMode = PoetConfigDataSO.SkipMode.All;
            }

            else if (enumValue == (int)PoetConfigDataSO.SkipMode.AlreadyRead)
            {
                conf.skipMode = PoetConfigDataSO.SkipMode.AlreadyRead;
            }

        }

        /// <summary>
        /// Set change already read text color mode
        /// </summary>
        /// <param name="on">on</param>
        // ---------------------------------------------------------------------------
        public void setChangeAlreadyTextColor(bool on)
        {

            PoetConfigDataSO conf = SystemManager.Instance.configDataSO<PoetConfigDataSO>();

            conf.changeAlreadyReadTextColor = on;

        }

        /// <summary>
        /// Set voice stop mode
        /// </summary>
        /// <param name="enumValue">enumValue</param>
        // ---------------------------------------------------------------------------
        public void setVoiceStopMode(int enumValue)
        {

            PoetConfigDataSO conf = SystemManager.Instance.configDataSO<PoetConfigDataSO>();

            if (enumValue == (int)PoetConfigDataSO.VoiceStopMode.NextText)
            {
                conf.voiceStopMode = PoetConfigDataSO.VoiceStopMode.NextText;
            }

            else if (enumValue == (int)PoetConfigDataSO.VoiceStopMode.NextVoice)
            {
                conf.voiceStopMode = PoetConfigDataSO.VoiceStopMode.NextVoice;
            }

        }

        /// <summary>
        /// Set text speed
        /// </summary>
        /// <param name="value01">normalized value</param>
        // ---------------------------------------------------------------------------
        public void setTextSpeed(float value01)
        {

            PoetConfigDataSO conf = SystemManager.Instance.configDataSO<PoetConfigDataSO>();

            conf.textSpeed01 = Mathf.Clamp01(value01);

        }

        /// <summary>
        /// Set auto text speed
        /// </summary>
        /// <param name="value01">normalized value</param>
        // ---------------------------------------------------------------------------
        public void setAutoTextSpeed(float value01)
        {

            PoetConfigDataSO conf = SystemManager.Instance.configDataSO<PoetConfigDataSO>();

            conf.autoModeSpeed01 = Mathf.Clamp01(value01);

        }

        /// <summary>
        /// Set text window opacity
        /// </summary>
        /// <param name="value01">normalized value</param>
        // ---------------------------------------------------------------------------
        public void setTextWindowOpacity(float value01)
        {

            PoetConfigDataSO conf = SystemManager.Instance.configDataSO<PoetConfigDataSO>();

            conf.textWindowOpacity01 = Mathf.Clamp01(value01);

        }

        /// <summary>
        /// Set master volume
        /// </summary>
        /// <param name="value01">normalized value</param>
        // ---------------------------------------------------------------------------
        public void setMasterVolume(float value01)
        {

            PoetConfigDataSO conf = SystemManager.Instance.configDataSO<PoetConfigDataSO>();

            value01 = Mathf.Clamp01(value01);
            conf.masterVolume01 = value01;
            AudioListener.volume = value01;

        }

        /// <summary>
        /// Set bgm volume
        /// </summary>
        /// <param name="value01">normalized value</param>
        // ---------------------------------------------------------------------------
        public void setBgmVolume(float value01)
        {

            PoetConfigDataSO conf = SystemManager.Instance.configDataSO<PoetConfigDataSO>();

            value01 = Mathf.Clamp01(value01);
            conf.bgmVolume01 = value01;
            
            if(PoetAudioManager.Instance.refBGM)
            {
                PoetAudioManager.Instance.refBGM.setVolume(value01);
            }

        }

        /// <summary>
        /// Set se volume
        /// </summary>
        /// <param name="value01">normalized value</param>
        // ---------------------------------------------------------------------------
        public void setSeVolume(float value01)
        {

            PoetConfigDataSO conf = SystemManager.Instance.configDataSO<PoetConfigDataSO>();

            value01 = Mathf.Clamp01(value01);
            conf.seVolume01 = value01;

            if (PoetAudioManager.Instance.refSE)
            {
                PoetAudioManager.Instance.refSE.setVolume(value01);
            }

        }

        /// <summary>
        /// Set voice volume
        /// </summary>
        /// <param name="value01">normalized value</param>
        // ---------------------------------------------------------------------------
        public void setVoiceVolume(float value01)
        {

            PoetConfigDataSO conf = SystemManager.Instance.configDataSO<PoetConfigDataSO>();

            value01 = Mathf.Clamp01(value01);
            conf.voiceVolume01 = value01;

            if (PoetAudioManager.Instance.refVoice)
            {
                PoetAudioManager.Instance.refVoice.setVolume(value01);
            }

        }

        /// <summary>
        /// Set environment volume
        /// </summary>
        /// <param name="value01">normalized value</param>
        // ---------------------------------------------------------------------------
        public void setEnvironmentVolume(float value01)
        {

            PoetConfigDataSO conf = SystemManager.Instance.configDataSO<PoetConfigDataSO>();

            value01 = Mathf.Clamp01(value01);
            conf.environmentVolume01 = value01;

            if (PoetAudioManager.Instance.refEnvironment)
            {
                PoetAudioManager.Instance.refEnvironment.setVolume(value01);
            }

        }

        /// <summary>
        /// Set extra volume
        /// </summary>
        /// <param name="value01">normalized value</param>
        // ---------------------------------------------------------------------------
        public void setExtraVolume(float value01)
        {

            PoetConfigDataSO conf = SystemManager.Instance.configDataSO<PoetConfigDataSO>();

            value01 = Mathf.Clamp01(value01);
            conf.extraVolume01 = value01;

            if (PoetAudioManager.Instance.refExtra)
            {
                PoetAudioManager.Instance.refExtra.setVolume(value01);
            }

        }

        /// <summary>
        /// Set character voice volume
        /// </summary>
        /// <param name="characterVoiceId">character voice id</param>
        /// <param name="value01">normalized value</param>
        // ---------------------------------------------------------------------------
        public void setCharacterVoiceVolume(string characterVoiceId, float value01)
        {

            PoetConfigDataSO conf = SystemManager.Instance.configDataSO<PoetConfigDataSO>();

            value01 = Mathf.Clamp01(value01);

            if (conf.characterVoiceIdAndVolumeDict.ContainsKey(characterVoiceId))
            {

                conf.characterVoiceIdAndVolumeDict[characterVoiceId].volume01 = value01;

            }

        }

        /// <summary>
        /// Back to title scene button
        /// </summary>
        // ---------------------------------------------------------------------------
        public void onClickBackToTitleButton()
        {

            var pscm = PoetSceneChangeManager.Instance as PoetSceneChangeManager;

            if (pscm.isLoadingAlbumMemoryScene())
            {

                PoetDialogManager.Instance.showYesNoDialog(
                    PoetDialogMessageManager.Instance.backToAlbumConfirmationMessage(),
                    () =>
                    {
                        (PoetSceneChangeManager.Instance as PoetSceneChangeManager).finishAlbumMemoryScene();
                    },
                    null
                    );

            }

            else
            {

                if (PoetSceneChangeManager.Instance.nowLoadingSceneName == PoetSceneChangeManager.Instance.titleSceneName)
                {
                    PoetCommonUiManager.Instance.back(true);
                }

                else
                {

                    PoetDialogManager.Instance.showYesNoDialog(
                        PoetDialogMessageManager.Instance.backToTitleConfirmationMessage(),
                        () =>
                        {
                            (PoetSceneChangeManager.Instance as PoetSceneChangeManager).useSimpleNowLoadingOnce();
                            PoetSceneChangeManager.Instance.backToTitleScene();
                        },
                        null
                        );

                }

            }

        }

        /// <summary>
        /// Quit application button
        /// </summary>
        // ---------------------------------------------------------------------------
        public void onClickQuitApplicationButton()
        {
            SystemManager.Instance.quitApp();
        }

        /// <summary>
        /// Set system language
        /// </summary>
        /// <param name="sl">SystemLanguage</param>
        // -------------------------------------------------------------------------------------
        public virtual void setSystemLanguage(string sl)
        {

            SystemLanguage language = SystemLanguage.Unknown;

            try
            {
                language = (SystemLanguage)Enum.Parse(typeof(SystemLanguage), sl);
            }

            catch (Exception e)
            {
                Debug.LogError(e.Message);
                return;
            }

            SystemManager.Instance.setSystemLanguage(language);

        }

    }

}
