﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poet
{

    [RequireComponent(typeof(CanvasGroup))]
    public class ApplyTextWindowOpacityScript : MonoBehaviour
    {

        CanvasGroup m_refCanvasGroup = null;

        void Awake()
        {
            this.m_refCanvasGroup = this.GetComponent<CanvasGroup>();
        }

        public void applyTextWindowOpacity()
        {
            this.m_refCanvasGroup.alpha = SystemManager.Instance.configDataSO<PoetConfigDataSO>().textWindowOpacity01;
        }

    }

}
