﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Poet
{

    [RequireComponent(typeof(Toggle))]
    public class AutoToggleButtonScript : MonoBehaviour
    {

        Toggle m_refToggle = null;

        void Awake()
        {

            this.m_refToggle = this.GetComponent<Toggle>();

        }

        void Update()
        {

            if (SystemManager.Instance.isAuto != this.m_refToggle.isOn)
            {
                this.m_refToggle.isOn = SystemManager.Instance.isAuto;
            }

        }

        public void onAutoValueChanged(bool isOn)
        {

            if (SystemManager.Instance.isAuto != isOn)
            {
                SystemManager.Instance.isAuto = isOn;
            }

        }

    }

}
