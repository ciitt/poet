﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poet
{

    /// <summary>
    /// Save and load transform info
    /// </summary>
    public class SaveAndLoadTransformInfoScript : MonoBehaviour
    {

        /// <summary>
        /// User progress data
        /// </summary>
        [Serializable]
        protected class UserProgressData
        {

            public Vector3 position = Vector3.zero;
            public Quaternion rotation = Quaternion.identity;
            public Vector3 localScale = Vector3.one;

        }

        /// <summary>
        /// Local position and rotation
        /// </summary>
        [SerializeField]
        [Tooltip("Local position and rotation")]
        protected bool m_localPositionAndRotation = false;

        [Space(20.0f)]

        /// <summary>
        /// Save position
        /// </summary>
        [SerializeField]
        [Tooltip("Save position")]
        protected bool m_savePosition = true;

        /// <summary>
        /// Save rotation
        /// </summary>
        [SerializeField]
        [Tooltip("Save rotation")]
        protected bool m_saveRotation = true;

        /// <summary>
        /// Save scale
        /// </summary>
        [SerializeField]
        [Tooltip("Save scale")]
        protected bool m_saveScale = true;

        /// <summary>
        /// UserProgressData
        /// </summary>
        protected UserProgressData m_userProgressData = new UserProgressData();

        /// <summary>
        /// Start
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected virtual void Start()
        {

            PoetReduxManager prm = PoetReduxManager.Instance as PoetReduxManager;

            prm.addUserProgressDataSignalReceiver(this.onUserProgressDataSignal);
            prm.addSceneChangeStateReceiver(this.onSceneChangeStateReceiver);

        }

        /// <summary>
        /// UserProgressDataSignal receiver
        /// </summary>
        /// <param name="updSignal">UserProgressDataSignal</param>
        // ------------------------------------------------------------------------------------------
        protected virtual void onUserProgressDataSignal(UserProgressDataSignal updSignal)
        {

            this.m_userProgressData.position =
                (this.m_localPositionAndRotation) ?
                this.transform.localPosition :
                this.transform.position
                ;
            this.m_userProgressData.rotation =
                (this.m_localPositionAndRotation) ? 
                this.transform.localRotation :
                this.transform.rotation
                ;

            this.m_userProgressData.localScale = this.transform.localScale;

            updSignal.addDataAction(this.transform, this, JsonUtility.ToJson(this.m_userProgressData));

        }

        /// <summary>
        /// SceneChangeState receiver
        /// </summary>
        /// <param name="scState">SceneChangeState</param>
        // ------------------------------------------------------------------------------------------
        protected virtual void onSceneChangeStateReceiver(SSC.SceneChangeState scState)
        {

            if (scState.stateEnum == SSC.SceneChangeState.StateEnum.AllStartupsDone)
            {

                PoetSceneChangeManager pscm = PoetSceneChangeManager.Instance as PoetSceneChangeManager;

                if (pscm.isLoadingCurrentSceneWithUserProgressData())
                {

                    this.m_userProgressData = pscm.getDataFromCurrentUserProgressData<UserProgressData>(this.transform, this);

                    if (this.m_savePosition)
                    {

                        if(this.m_localPositionAndRotation)
                        {
                            this.transform.localPosition = this.m_userProgressData.position;
                        }

                        else
                        {
                            this.transform.position = this.m_userProgressData.position;
                        }
                        
                    }

                    if (this.m_saveRotation)
                    {

                        if (this.m_localPositionAndRotation)
                        {
                            this.transform.localRotation = this.m_userProgressData.rotation;
                        }

                        else
                        {
                            this.transform.rotation = this.m_userProgressData.rotation;
                        }
                            
                    }

                    if (this.m_saveScale)
                    {
                        this.transform.localScale = this.m_userProgressData.localScale;
                    }

                }

            }

        }

    }

}
