﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poet
{

    /// <summary>
    /// Save and load Rigidbody info
    /// </summary>
    [RequireComponent(typeof(Rigidbody))]
    public class SaveAndLoadRigidBodyInfoScript : SaveAndLoadTransformInfoScript
    {

        /// <summary>
        /// User progress data
        /// </summary>
        [Serializable]
        protected class UserProgressDataRB : UserProgressData
        {

            public Vector3 velocity = Vector3.zero;
            public Vector3 angularVelocity = Vector3.zero;
            public bool isKinematic = false;

        }

        /// <summary>
        /// Reference to Rigidbody
        /// </summary>
        protected Rigidbody m_refRigidbody = null;

        /// <summary>
        /// Temp for velocity
        /// </summary>
        protected Vector3 m_tempVelocity = Vector3.zero;

        /// <summary>
        /// Temp for angularVelocity
        /// </summary>
        protected Vector3 m_tempAngularVelocity = Vector3.zero;

        /// <summary>
        /// Temp for isKinematic
        /// </summary>
        protected bool m_tempIsKinematic = false;

        /// <summary>
        /// Original isKinematic
        /// </summary>
        protected bool m_oriIsKinematic = false;

        // ------------------------------------------------------------------------------------------

        protected UserProgressDataRB userProgressDataRB { get { return (UserProgressDataRB)this.m_userProgressData; } }

        // ------------------------------------------------------------------------------------------

        /// <summary>
        /// Awake
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected virtual void Awake()
        {

            // m_userProgressData
            {
                this.m_userProgressData = new UserProgressDataRB();
            }

            // m_refRigidbody
            {
                this.m_refRigidbody = this.GetComponent<Rigidbody>();
            }

            // m_oriIsKinematic
            {
                this.m_oriIsKinematic = this.m_refRigidbody.isKinematic;
            }

        }

        /// <summary>
        /// Start
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected override void Start()
        {

            base.Start();

            // pause
            {
                this.pauseOn();
            }

            // PoetReduxManager
            {
                var prm = PoetReduxManager.Instance as PoetReduxManager;

                prm.addPauseStateReceiver(this.onPauseStateReceiver);

            }

        }

        /// <summary>
        /// UserProgressDataSignal receiver
        /// </summary>
        /// <param name="updSignal">UserProgressDataSignal</param>
        // ------------------------------------------------------------------------------------------
        protected override void onUserProgressDataSignal(UserProgressDataSignal updSignal)
        {

            this.userProgressDataRB.velocity = this.m_refRigidbody.velocity;
            this.userProgressDataRB.angularVelocity = this.m_refRigidbody.angularVelocity;
            this.userProgressDataRB.isKinematic = this.m_refRigidbody.isKinematic;

            base.onUserProgressDataSignal(updSignal);

        }

        /// <summary>
        /// Pause Rigidbody
        /// </summary>
        // ------------------------------------------------------------------------------------------
        public void pauseOn()
        {

            this.m_tempVelocity = this.m_refRigidbody.velocity;
            this.m_tempAngularVelocity = this.m_refRigidbody.angularVelocity;
            this.m_tempIsKinematic = this.m_refRigidbody.isKinematic;

            this.m_refRigidbody.isKinematic = true;

        }

        /// <summary>
        /// Pause Rigidbody
        /// </summary>
        // ------------------------------------------------------------------------------------------
        public void pauseOff(Vector3 velocity, Vector3 angularVelocity, bool isKinematic)
        {
            this.m_refRigidbody.velocity = velocity;
            this.m_refRigidbody.angularVelocity = angularVelocity;
            this.m_refRigidbody.isKinematic = isKinematic;
        }

        /// <summary>
        /// SceneChangeState receiver
        /// </summary>
        /// <param name="scState">SceneChangeState</param>
        // ------------------------------------------------------------------------------------------
        protected override void onSceneChangeStateReceiver(SSC.SceneChangeState scState)
        {

            base.onSceneChangeStateReceiver(scState);

            if (scState.stateEnum == SSC.SceneChangeState.StateEnum.ScenePlaying)
            {

                PoetSceneChangeManager pscm = PoetSceneChangeManager.Instance as PoetSceneChangeManager;

                if (pscm.isLoadingCurrentSceneWithUserProgressData())
                {
                    this.m_userProgressData = pscm.getDataFromCurrentUserProgressData<UserProgressDataRB>(this.transform, this);
                    this.pauseOff(this.userProgressDataRB.velocity, this.userProgressDataRB.angularVelocity, this.userProgressDataRB.isKinematic);
                }

                else
                {
                    this.pauseOff(Vector3.zero, Vector3.zero, this.m_oriIsKinematic);
                }

            }

        }

        /// <summary>
        /// PauseState receiver
        /// </summary>
        /// <param name="pState">PauseState</param>
        // ------------------------------------------------------------------------------------------
        protected virtual void onPauseStateReceiver(SSC.PauseState pState)
        {

            if(pState.pause)
            {
                this.pauseOn();
            }

            else
            {
                this.pauseOff(this.m_tempVelocity, this.m_tempAngularVelocity, this.m_tempIsKinematic);
            }

        }

    }

}
