﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poet
{

    /// <summary>
    /// Save and load Animator info
    /// </summary>
    [RequireComponent(typeof(Animator))]
    public class SaveAndLoadAnimatorScript : MonoBehaviour
    {

        [Serializable]
        protected class IntegerParam
        {

            public string name = "";
            public int value = 0;

            public IntegerParam(string _name, int _value)
            {
                this.name = _name;
                this.value = _value;
            }

        }

        [Serializable]
        protected class FloatParam
        {

            public string name = "";
            public float value = 0.0f;

            public FloatParam(string _name, float _value)
            {
                this.name = _name;
                this.value = _value;
            }

        }

        [Serializable]
        protected class BoolParam
        {

            public string name = "";
            public bool value = false;

            public BoolParam(string _name, bool _value)
            {
                this.name = _name;
                this.value = _value;
            }

        }

        /// <summary>
        /// User progress data
        /// </summary>
        [Serializable]
        protected class UserProgressData
        {

            public List<int> stateHashList = new List<int>();
            public List<float> normalizedTimeList = new List<float>();
            public List<IntegerParam> intParamList = new List<IntegerParam>();
            public List<FloatParam> floatParamList = new List<FloatParam>();
            public List<BoolParam> boolParamList = new List<BoolParam>();

        }

        /// <summary>
        /// Reference to Animator
        /// </summary>
        protected Animator m_refAnimator = null;

        /// <summary>
        /// UserProgressData
        /// </summary>
        protected UserProgressData m_userProgressData = new UserProgressData();

        /// <summary>
        /// Awake
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected virtual void Awake()
        {

            // m_refAnimator
            {
                this.m_refAnimator = this.GetComponent<Animator>();
            }

        }

        /// <summary>
        /// Start
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected virtual void Start()
        {

            // PoetReduxManager
            {

                PoetReduxManager prm = PoetReduxManager.Instance as PoetReduxManager;

                prm.addUserProgressDataSignalReceiver(this.onUserProgressDataSignal);
                prm.addSceneChangeStateReceiver(this.onSceneChangeStateReceiver);

            }

        }

        /// <summary>
        /// Resume Animator
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected void resumeAnimator()
        {

            // stateHashList
            {

                if(this.m_refAnimator.layerCount == this.m_userProgressData.stateHashList.Count)
                {

                    int size = Mathf.Min(
                        this.m_refAnimator.layerCount,
                        this.m_userProgressData.stateHashList.Count,
                        this.m_userProgressData.normalizedTimeList.Count
                        );

                    for (int i = 0; i < size; i++)
                    {
                        this.m_refAnimator.Play(
                            this.m_userProgressData.stateHashList[i],
                            i,
                            this.m_userProgressData.normalizedTimeList[i]
                            );
                    }

                }

            }

            // parameters
            {

                foreach (var val in this.m_userProgressData.intParamList)
                {
                    this.m_refAnimator.SetInteger(val.name, val.value);
                }

                foreach (var val in this.m_userProgressData.floatParamList)
                {
                    this.m_refAnimator.SetFloat(val.name, val.value);
                }

                foreach (var val in this.m_userProgressData.boolParamList)
                {
                    this.m_refAnimator.SetBool(val.name, val.value);
                }

            }

        }

        /// <summary>
        /// UserProgressDataSignal receiver
        /// </summary>
        /// <param name="updSignal">UserProgressDataSignal</param>
        // ------------------------------------------------------------------------------------------
        protected virtual void onUserProgressDataSignal(UserProgressDataSignal updSignal)
        {

            this.m_userProgressData.stateHashList.Clear();
            this.m_userProgressData.normalizedTimeList.Clear();
            this.m_userProgressData.intParamList.Clear();
            this.m_userProgressData.floatParamList.Clear();
            this.m_userProgressData.boolParamList.Clear();

            // stateHashList
            {

                for (int i = 0; i < this.m_refAnimator.layerCount; i++)
                {

                    this.m_userProgressData.stateHashList.Add(
                        this.m_refAnimator.GetCurrentAnimatorStateInfo(i).fullPathHash
                        );

                    this.m_userProgressData.normalizedTimeList.Add(this.m_refAnimator.GetCurrentAnimatorStateInfo(i).normalizedTime);

                }

            }

            // parameters
            {

                foreach (var val in this.m_refAnimator.parameters)
                {

                    if (val.type == AnimatorControllerParameterType.Int)
                    {
                        this.m_userProgressData.intParamList.Add(
                            new IntegerParam(val.name, this.m_refAnimator.GetInteger(val.name))
                            );
                    }

                    else if (val.type == AnimatorControllerParameterType.Float)
                    {
                        this.m_userProgressData.floatParamList.Add(
                            new FloatParam(val.name, this.m_refAnimator.GetFloat(val.name))
                            );
                    }

                    else if (val.type == AnimatorControllerParameterType.Bool)
                    {
                        this.m_userProgressData.boolParamList.Add(
                            new BoolParam(val.name, this.m_refAnimator.GetBool(val.name))
                            );
                    }

                }

            }

            updSignal.addDataAction(this.transform, this, JsonUtility.ToJson(this.m_userProgressData));

        }

        /// <summary>
        /// SceneChangeState receiver
        /// </summary>
        /// <param name="scState">SceneChangeState</param>
        // ------------------------------------------------------------------------------------------
        protected virtual void onSceneChangeStateReceiver(SSC.SceneChangeState scState)
        {

            if (scState.stateEnum == SSC.SceneChangeState.StateEnum.AllStartupsDone)
            {

                PoetSceneChangeManager pscm = PoetSceneChangeManager.Instance as PoetSceneChangeManager;

                if (pscm.isLoadingCurrentSceneWithUserProgressData())
                {

                    this.m_userProgressData = pscm.getDataFromCurrentUserProgressData<UserProgressData>(this.transform, this);

                    this.resumeAnimator();

                }

            }

        }

    }

}
