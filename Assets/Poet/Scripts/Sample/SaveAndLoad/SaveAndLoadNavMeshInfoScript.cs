﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace Poet
{

    /// <summary>
    /// Save and load NavMeshAgent info
    /// </summary>
    [RequireComponent(typeof(NavMeshAgent))]
    public class SaveAndLoadNavMeshInfoScript : SaveAndLoadTransformInfoScript
    {
        /// <summary>
        /// User progress data
        /// </summary>
        [Serializable]
        protected class UserProgressDataNMA : UserProgressData
        {
            public float speed = 0.0f;
            public Vector3 velocity = Vector3.zero;
            public Vector3 destination = Vector3.zero;
            public bool isStopped = false;
            public bool lockPause = false;
        }

        /// <summary>
        /// Reference to NavMeshAgent
        /// </summary>
        protected NavMeshAgent m_refNavMeshAgent = null;

        /// <summary>
        /// NavMeshAgent velocity at stop
        /// </summary>
        protected Vector3 m_velocityAtStop = Vector3.zero;

        /// <summary>
        /// Lock pause state
        /// </summary>
        protected bool m_lockPause = false;

        // ------------------------------------------------------------------------------------------

        protected UserProgressDataNMA userProgressDataNMA { get { return (UserProgressDataNMA)this.m_userProgressData; } }

        /// <summary>
        /// Lock pause state
        /// </summary>
        public bool lockPauseState { get { return this.m_lockPause; } set { this.m_lockPause = value; } }

        // ------------------------------------------------------------------------------------------

        /// <summary>
        /// Awake
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected virtual void Awake()
        {

            // m_userProgressData
            {
                this.m_userProgressData = new UserProgressDataNMA();
            }

            // m_refNavMeshAgent
            {
                this.m_refNavMeshAgent = this.GetComponent<NavMeshAgent>();
            }

        }

        /// <summary>
        /// Start
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected override void Start()
        {

            base.Start();

            // pause
            {
                this.pause(true);
            }

            // addPauseStateReceiver
            {
                PoetReduxManager.Instance.addPauseStateReceiver(this.onPauseStateReceiver);
            }

        }

        /// <summary>
        /// UserProgressDataSignal receiver
        /// </summary>
        /// <param name="updSignal">UserProgressDataSignal</param>
        // ------------------------------------------------------------------------------------------
        protected override void onUserProgressDataSignal(UserProgressDataSignal updSignal)
        {

            this.userProgressDataNMA.speed = this.m_refNavMeshAgent.speed;

            this.userProgressDataNMA.velocity =
                (this.m_refNavMeshAgent.isStopped) ?
                this.m_velocityAtStop :
                this.m_refNavMeshAgent.velocity
                ;

            this.userProgressDataNMA.destination = this.m_refNavMeshAgent.destination;
            this.userProgressDataNMA.isStopped = this.m_refNavMeshAgent.isStopped;
            this.userProgressDataNMA.lockPause = this.m_lockPause;

            base.onUserProgressDataSignal(updSignal);

        }


        /// <summary>
        /// Pause NavMeshAgent
        /// </summary>
        /// <param name="isOn">pause</param>
        // ------------------------------------------------------------------------------------------
        public void pause(bool isOn)
        {

            if (this.m_lockPause)
            {
                return;
            }

            // -----------------

            if (isOn)
            {
                this.m_velocityAtStop = this.m_refNavMeshAgent.velocity;
                this.m_refNavMeshAgent.isStopped = true;
                this.m_refNavMeshAgent.velocity = Vector3.zero;
            }

            else
            {
                this.m_refNavMeshAgent.isStopped = false;
                this.m_refNavMeshAgent.velocity = this.m_velocityAtStop;
            }

        }

        /// <summary>
        /// SceneChangeState receiver
        /// </summary>
        /// <param name="scState">SceneChangeState</param>
        // ------------------------------------------------------------------------------------------
        protected override void onSceneChangeStateReceiver(SSC.SceneChangeState scState)
        {

            base.onSceneChangeStateReceiver(scState);

            if (scState.stateEnum == SSC.SceneChangeState.StateEnum.ScenePlaying)
            {

                PoetSceneChangeManager pscm = PoetSceneChangeManager.Instance as PoetSceneChangeManager;

                if (pscm.isLoadingCurrentSceneWithUserProgressData())
                {

                    this.m_userProgressData = pscm.getDataFromCurrentUserProgressData<UserProgressDataNMA>(this.transform, this);

                    this.m_refNavMeshAgent.speed = this.userProgressDataNMA.speed;
                    this.m_refNavMeshAgent.SetDestination(this.userProgressDataNMA.destination);
                    this.m_refNavMeshAgent.velocity = this.userProgressDataNMA.velocity;
                    this.pause(this.userProgressDataNMA.isStopped);

                    this.m_lockPause = this.userProgressDataNMA.lockPause;

                }

                else
                {
                    this.pause(false);
                }

            }

        }

        /// <summary>
        /// PauseState receiver
        /// </summary>
        /// <param name="pState">PauseState</param>
        // ------------------------------------------------------------------------------------------
        protected virtual void onPauseStateReceiver(SSC.PauseState pState)
        {

            this.pause(pState.pause);

        }

    }

}
