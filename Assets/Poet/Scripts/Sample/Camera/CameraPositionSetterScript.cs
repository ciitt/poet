﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poet
{

    public class CameraPositionSetterScript : MonoBehaviour
    {

        /// <summary>
        /// Player GameObject tag
        /// </summary>
        [SerializeField]
        [Tooltip("Player GameObject tag")]
        protected string m_playerTag = "Player";

        /// <summary>
        /// Rotate speed
        /// </summary>
        [SerializeField]
        [Tooltip("Rotate speed")]
        [Range(0.0f, 1.0f)]
        protected float m_rotateSpeed = 0.1f;

        /// <summary>
        /// Reference to player
        /// </summary>
        protected Transform m_refPlayer = null;

        /// <summary>
        /// Start
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected virtual void Start()
        {

            var player = GameObject.FindGameObjectWithTag(this.m_playerTag);

            if (player)
            {
                this.m_refPlayer = player.transform;
            }

            StartCoroutine(this.LateUpdateIE());
        }

        /// <summary>
        /// LateUpdate
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected virtual IEnumerator LateUpdateIE()
        {

            if(!this.m_refPlayer)
            {
                yield break;
            }

            // ----------------------------

            while(true)
            {

                // position
                {
                    this.transform.position = this.m_refPlayer.position;
                }

                // rotation
                {

                    this.transform.rotation = Quaternion.Slerp(
                        this.transform.rotation,
                        Quaternion.LookRotation(this.m_refPlayer.forward),
                        this.m_rotateSpeed
                        );

                }

                yield return null;

            }

        }

    }

}
