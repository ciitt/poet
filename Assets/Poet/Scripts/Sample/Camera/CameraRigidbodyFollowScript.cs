﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poet
{

    /// <summary>
    /// Camera Rigidbody
    /// </summary>
    [RequireComponent(typeof(Rigidbody))]
    public class CameraRigidbodyFollowScript : MonoBehaviour
    {

        /// <summary>
        /// Identifier for target to follow
        /// </summary>
        [SerializeField]
        [Tooltip("Identifier for target to follow")]
        protected string m_followTargetIdentifier = "";

        /// <summary>
        /// Max speed to follow
        /// </summary>
        [SerializeField]
        [Tooltip("Max speed to follow")]
        protected float m_maxSpeed = 5.0f;

        /// <summary>
        /// Min distance to follow
        /// </summary>
        [SerializeField]
        [Tooltip("Min distance to follow")]
        protected float m_minDistance = 0.1f;

        /// <summary>
        /// Max distance to follow
        /// </summary>
        [SerializeField]
        [Tooltip("Max distance to follow")]
        protected float m_maxDistance = 3.0f;

        /// <summary>
        /// Reference to Rigidbody
        /// </summary>
        protected Rigidbody m_refRigidbody = null;

        /// <summary>
        /// Reference to current target to follow
        /// </summary>
        protected Transform m_refCurrentFollowTarget = null;

        /// <summary>
        /// Reference to SceneChangeState
        /// </summary>
        protected SSC.SceneChangeState m_refSceneChangeState = null;

        /// <summary>
        /// Reference to PauseState
        /// </summary>
        protected SSC.PauseState m_refPauseState = null;

        /// <summary>
        /// Start
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected virtual void Start()
        {

            this.m_refRigidbody = this.GetComponent<Rigidbody>();

            if (PositionAndRotateTargetManager.isAvailable())
            {

                this.m_refCurrentFollowTarget =
                    (!string.IsNullOrEmpty(this.m_followTargetIdentifier)) ?
                    PositionAndRotateTargetManager.Instance.getTargetTransform(this.m_followTargetIdentifier) :
                    null
                    ;

            }

            // PoetReduxManager
            {

                PoetReduxManager prm = PoetReduxManager.Instance as PoetReduxManager;

                this.m_refSceneChangeState = prm.SceneChangeStateWatcher.state();
                this.m_refPauseState = prm.PauseStateWatcher.state();

            }

        }

        /// <summary>
        /// FixedUpdate
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected virtual void FixedUpdate()
        {

            if (this.m_refPauseState.pause || this.m_refSceneChangeState.stateEnum != SSC.SceneChangeState.StateEnum.ScenePlaying)
            {
                return;
            }

            if (!this.m_refCurrentFollowTarget)
            {
                return;
            }

            // --------------------------

            //
            {

                Vector3 diff = this.m_refCurrentFollowTarget.position - this.m_refRigidbody.position;
                Vector3 dir = diff.normalized;

                float distance = diff.magnitude;

                this.m_refRigidbody.velocity =
                    dir *
                    this.m_maxSpeed *
                    Mathf.InverseLerp(this.m_minDistance, this.m_maxDistance, distance)
                    ;

            }

        }

    }

}
