﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poet
{

    /// <summary>
    /// Sample camera
    /// </summary>
    public class SamplePlayerFollowCameraScript : ObjectFollowControllerRBScript
    {

        /// <summary>
        /// Player GameObject tag
        /// </summary>
        [SerializeField]
        [Tooltip("Player GameObject tag")]
        protected string m_playerTag = "Player";

        /// <summary>
        /// Reference to main camera. if null, Camera.main
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to main camera. if null, Camera.main")]
        protected Camera m_refCamera = null;

        /// <summary>
        /// Reference to player
        /// </summary>
        protected Transform m_refPlayer = null;

        /// <summary>
        /// Dot lock
        /// </summary>
        protected bool m_dotLock = true;

        /// <summary>
        /// Counter for dot lock
        /// </summary>
        protected int m_dotLockCounter = 0;

        /// <summary>
        /// Start
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected virtual void Start()
        {

            var player = GameObject.FindGameObjectWithTag(this.m_playerTag);

            if (player)
            {
                this.m_refPlayer = player.transform;
            }

            if (!this.m_refCamera)
            {
                this.m_refCamera = Camera.main;
            }

        }

        /// <summary>
        /// PoetSequenceState receiver
        /// </summary>
        /// <param name="psState">PoetSequenceState</param>
        // ------------------------------------------------------------------------------------------
        protected override void onPoetSequenceState(PoetSequenceState psState)
        {

            if (psState.stateEnum == PoetSequenceState.StateEnum.FinishOperation)
            {

                if(this.m_userProgressData.followTargetIdentifier == this.m_defaultFollowTargetIdentifier)
                {
                    this.m_dotLock = true;
                    this.m_refRigidbody.velocity = Vector3.zero;
                    this.m_dotLockCounter = 0;
                }

                else
                {
                    this.m_dotLock = false;
                }

            }

            base.onPoetSequenceState(psState);

        }

        /// <summary>
        /// SceneChangeState receiver
        /// </summary>
        /// <param name="scState">SceneChangeState</param>
        // ------------------------------------------------------------------------------------------
        protected override void onSceneChangeStateReceiver(SSC.SceneChangeState scState)
        {

            base.onSceneChangeStateReceiver(scState);

            if(scState.stateEnum == SSC.SceneChangeState.StateEnum.NowLoadingOutro)
            {
                this.m_dotLock = true;
                this.m_refRigidbody.velocity = Vector3.zero;
                this.m_dotLockCounter = 0;
            }

        }

        /// <summary>
        /// FixedUpdate
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected override void FixedUpdate()
        {

            if(PoetOneSceneManager.isAvailable() && PoetOneSceneManager.Instance.isCurrentCsvHolderActive)
            {
                base.FixedUpdate();
                return;
            }

            // --------------------------

            if (this.m_refRigidbody.isKinematic)
            {
                return;
            }

            if (this.m_refPauseState.pause || this.m_refSceneChangeState.stateEnum != SSC.SceneChangeState.StateEnum.ScenePlaying)
            {
                return;
            }

            if (!this.m_refCurrentFollowTarget || !this.m_refPlayer || !this.m_refCamera)
            {
                return;
            }

            // --------------------------

            //
            {

                Vector3 playerForward = this.m_refPlayer.forward;
                Vector3 camForward = this.m_refCamera.transform.forward;
                playerForward.y = 0.0f;
                playerForward.Normalize();
                camForward.y = 0.0f;
                camForward.Normalize();

                float dot = Vector3.Dot(camForward, this.m_refPlayer.forward);

                if (this.m_dotLock)
                {

                    if(dot > 0.0f)
                    {
                        this.m_dotLockCounter = (this.m_dotLockCounter + 1 % 10);
                        this.m_dotLock = this.m_dotLockCounter < 10;
                    }

                    this.m_refRigidbody.velocity = Vector3.zero;

                }

                else
                {

                    Vector3 diff = this.m_refCurrentFollowTarget.position - this.m_refRigidbody.position;
                    Vector3 dir = diff.normalized;

                    this.m_refRigidbody.velocity =
                        dir *
                        this.m_maxSpeed *
                        Mathf.InverseLerp(this.m_minDistance, this.m_maxDistance, diff.magnitude)
                        ;

                }

            }

        }

    }

}
