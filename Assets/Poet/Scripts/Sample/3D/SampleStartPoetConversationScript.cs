﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Poet
{

    /// <summary>
    /// Start Poet conversation
    /// </summary>
    public class SampleStartPoetConversationScript : MonoBehaviour
    {

        protected enum ItemOrFlag
        {
            Item,
            Flag
        }

        [Serializable]
        protected class CovnersationCondition
        {

            public ItemOrFlag _type = ItemOrFlag.Item;

            public string csvRowIdentifier = "ID_3Coin";

            public string itemOrFlagIdentifier = "Coin";

            public string comparison = ">=";

            public string value = "0";

            public bool conditionsAreMet()
            {

                if (SolveJumpIfManager.isAvailable())
                {

                    string temp = SolveJumpIfManager.Instance.solveJumpIfItem(
                            this.itemOrFlagIdentifier,
                            this.comparison,
                            this.value,
                            this.csvRowIdentifier
                            )
                            ;
                    return !string.IsNullOrEmpty(temp);

                }

                return false;

            }

        }

        /// <summary>
        /// User progress data
        /// </summary>
        [Serializable]
        protected class UserProgressData
        {
            public bool nowConversation = false;
        }

        /// <summary>
        /// Conversation distance
        /// </summary>
        [SerializeField]
        [Tooltip("Conversation distance")]
        protected float m_conversationDistance = 2.0f;

        /// <summary>
        /// Dot product to start conversation
        /// </summary>
        [SerializeField]
        [Tooltip("Dot product to start conversation")]
        [Range(-1.0f, 1.0f)]
        protected float m_conversationDotProduct = 0.2f;

        /// <summary>
        /// Reference to PoetCsvHolder
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to PoetCsvHolder")]
        protected PoetCsvHolder m_refPoetCsvHolder = null;

        /// <summary>
        /// Default csv row ientifier for conversation
        /// </summary>
        [SerializeField]
        [Tooltip("Default csv row ientifier for conversation")]
        protected string m_defaultCsvRowIdentifier = "";

        /// <summary>
        /// Conversation for conversation
        /// </summary>
        [SerializeField]
        [Tooltip("Conversation for conversation")]
        protected List<CovnersationCondition> m_conversationConditions = new List<CovnersationCondition>();

        /// <summary>
        /// Player GameObject tag
        /// </summary>
        [SerializeField]
        [Tooltip("Player GameObject tag")]
        protected string m_playerTag = "Player";

        /// <summary>
        /// Look at player when conversation started
        /// </summary>
        [SerializeField]
        [Tooltip("Look at player when conversation started")]
        protected bool m_lookAtPlayerAtFirst = true;

        /// <summary>
        /// UnityEvent at poet start
        /// </summary>
        [SerializeField]
        [Tooltip("UnityEvent at poet start")]
        protected UnityEvent m_eventAtPoetStart = null;

        /// <summary>
        /// UnityEvent at poet finish
        /// </summary>
        [SerializeField]
        [Tooltip("UnityEvent at poet finish")]
        protected UnityEvent m_eventAtPoetFinish = null;

        /// <summary>
        /// Reference to player Transform
        /// </summary>
        protected Transform m_refPlayer = null;

        /// <summary>
        /// Conversation Ok
        /// </summary>
        static protected bool conversationOk = true;

        /// <summary>
        /// Reference to SceneChangeState
        /// </summary>
        protected SSC.SceneChangeState m_refSceneChangeState = null;

        /// <summary>
        /// Reference to PauseState
        /// </summary>
        protected SSC.PauseState m_refPauseState = null;

        /// <summary>
        /// UserProgressData
        /// </summary>
        protected UserProgressData m_userProgressData = new UserProgressData();

        /// <summary>
        /// Start
        /// </summary>
        // -------------------------------------------------------------------------------
        protected virtual void Start()
        {

            // m_refPoetCsvHolder
            {

                if (!this.m_refPoetCsvHolder)
                {
                    Debug.LogWarning("m_refPoetCsvHolder is null : " + this.gameObject.name);
                }

            }

            // m_refPlayer
            {

                var player = GameObject.FindGameObjectWithTag(this.m_playerTag);

                if (player)
                {
                    this.m_refPlayer = player.transform;
                }

                else
                {
                    Debug.LogWarning("Failed FindGameObjectWithTag : " + this.m_playerTag + " : " + this.gameObject.name);
                }

            }

            // addPoetSequenceStateReceiver
            {

                Poet.PoetReduxManager prm = Poet.PoetReduxManager.Instance as Poet.PoetReduxManager;
                prm.addPoetSequenceStateReceiver(this.onPoetSequenceState);
                prm.addUserProgressDataSignalReceiver(this.onUserProgressDataSignal);
                prm.addSceneChangeStateReceiver(this.onSceneChangeStateReceiver);

                this.m_refSceneChangeState = prm.SceneChangeStateWatcher.state();
                this.m_refPauseState = prm.PauseStateWatcher.state();

            }

        }

        /// <summary>
        /// UserProgressDataSignal receiver
        /// </summary>
        /// <param name="updSignal">UserProgressDataSignal</param>
        // ------------------------------------------------------------------------------------------
        protected virtual void onUserProgressDataSignal(UserProgressDataSignal updSignal)
        {
            updSignal.addDataAction(this.transform, this, JsonUtility.ToJson(this.m_userProgressData));
        }

        /// <summary>
        /// SceneChangeState receiver
        /// </summary>
        /// <param name="scState">SceneChangeState</param>
        // ------------------------------------------------------------------------------------------
        protected virtual void onSceneChangeStateReceiver(SSC.SceneChangeState scState)
        {

            if (scState.stateEnum == SSC.SceneChangeState.StateEnum.NowLoadingMain)
            {
                conversationOk = true;
            }

            else if (scState.stateEnum == SSC.SceneChangeState.StateEnum.AllStartupsDone)
            {

                PoetSceneChangeManager pscm = PoetSceneChangeManager.Instance as PoetSceneChangeManager;

                if (pscm.isLoadingCurrentSceneWithUserProgressData())
                {

                    this.m_userProgressData = pscm.getDataFromCurrentUserProgressData<UserProgressData>(this.transform, this);

                    if (this.m_userProgressData.nowConversation)
                    {
                        conversationOk = false;
                    }

                }

            }

        }

        /// <summary>
        /// Update
        /// </summary>
        // -------------------------------------------------------------------------------
        public virtual void Update()
        {

            if (!this.m_refPoetCsvHolder || !this.m_refPlayer)
            {
                return;
            }

            if (
                this.m_refSceneChangeState.stateEnum != SSC.SceneChangeState.StateEnum.ScenePlaying ||
                this.m_refPauseState.pause
                )
            {
                return;
            }

            if (
                PoetCommonUiManager.Instance.nowInShowingOrHidingTransition ||
                (SSC.SceneUiManager.isAvailable() && SSC.SceneUiManager.Instance.nowInShowingOrHidingTransition)
                )
            {
                return;
            }

            if (!conversationOk)
            {
                return;
            }

            // -------------------------------

            if (
                Input.GetAxis("Submit") > 0.0f ||
                (Input.GetMouseButtonDown(0) && !UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
                )
            {

                if ((this.m_refPlayer.position - this.transform.position).sqrMagnitude < this.m_conversationDistance * this.m_conversationDistance)
                {

                    if (Vector3.Dot(this.m_refPlayer.forward, (this.transform.position - this.m_refPlayer.position).normalized) >= this.m_conversationDotProduct)
                    {

                        if (Poet.PoetOneSceneManager.isAvailable() && !Poet.PoetOneSceneManager.Instance.isCurrentCsvHolderActive)
                        {

                            string identifier = this.getCsvRowIdentifierToStart();

                            if (!string.IsNullOrEmpty(identifier))
                            {

                                this.m_userProgressData.nowConversation = true;
                                conversationOk = false;

                                this.m_eventAtPoetStart.Invoke();

                                if (this.m_lookAtPlayerAtFirst)
                                {
                                    StartCoroutine(this.lookAtPlayer());
                                }

                                this.m_refPoetCsvHolder.showThisCsvHolder(identifier);

                            }

                        }

                    }

                }

            }

        }

        /// <summary>
        /// Get csv row identifier to start
        /// </summary>
        // -------------------------------------------------------------------------------
        public virtual string getCsvRowIdentifierToStart()
        {

            for(int i = this.m_conversationConditions.Count - 1; i >= 0; i--)
            {

                if(this.m_conversationConditions[i].conditionsAreMet())
                {
                    return this.m_conversationConditions[i].csvRowIdentifier;
                }

            }

            return this.m_defaultCsvRowIdentifier;

        }

        /// <summary>
        /// PoetSequenceState receiver
        /// </summary>
        /// <param name="psState">PoetSequenceState</param>
        // ------------------------------------------------------------------------------------------
        protected virtual void onPoetSequenceState(Poet.PoetSequenceState psState)
        {

            if (psState.stateEnum == Poet.PoetSequenceState.StateEnum.FinishOperation)
            {

                if (this.m_userProgressData.nowConversation)
                {
                    this.m_userProgressData.nowConversation = false;
                    this.m_eventAtPoetFinish.Invoke();
                    Invoke("resumeOkFlag", 3.0f);
                }

            }

        }

        /// <summary>
        /// Resume ok flag
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected void resumeOkFlag()
        {
            conversationOk = true;
        }

        /// <summary>
        /// Look at player
        /// </summary>
        /// <returns>IEnumerator</returns>
        // ------------------------------------------------------------------------------------------
        protected virtual IEnumerator lookAtPlayer()
        {

            if (!this.m_refPlayer)
            {
                yield break;
            }

            // ------------------

            Vector3 temp = this.m_refPlayer.position - this.transform.position;

            temp.y = 0.0f;

            Quaternion start = this.transform.rotation;
            Quaternion end = Quaternion.LookRotation(temp);

            float timer = 0.0f;

            if (temp != Vector3.zero)
            {

                while (timer < 1.0f)
                {

                    timer += Time.deltaTime;

                    this.transform.rotation = Quaternion.Slerp(
                        start,
                        end,
                        timer
                        );

                    yield return null;

                }

                this.transform.rotation = end;

            }

        }

    }

}
