﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poet
{

    /// <summary>
    /// Add new PositionAndRotateTarget
    /// </summary>
    public class RegisterPositionAndRotateTargetScript : MonoBehaviour
    {

        /// <summary>
        /// Identifier for PositionAndRotateTargetManager
        /// </summary>
        [SerializeField]
        [Tooltip("Identifier for PositionAndRotateTargetManager")]
        protected string m_identifier = "";

        /// <summary>
        /// Start
        /// </summary>
        // --------------------------------------------------------------------------------
        void Start()
        {

            // PoetReduxManager
            {

                PoetReduxManager prm = PoetReduxManager.Instance as PoetReduxManager;

                prm.addSceneChangeStateReceiver(this.onSceneChangeStateReceiver);

            }

        }

        /// <summary>
        /// SceneChangeState receiver
        /// </summary>
        /// <param name="scState">SceneChangeState</param>
        // ------------------------------------------------------------------------------------------
        protected virtual void onSceneChangeStateReceiver(SSC.SceneChangeState scState)
        {

            if (scState.stateEnum == SSC.SceneChangeState.StateEnum.AllStartupsDone)
            {

                if (PositionAndRotateTargetManager.isAvailable())
                {
                    PositionAndRotateTargetManager.Instance.addNewIdAndTransform(this.m_identifier, this.transform);
                }

            }

        }

    }

}
