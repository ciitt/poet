﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Poet
{

    /// <summary>
    /// Player teleport
    /// </summary>
    public class PlayerTeleportScript : MonoBehaviour
    {

        /// <summary>
        /// PoetCsvHolder for teleport
        /// </summary>
        [SerializeField]
        [Tooltip("PoetCsvHolder for teleport")]
        protected PoetCsvHolder m_refPoetCsvHolderForTeleport = null;

        /// <summary>
        /// Csv row identifier for teleport
        /// </summary>
        [SerializeField]
        [Tooltip("Csv row identifier for teleport")]
        protected string m_csvRowIdentifierForTeleport = "";

        /// <summary>
        /// Player GameObject tag
        /// </summary>
        [SerializeField]
        [Tooltip("Player GameObject tag")]
        protected string m_playerTag = "Player";

        /// <summary>
        /// UnityEvent at teleport start
        /// </summary>
        [SerializeField]
        [Tooltip("UnityEvent at teleport start")]
        protected UnityEvent m_eventAtTeleportStart = null;

        /// <summary>
        /// UnityEvent at teleport end
        /// </summary>
        [SerializeField]
        [Tooltip("UnityEvent at teleport end")]
        protected UnityEvent m_eventAtTeleportEnd = null;

        /// <summary>
        /// Reference to player
        /// </summary>
        protected Transform m_refPlayer = null;

        /// <summary>
        /// Teleportable cycle time
        /// </summary>
        protected static float teleportableOkTime = 3.0f;

        /// <summary>
        /// Is teleportable
        /// </summary>
        protected static bool isTeleportableTimeOk = false;

        /// <summary>
        /// Now teleporting
        /// </summary>
        protected bool m_nowTeleporting = false;

        // --------------------------------------------------------------------------------------

        /// <summary>
        /// Teleportable cycle time
        /// </summary>
        public static float TeleportableOkTime { get { return teleportableOkTime; } set { teleportableOkTime = value; } }

        // --------------------------------------------------------------------------------------

        /// <summary>
        /// Start
        /// </summary>
        // --------------------------------------------------------------------------------------
        protected virtual void Start()
        {

#if UNITY_EDITOR

            Collider collider = this.GetComponent<Collider>();

            if (!collider)
            {
                Debug.LogWarning("Collider not found : " + this.gameObject.name);
            }

            if (collider && !collider.isTrigger)
            {
                Debug.LogWarning("Collider.isTrigger should be true : " + this.gameObject.name);
            }

            if (!this.m_refPoetCsvHolderForTeleport)
            {
                Debug.LogWarning("m_refPoetCsvHolderForTeleport is null : " + this.gameObject.name);
            }

            if (string.IsNullOrEmpty(this.m_csvRowIdentifierForTeleport))
            {
                Debug.LogWarning("m_csvRowIdentifierForTeleport is empty : " + this.gameObject.name);
            }

#endif

            // PoetReduxManager
            {

                PoetReduxManager prm = PoetReduxManager.Instance as PoetReduxManager;

                prm.addSceneChangeStateReceiver(this.onSceneChangeStateReceiver);
                prm.addPoetSequenceStateReceiver(this.onPoetSequenceState);

            }

            // m_refPlayer
            {

                var player = GameObject.FindGameObjectWithTag(this.m_playerTag);

                if (player)
                {
                    this.m_refPlayer = player.transform;
                }

                else
                {
                    Debug.LogWarning("Failed FindGameObjectWithTag : " + this.m_playerTag + " : " + this.gameObject.name);
                }

            }

        }

        /// <summary>
        /// SceneChangeState receiver
        /// </summary>
        /// <param name="scState">SceneChangeState</param>
        // ------------------------------------------------------------------------------------------
        protected virtual void onSceneChangeStateReceiver(SSC.SceneChangeState scState)
        {

            if(scState.stateEnum == SSC.SceneChangeState.StateEnum.ScenePlaying)
            {
                Invoke("resetTeleportableOkFlag", teleportableOkTime);
            }

            else if (scState.stateEnum == SSC.SceneChangeState.StateEnum.NowLoadingIntro)
            {
                isTeleportableTimeOk = false;
            }

        }

        /// <summary>
        /// PoetSequenceState receiver
        /// </summary>
        /// <param name="psState">PoetSequenceState</param>
        // ------------------------------------------------------------------------------------------
        protected virtual void onPoetSequenceState(PoetSequenceState psState)
        {

            if(psState.stateEnum == PoetSequenceState.StateEnum.FinishOperation)
            {

                if(this.m_nowTeleporting)
                {
                    this.m_eventAtTeleportEnd.Invoke();
                }

                this.m_nowTeleporting = false;

            }

        }

        /// <summary>
        /// Is teleportable
        /// </summary>
        /// <returns></returns>
        // --------------------------------------------------------------------------------------
        protected virtual bool isTeleportable()
        {

            PoetReduxManager prm = PoetReduxManager.Instance as PoetReduxManager;

            var scState = prm.SceneChangeStateWatcher.state();
            var pauseState = prm.PauseStateWatcher.state();

            return (
                scState.stateEnum == SSC.SceneChangeState.StateEnum.ScenePlaying &&
                PoetOneSceneManager.isAvailable() &&
                !PoetOneSceneManager.Instance.isCurrentCsvHolderActive &&
                !pauseState.pause &&
                isTeleportableTimeOk
                )
                ;

        }


        /// <summary>
        /// OnTriggerEnter
        /// </summary>
        /// <param name="other">Collider</param>
        // --------------------------------------------------------------------------------------
        protected virtual void OnTriggerEnter(Collider other)
        {

            if (this.isTeleportable() && this.m_refPoetCsvHolderForTeleport && this.m_refPlayer)
            {

                if (this.m_refPlayer == other.transform)
                {
                    isTeleportableTimeOk = false;
                    Invoke("resetTeleportableOkFlag", teleportableOkTime);
                    this.m_nowTeleporting = true;
                    this.m_eventAtTeleportStart.Invoke();
                    this.m_refPoetCsvHolderForTeleport.showThisCsvHolder(this.m_csvRowIdentifierForTeleport);
                }

            }

        }

        /// <summary>
        /// Reset isTeleportableTimeOk
        /// </summary>
        // --------------------------------------------------------------------------------------
        protected void resetTeleportableOkFlag()
        {
            isTeleportableTimeOk = true;
        }

    }

}
