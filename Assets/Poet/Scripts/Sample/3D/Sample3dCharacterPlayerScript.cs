﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poet
{

    [RequireComponent(typeof(CharacterController))]
    public class Sample3dCharacterPlayerScript : MonoBehaviour
    {

        /// <summary>
        /// Reference to main camera. if null, Camera.main
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to main camera. if null, Camera.main")]
        protected Camera m_refCamera = null;

        /// <summary>
        /// Reference to Animator
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to Animator")]
        protected Animator m_refAnimator = null;

        /// <summary>
        /// Walk speed
        /// </summary>
        [SerializeField]
        [Tooltip("Walk speed")]
        protected float m_walkSpeed = 2.0f;

        /// <summary>
        /// Rotate speed
        /// </summary>
        [SerializeField]
        [Tooltip("Rotate speed")]
        [Range(0.0f, 1.0f)]
        protected float m_rotateSpeed = 0.1f;

        /// <summary>
        /// Additional down force
        /// </summary>
        [SerializeField]
        [Tooltip("Additional down force")]
        protected float m_additionalDownForce = 3.0f;

        /// <summary>
        /// Animator walk parameter name
        /// </summary>
        [SerializeField]
        [Tooltip("Animator walk parameter name")]
        protected string m_animatorWalkParamName = "IdleWalkRunSpeed";

        /// <summary>
        /// Reference to CharacterController
        /// </summary>
        protected CharacterController m_refCharacterController = null;

        /// <summary>
        /// Reference to SceneChangeState
        /// </summary>
        protected SSC.SceneChangeState m_refSceneChangeState = null;

        /// <summary>
        /// Reference to PauseState
        /// </summary>
        protected SSC.PauseState m_refPauseState = null;

        /// <summary>
        /// Vertical input
        /// </summary>
        protected float m_vertical = 0.0f;

        /// <summary>
        /// Vertical input
        /// </summary>
        protected float m_horizontal = 0.0f;

        /// <summary>
        /// Current walk Vector3
        /// </summary>
        protected Vector3 m_currentWalk = Vector3.forward;

        /// <summary>
        /// Current speed
        /// </summary>
        protected float m_currentSpeed = 0.0f;

        /// <summary>
        /// Previous vertical input
        /// </summary>
        protected float m_previousVertical = 0.0f;

        /// <summary>
        /// Previous direction to walk
        /// </summary>
        protected Vector3 m_previousDir = Vector3.forward;

        /// <summary>
        /// Point touch began
        /// </summary>
        protected Vector2 m_pointTouchBegan = Vector2.zero;

        /// <summary>
        /// Start
        /// </summary>
        // -----------------------------------------------------------------------------------
        protected virtual void Start()
        {

            // m_refCharacterController
            {
                this.m_refCharacterController = this.GetComponent<CharacterController>();
            }

            if(!this.m_refCamera)
            {
                this.m_refCamera = Camera.main;
            }

            // PoetReduxManager
            {

                PoetReduxManager prm = PoetReduxManager.Instance as PoetReduxManager;

                this.m_refSceneChangeState = prm.SceneChangeStateWatcher.state();
                this.m_refPauseState = prm.PauseStateWatcher.state();

            }

        }

        /// <summary>
        /// Update
        /// </summary>
        // -----------------------------------------------------------------------------------
        protected virtual void Update()
        {

            // updateInput
            {
                this.updateInput();
            }

            // moveCharacter
            {
                this.moveCharacter();
            }

            // setAnimation
            {
                this.setAnimation();
            }

        }

        /// <summary>
        /// Update
        /// </summary>
        // -----------------------------------------------------------------------------------
        protected void updateInput()
        {

            this.m_vertical = Input.GetAxis("Vertical");
            this.m_horizontal = Input.GetAxis("Horizontal");

            //
            {

                if (Input.touchCount > 0)
                {

                    Vector2 pointTouchCurrent = this.m_pointTouchBegan;

                    Touch touch0 = Input.GetTouch(0);

                    if (touch0.phase == TouchPhase.Began)
                    {
                        this.m_pointTouchBegan = touch0.position;
                    }

                    else if (touch0.phase == TouchPhase.Moved || touch0.phase == TouchPhase.Stationary)
                    {
                        pointTouchCurrent = touch0.position;
                    }

                    Vector2 diff = pointTouchCurrent - this.m_pointTouchBegan;

                    if(diff.sqrMagnitude > 100.0f)
                    {
                        this.m_horizontal = Mathf.Clamp(diff.x / (Screen.width * 0.1f), -1.0f, 1.0f);
                        this.m_vertical = Mathf.Clamp(diff.y / (Screen.height * 0.1f), -1.0f, 1.0f);
                    }

                }

            }

            Vector3 camForward = this.m_refCamera.transform.forward;
            Vector3 camRight = Vector3.Cross(Vector3.up, (this.transform.position - this.m_refCamera.transform.position).normalized);

            camForward.y = 0.0f;
            camRight.y = 0.0f;

            camForward.Normalize();
            camRight.Normalize();

            if (this.m_vertical < 0.0f && this.m_horizontal == 0.0f)
            {
                camForward = (camForward + (camRight * 0.1f)).normalized;
            }

            Vector3 dir = ((this.m_vertical * camForward) + (this.m_horizontal * camRight)).normalized;

            if (this.m_previousVertical < 0.0f && this.m_vertical < 0.0f && this.m_horizontal == 0.0f)
            {
                dir = this.m_previousDir;
            }

            // m_currentSpeed
            {
                this.m_currentSpeed = this.m_walkSpeed * Mathf.Max(Mathf.Abs(this.m_vertical), Mathf.Abs(this.m_horizontal));
            }

            // m_currentWalk
            {
                this.m_currentWalk = dir * this.m_currentSpeed;
            }

            // previous
            {
                this.m_previousDir = dir;
                this.m_previousVertical = this.m_vertical;
            }

        }

        /// <summary>
        /// Set animation
        /// </summary>
        // -----------------------------------------------------------------------------------
        protected void setAnimation()
        {

            if (this.m_refAnimator)
            {

                if (this.m_refSceneChangeState.stateEnum != SSC.SceneChangeState.StateEnum.ScenePlaying)
                {
                    this.m_refAnimator.speed = 1.0f;
                    this.m_refAnimator.SetFloat(this.m_animatorWalkParamName, 0.0f);
                }

                else if (this.m_refPauseState.pause)
                {
                    this.m_refAnimator.speed = 0.0f;
                }

                else if (PoetOneSceneManager.isAvailable() && PoetOneSceneManager.Instance.isCurrentCsvHolderActive)
                {
                    this.m_refAnimator.speed = 1.0f;
                    this.m_refAnimator.SetFloat(this.m_animatorWalkParamName, 0.0f);
                }

                else
                {

                    this.m_refAnimator.speed = 1.0f;

                    float temp = this.m_refAnimator.GetFloat(this.m_animatorWalkParamName);

                    if (this.m_currentSpeed < temp)
                    {
                        this.m_refAnimator.SetFloat(this.m_animatorWalkParamName, temp * 0.9f);
                    }

                    else
                    {
                        this.m_refAnimator.SetFloat(this.m_animatorWalkParamName, this.m_currentSpeed);
                    }
                    
                }

            }

        }

        /// <summary>
        /// Move character
        /// </summary>
        // -----------------------------------------------------------------------------------
        protected void moveCharacter()
        {

            if(!this.m_refCharacterController || !this.m_refCamera)
            {
                return;
            }

            // -------------------

            if (this.m_refSceneChangeState.stateEnum != SSC.SceneChangeState.StateEnum.ScenePlaying)
            {
                this.m_refCharacterController.SimpleMove(Vector3.zero);
                return;
            }

            if (PoetOneSceneManager.isAvailable() && PoetOneSceneManager.Instance.isCurrentCsvHolderActive)
            {
                this.m_refCharacterController.SimpleMove(Vector3.zero);
                return;
            }

            if (this.m_refPauseState.pause)
            {
                this.m_refCharacterController.SimpleMove(Vector3.zero);
                return;
            }

            // -------------------

            if(this.m_currentWalk != Vector3.zero)
            {
                this.transform.forward = Vector3.Lerp(this.transform.forward, this.m_currentWalk.normalized, this.m_rotateSpeed);
            }

            //this.m_refCharacterController.SimpleMove(this.m_currentWalk);
            this.m_refCharacterController.Move((this.m_currentWalk - (Vector3.up * this.m_additionalDownForce)) * Time.deltaTime);

        }

    }

}
