﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poet
{

    /// <summary>
    /// string id and AudioClip
    /// </summary>
    [Serializable]
    public class IdAndAudioClip : IdentifierInterface
    {

        public string identifier = "";
        public AudioClip audioClip = null;

        public string getIdentifier()
        {
            return this.identifier;
        }

    }

}