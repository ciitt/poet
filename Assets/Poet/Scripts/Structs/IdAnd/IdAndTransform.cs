﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Poet
{

    /// <summary>
    /// Identifier and its transform
    /// </summary>
    [Serializable]
    public class IdAndTransform : IdentifierInterface
    {

        public string identifier = "";
        public Transform transform = null;

        public string getIdentifier()
        {
            return this.identifier;
        }

    }

}
