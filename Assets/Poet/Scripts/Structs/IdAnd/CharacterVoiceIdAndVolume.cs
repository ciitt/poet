﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poet
{

    /// <summary>
    /// Character voice id and volume
    /// </summary>
    [Serializable]
    public class CharacterVoiceIdAndVolume : IdentifierInterface
    {

        public string identifier = "";

        [Range(0.0f, 1.0f)]
        public float volume01 = 1.0f;

        public string getIdentifier()
        {
            return this.identifier;
        }

    }

}