﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;

namespace Poet
{

    [Serializable]
    public class IdAndTextureDetail : IdAndTexture, AssetBundleNameInterface
    {

        public static Vector4 defaultPosAndSizeForSpriteAtlas = new Vector4(0,1,0,1);

#if UNITY_EDITOR
        public Texture2D textureEditorOnly = null;
        public SpriteAtlas spriteAtlasEditorOnly = null;
#endif

        public Vector2 tilling = Vector2.one;
        public Vector2 offset = Vector2.zero;

        public SpriteAtlas spriteAtlas = null;
        public string assetNameInSpriteAtlas = "";

        [HideInInspector]
        public Vector4 posAndSizeForSpriteAtlas = Vector4.zero;

        public string assetBundleName = "";
        public string assetBundleVariant = "";
        public string assetNameInAssetBundle = "";

#if UNITY_EDITOR

        [HideInInspector]
        public bool keepTillingAspect = false;

        [HideInInspector]
        public bool usePixelOffset = false;

        [HideInInspector]
        public Vector2 pixelOffset = new Vector2(0.5f, 0.5f);

#endif

        /// <summary>
        /// AssetBundle name
        /// </summary>
        public string AssetBundleName { get { return this.assetBundleName; } set { this.assetBundleName = value; } }

        /// <summary>
        /// AssetBundle variant
        /// </summary>
        public string AssetBundleVariant { get { return this.assetBundleVariant; } set { this.assetBundleVariant = value; } }

        /// <summary>
        /// Asset name in AssetBundle
        /// </summary>
        public string AssetNameInAssetBundle { get { return this.assetNameInAssetBundle; } set { this.assetNameInAssetBundle = value; } }

        /// <summary>
        /// Calculate pos and size for SpriteAtlas
        /// </summary>
        /// <param name="dict">Dictionary</param>
        // --------------------------------------------------------------------------------------------------------------------
        public void calcPosAndSizeForSpriteAtlas()
        {

            Sprite tempSprite = null;

            if (this.spriteAtlas)
            {

                tempSprite = this.spriteAtlas.GetSprite(this.assetNameInSpriteAtlas);

                if (tempSprite && tempSprite.texture)
                {

                    this.texture = tempSprite.texture;

                    try
                    {

                        this.posAndSizeForSpriteAtlas = new Vector4(
                            tempSprite.textureRect.x / (float)tempSprite.texture.width,
                            tempSprite.textureRect.width / (float)tempSprite.texture.width,
                            tempSprite.textureRect.y / (float)tempSprite.texture.height,
                            tempSprite.textureRect.height / (float)tempSprite.texture.height
                            );

                    }

                    catch (Exception e)
                    {
                        Debug.LogError(e.Message + "\n" + this.assetNameInSpriteAtlas);
                        this.posAndSizeForSpriteAtlas = defaultPosAndSizeForSpriteAtlas;
                    }

                }

            }

        }

        /// <summary>
        /// Unload
        /// </summary>
        // --------------------------------------------------------------------------------------------------------------------
        public void unload()
        {

            if(this.texture)
            {
                Resources.UnloadAsset(this.texture);
            }

            if(this.spriteAtlas)
            {
                Resources.UnloadAsset(this.spriteAtlas);
            }

        }

    }

}
