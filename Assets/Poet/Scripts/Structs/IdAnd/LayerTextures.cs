﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poet
{

    public interface LayerTexturesControllerInterface
    {
        LoadingMode loadingMode();
    }

    /// <summary>
    /// Layer Textures
    /// </summary>
    [Serializable]
    public class LayerTextures : IdentifierInterface
    {

        public IdAndTextureDetail layer0 = new IdAndTextureDetail();

        public List<IdAndTextureDetail> layer1s = new List<IdAndTextureDetail>();

        public List<IdAndTextureDetail> layer2s = new List<IdAndTextureDetail>();

        public List<IdAndTextureDetail> layer3s = new List<IdAndTextureDetail>();

        public List<IdAndTextureDetail> layer4s = new List<IdAndTextureDetail>();

        public List<IdAndTextureDetail> layer5s = new List<IdAndTextureDetail>();

        public List<IdAndTextureDetail> overlays = new List<IdAndTextureDetail>();

        public Dictionary<string, IdAndTextureDetail> layer1Dictionary = null;
        public Dictionary<string, IdAndTextureDetail> layer2Dictionary = null;
        public Dictionary<string, IdAndTextureDetail> layer3Dictionary = null;
        public Dictionary<string, IdAndTextureDetail> layer4Dictionary = null;
        public Dictionary<string, IdAndTextureDetail> layer5Dictionary = null;
        public Dictionary<string, IdAndTextureDetail> overlayDictionary = null;

        public void initDictionaries()
        {

            this.layer1Dictionary = Funcs.listToDictionary<IdAndTextureDetail>(this.layer1s);
            this.layer2Dictionary = Funcs.listToDictionary<IdAndTextureDetail>(this.layer2s);
            this.layer3Dictionary = Funcs.listToDictionary<IdAndTextureDetail>(this.layer3s);
            this.layer4Dictionary = Funcs.listToDictionary<IdAndTextureDetail>(this.layer4s);
            this.layer5Dictionary = Funcs.listToDictionary<IdAndTextureDetail>(this.layer5s);
            this.overlayDictionary = Funcs.listToDictionary<IdAndTextureDetail>(this.overlays);

        }

        public Dictionary<string, IdAndTextureDetail> dictionary(string layerId)
        {

            if (layerId == "Layer1") { return this.layer1Dictionary; }
            else if (layerId == "Layer2") { return this.layer2Dictionary; }
            else if (layerId == "Layer3") { return this.layer3Dictionary; }
            else if (layerId == "Layer4") { return this.layer4Dictionary; }
            else if (layerId == "Layer5") { return this.layer5Dictionary; }
            else if (layerId == "Overlay") { return this.overlayDictionary; }

            return null;

        }

        public string getIdentifier()
        {
            return this.layer0.identifier;
        }

    }

}