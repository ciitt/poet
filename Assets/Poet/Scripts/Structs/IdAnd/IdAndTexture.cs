﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poet
{

    [Serializable]
    public class IdAndTexture : IdentifierInterface
    {

        public string identifier = "";

        public Texture2D texture = null;

        public string getIdentifier()
        {
            return this.identifier;
        }

    }

}
