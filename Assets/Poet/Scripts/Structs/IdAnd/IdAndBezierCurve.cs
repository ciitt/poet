﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poet
{

    /// <summary>
    /// string id and BezierCurve
    /// </summary>
    [Serializable]
    public class IdAndBezierCurve : IdentifierInterface
    {

        public string identifier = "";
        public BezierCurveScript bezier = null;

        public string getIdentifier()
        {
            return this.identifier;
        }

    }

}