﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Poet
{

    /// <summary>
    /// Identifier and its position
    /// </summary>
    [Serializable]
    public class IdAndPosition : IdentifierInterface
    {

        public string identifier = "";
        public Vector3 position = Vector3.zero;

        public string getIdentifier()
        {
            return this.identifier;
        }

    }

}
