﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Poet
{

    /// <summary>
    /// Identifier and its navigation
    /// </summary>
    [Serializable]
    public class IdAndNavigation : IdentifierInterface
    {

        public string identifier = "";

        public Selectable selectOnUp = null;
        public Selectable selectOnDown = null;
        public Selectable selectOnLeft = null;
        public Selectable selectOnRight = null;

        public string getIdentifier()
        {
            return this.identifier;
        }

    }

}
