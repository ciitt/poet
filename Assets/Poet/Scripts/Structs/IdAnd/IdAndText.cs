﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poet
{

    [Serializable]
    public class IdAndText : IdentifierInterface
    {

        public string identifier = "";
        public string text = "";

        public string getIdentifier()
        {
            return this.identifier;
        }

        public IdAndText()
        {

        }

        public IdAndText(string _text, string _identifier)
        {
            this.identifier = _identifier;
            this.text = _text;
        }

    }

}
