﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poet
{

    /// <summary>
    /// SystemLanguage and csv header
    /// </summary>
    [Serializable]
    public class LanguageAndHeaders
    {

        /// <summary>
        /// SystemLanguage and header dictionary
        /// </summary>
        public Dictionary<SystemLanguage, string> languageAndHeaderDictionary = new Dictionary<SystemLanguage, string>();

        /// <summary>
        /// Languaget and its header
        /// </summary>
        [SerializeField]
        [Tooltip("Languaget and its header")]
        protected List<LanguageAndHeaderStruct> languageAndHeaders = null;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="baseString">base string</param>
        public LanguageAndHeaders(string baseString)
        {
            this.languageAndHeaders = LanguageAndHeaderStruct.createDefaultList(baseString);
        }

        /// <summary>
        /// Init dictionary
        /// </summary>
        public void initDictionary()
        {

            foreach (var val in this.languageAndHeaders)
            {

                if (!this.languageAndHeaderDictionary.ContainsKey(val.systemLanguage))
                {
                    this.languageAndHeaderDictionary.Add(val.systemLanguage, val.header);
                }

                else
                {
                    Debug.LogWarning("Already contains key : " + val.systemLanguage.ToString());
                }

            }

        }

    }

    /// <summary>
    /// SystemLanguage and csv header
    /// </summary>
    [Serializable]
    public class LanguageAndHeaderStruct
    {

        /// <summary>
        /// SystemLanguage
        /// </summary>
        public SystemLanguage systemLanguage = SystemLanguage.Unknown;

        /// <summary>
        /// Header
        /// </summary>
        public string header = "";

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="_systemLanguage">systemLanguage</param>
        /// <param name="_header">header</param>
        public LanguageAndHeaderStruct(SystemLanguage _systemLanguage, string _header)
        {
            this.systemLanguage = _systemLanguage;
            this.header = _header;
        }

        /// <summary>
        /// Default list
        /// </summary>
        /// <returns>list</returns>
        public static List<LanguageAndHeaderStruct> createDefaultList(string str)
        {

            List<LanguageAndHeaderStruct> ret = new List<LanguageAndHeaderStruct>();

            SystemLanguage previous = SystemLanguage.Unknown;

            foreach (var val in Enum.GetValues(typeof(SystemLanguage)))
            {

                if(previous != (SystemLanguage)val)
                {
                    ret.Add(new LanguageAndHeaderStruct((SystemLanguage)val, str + " (" + val.ToString() + ")"));
                }

                previous = (SystemLanguage)val;

            }

            return ret;

        }

    }

}
