﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poet
{

    /// <summary>
    /// SystemLanguage and text
    /// </summary>
    [Serializable]
    public class LanguageAndImages
    {

        /// <summary>
        /// SystemLanguage and image dictionary
        /// </summary>
        public Dictionary<SystemLanguage, Sprite> languageAndImageDictionary = new Dictionary<SystemLanguage, Sprite>();

        /// <summary>
        /// Languaget and its image
        /// </summary>
        [SerializeField]
        [Tooltip("Languaget and its image")]
        protected List<LanguageAndImageStruct> languageAndImages = null;

        /// <summary>
        /// Constructor
        /// </summary>
        public LanguageAndImages()
        {
            this.languageAndImages = LanguageAndImageStruct.createDefaultList();
        }

        /// <summary>
        /// Init dictionary
        /// </summary>
        public void initDictionary()
        {

            foreach (var val in this.languageAndImages)
            {

                if (!this.languageAndImageDictionary.ContainsKey(val.systemLanguage))
                {
                    this.languageAndImageDictionary.Add(val.systemLanguage, val.sprite);
                }

                else
                {
                    Debug.LogWarning("Already contains key : " + val.systemLanguage.ToString());
                }

            }

        }

    }

    /// <summary>
    /// SystemLanguage and image
    /// </summary>
    [Serializable]
    public class LanguageAndImageStruct
    {

        /// <summary>
        /// SystemLanguage
        /// </summary>
        public SystemLanguage systemLanguage = SystemLanguage.Unknown;

        /// <summary>
        /// Sprite
        /// </summary>
        public Sprite sprite = null;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="_systemLanguage">systemLanguage</param>
        public LanguageAndImageStruct(SystemLanguage _systemLanguage)
        {
            this.systemLanguage = _systemLanguage;
        }

        /// <summary>
        /// Default list
        /// </summary>
        /// <returns>list</returns>
        public static List<LanguageAndImageStruct> createDefaultList()
        {

            List<LanguageAndImageStruct> ret = new List<LanguageAndImageStruct>();

            SystemLanguage previous = SystemLanguage.Unknown;

            foreach (var val in Enum.GetValues(typeof(SystemLanguage)))
            {

                if (previous != (SystemLanguage)val)
                {
                    ret.Add(new LanguageAndImageStruct((SystemLanguage)val));
                }

                previous = (SystemLanguage)val;

            }

            return ret;

        }

    }

}
