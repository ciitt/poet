﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poet
{

    /// <summary>
    /// interface for AssetBundle
    /// </summary>
    public interface AssetBundleNameInterface
    {

        /// <summary>
        /// AssetBundle name
        /// </summary>
        string AssetBundleName
        {
            get;
            set;
        }

        /// <summary>
        /// AssetBundle variant
        /// </summary>
        string AssetBundleVariant
        {
            get;
            set;
        }

        /// <summary>
        /// Asset name in AssetBundle
        /// </summary>
        string AssetNameInAssetBundle
        {
            get;
            set;
        }

    }

}
