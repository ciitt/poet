﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poet
{

    /// <summary>
    /// Function name and string parameters
    /// </summary>
    [Serializable]
    public class FuncNameAndParams
    {

        /// <summary>
        /// Function name
        /// </summary>
        public string functionName = "";

        /// <summary>
        /// Parameters
        /// </summary>
        public List<string> parameters = new List<string>();

        /// <summary>
        /// Get string parameter
        /// </summary>
        /// <param name="index">parameters index</param>
        /// <param name="defaultValue">default value</param>
        /// <returns>parameter</returns>
        public string getStringParameter(int index, string defaultValue = "")
        {

            if (0 <= index && index < this.parameters.Count)
            {
                return this.parameters[index];
            }

            return defaultValue;

        }

        /// <summary>
        /// Get float parameter
        /// </summary>
        /// <param name="index">parameters index</param>
        /// <param name="defaultValue">default value</param>
        /// <returns>parameter</returns>
        public float getFloatParameter(int index, float defaultValue = 0.0f)
        {

            if (0 <= index && index < this.parameters.Count)
            {
                float ret = defaultValue;

                if (float.TryParse(this.parameters[index], out ret))
                {
                    return ret;
                }

                else
                {
                    return defaultValue;
                }

            }

            return defaultValue;

        }

        /// <summary>
        /// Get int parameter
        /// </summary>
        /// <param name="index">parameters index</param>
        /// <param name="defaultValue">default value</param>
        /// <returns>parameter</returns>
        public int getIntParameter(int index, int defaultValue = 0)
        {

            if (0 <= index && index < this.parameters.Count)
            {
                int ret = defaultValue;

                if (int.TryParse(this.parameters[index], out ret))
                {
                    return ret;
                }

                else
                {
                    return defaultValue;
                }

            }

            return defaultValue;

        }

    }

    /// <summary>
    /// Function name and string parameters list
    /// </summary>
    [Serializable]
    public class FuncNameAndParamsList
    {

        public bool preProcess = false;
        public bool mainProcess = false;

        public float invokeTime = 0.0f;

        public List<FuncNameAndParams> list = new List<FuncNameAndParams>();

        public FuncNameAndParamsList()
        {

        }

        public FuncNameAndParamsList(FuncNameAndParamsList ins)
        {

            this.preProcess = ins.preProcess;
            this.mainProcess = ins.mainProcess;
            this.invokeTime = ins.invokeTime;

            this.list.Clear();
            this.list.AddRange(ins.list);

        }

        public void clear()
        {
            this.preProcess = false;
            this.mainProcess = false;
            this.invokeTime = 0.0f;
            this.list.Clear();
        }

    }

}
