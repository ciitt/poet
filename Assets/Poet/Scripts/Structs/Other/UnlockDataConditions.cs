﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poet
{

    /// <summary>
    /// Unlock data conditions
    /// </summary>
    [Serializable]
    public class UnlockDataConditions
    {

        /// <summary>
        /// Csv name
        /// </summary>
        [Tooltip("Csv name to unlock")]
        public string csvName = "";

        /// <summary>
        /// Csv row identifier to unlock
        /// </summary>
        [Tooltip("Csv row identifier to unlock")]
        public string csvRowIdentifier = "";

        /// <summary>
        /// Is locked
        /// </summary>
        /// <returns>locked</returns>
        public bool isLocked()
        {

            if (
                !SystemManager.Instance.saveAndLoadConfigFileFlag ||
                string.IsNullOrEmpty(this.csvName) ||
                string.IsNullOrEmpty(this.csvRowIdentifier)
                )
            {
                return false;
            }

            return !SystemManager.Instance.isAlreadyRead(this.csvName, this.csvRowIdentifier);

        }

    }

}
