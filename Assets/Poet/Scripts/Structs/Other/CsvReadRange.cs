﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poet
{

    /// <summary>
    /// Read range class
    /// </summary>
    [Serializable]
    public class CsvReadRange
    {
        public string csvRowIdentifier = "";
        public int count = 0;

        public CsvReadRange(string _csvRowIdentifier)
        {
            this.csvRowIdentifier = _csvRowIdentifier;
        }

        public CsvReadRange(string _csvRowIdentifier, int _count)
        {
            this.csvRowIdentifier = _csvRowIdentifier;
            this.count = _count;
        }

        public bool isInRange(CsvReadRange target)
        {

            if(target != null)
            {
                if(this.csvRowIdentifier == target.csvRowIdentifier)
                {
                    return this.count >= target.count;
                }
            }

            return false;

        }

        public void updateCount(int newCount)
        {
            if(this.count < newCount)
            {
                this.count = newCount;
            }
        }

    }

    [Serializable]
    public class KeyAndAlreadyReadList : IdentifierInterface
    {

        public string textAssetName = "";
        public List<CsvReadRange> alreadyReadRangeList = new List<CsvReadRange>();

        public string getIdentifier()
        {
            return this.textAssetName;
        }

        public KeyAndAlreadyReadList(string _textAssetName, CsvReadRange range)
        {
            this.textAssetName = _textAssetName;
            this.alreadyReadRangeList.Add(range);
        }

        public bool isAlreadyRead(CsvReadRange range)
        {

            foreach (var val in this.alreadyReadRangeList)
            {

                if (val.isInRange(range))
                {
                    return true;
                }

            }

            return false;

        }

        public void updateAlreadyReadRange(CsvReadRange range)
        {

            foreach (var val in this.alreadyReadRangeList)
            {

                if (val.csvRowIdentifier == range.csvRowIdentifier)
                {
                    val.updateCount(range.count);
                    return;
                }

            }

            // not found
            {
                this.alreadyReadRangeList.Add(range);
            }

        }

    }

}
