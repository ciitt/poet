﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poet
{

    public class PoetUiIdentifiers
    {

        public class Common
        {

            public static string Text = "Text";
            public static string Save = "Save";
            public static string Load = "Load";
            public static string TextLog = "TextLog";
            public static string SystemConfig = "SystemConfig";
            public static string SoundConfig = "SoundConfig";
            public static string Choice = "Choice";
            public static string GoToNextOnly = "GoToNextOnly";

        }

        public class AlbumScene
        {

            public static string AlbumGraphicSelectForFirst = "AlbumGraphicSelectForFirst";
            public static string AlbumGraphicSelectForBackFromView = "AlbumGraphicSelectForBackFromView";
            public static string AlbumGraphicView = "AlbumGraphicView";
            public static string AlbumMemoryForFirst = "AlbumMemoryForFirst";
            public static string AlbumMemoryForBackFromScene = "AlbumMemoryForBackFromScene";
            public static string AlbumMusic = "AlbumMusic";

        }

    }

}
