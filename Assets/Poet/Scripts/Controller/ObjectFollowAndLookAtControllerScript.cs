﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poet
{

    /// <summary>
    /// Object controller for Sample
    /// </summary>
    public partial class ObjectFollowAndLookAtControllerScript : PoetBaseReceiverScript
    {

        /// <summary>
        /// User progress data
        /// </summary>
        [Serializable]
        protected class UserProgressData
        {

            public float resumeTimeForFollow = 0.0f;
            public float resumeTimeForLookAt = 0.0f;
            public FuncNameAndParamsList fnapListForFollow = null;
            public FuncNameAndParamsList fnapListForLookAt = null;

            public string followTargetIdentifier = "";
            public Vector3 firstPositionForBezier = Vector3.zero;

            public string lookAtTargetIdentifier = "";
            public LookAtType lookAtType = LookAtType.None;

            public void clearFollow()
            {

                this.resumeTimeForFollow = 0.0f;
                this.fnapListForFollow = null;

                this.followTargetIdentifier = "";
                this.firstPositionForBezier = Vector3.zero;

            }

            public void clearLookAt()
            {

                this.resumeTimeForLookAt = 0.0f;
                this.fnapListForLookAt = null;

                this.lookAtTargetIdentifier = "";
                this.lookAtType = LookAtType.None;

            }

        }

        /// <summary>
        /// UserProgressData
        /// </summary>
        protected UserProgressData m_userProgressData = new UserProgressData();

        /// <summary>
        /// Reference to SceneChangeState
        /// </summary>
        protected SSC.SceneChangeState m_refSceneChangeState = null;

        /// <summary>
        /// Reference to PauseState
        /// </summary>
        protected SSC.PauseState m_refPauseState = null;

        /// <summary>
        /// Current FuncNameAndParamsList
        /// </summary>
        protected FuncNameAndParamsList m_currentFnapListForFollow = new FuncNameAndParamsList();

        /// <summary>
        /// Current FuncNameAndParamsList
        /// </summary>
        protected FuncNameAndParamsList m_currentFnapListForLookAt = new FuncNameAndParamsList();

        /// <summary>
        /// initOnAwake() is called after Awake().
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected override void initOnAwake()
        {

            // PoetReduxManager
            {

                PoetReduxManager prm = PoetReduxManager.Instance as PoetReduxManager;

                this.m_refSceneChangeState = prm.SceneChangeStateWatcher.state();
                this.m_refPauseState = prm.PauseStateWatcher.state();

            }
            
        }

        /// <summary>
        /// LateUpdate
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected virtual void LateUpdate()
        {
            this.LateUpdateForFollow();
            this.LateUpdateForLookAt();
        }

        /// <summary>
        /// PoetSequenceState receiver
        /// </summary>
        /// <param name="psState">PoetSequenceState</param>
        // ------------------------------------------------------------------------------------------
        protected override void onPoetSequenceState(PoetSequenceState psState)
        {

            if (psState.stateEnum == PoetSequenceState.StateEnum.PreProcess)
            {

                if (psState.headerAndCells.ContainsKey(this.m_csvHeaderForFollow))
                {

                    this.m_currentFnapListForFollow.clear();

                    CsvParser.parseFunctionsInCell(psState.headerAndCells[this.m_csvHeaderForFollow], this.m_currentFnapListForFollow);

                    if (this.m_currentFnapListForFollow.preProcess)
                    {
                        this.solveFollowFunctions(new FuncNameAndParamsList(this.m_currentFnapListForFollow), 0.0f);
                    }

                }

                if (psState.headerAndCells.ContainsKey(this.m_csvHeaderForLookAt))
                {

                    this.m_currentFnapListForLookAt.clear();

                    CsvParser.parseFunctionsInCell(psState.headerAndCells[this.m_csvHeaderForLookAt], this.m_currentFnapListForLookAt);

                    if (this.m_currentFnapListForLookAt.preProcess)
                    {
                        this.solveLookAtFunctions(new FuncNameAndParamsList(this.m_currentFnapListForLookAt), 0.0f);
                    }

                }

            }

            else if (psState.stateEnum == PoetSequenceState.StateEnum.MainProcess)
            {

                if (psState.headerAndCells.ContainsKey(this.m_csvHeaderForFollow))
                {

                    if (!this.m_currentFnapListForFollow.preProcess)
                    {
                        this.solveFollowFunctions(new FuncNameAndParamsList(this.m_currentFnapListForFollow), 0.0f);
                    }

                }

                if (psState.headerAndCells.ContainsKey(this.m_csvHeaderForLookAt))
                {

                    if (!this.m_currentFnapListForLookAt.preProcess)
                    {
                        this.solveLookAtFunctions(new FuncNameAndParamsList(this.m_currentFnapListForLookAt), 0.0f);
                    }

                }

            }

        }

        /// <summary>
        /// PoetInitState receiver
        /// </summary>
        /// <param name="piState">PoetInitState</param>
        // ------------------------------------------------------------------------------------------
        protected override void onPoetInitState(PoetInitState piState)
        {

            this.onPoetInitStateForFollow(piState);
            this.onPoetInitStateForLookAt(piState);

        }

        /// <summary>
        /// TextShowingState receiver
        /// </summary>
        /// <param name="tsState">TextShowingState</param>
        // ------------------------------------------------------------------------------------------
        protected override void onTextShowingState(TextShowingState tsState)
        {

        }

        /// <summary>
        /// UserProgressDataSignal receiver
        /// </summary>
        /// <param name="updSignal">UserProgressDataSignal</param>
        // ------------------------------------------------------------------------------------------
        protected override void onUserProgressDataSignal(UserProgressDataSignal updSignal)
        {
            updSignal.addDataAction(this.transform, this, JsonUtility.ToJson(this.m_userProgressData));
        }

        /// <summary>
        /// SceneChangeState receiver
        /// </summary>
        /// <param name="scState">SceneChangeState</param>
        // ------------------------------------------------------------------------------------------
        protected override void onSceneChangeStateReceiver(SSC.SceneChangeState scState)
        {

            if (scState.stateEnum == SSC.SceneChangeState.StateEnum.AllStartupsDone)
            {

                PoetSceneChangeManager pscm = PoetSceneChangeManager.Instance as PoetSceneChangeManager;

                if (pscm.isLoadingCurrentSceneWithUserProgressData())
                {

                    this.m_userProgressData = pscm.getDataFromCurrentUserProgressData<UserProgressData>(this.transform, this);

                    this.setCurrentFollowTarget(this.m_userProgressData.followTargetIdentifier, false);
                    this.setCurrentLookAtTarget(this.m_userProgressData.lookAtTargetIdentifier, this.m_userProgressData.lookAtType, false);

                }

                else
                {
                    this.setCurrentFollowTarget(this.m_defaultFollowTargetIdentifier, false);
                    this.setCurrentLookAtTarget(this.m_defaultLookAtTargetIdentifier, this.m_defaultLookAtType, false);
                }

            }

            else if (scState.stateEnum == SSC.SceneChangeState.StateEnum.ScenePlaying)
            {

                PoetSceneChangeManager pscm = PoetSceneChangeManager.Instance as PoetSceneChangeManager;

                if (pscm.isLoadingCurrentSceneWithUserProgressData())
                {

                    if (this.m_userProgressData.fnapListForFollow != null)
                    {
                        this.solveFollowFunctions(this.m_userProgressData.fnapListForFollow, this.m_userProgressData.resumeTimeForFollow);
                    }

                    if (this.m_userProgressData.fnapListForLookAt != null)
                    {
                        this.solveLookAtFunctions(this.m_userProgressData.fnapListForLookAt, this.m_userProgressData.resumeTimeForLookAt);
                    }

                }

            }

        }

        /// <summary>
        /// PauseState receiver
        /// </summary>
        /// <param name="pState">PauseState</param>
        // ------------------------------------------------------------------------------------------
        protected override void onPauseStateReceiver(SSC.PauseState pState)
        {

        }

    }

}
