﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Poet
{

    /// <summary>
    /// Particle
    /// </summary>
    [RequireComponent(typeof(ParticleSystem))]
    public class ObjectParticleControllerScript : PoetBaseReceiverScript
    {

        /// <summary>
        /// User progress data
        /// </summary>
        [Serializable]
        protected class UserProgressData
        {

            public float resumeTime = 0.0f;
            public FuncNameAndParamsList fnapList = null;

            public int orderInLayer = 0;

            public void clear()
            {
                this.resumeTime = 0.0f;
                this.fnapList = null;
            }

        }

        /// <summary>
        /// Csv header
        /// </summary>
        [SerializeField]
        [Tooltip("Csv header")]
        protected string m_csvHeaderForParticle = "Particle";

        /// <summary>
        /// Identifier of this particle object
        /// </summary>
        [SerializeField]
        [Tooltip("Identifier of this particle object")]
        protected string m_thisIdentifier = "Shooting Star";

        /// <summary>
        /// UnityEvent before play
        /// </summary>
        [SerializeField]
        [Tooltip("UnityEvent before play")]
        protected UnityEvent m_eventBeforePlay = null;

        /// <summary>
        /// Reference to ParticleSystem
        /// </summary>
        protected ParticleSystem m_refParticleSystem = null;

        /// <summary>
        /// UserProgressData
        /// </summary>
        protected UserProgressData m_userProgressData = new UserProgressData();

        /// <summary>
        /// temp for stop
        /// </summary>
        protected bool m_tempForStop = false;

        /// <summary>
        /// Reference to Renderer
        /// </summary>
        protected Renderer m_refRenderer;

        /// <summary>
        /// initOnAwake() is called after Awake().
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected override void initOnAwake()
        {

            this.m_refParticleSystem = this.GetComponent<ParticleSystem>();
            this.m_refRenderer = this.m_refParticleSystem.GetComponent<Renderer>();

        }

        /// <summary>
        /// Solve functions
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected void solveFunctions(FuncNameAndParamsList fnapList, float resumeTime)
        {

            if (fnapList == null)
            {
                return;
            }

            if (string.IsNullOrEmpty(this.m_thisIdentifier))
            {
                return;
            }

            // -------------------

            string identifier = "";

            FuncNameAndParams tempFnap = null;
            bool playTrueStopFalse = false;

            foreach (var fnap in fnapList.list)
            {
                
                identifier = fnap.getStringParameter(0);

                if (fnap.functionName == "Play" && this.m_thisIdentifier == identifier)
                {
                    tempFnap = fnap;
                    playTrueStopFalse = true;
                }

                else if (fnap.functionName == "Stop" && this.m_thisIdentifier == identifier)
                {
                    tempFnap = fnap;
                    playTrueStopFalse = false;
                }

                else if (fnap.functionName == "OrderInLayer" && this.m_thisIdentifier == identifier)
                {
                    this.m_refRenderer.sortingOrder = fnap.getIntParameter(1);
                }

            }

            if(tempFnap != null)
            {
                StopAllCoroutines();
                StartCoroutine(this.startPlayOrStopParticle(tempFnap, fnapList, resumeTime, playTrueStopFalse));
            }

        }

        /// <summary>
        /// Play or stop particle
        /// </summary>
        /// <param name="fnap">FuncNameAndParams</param>
        /// <param name="fnapList">FuncNameAndParamsList</param>
        /// <param name="resumeTime">resume time</param>
        /// <returns>IEnumerator</returns>
        // ------------------------------------------------------------------------------------------
        protected IEnumerator startPlayOrStopParticle(
            FuncNameAndParams fnap,
            FuncNameAndParamsList fnapList,
            float resumeTime,
            bool playTrueStopFalse
            )
        {

            if (fnap == null || fnapList == null)
            {
                yield break;
            }

            // -------------------

            // m_userProgressData
            {
                this.m_userProgressData.resumeTime = resumeTime;
                this.m_userProgressData.fnapList = fnapList;
            }

            // wait for invoke
            {
                yield return this.waitInvokeTime(fnapList.invokeTime, resumeTime, () =>
                {
                    this.m_userProgressData.resumeTime += Time.deltaTime;
                });
                this.m_userProgressData.resumeTime = Mathf.Max(fnapList.invokeTime, resumeTime);
            }

            // Play
            {

                if(playTrueStopFalse)
                {
                    this.m_eventBeforePlay.Invoke();
                    this.m_refParticleSystem.Play();
                }

                else
                {
                    this.m_refParticleSystem.Stop();
                }
                
            }

            if (!this.m_refParticleSystem.main.loop)
            {
                this.m_userProgressData.clear();
            }

        }

        /// <summary>
        /// PoetSequenceState receiver
        /// </summary>
        /// <param name="psState">PoetSequenceState</param>
        // ------------------------------------------------------------------------------------------
        protected override void onPoetSequenceState(PoetSequenceState psState)
        {

            if (psState.stateEnum == PoetSequenceState.StateEnum.PreProcess)
            {

                if (psState.headerAndCells.ContainsKey(this.m_csvHeaderForParticle))
                {

                    this.m_currentFnapList.clear();

                    CsvParser.parseFunctionsInCell(psState.headerAndCells[this.m_csvHeaderForParticle], this.m_currentFnapList);

                    if (this.m_currentFnapList.preProcess)
                    {
                        this.solveFunctions(new FuncNameAndParamsList(this.m_currentFnapList), 0.0f);
                    }

                }

            }

            else if (psState.stateEnum == PoetSequenceState.StateEnum.MainProcess)
            {

                if (psState.headerAndCells.ContainsKey(this.m_csvHeaderForParticle))
                {

                    if (!this.m_currentFnapList.preProcess)
                    {
                        this.solveFunctions(new FuncNameAndParamsList(this.m_currentFnapList), 0.0f);
                    }

                }

            }

        }

        /// <summary>
        /// PoetInitState receiver
        /// </summary>
        /// <param name="piState">PoetInitState</param>
        // ------------------------------------------------------------------------------------------
        protected override void onPoetInitState(PoetInitState piState)
        {

#if UNITY_EDITOR

            if (piState.headerAndCells.ContainsKey(this.m_csvHeaderForParticle))
            {

                FuncNameAndParamsList fnapList = CsvParser.parseFunctionsInCell(piState.headerAndCells[this.m_csvHeaderForParticle]);

                foreach (var val in fnapList.list)
                {

                    if (
                        val.functionName != "Play" &&
                        val.functionName != "Stop" &&
                        val.functionName != "OrderInLayer"
                        )
                    {

                        Debug.LogWarning(
                            "Unknown function name : " +
                            val.functionName + " : " +
                            this.m_csvHeaderForParticle + " : " +
                            piState.textAssetName + " : " +
                            piState.index
                            );

                    }

                }

            }

#endif

        }

        /// <summary>
        /// TextShowingState receiver
        /// </summary>
        /// <param name="tsState">TextShowingState</param>
        // ------------------------------------------------------------------------------------------
        protected override void onTextShowingState(TextShowingState tsState)
        {

        }

        /// <summary>
        /// UserProgressDataSignal receiver
        /// </summary>
        /// <param name="updSignal">UserProgressDataSignal</param>
        // ------------------------------------------------------------------------------------------
        protected override void onUserProgressDataSignal(UserProgressDataSignal updSignal)
        {

            this.m_userProgressData.orderInLayer = this.m_refRenderer.sortingOrder;

            updSignal.addDataAction(this.transform, this, JsonUtility.ToJson(this.m_userProgressData));

        }

        /// <summary>
        /// SceneChangeState receiver
        /// </summary>
        /// <param name="scState">SceneChangeState</param>
        // ------------------------------------------------------------------------------------------
        protected override void onSceneChangeStateReceiver(SSC.SceneChangeState scState)
        {

            if (scState.stateEnum == SSC.SceneChangeState.StateEnum.AllStartupsDone)
            {

                PoetSceneChangeManager pscm = PoetSceneChangeManager.Instance as PoetSceneChangeManager;

                if (pscm.isLoadingCurrentSceneWithUserProgressData())
                {

                    this.m_userProgressData = pscm.getDataFromCurrentUserProgressData<UserProgressData>(this.transform, this);

                    this.m_refRenderer.sortingOrder = this.m_userProgressData.orderInLayer;

                }

            }

            else if (scState.stateEnum == SSC.SceneChangeState.StateEnum.ScenePlaying)
            {

                PoetSceneChangeManager pscm = PoetSceneChangeManager.Instance as PoetSceneChangeManager;

                if (pscm.isLoadingCurrentSceneWithUserProgressData())
                {

                    if (this.m_userProgressData.fnapList != null)
                    {
                        this.solveFunctions(this.m_userProgressData.fnapList, this.m_userProgressData.resumeTime);
                    }
                    
                }

            }

        }

        /// <summary>
        /// PauseState receiver
        /// </summary>
        /// <param name="pState">PauseState</param>
        // ------------------------------------------------------------------------------------------
        protected override void onPauseStateReceiver(SSC.PauseState pState)
        {

            if (pState.pause && this.m_refParticleSystem.isPlaying)
            {
                this.m_tempForStop = !this.m_refParticleSystem.isEmitting;
                this.m_refParticleSystem.Pause();
            }

            else if (!pState.pause && this.m_refParticleSystem.isPaused)
            {
                
                this.m_refParticleSystem.Play();

                if(this.m_tempForStop)
                {
                    this.m_refParticleSystem.Stop();
                }

                this.m_tempForStop = false;

            }

        }

        /// <summary>
        /// Reset
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected override void Reset()
        {

            this.m_receiverMask =
                ReceiverMask.UsePoetSequenceStateReceiver |
                ReceiverMask.UsePoetInitStateReceiver |
                ReceiverMask.UseUserProgressDataSignalReceiver |
                ReceiverMask.UseSceneChangeStateReceiver |
                ReceiverMask.UsePauseStateReceiver
                ;

        }

    }

}
