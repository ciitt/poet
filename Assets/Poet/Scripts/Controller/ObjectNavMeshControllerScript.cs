﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace Poet
{

    /// <summary>
    /// Controller for NavMeshAgent
    /// </summary>
    [RequireComponent(typeof(NavMeshAgent))]
    public class ObjectNavMeshControllerScript : PoetBaseReceiverScript
    {

        /// <summary>
        /// User progress data
        /// </summary>
        [Serializable]
        protected class UserProgressData
        {

            public float resumeTime = 0.0f;
            public FuncNameAndParamsList fnapList = null;

            public void clear()
            {
                this.resumeTime = 0.0f;
                this.fnapList = null;
            }

        }

        /// <summary>
        /// Csv header for NavMesh
        /// </summary>
        [SerializeField]
        [Tooltip("Csv header for NavMesh")]
        protected string m_csvHeaderForNavMesh = "CharacterA NavMesh";

        /// <summary>
        /// Reference to NavMeshAgent
        /// </summary>
        protected NavMeshAgent m_refNavMeshAgent = null;

        /// <summary>
        /// Reference to Animator
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to Animator")]
        protected Animator m_refAnimator = null;

        /// <summary>
        /// Animator walk parameter name
        /// </summary>
        [SerializeField]
        [Tooltip("Animator walk parameter name")]
        protected string m_animatorWalkParamName = "IdleWalkRunSpeed";

        /// <summary>
        /// UserProgressData
        /// </summary>
        protected UserProgressData m_userProgressData = new UserProgressData();

        /// <summary>
        /// NavMeshAgent velocity at stop
        /// </summary>
        protected Vector3 m_velocityAtStop = Vector3.zero;

        /// <summary>
        /// Previous NavMeshAgent isStopped
        /// </summary>
        protected bool m_previousIsStopped = false;

        /// <summary>
        /// Original NavMeshAgent speed
        /// </summary>
        protected float m_oriNavMeshAgentSpeed = 1.0f;

        /// <summary>
        /// initOnAwake() is called after Awake().
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected override void initOnAwake()
        {

            {
                this.m_refNavMeshAgent = this.GetComponent<NavMeshAgent>();
                this.m_oriNavMeshAgentSpeed = this.m_refNavMeshAgent.speed;
            }

        }

        /// <summary>
        /// Update
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected virtual void Update()
        {

            if(this.m_refAnimator && this.m_refNavMeshAgent && !string.IsNullOrEmpty(this.m_animatorWalkParamName))
            {

                if (!this.m_refNavMeshAgent.isStopped && !this.m_previousIsStopped)
                {
                    this.m_refAnimator.SetFloat(this.m_animatorWalkParamName, this.m_refNavMeshAgent.velocity.sqrMagnitude);
                }

                this.m_previousIsStopped = this.m_refNavMeshAgent.isStopped;

            }

        }

        /// <summary>
        /// Solve functions
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected void solveFunctions(FuncNameAndParamsList fnapList, float resumeTime)
        {

            if (fnapList == null)
            {
                return;
            }

            // -------------------

            foreach (var fnap in fnapList.list)
            {
                
                if (fnap.functionName == "SetDestination")
                {
                    StopAllCoroutines();
                    StartCoroutine(this.startSetDestination(fnap, fnapList, resumeTime));
                    break;
                }

            }

        }

        /// <summary>
        /// Start setDestinationIE
        /// </summary>
        /// <param name="fnap">FuncNameAndParams</param>
        /// <param name="fnapList">FuncNameAndParamsList</param>
        /// <param name="resumeTime">resume time</param>
        // ------------------------------------------------------------------------------------------
        protected IEnumerator startSetDestination(FuncNameAndParams fnap, FuncNameAndParamsList fnapList, float resumeTime)
        {

            if (fnap == null || fnapList == null)
            {
                yield break;
            }

            // -------------------

            // m_userProgressData
            {
                this.m_userProgressData.resumeTime = resumeTime;
                this.m_userProgressData.fnapList = fnapList;
            }

            // wait for invoke
            {
                yield return this.waitInvokeTime(fnapList.invokeTime, resumeTime, () =>
                {
                    this.m_userProgressData.resumeTime += Time.deltaTime;
                });
                this.m_userProgressData.resumeTime = Mathf.Max(fnapList.invokeTime, resumeTime);
            }

            this.setDestination(
                fnap.getStringParameter(0),
                fnap.getFloatParameter(1, this.m_oriNavMeshAgentSpeed)
                );

            // m_userProgressData
            {
                this.m_userProgressData.clear();
            }

        }

        /// <summary>
        /// SetDestination 
        /// </summary>
        /// <param name="destination">destination</param>
        /// <param name="navMeshAgentSpeed">NavMeshAgent speed</param>
        // ------------------------------------------------------------------------------------------
        protected void setDestination(string destination, float navMeshAgentSpeed)
        {

            if (this.m_refNavMeshAgent)
            {

                this.m_refNavMeshAgent.speed = navMeshAgentSpeed;

                if (PositionAndRotateTargetManager.isAvailable())
                {
                    this.m_refNavMeshAgent.SetDestination(PositionAndRotateTargetManager.Instance.getTargetTransformPosition(destination));
                }
                
            }
            
        }

        /// <summary>
        /// PoetSequenceState receiver
        /// </summary>
        /// <param name="psState">PoetSequenceState</param>
        // ------------------------------------------------------------------------------------------
        protected override void onPoetSequenceState(PoetSequenceState psState)
        {

            if (psState.stateEnum == PoetSequenceState.StateEnum.PreProcess)
            {

                if (psState.headerAndCells.ContainsKey(this.m_csvHeaderForNavMesh))
                {

                    this.m_currentFnapList.clear();

                    CsvParser.parseFunctionsInCell(psState.headerAndCells[this.m_csvHeaderForNavMesh], this.m_currentFnapList);

                    if (this.m_currentFnapList.preProcess)
                    {
                        this.solveFunctions(new FuncNameAndParamsList(this.m_currentFnapList), 0.0f);
                    }

                }

            }

            else if (psState.stateEnum == PoetSequenceState.StateEnum.MainProcess)
            {

                if (psState.headerAndCells.ContainsKey(this.m_csvHeaderForNavMesh))
                {

                    if (!this.m_currentFnapList.preProcess)
                    {
                        this.solveFunctions(new FuncNameAndParamsList(this.m_currentFnapList), 0.0f);
                    }

                }

            }

        }

        /// <summary>
        /// PoetInitState receiver
        /// </summary>
        /// <param name="piState">PoetInitState</param>
        // ------------------------------------------------------------------------------------------
        protected override void onPoetInitState(PoetInitState piState)
        {

#if UNITY_EDITOR

            if (piState.headerAndCells.ContainsKey(this.m_csvHeaderForNavMesh))
            {

                FuncNameAndParamsList fnapList = CsvParser.parseFunctionsInCell(piState.headerAndCells[this.m_csvHeaderForNavMesh]);

                foreach (var val in fnapList.list)
                {

                    if (
                        val.functionName != "SetDestination"
                        )
                    {

                        Debug.LogWarning(
                            "Unknown function name : " +
                            val.functionName + " : " +
                            this.m_csvHeaderForNavMesh + " : " +
                            piState.textAssetName + " : " +
                            piState.index
                            );

                    }

                }

            }

#endif

        }

        /// <summary>
        /// TextShowingState receiver
        /// </summary>
        /// <param name="tsState">TextShowingState</param>
        // ------------------------------------------------------------------------------------------
        protected override void onTextShowingState(TextShowingState tsState)
        {

        }

        /// <summary>
        /// UserProgressDataSignal receiver
        /// </summary>
        /// <param name="updSignal">UserProgressDataSignal</param>
        // ------------------------------------------------------------------------------------------
        protected override void onUserProgressDataSignal(UserProgressDataSignal updSignal)
        {
            updSignal.addDataAction(this.transform, this, JsonUtility.ToJson(this.m_userProgressData));
        }

        /// <summary>
        /// SceneChangeState receiver
        /// </summary>
        /// <param name="scState">SceneChangeState</param>
        // ------------------------------------------------------------------------------------------
        protected override void onSceneChangeStateReceiver(SSC.SceneChangeState scState)
        {

            if (scState.stateEnum == SSC.SceneChangeState.StateEnum.AllStartupsDone)
            {

                PoetSceneChangeManager pscm = PoetSceneChangeManager.Instance as PoetSceneChangeManager;

                if (pscm.isLoadingCurrentSceneWithUserProgressData())
                {

                    this.m_userProgressData = pscm.getDataFromCurrentUserProgressData<UserProgressData>(this.transform, this);

                }

            }

            else if (scState.stateEnum == SSC.SceneChangeState.StateEnum.ScenePlaying)
            {

                PoetSceneChangeManager pscm = PoetSceneChangeManager.Instance as PoetSceneChangeManager;

                if (pscm.isLoadingCurrentSceneWithUserProgressData())
                {

                    if (this.m_userProgressData.fnapList != null)
                    {
                        this.solveFunctions(this.m_userProgressData.fnapList, this.m_userProgressData.resumeTime);
                    }

                }

            }

        }

        /// <summary>
        /// PauseState receiver
        /// </summary>
        /// <param name="pState">PauseState</param>
        // ------------------------------------------------------------------------------------------
        protected override void onPauseStateReceiver(SSC.PauseState pState)
        {

            if (this.m_refAnimator)
            {
                this.m_refAnimator.speed = (pState.pause) ? 0.0f : 1.0f;
            }

        }

    }

}
