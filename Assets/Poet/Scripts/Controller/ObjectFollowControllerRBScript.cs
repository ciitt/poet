﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poet
{

    /// <summary>
    /// Object controller for Follow
    /// </summary>
    [RequireComponent(typeof(Rigidbody))]
    public class ObjectFollowControllerRBScript : ObjectFollowAndLookAtControllerScript
    {

        [Space(10.0f)]

        /// <summary>
        /// Max speed to follow
        /// </summary>
        [SerializeField]
        [Tooltip("Max speed to follow")]
        protected float m_maxSpeed = 5.0f;

        /// <summary>
        /// Min distance to follow
        /// </summary>
        [SerializeField]
        [Tooltip("Min distance to follow")]
        protected float m_minDistance = 0.5f;

        /// <summary>
        /// Max distance to follow
        /// </summary>
        [SerializeField]
        [Tooltip("Max distance to follow")]
        protected float m_maxDistance = 3.0f;

        /// <summary>
        /// Reference to Rigidbody
        /// </summary>
        protected Rigidbody m_refRigidbody = null;

        /// <summary>
        /// Original isKinematic
        /// </summary>
        protected bool m_oriIsKinematic = false;

        /// <summary>
        /// initOnAwake() is called after Awake().
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected override void initOnAwake()
        {

            base.initOnAwake();

            // m_refRigidbody
            {
                this.m_refRigidbody = this.GetComponent<Rigidbody>();
            }

            // m_oriIsKinematic
            {
                this.m_oriIsKinematic = this.m_refRigidbody.isKinematic;
            }

        }

        /// <summary>
        /// PoetSequenceState receiver
        /// </summary>
        /// <param name="psState">PoetSequenceState</param>
        // ------------------------------------------------------------------------------------------
        protected override void onPoetSequenceState(PoetSequenceState psState)
        {

            base.onPoetSequenceState(psState);

            if (psState.stateEnum == PoetSequenceState.StateEnum.StartOperation)
            {
                this.m_refRigidbody.isKinematic = true;
            }

            else if (psState.stateEnum == PoetSequenceState.StateEnum.FinishOperation)
            {
                this.m_refRigidbody.isKinematic = this.m_oriIsKinematic;
            }

        }

        /// <summary>
        /// FixedUpdate
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected virtual void FixedUpdate()
        {

            if (this.m_refRigidbody.isKinematic)
            {
                return;
            }

            if (this.m_refPauseState.pause || this.m_refSceneChangeState.stateEnum != SSC.SceneChangeState.StateEnum.ScenePlaying)
            {
                return;
            }

            if (!this.m_refCurrentFollowTarget)
            {
                return;
            }

            // --------------------------

            //
            {

                Vector3 diff = this.m_refCurrentFollowTarget.position - this.m_refRigidbody.position;
                Vector3 dir = diff.normalized;

                float distance = diff.magnitude;

                this.m_refRigidbody.velocity =
                    dir *
                    this.m_maxSpeed *
                    Mathf.InverseLerp(this.m_minDistance, this.m_maxDistance, distance)
                    ;

            }

        }

        /// <summary>
        /// LateUpdate for follow
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected override void LateUpdateForFollow()
        {

            if(this.m_refRigidbody.isKinematic)
            {
                base.LateUpdateForFollow();
            }

        }

    }

}
