﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poet
{

    /// <summary>
    /// Poet base receiver class
    /// </summary>
    public abstract class PoetBaseReceiverScript : MonoBehaviour
    {

        [System.Flags]
        public enum ReceiverMask
        {

            UsePoetSequenceStateReceiver = 1 << 0,
            UsePoetInitStateReceiver = 1 << 1,
            UseTextShowingStateReceiver = 1 << 2,
            UseUserProgressDataSignalReceiver = 1 << 3,
            UseSceneChangeStateReceiver = 1 << 4,
            UsePauseStateReceiver = 1 << 5,

        }

        /// <summary>
        /// Receive mask
        /// </summary>
        [SerializeField]
        [SSC.BiMask]
        protected ReceiverMask m_receiverMask;

        /// <summary>
        /// Current FuncNameAndParamsList
        /// </summary>
        protected FuncNameAndParamsList m_currentFnapList = new FuncNameAndParamsList();

        // ------------------------------------------------------------------------------------------

        /// <summary>
        /// Receive mask
        /// </summary>
        public ReceiverMask receiverMask { get { return this.m_receiverMask; } }

        // ------------------------------------------------------------------------------------------

        /// <summary>
        /// initOnAwake() is called after Awake().
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected abstract void initOnAwake();

        /// <summary>
        /// PoetSequenceState receiver
        /// </summary>
        /// <param name="psState">PoetSequenceState</param>
        // ------------------------------------------------------------------------------------------
        protected abstract void onPoetSequenceState(PoetSequenceState psState);

        /// <summary>
        /// PoetInitState receiver
        /// </summary>
        /// <param name="piState">PoetInitState</param>
        // ------------------------------------------------------------------------------------------
        protected abstract void onPoetInitState(PoetInitState piState);

        /// <summary>
        /// TextShowingState receiver
        /// </summary>
        /// <param name="tsState">TextShowingState</param>
        // ------------------------------------------------------------------------------------------
        protected abstract void onTextShowingState(TextShowingState tsState);

        /// <summary>
        /// UserProgressDataSignal receiver
        /// </summary>
        /// <param name="updSignal">UserProgressDataSignal</param>
        // ------------------------------------------------------------------------------------------
        protected abstract void onUserProgressDataSignal(UserProgressDataSignal updSignal);

        /// <summary>
        /// SceneChangeState receiver
        /// </summary>
        /// <param name="scState">SceneChangeState</param>
        // ------------------------------------------------------------------------------------------
        protected abstract void onSceneChangeStateReceiver(SSC.SceneChangeState scState);

        /// <summary>
        /// PauseState receiver
        /// </summary>
        /// <param name="pState">PauseState</param>
        // ------------------------------------------------------------------------------------------
        protected abstract void onPauseStateReceiver(SSC.PauseState pState);

        /// <summary>
        /// Reset
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected virtual void Reset()
        {

            this.m_receiverMask =
                ReceiverMask.UsePoetSequenceStateReceiver |
                ReceiverMask.UsePoetInitStateReceiver |
                ReceiverMask.UseUserProgressDataSignalReceiver |
                ReceiverMask.UseSceneChangeStateReceiver
                ;

        }

        /// <summary>
        /// Awake
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected virtual void Awake()
        {

            PoetReduxManager prm = PoetReduxManager.Instance as PoetReduxManager;

            if((this.m_receiverMask & ReceiverMask.UsePoetSequenceStateReceiver)==ReceiverMask.UsePoetSequenceStateReceiver)
            {
                prm.addPoetSequenceStateReceiver(this.onPoetSequenceState);
            }

            if ((this.m_receiverMask & ReceiverMask.UsePoetInitStateReceiver) == ReceiverMask.UsePoetInitStateReceiver)
            {
                prm.addPoetInitStateReceiver(this.onPoetInitState);
            }

            if ((this.m_receiverMask & ReceiverMask.UseTextShowingStateReceiver) == ReceiverMask.UseTextShowingStateReceiver)
            {
                prm.addTextFinishedStateReceiver(this.onTextShowingState);
            }

            if ((this.m_receiverMask & ReceiverMask.UseUserProgressDataSignalReceiver) == ReceiverMask.UseUserProgressDataSignalReceiver)
            {
                prm.addUserProgressDataSignalReceiver(this.onUserProgressDataSignal);
            }

            if ((this.m_receiverMask & ReceiverMask.UseSceneChangeStateReceiver) == ReceiverMask.UseSceneChangeStateReceiver)
            {
                prm.addSceneChangeStateReceiver(this.onSceneChangeStateReceiver);
            }

            if ((this.m_receiverMask & ReceiverMask.UsePauseStateReceiver) == ReceiverMask.UsePauseStateReceiver)
            {
                prm.addPauseStateReceiver(this.onPauseStateReceiver);
            }

#if UNITY_EDITOR

            bool temp1 = ((this.m_receiverMask & ReceiverMask.UseUserProgressDataSignalReceiver) == ReceiverMask.UseUserProgressDataSignalReceiver);
            bool temp2 = ((this.m_receiverMask & ReceiverMask.UseSceneChangeStateReceiver) == ReceiverMask.UseSceneChangeStateReceiver);

            if(temp1 && !temp2)
            {
                Debug.LogWarning("(#if UNITY_EDITOR) UseUserProgressDataSignalReceiver && !UseSceneChangeStateReceiver : " + this.gameObject.name);
            }

            if(this.m_receiverMask == 0 && this.useReceiverMask0Warning())
            {
                Debug.LogWarning("(#if UNITY_EDITOR) m_receiverMask == 0 : " + this.gameObject.name);
            }

#endif

            this.initOnAwake();

        }

#if UNITY_EDITOR

        /// <summary>
        /// Use m_receiverMask == 0 warning
        /// </summary>
        /// <returns>use</returns>
        // ------------------------------------------------------------------------------------------
        protected virtual bool useReceiverMask0Warning()
        {
            return true;
        }

#endif

        /// <summary>
        /// Wait for invoke time
        /// </summary>
        /// <param name="invokeTime">invoke time</param>
        /// <param name="resumeTime">resume time</param>
        /// <param name="anyAction">Action</param>
        /// <returns>IEnumerator</returns>
        // ------------------------------------------------------------------------------------------
        protected virtual IEnumerator waitInvokeTime(float invokeTime, float resumeTime, Action anyAction)
        {

            float timer = resumeTime;

            var pauseState = PoetReduxManager.Instance.PauseStateWatcher.state();

            while (timer < invokeTime)
            {

                // break
                {
                    if (PoetOneSceneManager.isAvailable() && PoetOneSceneManager.Instance.shouldSkip())
                    {
                        break;
                    }
                }

                // pauseState
                {
                    if (pauseState.pause)
                    {
                        yield return null;
                        continue;
                    }
                }

                // timer
                {
                    timer += Time.deltaTime;
                }

                // anyAction
                {
                    if(anyAction != null)
                    {
                        anyAction();
                    }
                }

                yield return null;

            }

        }

    }

}
