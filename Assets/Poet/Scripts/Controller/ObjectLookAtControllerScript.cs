﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poet
{

    /// <summary>
    /// Object controller for LookAt
    /// </summary>
    public partial class ObjectFollowAndLookAtControllerScript : PoetBaseReceiverScript
    {

        protected enum LookAtType
        {
            None,
            LookAt,
            LookAtOnlyY,
            SyncWithForwardDirection
        }

        /// <summary>
        /// Csv header for LookAt
        /// </summary>
        [SerializeField]
        [Tooltip("Csv header for LookAt")]
        protected string m_csvHeaderForLookAt = "CharacterA LookAt";

        /// <summary>
        /// Identifier for default target to look at
        /// </summary>
        [SerializeField]
        [Tooltip("Identifier for default target to look at")]
        protected string m_defaultLookAtTargetIdentifier = "";

        /// <summary>
        /// Default LookAtType
        /// </summary>
        [SerializeField]
        [Tooltip("Default LookAtType")]
        protected LookAtType m_defaultLookAtType = LookAtType.None;

        /// <summary>
        /// LookAt speed
        /// </summary>
        [SerializeField]
        [Tooltip("LookAt speed")]
        [Range(0.0f, 1.0f)]
        protected float m_lookAtSpeed = 0.1f;

        /// <summary>
        /// Reference to current target to look at
        /// </summary>
        protected Transform m_refCurrentLookAtTarget = null;

        /// <summary>
        /// Current LookAtType
        /// </summary>
        protected LookAtType m_currentLookAtType = LookAtType.LookAt;

        /// <summary>
        /// IEnumerator for LookAt
        /// </summary>
        protected IEnumerator m_lookAtIE = null;

        /// <summary>
        /// LateUpdate for lookat
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected virtual void LateUpdateForLookAt()
        {

            if (!this.m_refCurrentLookAtTarget || this.m_refPauseState.pause || this.m_refSceneChangeState.stateEnum != SSC.SceneChangeState.StateEnum.ScenePlaying)
            {
                return;
            }

            // --------------------------

            Vector3 temp = Vector3.zero;

            if (this.m_currentLookAtType == LookAtType.LookAt)
            {

                temp = this.m_refCurrentLookAtTarget.position - this.transform.position;

                if (temp != Vector3.zero)
                {

                    this.transform.rotation = Quaternion.Slerp(
                        this.transform.rotation,
                        Quaternion.LookRotation(temp),
                        this.m_lookAtSpeed
                        );

                }

            }

            else if (this.m_currentLookAtType == LookAtType.LookAtOnlyY)
            {

                temp = this.m_refCurrentLookAtTarget.position - this.transform.position;

                temp.y = 0.0f;

                if (temp != Vector3.zero)
                {

                    this.transform.rotation = Quaternion.Slerp(
                        this.transform.rotation,
                        Quaternion.LookRotation(temp),
                        this.m_lookAtSpeed
                        );

                }

            }

            else if (this.m_currentLookAtType == LookAtType.SyncWithForwardDirection)
            {

                this.transform.rotation = Quaternion.Slerp(
                    this.transform.rotation,
                    Quaternion.LookRotation(this.m_refCurrentLookAtTarget.forward),
                    this.m_lookAtSpeed
                    );

            }

        }

        /// <summary>
        /// Set current lookat target
        /// </summary>
        /// <param name="lookAtTargetIdentifier">lookAtTargetIdentifier</param>
        /// <param name="immediately">set immediately</param>
        // ------------------------------------------------------------------------------------------
        protected virtual void setCurrentLookAtTarget(string lookAtTargetIdentifier, LookAtType lookAtType, bool immediately)
        {

            if (!PositionAndRotateTargetManager.isAvailable())
            {
                return;
            }

            // ------------

            // m_userProgressData
            {
                this.m_userProgressData.lookAtTargetIdentifier = lookAtTargetIdentifier;
                this.m_userProgressData.lookAtType = lookAtType;
            }

            // set
            {

                this.m_refCurrentLookAtTarget =
                    (!string.IsNullOrEmpty(lookAtTargetIdentifier)) ?
                    PositionAndRotateTargetManager.Instance.getTargetTransform(lookAtTargetIdentifier) :
                    null
                    ;

                this.m_currentLookAtType = lookAtType;

            }

            if (this.m_refCurrentLookAtTarget && immediately)
            {

                Vector3 temp = Vector3.zero;

                if (lookAtType == LookAtType.LookAt)
                {

                    temp = this.m_refCurrentLookAtTarget.position - this.transform.position;

                    if (temp != Vector3.zero)
                    {
                        this.transform.rotation = Quaternion.LookRotation(temp);
                    }

                }

                else if(lookAtType == LookAtType.LookAtOnlyY)
                {

                    temp = this.m_refCurrentLookAtTarget.position - this.transform.position;

                    temp.y = 0.0f;

                    if (temp != Vector3.zero)
                    {
                        this.transform.rotation = Quaternion.LookRotation(temp);
                    }

                }

                else if (lookAtType == LookAtType.SyncWithForwardDirection)
                {
                    this.transform.forward = this.m_refCurrentLookAtTarget.forward;
                }

            }

        }

        /// <summary>
        /// Stop lookat coroutine
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected void stopLookAtCoroutine()
        {

            if (this.m_lookAtIE != null)
            {
                StopCoroutine(this.m_lookAtIE);
            }

        }

        /// <summary>
        /// Solve functions for lookat
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected void solveLookAtFunctions(FuncNameAndParamsList fnapList, float resumeTime)
        {

            if (fnapList == null)
            {
                return;
            }

            // -------------------

            foreach (var fnap in fnapList.list)
            {

                if (
                    fnap.functionName == "LookAtTransform" ||
                    fnap.functionName == "LookAtTransformOnlyY" ||
                    fnap.functionName == "LookAtForward" ||
                    fnap.functionName == "LookAtBezierHandleDirection" ||
                    fnap.functionName == "LookAtBezierTangent" ||
                    fnap.functionName == "Stop" ||
                    fnap.functionName == "Resume"
                    )
                {
                    this.stopLookAtCoroutine();
                    StartCoroutine(this.m_lookAtIE = this.startLookAt(fnapList.list[0], fnapList, resumeTime));
                    break;
                }

            }

        }

        /// <summary>
        /// Start LookAt
        /// </summary>
        /// <param name="fnap">FuncNameAndParams</param>
        /// <param name="fnapList">FuncNameAndParamsList</param>
        /// <param name="resumeTime">resume time</param>
        /// <returns>IEnumerator</returns>
        // ------------------------------------------------------------------------------------------
        protected IEnumerator startLookAt(FuncNameAndParams fnap, FuncNameAndParamsList fnapList, float resumeTime)
        {

            if (fnap == null || fnapList == null)
            {
                yield break;
            }

            // -------------------

            // m_userProgressData
            {
                this.m_userProgressData.resumeTimeForLookAt = resumeTime;
                this.m_userProgressData.fnapListForLookAt = fnapList;
            }

            // wait for invoke
            {
                yield return this.waitInvokeTime(fnapList.invokeTime, resumeTime, () =>
                {
                    this.m_userProgressData.resumeTimeForLookAt += Time.deltaTime;
                });
                this.m_userProgressData.resumeTimeForLookAt = Mathf.Max(fnapList.invokeTime, resumeTime);
            }

            //
            {

                if(fnap.functionName == "LookAtBezierHandleDirection")
                {

                    this.setCurrentLookAtTarget("", LookAtType.None, false);

                    StartCoroutine(this.m_lookAtIE = this.lookAtBezier(
                        fnap.getStringParameter(0),
                        fnap.getFloatParameter(1, 1.0f),
                        fnap.getFloatParameter(2, 1.0f),
                        fnap.getStringParameter(3, "Linear"),
                        "HandleDirection",
                        fnapList.invokeTime,
                        resumeTime
                        ));

                }

                else if (fnap.functionName == "LookAtBezierTangent")
                {

                    this.setCurrentLookAtTarget("", LookAtType.None, false);

                    StartCoroutine(this.m_lookAtIE = this.lookAtBezier(
                        fnap.getStringParameter(0),
                        fnap.getFloatParameter(1, 1.0f),
                        fnap.getFloatParameter(2, 1.0f),
                        fnap.getStringParameter(3, "Linear"),
                        "Tangent",
                        fnapList.invokeTime,
                        resumeTime
                        ));

                }

                else
                {

                    this.m_lookAtIE = null;

                    // m_userProgressData
                    {
                        this.m_userProgressData.clearLookAt();
                    }

                    this.lookAt(fnap);

                }

            }

        }

        /// <summary>
        /// FollowBezierFromTo IEnumerator
        /// </summary>
        /// <param name="bezierIdentifier">bezier identifier</param>
        /// <param name="secondsFromCurrentPosToStartPos">time to current pos to start pos</param>
        /// <param name="secondsFromStartPosToEndPos">time to start pos to end pos</param>
        /// <param name="easing">easing</param>
        /// <param name="lookAtType">lookat type</param>
        /// <param name="invokeTime">delay seconds</param>
        /// <param name="resumeTime">resume seconds</param>
        /// <returns>IEnumerator</returns>
        // ------------------------------------------------------------------------------------------
        protected virtual IEnumerator lookAtBezier(
            string bezierIdentifier,
            float secondsFromCurrentPosToStartPos,
            float secondsFromStartPosToEndPos,
            string easing,
            string lookAtType,
            float invokeTime,
            float resumeTime
            )
        {

            if (!BezierCurveManager.isAvailable())
            {
                this.m_lookAtIE = null;
                yield break;
            }

            BezierCurveScript bezier = BezierCurveManager.Instance.getBezierCurveScript(bezierIdentifier);

            if (!bezier)
            {
                this.m_lookAtIE = null;
                yield break;
            }

            // ------------------------------------

            float timer = 0.0f;

            Quaternion fromRotation = Quaternion.identity;

            var pauseState = PoetReduxManager.Instance.PauseStateWatcher.state();

            BezierPoint bezierPoint = null;

            float val = 0.0f;

            // secondsFromCurrentPosToStartPos
            {

                timer = Mathf.Max(resumeTime - invokeTime, 0.0f);
                this.m_userProgressData.resumeTimeForLookAt = Mathf.Max(invokeTime, resumeTime);

                bezierPoint = bezier.pointAtBezierCurveDetail(0.0f);

                fromRotation = this.transform.rotation;

                while (timer < secondsFromCurrentPosToStartPos)
                {

                    // break
                    {
                        if (PoetOneSceneManager.isAvailable() && PoetOneSceneManager.Instance.shouldSkip())
                        {
                            break;
                        }
                    }

                    // pauseState
                    {
                        if (pauseState.pause)
                        {
                            yield return null;
                            continue;
                        }
                    }

                    // val
                    {
                        val = PoetAnimationCurveManager.Instance.evaluate(easing, timer / secondsFromCurrentPosToStartPos);
                    }

                    // lookAtType
                    {

                        if (lookAtType == "HandleDirection")
                        {
                            this.transform.rotation = Quaternion.Slerp(fromRotation, bezierPoint.rotation, val);
                        }

                        else if (lookAtType == "Tangent")
                        {
                            this.transform.rotation = Quaternion.Slerp(
                                fromRotation,
                                Quaternion.LookRotation(bezierPoint.tangent, Vector3.up),
                                val
                                );
                        }

                    }

                    // timer
                    {
                        timer += Time.deltaTime;
                        this.m_userProgressData.resumeTimeForLookAt += Time.deltaTime;
                    }

                    yield return null;

                }

                // finish
                {

                    if (lookAtType == "HandleDirection")
                    {
                        this.transform.rotation = bezierPoint.rotation;
                    }

                    else if (lookAtType == "Tangent")
                    {
                        this.transform.rotation = Quaternion.LookRotation(bezierPoint.tangent, Vector3.up);
                    }

                }

            }

            // secondsFromStartPosToEndPos
            {

                timer = Mathf.Max(resumeTime - invokeTime - secondsFromCurrentPosToStartPos, 0.0f);
                this.m_userProgressData.resumeTimeForLookAt = Mathf.Max(invokeTime + secondsFromCurrentPosToStartPos, resumeTime);

                while (timer < secondsFromStartPosToEndPos)
                {

                    // break
                    {
                        if (PoetOneSceneManager.isAvailable() && PoetOneSceneManager.Instance.shouldSkip())
                        {
                            break;
                        }
                    }

                    // pauseState
                    {
                        if (pauseState.pause)
                        {
                            yield return null;
                            continue;
                        }
                    }

                    // val
                    {
                        val = PoetAnimationCurveManager.Instance.evaluate(easing, timer / secondsFromStartPosToEndPos);
                    }

                    bezierPoint = bezier.pointAtBezierCurveDetail(val);

                    // lookAtType
                    {

                        if (lookAtType == "HandleDirection")
                        {
                            this.transform.rotation = bezierPoint.rotation;
                        }

                        else if (lookAtType == "Tangent")
                        {
                            this.transform.rotation = Quaternion.LookRotation(bezierPoint.tangent, Vector3.up);
                        }

                    }

                    // timer
                    {
                        timer += Time.deltaTime;
                        this.m_userProgressData.resumeTimeForLookAt += Time.deltaTime;
                    }

                    yield return null;

                }

            }

            // finish resumeTime
            {

                this.m_userProgressData.resumeTimeForLookAt =
                    invokeTime +
                    secondsFromCurrentPosToStartPos +
                    secondsFromStartPosToEndPos
                    ;

            }

            // finish
            {

                bezierPoint = bezier.pointAtBezierCurveDetail(1.0f);

                {

                    if (lookAtType == "HandleDirection")
                    {
                        this.transform.rotation = bezierPoint.rotation;
                    }

                    else if (lookAtType == "Tangent")
                    {
                        this.transform.rotation = Quaternion.LookRotation(bezierPoint.tangent, Vector3.up);
                    }

                }

            }

            this.m_lookAtIE = null;

        }

        /// <summary>
        /// Look at
        /// </summary>
        /// <param name="fnap">FuncNameAndParams</param>
        // ------------------------------------------------------------------------------------------
        protected virtual void lookAt(FuncNameAndParams fnap)
        {

            if (!PositionAndRotateTargetManager.isAvailable() || fnap == null)
            {
                return;
            }

            // ------------------------------------

            // set
            {

                string functionName = fnap.functionName;

                if (functionName == "LookAtTransform")
                {
                    this.setCurrentLookAtTarget(fnap.getStringParameter(0), LookAtType.LookAt, false);
                }

                else if (functionName == "LookAtTransformOnlyY")
                {
                    this.setCurrentLookAtTarget(fnap.getStringParameter(0), LookAtType.LookAtOnlyY, false);
                }

                else if (functionName == "LookAtForward")
                {
                    this.setCurrentLookAtTarget(fnap.getStringParameter(0), LookAtType.SyncWithForwardDirection, false);
                }

                else if (functionName == "Stop")
                {
                    this.setCurrentLookAtTarget("", LookAtType.None, false);
                }

                else if (functionName == "Resume")
                {
                    this.setCurrentLookAtTarget(this.m_defaultLookAtTargetIdentifier, this.m_defaultLookAtType, false);
                }

            }

        }

        /// <summary>
        /// PoetInitState receiver for lookat
        /// </summary>
        /// <param name="piState">PoetInitState</param>
        // ------------------------------------------------------------------------------------------
        protected virtual void onPoetInitStateForLookAt(PoetInitState piState)
        {

#if UNITY_EDITOR

            if (piState.headerAndCells.ContainsKey(this.m_csvHeaderForLookAt))
            {

                FuncNameAndParamsList fnapList = CsvParser.parseFunctionsInCell(piState.headerAndCells[this.m_csvHeaderForLookAt]);

                foreach (var val in fnapList.list)
                {

                    if (
                        val.functionName != "LookAtTransform" &&
                        val.functionName != "LookAtTransformOnlyY" &&
                        val.functionName != "LookAtForward" &&
                        val.functionName != "LookAtBezierHandleDirection" &&
                        val.functionName != "LookAtBezierTangent" &&
                        val.functionName != "Stop" &&
                        val.functionName != "Resume"
                        )
                    {

                        Debug.LogWarning(
                            "Unknown function name : " +
                            val.functionName + " : " +
                            this.m_csvHeaderForLookAt + " : " +
                            piState.textAssetName + " : " +
                            piState.index
                            );

                    }

                }

            }

#endif

        }

    }

}
