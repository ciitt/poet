﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;

namespace Poet
{

    /// <summary>
    /// Object controller for textures
    /// </summary>
    [RequireComponent(typeof(Renderer))]
    public class ObjectLayerTexturesControllerScript : PoetBaseReceiverScript, LayerTexturesControllerInterface
    {

        protected class IdAndTextureDetailWithLayerId
        {

            public int poseIndex = -1;
            public string layerId = "";
            public IdAndTextureDetail idAndTextureDetail = null;

            public IdAndTextureDetailWithLayerId(int _poseIndex, string _layerId, IdAndTextureDetail _idAndTextureDetail)
            {
                this.poseIndex = _poseIndex;
                this.layerId = _layerId;
                this.idAndTextureDetail = _idAndTextureDetail;
            }

        }

        /// <summary>
        /// User progress data
        /// </summary>
        [Serializable]
        protected class UserProgressData
        {

            public string layer0 = "";
            public string layer1 = "";
            public string layer2 = "";
            public string layer3 = "";
            public string layer4 = "";
            public string layer5 = "";
            public string overlay = "";

            public bool flipX = false;
            public bool flipY = false;

            public int orderInLayer = 0;

        }

        [Serializable]
        protected class FirstIdentifiers
        {

            public string layer0 = "";
            public string layer1 = "";
            public string layer2 = "";
            public string layer3 = "";
            public string layer4 = "";
            public string layer5 = "";
            public string overlay = "";

            public bool flipX = false;
            public bool flipY = false;

        }

        /// <summary>
        /// LoadingMode
        /// </summary>
        [SerializeField]
        [Tooltip("LoadingMode")]
        protected LoadingMode m_loadingMode = LoadingMode.Normal;

        /// <summary>
        /// Reference to Renderer
        /// </summary>
        protected Renderer m_refRenderer;

        /// <summary>
        /// Csv header for texture
        /// </summary>
        [SerializeField]
        [Tooltip("Csv header for texture")]
        protected string m_csvHeaderForTexture = "CharacterA Texture";

        /// <summary>
        /// Default fade seconds
        /// </summary>
        [SerializeField]
        [Tooltip("Default fade seconds")]
        protected float m_defaultFadeSeconds = 0.5f;

        /// <summary>
        /// First identifiers at scene start
        /// </summary>
        [SerializeField]
        [Tooltip("First identifiers at scene start")]
        protected FirstIdentifiers m_firstIdentifiers = new FirstIdentifiers();

        /// <summary>
        /// LayerTextures list
        /// </summary>
        [SerializeField]
        [Tooltip("LayerTextures list")]
        protected List<LayerTextures> m_layerTextures = new List<LayerTextures>();

        /// <summary>
        /// UserProgressData
        /// </summary>
        protected UserProgressData m_userProgressData = new UserProgressData();

        /// <summary>
        /// LayerTextures dictionary
        /// </summary>
        protected Dictionary<string, LayerTextures> m_layerTexturesDict = new Dictionary<string, LayerTextures>();

        /// <summary>
        /// Is mobile shader
        /// </summary>
        protected bool m_isMobileShader = false;

        // --------------------------------------------------------------------------------------------------------------------

        /// <summary>
        /// Material
        /// </summary>
        protected Material material { get { return this.m_refRenderer.material; } }

        // --------------------------------------------------------------------------------------------------------------------

        /// <summary>
        /// initOnAwake() is called after Awake().
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected override void initOnAwake()
        {

            // m_refRenderer
            {
                this.m_refRenderer = this.GetComponent<Renderer>();
            }

            // m_isMobileShader
            {
                this.m_isMobileShader = !this.material.HasProperty("_Layer2Tex");
            }

            // init dictionary
            {

                this.m_layerTexturesDict = Funcs.listToDictionary<LayerTextures>(this.m_layerTextures);

                foreach (var val in this.m_layerTextures)
                {
                    val.initDictionaries();
                }

            }

            if (this.m_loadingMode == LoadingMode.AssetBundle)
            {

                int size = this.m_layerTextures.Count;

                for (int i = 0; i < size; i++)
                {

                    // addAssetBundleStartUp
                    {

                        this.addAssetBundleStartUp(i, "Layer0", this.m_layerTextures[i].layer0);
                        this.addAssetBundleStartUpForeach(i, "Layer1", this.m_layerTextures[i].layer1Dictionary);

                        if (!this.m_isMobileShader)
                        {

                            this.addAssetBundleStartUpForeach(i, "Layer2", this.m_layerTextures[i].layer2Dictionary);
                            this.addAssetBundleStartUpForeach(i, "Layer3", this.m_layerTextures[i].layer3Dictionary);
                            this.addAssetBundleStartUpForeach(i, "Layer4", this.m_layerTextures[i].layer4Dictionary);
                            this.addAssetBundleStartUpForeach(i, "Layer5", this.m_layerTextures[i].layer5Dictionary);
                            this.addAssetBundleStartUpForeach(i, "Overlay", this.m_layerTextures[i].overlayDictionary);

                        }

                    }

                }

            }

            // calcSpriteAtlasPsitions
            {

                //if (this.m_textureType == TextureType.SpriteAtlas)
                {
                    PoetIEnumeratorStartupManager.Instance.addSceneStartupIEnumerator(
                        this.calcSpriteAtlasPsitions(), null, null, SSC.IEnumeratorStartupManager.BeforeAfter.After
                        );
                }
                
            }

        }

        /// <summary>
        /// OnDestroy
        /// </summary>
        // --------------------------------------------------------------------------------------------------------------------
        protected virtual void OnDestroy()
        {

            if (this.m_loadingMode == LoadingMode.AssetBundle)
            {

                foreach (var layers in this.m_layerTextures)
                {

                    layers.layer0.unload();

                    foreach (var val in layers.layer1s) { val.unload(); }
                    foreach (var val in layers.layer2s) { val.unload(); }
                    foreach (var val in layers.layer3s) { val.unload(); }
                    foreach (var val in layers.layer4s) { val.unload(); }
                    foreach (var val in layers.layer5s) { val.unload(); }
                    foreach (var val in layers.overlays) { val.unload(); }

                }

            }

            if (this.material)
            {
                Destroy(this.material);
            }

        }

        /// <summary>
        /// Get LoadingMode
        /// </summary>
        /// <returns>LoadingMode</returns>
        // --------------------------------------------------------------------------------------------------------------------
        public LoadingMode loadingMode()
        {
            return this.m_loadingMode;
        }

        /// <summary>
        /// Add AssetBundle startup
        /// </summary>
        // --------------------------------------------------------------------------------------------------------------------
        protected void addAssetBundleStartUp(int poseIndex, string layerId, IdAndTextureDetail val)
        {

#if UNITY_EDITOR

            if(string.IsNullOrEmpty(val.assetBundleName))
            {
                string temp = (val.textureEditorOnly) ? val.textureEditorOnly.name : "";
                Debug.LogError("(#if UNITY_EDITOR) assetBundleName is empty : " + temp + " : " + this.gameObject.name);
                return;
            }

#endif

            PoetAssetBundleStartupManager.Instance.addSceneStartupAssetBundle(
                val.assetBundleName,
                val.assetBundleVariant,
                this.onSuccessAssetBundleCallback,
                null,
                null,
                new IdAndTextureDetailWithLayerId(poseIndex, layerId, val)
                );

        }

        /// <summary>
        /// Add AssetBundle startup
        /// </summary>
        // --------------------------------------------------------------------------------------------------------------------
        protected void addAssetBundleStartUpForeach(int poseIndex, string layerId, Dictionary<string, IdAndTextureDetail> layer)
        {

            foreach (var val in layer)
            {
                this.addAssetBundleStartUp(poseIndex, layerId, val.Value);
            }

        }

        /// <summary>
        /// AssetBundle success callback
        /// </summary>
        /// <param name="ab">AssetBundle</param>
        /// <param name="info">information</param>
        /// <param name="finishCallback">callback to finish</param>
        // --------------------------------------------------------------------------------------------------------------------
        protected void onSuccessAssetBundleCallback(AssetBundle ab, System.Object info, Action finishCallback)
        {
            StartCoroutine(this.onSuccessAssetBundleCallbackIE(ab, info, finishCallback));
        }

        /// <summary>
        /// AssetBundle success callback
        /// </summary>
        /// <param name="ab">AssetBundle</param>
        /// <param name="info">information</param>
        /// <param name="finishCallback">callback to finish</param>
        /// <returns>IEnumerator</returns>
        // --------------------------------------------------------------------------------------------------------------------
        protected virtual IEnumerator onSuccessAssetBundleCallbackIE(AssetBundle ab, System.Object info, Action finishCallback)
        {

            yield return null;

            if (ab && info is IdAndTextureDetailWithLayerId)
            {

                IdAndTextureDetailWithLayerId asInfo = info as IdAndTextureDetailWithLayerId;

                string[] temps = ab.GetAllAssetNames();

                AssetBundleRequest request = null;

                if (temps.Length == 1)
                {
                    request = ab.LoadAssetAsync(temps[0]);
                }

                else if (temps.Length >= 2)
                {
                    request = ab.LoadAssetAsync(asInfo.idAndTextureDetail.assetNameInAssetBundle);
                }

                else
                {
                    Debug.LogError("GetAllAssetNames() <= 0 : " + this.gameObject.name + " : " + asInfo.idAndTextureDetail.assetBundleName);
                }

                // ----------

                if (request != null)
                {
                    
                    while (!request.isDone)
                    {
                        yield return null;
                    }

                    //
                    {

                        if (asInfo.poseIndex >= 0 && asInfo.poseIndex < this.m_layerTextures.Count)
                        {

                            if (asInfo.layerId == "Layer0")
                            {

                                if (request.asset is Texture2D)
                                {
                                    this.m_layerTextures[asInfo.poseIndex].layer0.texture = request.asset as Texture2D;
                                }

                                else if (request.asset is SpriteAtlas)
                                {
                                    this.m_layerTextures[asInfo.poseIndex].layer0.spriteAtlas = request.asset as SpriteAtlas;
                                }

                                else if (request.asset is Sprite)
                                {
                                    Debug.LogError("(#if UNITY_EDITOR) Use not Sprite but Texture : " + this.gameObject.name + " : " + request.asset.name);
                                }

#if UNITY_EDITOR
                                else
                                {

                                    if(request.asset)
                                    {
                                        Debug.LogError("(#if UNITY_EDITOR) Invalid type : " + request.asset.GetType().Name + " : " + this.gameObject.name);
                                    }
                                    
                                }
#endif

                            }

                            else
                            {

                                var dict = this.m_layerTextures[asInfo.poseIndex].dictionary(asInfo.layerId);

                                if (dict != null && dict.ContainsKey(asInfo.idAndTextureDetail.identifier))
                                {

                                    if (request.asset is Texture2D)
                                    {
                                        dict[asInfo.idAndTextureDetail.identifier].texture = request.asset as Texture2D;
                                    }

                                    else if (request.asset is SpriteAtlas)
                                    {
                                        dict[asInfo.idAndTextureDetail.identifier].spriteAtlas = request.asset as SpriteAtlas;
                                    }

                                    else if (request.asset is Sprite)
                                    {
                                        dict[asInfo.idAndTextureDetail.identifier].texture = (request.asset as Sprite).texture;
                                    }

#if UNITY_EDITOR
                                    else
                                    {

                                        if (request.asset)
                                        {
                                            Debug.LogError("(#if UNITY_EDITOR) Invalid type : " + request.asset.GetType().Name + " : " + this.gameObject.name);
                                        }

                                    }
#endif
                                }

                                else
                                {
                                    Debug.LogError("Implementation Error");
                                }

                            }

                        }

                        else
                        {
                            Debug.LogError("Implementation Error");
                        }

                    }

                }

            }

            if(finishCallback != null)
            {
                finishCallback();
            }

        }

        /// <summary>
        /// Solve functions
        /// </summary>
        /// <param name="fnapList">FuncNameAndParamsList</param>
        /// <param name="resumeTime">resume time</param>
        // ------------------------------------------------------------------------------------------
        protected void solveFunctions(FuncNameAndParamsList fnapList, float resumeTime)
        {

            if (fnapList == null)
            {
                return;
            }

            // -------------------

            float fadeSeconds = this.m_defaultFadeSeconds;

            foreach (var fnap in fnapList.list)
            {

                if (fnap.functionName == "Layer0") { this.m_userProgressData.layer0 = fnap.getStringParameter(0); }
                else if (fnap.functionName == "Layer1") { this.m_userProgressData.layer1 = fnap.getStringParameter(0); }
                else if (fnap.functionName == "Layer2") { this.m_userProgressData.layer2 = fnap.getStringParameter(0); }
                else if (fnap.functionName == "Layer3") { this.m_userProgressData.layer3 = fnap.getStringParameter(0); }
                else if (fnap.functionName == "Layer4") { this.m_userProgressData.layer4 = fnap.getStringParameter(0); }
                else if (fnap.functionName == "Layer5") { this.m_userProgressData.layer5 = fnap.getStringParameter(0); }
                else if (fnap.functionName == "Overlay") { this.m_userProgressData.overlay = fnap.getStringParameter(0); }

                else if (fnap.functionName == "LookRight") { this.m_userProgressData.flipX = false; }
                else if (fnap.functionName == "LookLeft") { this.m_userProgressData.flipX = true; }

                else if (fnap.functionName == "LookUp") { this.m_userProgressData.flipY = false; }
                else if (fnap.functionName == "LookDown") { this.m_userProgressData.flipY = true; }

                else if (fnap.functionName == "Time") { fadeSeconds = fnap.getFloatParameter(0, fadeSeconds); }

                else if (fnap.functionName == "OrderInLayer")
                {
                    if (this.m_refRenderer is SpriteRenderer)
                    {
                        (this.m_refRenderer as SpriteRenderer).sortingOrder = fnap.getIntParameter(0);
                    }
                }

                else if (fnap.functionName == "Hide")
                {
                    this.m_userProgressData.layer0 = "NULL";
                    this.m_userProgressData.layer1 = "NULL";
                    this.m_userProgressData.layer2 = "NULL";
                    this.m_userProgressData.layer3 = "NULL";
                    this.m_userProgressData.layer4 = "NULL";
                    this.m_userProgressData.layer5 = "NULL";
                    this.m_userProgressData.overlay = "NULL";
                }

            }

            if(fnapList.list.Count > 0)
            {

                this.startApplyTextures(
                    this.m_userProgressData.layer0,
                    this.m_userProgressData.layer1,
                    this.m_userProgressData.layer2,
                    this.m_userProgressData.layer3,
                    this.m_userProgressData.layer4,
                    this.m_userProgressData.layer5,
                    this.m_userProgressData.overlay,
                    this.m_userProgressData.flipX,
                    this.m_userProgressData.flipY,
                    fadeSeconds,
                    fnapList.invokeTime,
                    resumeTime
                    );
                
            }

        }

        /// <summary>
        /// Apply parameters
        /// </summary>
        /// <param name="layer0">layer0</param>
        /// <param name="layer1">layer1</param>
        /// <param name="layer2">layer2</param>
        /// <param name="layer3">layer3</param>
        /// <param name="layer4">layer4</param>
        /// <param name="layer5">layer5</param>
        /// <param name="overlay">overlay</param>
        /// <param name="flipX">flipX</param>
        /// <param name="flipY">flipY</param>
        /// <param name="fadeSeconds">fade seconds</param>
        /// <param name="invokeTime">invoke time</param>
        /// <param name="resumeTime">resume time</param>
        // ------------------------------------------------------------------------------------------
        protected void startApplyTextures(
            string layer0,
            string layer1,
            string layer2,
            string layer3,
            string layer4,
            string layer5,
            string overlay,
            bool flipX,
            bool flipY,
            float fadeSeconds,
            float invokeTime,
            float resumeTime
            )
        {

            // setPrevParamsToCurrentParams
            {
                this.setPrevParamsToCurrentParams();
            }

            // setNewCurrentParams
            {

                this.setNewCurrentParams(
                    layer0,
                    layer1,
                    layer2,
                    layer3,
                    layer4,
                    layer5,
                    overlay,
                    flipX,
                    flipY
                    );

            }

            // m_userProgressData
            {

                this.m_userProgressData.layer0 = layer0;
                this.m_userProgressData.layer1 = layer1;
                this.m_userProgressData.layer2 = layer2;
                this.m_userProgressData.layer3 = layer3;
                this.m_userProgressData.layer4 = layer4;
                this.m_userProgressData.layer5 = layer5;
                this.m_userProgressData.overlay = overlay;
                this.m_userProgressData.flipX = flipX;
                this.m_userProgressData.flipY = flipY;

            }

            // fadeTextureIE
            {
                this.material.SetFloat("_Fade", 0.0f);
                StopAllCoroutines();
                StartCoroutine(this.fadeTextureIE(fadeSeconds, invokeTime));
            }

        }


        /// <summary>
        /// Set new current parameters
        /// </summary>
        /// <param name="layer0">layer0</param>
        /// <param name="layer1">layer1</param>
        /// <param name="layer2">layer2</param>
        /// <param name="layer3">layer3</param>
        /// <param name="layer4">layer4</param>
        /// <param name="layer5">layer5</param>
        /// <param name="overlay">overlay</param>
        /// <param name="flipX">flipX</param>
        /// <param name="flipY">flipY</param>
        // ------------------------------------------------------------------------------------------
        protected void setNewCurrentParams(
            string layer0,
            string layer1,
            string layer2,
            string layer3,
            string layer4,
            string layer5,
            string overlay,
            bool flipX,
            bool flipY
            )
        {

            if (this.m_layerTexturesDict.ContainsKey(layer0))
            {

                var temp = this.m_layerTexturesDict[layer0];

                this.setMaterialValues("_Layer0Tex", layer0, temp.layer0);

                this.setMaterialValues("_Layer1Tex", layer1, temp.layer1Dictionary);

                if (!this.m_isMobileShader)
                {

                    this.setMaterialValues("_Layer2Tex", layer2, temp.layer2Dictionary);
                    this.setMaterialValues("_Layer3Tex", layer3, temp.layer3Dictionary);
                    this.setMaterialValues("_Layer4Tex", layer4, temp.layer4Dictionary);
                    this.setMaterialValues("_Layer5Tex", layer5, temp.layer5Dictionary);
                    this.setMaterialValues("_OverlayTex", overlay, temp.overlayDictionary);

                }

                // flip
                {

                    if (flipX) { this.material.EnableKeyword("FlipX"); }
                    else { this.material.DisableKeyword("FlipX"); }

                    if (flipY) { this.material.EnableKeyword("FlipY"); }
                    else { this.material.DisableKeyword("FlipY"); }

                }

                // SpriteAtlas
                {
                    
                    if (this.material.HasProperty("_Layer0TexPos"))
                    {

                        this.material.SetVector("_Layer0TexPos", temp.layer0.posAndSizeForSpriteAtlas);
                        this.material.SetVector("_Layer1TexPos", temp.layer1Dictionary.ContainsKey(layer1) ? temp.layer1Dictionary[layer1].posAndSizeForSpriteAtlas : IdAndTextureDetail.defaultPosAndSizeForSpriteAtlas);

                        if (!this.m_isMobileShader)
                        {
                            this.material.SetVector("_Layer2TexPos", temp.layer2Dictionary.ContainsKey(layer2) ? temp.layer2Dictionary[layer2].posAndSizeForSpriteAtlas : IdAndTextureDetail.defaultPosAndSizeForSpriteAtlas);
                            this.material.SetVector("_Layer3TexPos", temp.layer3Dictionary.ContainsKey(layer3) ? temp.layer3Dictionary[layer3].posAndSizeForSpriteAtlas : IdAndTextureDetail.defaultPosAndSizeForSpriteAtlas);
                            this.material.SetVector("_Layer4TexPos", temp.layer4Dictionary.ContainsKey(layer4) ? temp.layer4Dictionary[layer4].posAndSizeForSpriteAtlas : IdAndTextureDetail.defaultPosAndSizeForSpriteAtlas);
                            this.material.SetVector("_Layer5TexPos", temp.layer5Dictionary.ContainsKey(layer5) ? temp.layer5Dictionary[layer5].posAndSizeForSpriteAtlas : IdAndTextureDetail.defaultPosAndSizeForSpriteAtlas);
                            this.material.SetVector("_OverlayTexPos", temp.overlayDictionary.ContainsKey(overlay) ? temp.overlayDictionary[overlay].posAndSizeForSpriteAtlas : IdAndTextureDetail.defaultPosAndSizeForSpriteAtlas);
                        }

                    }

                }

            }

            else
            {
                
                this.setMaterialValues("_Layer0Tex", null, Vector2.zero, Vector2.one);

                this.setMaterialValues("_Layer1Tex", null, Vector2.zero, Vector2.one);

                if (!this.m_isMobileShader)
                {

                    this.setMaterialValues("_Layer2Tex", null, Vector2.zero, Vector2.one);
                    this.setMaterialValues("_Layer3Tex", null, Vector2.zero, Vector2.one);
                    this.setMaterialValues("_Layer4Tex", null, Vector2.zero, Vector2.one);
                    this.setMaterialValues("_Layer5Tex", null, Vector2.zero, Vector2.one);
                    this.setMaterialValues("_OverlayTex", null, Vector2.zero, Vector2.one);

                }

                this.material.DisableKeyword("FlipX");
                this.material.DisableKeyword("FlipY");

            }

        }

        /// <summary>
        /// Set previous parameters to current parameters
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected void setPrevParamsToCurrentParams()
        {

            // prev
            {

                this.setPrevTexToCurrentTex("_Layer0Tex", "_Layer0TexPrev");
                this.setPrevTexToCurrentTex("_Layer1Tex", "_Layer1TexPrev");

                if (!this.m_isMobileShader)
                {

                    this.setPrevTexToCurrentTex("_Layer2Tex", "_Layer2TexPrev");
                    this.setPrevTexToCurrentTex("_Layer3Tex", "_Layer3TexPrev");
                    this.setPrevTexToCurrentTex("_Layer4Tex", "_Layer4TexPrev");
                    this.setPrevTexToCurrentTex("_Layer5Tex", "_Layer5TexPrev");
                    this.setPrevTexToCurrentTex("_OverlayTex", "_OverlayTexPrev");

                }

                // flip
                {

                    if (this.material.IsKeywordEnabled("FlipX")) { this.material.EnableKeyword("FlipXPrev"); }
                    else { this.material.DisableKeyword("FlipXPrev"); }

                    if (this.material.IsKeywordEnabled("FlipY")) { this.material.EnableKeyword("FlipYPrev"); }
                    else { this.material.DisableKeyword("FlipYPrev"); }

                }

                //
                {

                    if (this.material.HasProperty("_Layer0TexPos"))
                    {

                        this.material.SetVector("_Layer0TexPosPrev", this.material.GetVector("_Layer0TexPos"));
                        this.material.SetVector("_Layer1TexPosPrev", this.material.GetVector("_Layer1TexPos"));

                        if (!this.m_isMobileShader)
                        {

                            this.material.SetVector("_Layer2TexPosPrev", this.material.GetVector("_Layer2TexPos"));
                            this.material.SetVector("_Layer3TexPosPrev", this.material.GetVector("_Layer3TexPos"));
                            this.material.SetVector("_Layer4TexPosPrev", this.material.GetVector("_Layer4TexPos"));
                            this.material.SetVector("_Layer5TexPosPrev", this.material.GetVector("_Layer5TexPos"));
                            this.material.SetVector("_OverlayTexPosPrev", this.material.GetVector("_OverlayTexPos"));

                        }

                    }

                }

            }
            
        }

        /// <summary>
        /// Set previous texture to current texture
        /// </summary>
        /// <param name="propCurrent">property name for current</param>
        /// <param name="propPrev">property name for prev</param>
        // ------------------------------------------------------------------------------------------
        protected void setPrevTexToCurrentTex(string propCurrent, string propPrev)
        {
            
            this.setMaterialValues(
                propPrev,
                this.material.GetTexture(propCurrent) as Texture2D,
                this.material.GetTextureOffset(propCurrent),
                this.material.GetTextureScale(propCurrent)
                );

        }

        /// <summary>
        /// Set material values
        /// </summary>
        /// <param name="propertyName">property name</param>
        /// <param name="identifier">identifier</param>
        /// <param name="idAndTextureDetail">IdAndTextureDetail</param>
        // ------------------------------------------------------------------------------------------
        protected void setMaterialValues(string propertyName, string identifier, IdAndTextureDetail idAndTextureDetail)
        {

            if (idAndTextureDetail == null)
            {
                return;
            }

            // ----------------------------

            this.setMaterialValues(propertyName, idAndTextureDetail.texture, idAndTextureDetail.offset, idAndTextureDetail.tilling);

        }

        /// <summary>
        /// Set material values
        /// </summary>
        /// <param name="propertyName">property name</param>
        /// <param name="identifier">identifier</param>
        /// <param name="dict">Dictionary</param>
        // ------------------------------------------------------------------------------------------
        protected void setMaterialValues(string propertyName, string identifier, Dictionary<string, IdAndTextureDetail> dict)
        {

            if (!string.IsNullOrEmpty(identifier))
            {

                if (dict.ContainsKey(identifier))
                {
                    this.setMaterialValues(propertyName, dict[identifier].texture, dict[identifier].offset, dict[identifier].tilling);
                }

                else
                {
                    this.setMaterialValues(propertyName, null, Vector2.zero, Vector2.one);
                }

            }

        }

        /// <summary>
        /// Set material values
        /// </summary>
        /// <param name="propertyName">property name</param>
        /// <param name="tex">texture</param>
        /// <param name="offset">offset</param>
        /// <param name="tilling">tilling</param>
        // ------------------------------------------------------------------------------------------
        protected void setMaterialValues(string propertyName, Texture2D tex, Vector2 offset, Vector2 tilling)
        {

            this.material.SetTexture(propertyName, tex);
            this.material.SetTextureOffset(propertyName, offset);
            this.material.SetTextureScale(propertyName, tilling);

        }

        /// <summary>
        /// Fade textures
        /// </summary>
        /// <param name="fadeSeconds">fade seconds</param>
        /// <param name="invokeTime">invoke time</param>
        /// <returns>IEnumerator</returns>
        // ------------------------------------------------------------------------------------------
        protected IEnumerator fadeTextureIE(
            float fadeSeconds,
            float invokeTime
            )
        {

            var pauseState = PoetReduxManager.Instance.PauseStateWatcher.state();

            // wait for invoke
            {
                yield return this.waitInvokeTime(invokeTime, 0.0f, null);
            }

            // fade
            {

                float timer = 0.0f;

                while (timer < fadeSeconds)
                {

                    // break
                    {
                        if (PoetOneSceneManager.isAvailable() && PoetOneSceneManager.Instance.shouldSkip())
                        {
                            break;
                        }
                    }

                    // pauseState
                    {
                        if (pauseState.pause)
                        {
                            yield return null;
                            continue;
                        }
                    }

                    // SetFloat
                    {
                        this.material.SetFloat("_Fade", timer / fadeSeconds);
                    }

                    // timer
                    {
                        timer += Time.deltaTime;
                    }

                    yield return null;

                }

            }

            // finish
            {
                this.material.SetFloat("_Fade", 1.0f);
            }

        }

        /// <summary>
        /// PoetSequenceState receiver
        /// </summary>
        /// <param name="psState">PoetSequenceState</param>
        // ------------------------------------------------------------------------------------------
        protected override void onPoetSequenceState(PoetSequenceState psState)
        {

            if (psState.stateEnum == PoetSequenceState.StateEnum.PreProcess)
            {

                if (psState.headerAndCells.ContainsKey(this.m_csvHeaderForTexture))
                {

                    this.m_currentFnapList.clear();

                    CsvParser.parseFunctionsInCell(psState.headerAndCells[this.m_csvHeaderForTexture], this.m_currentFnapList);

                    if (this.m_currentFnapList.preProcess)
                    {
                        this.solveFunctions(new FuncNameAndParamsList(this.m_currentFnapList), 0.0f);
                    }

                }

            }

            else if (psState.stateEnum == PoetSequenceState.StateEnum.MainProcess)
            {

                if (psState.headerAndCells.ContainsKey(this.m_csvHeaderForTexture))
                {

                    if (!this.m_currentFnapList.preProcess)
                    {
                        this.solveFunctions(new FuncNameAndParamsList(this.m_currentFnapList), 0.0f);
                    }

                }

            }

        }

        /// <summary>
        /// PoetInitState receiver
        /// </summary>
        /// <param name="piState">PoetInitState</param>
        // ------------------------------------------------------------------------------------------
        protected override void onPoetInitState(PoetInitState piState)
        {

#if UNITY_EDITOR

            if (piState.headerAndCells.ContainsKey(this.m_csvHeaderForTexture))
            {

                FuncNameAndParamsList fnapList = CsvParser.parseFunctionsInCell(piState.headerAndCells[this.m_csvHeaderForTexture]);

                foreach (var val in fnapList.list)
                {

                    if (
                        val.functionName != "Layer0" &&
                        val.functionName != "Layer1" &&
                        val.functionName != "Layer2" &&
                        val.functionName != "Layer3" &&
                        val.functionName != "Layer4" &&
                        val.functionName != "Layer5" &&
                        val.functionName != "Overlay" &&
                        val.functionName != "Time" &&
                        val.functionName != "OrderInLayer" &&
                        val.functionName != "Hide" &&
                        val.functionName != "LookRight" &&
                        val.functionName != "LookLeft" &&
                        val.functionName != "LookUp" &&
                        val.functionName != "LookDown"
                        )
                    {

                        Debug.LogWarning(
                            "Unknown function name : " +
                            val.functionName + " : " +
                            this.m_csvHeaderForTexture + " : " +
                            piState.textAssetName + " : " +
                            piState.index
                            );

                    }

                }

            }

#endif

        }

        /// <summary>
        /// TextShowingState receiver
        /// </summary>
        /// <param name="tsState">TextShowingState</param>
        // ------------------------------------------------------------------------------------------
        protected override void onTextShowingState(TextShowingState tsState)
        {

        }

        /// <summary>
        /// UserProgressDataSignal receiver
        /// </summary>
        /// <param name="updSignal">UserProgressDataSignal</param>
        // ------------------------------------------------------------------------------------------
        protected override void onUserProgressDataSignal(UserProgressDataSignal updSignal)
        {

            if (this.m_refRenderer is SpriteRenderer)
            {
                this.m_userProgressData.orderInLayer = (this.m_refRenderer as SpriteRenderer).sortingOrder;
            }

            updSignal.addDataAction(this.transform, this, JsonUtility.ToJson(this.m_userProgressData));

        }

        /// <summary>
        /// SceneChangeState receiver
        /// </summary>
        /// <param name="scState">SceneChangeState</param>
        // ------------------------------------------------------------------------------------------
        protected override void onSceneChangeStateReceiver(SSC.SceneChangeState scState)
        {

            if (scState.stateEnum == SSC.SceneChangeState.StateEnum.AllStartupsDone)
            {

                PoetSceneChangeManager pscm = PoetSceneChangeManager.Instance as PoetSceneChangeManager;

                if (pscm.isLoadingCurrentSceneWithUserProgressData())
                {

                    this.m_userProgressData = pscm.getDataFromCurrentUserProgressData<UserProgressData>(this.transform, this);

                    // init by m_userProgressData
                    {

                        this.startApplyTextures(
                            this.m_userProgressData.layer0,
                            this.m_userProgressData.layer1,
                            this.m_userProgressData.layer2,
                            this.m_userProgressData.layer3,
                            this.m_userProgressData.layer4,
                            this.m_userProgressData.layer5,
                            this.m_userProgressData.overlay,
                            this.m_userProgressData.flipX,
                            this.m_userProgressData.flipY,
                            0.0f,
                            0.0f,
                            0.0f
                            );

                        if (this.m_refRenderer is SpriteRenderer)
                        {
                            (this.m_refRenderer as SpriteRenderer).sortingOrder = this.m_userProgressData.orderInLayer;
                        }

                    }

                }

                else
                {

                    this.startApplyTextures(
                        this.m_firstIdentifiers.layer0,
                        this.m_firstIdentifiers.layer1,
                        this.m_firstIdentifiers.layer2,
                        this.m_firstIdentifiers.layer3,
                        this.m_firstIdentifiers.layer4,
                        this.m_firstIdentifiers.layer5,
                        this.m_firstIdentifiers.overlay,
                        this.m_firstIdentifiers.flipX,
                        this.m_firstIdentifiers.flipY,
                        0.0f,
                        0.0f,
                        0.0f
                        );

                }

            }

        }

        /// <summary>
        /// PauseState receiver
        /// </summary>
        /// <param name="pState">PauseState</param>
        // ------------------------------------------------------------------------------------------
        protected override void onPauseStateReceiver(SSC.PauseState pState)
        {

        }

        /// <summary>
        /// Clear material textures
        /// </summary>
        // --------------------------------------------------------------------------------------------------------------------
        public void clearMaterialTextures()
        {

            Material mat = null;

#if UNITY_EDITOR
            mat = this.GetComponent<Renderer>().sharedMaterial;
#else
            mat = this.m_refRenderer.material;
#endif
            
            mat.SetTexture("_Layer0Tex", null);
            mat.SetTexture("_Layer1Tex", null);

            mat.SetTexture("_Layer0TexPrev", null);
            mat.SetTexture("_Layer1TexPrev", null);

            if (!this.m_isMobileShader)
            {

                mat.SetTexture("_Layer2Tex", null);
                mat.SetTexture("_Layer3Tex", null);
                mat.SetTexture("_Layer4Tex", null);
                mat.SetTexture("_Layer5Tex", null);
                mat.SetTexture("_OverlayTex", null);

                mat.SetTexture("_Layer2TexPrev", null);
                mat.SetTexture("_Layer3TexPrev", null);
                mat.SetTexture("_Layer4TexPrev", null);
                mat.SetTexture("_Layer5TexPrev", null);
                mat.SetTexture("_OverlayTexPrev", null);

            }

            //mat.DisableKeyword("FlipX");
            //mat.DisableKeyword("FlipY");

            //mat.DisableKeyword("FlipXPrev");
            //mat.DisableKeyword("FlipYPrev");

        }

        /// <summary>
        /// Set SpriteAtlas positions for shader
        /// </summary>
        /// <returns>IEnumerator</returns>
        // --------------------------------------------------------------------------------------------------------------------
        protected virtual IEnumerator calcSpriteAtlasPsitions()
        {

            yield return null;

            foreach(var kv in this.m_layerTexturesDict)
            {

                kv.Value.layer0.calcPosAndSizeForSpriteAtlas();

                foreach (var val in kv.Value.layer1Dictionary) { val.Value.calcPosAndSizeForSpriteAtlas(); }

                if (!this.m_isMobileShader)
                {

                    foreach (var val in kv.Value.layer2Dictionary) { val.Value.calcPosAndSizeForSpriteAtlas(); }
                    foreach (var val in kv.Value.layer3Dictionary) { val.Value.calcPosAndSizeForSpriteAtlas(); }
                    foreach (var val in kv.Value.layer4Dictionary) { val.Value.calcPosAndSizeForSpriteAtlas(); }
                    foreach (var val in kv.Value.layer5Dictionary) { val.Value.calcPosAndSizeForSpriteAtlas(); }
                    foreach (var val in kv.Value.overlayDictionary) { val.Value.calcPosAndSizeForSpriteAtlas(); }

                }

            }

        }

#if UNITY_EDITOR

        /// <summary>
        /// Apply texture in editor
        /// </summary>
        /// <param name="layerId">layer id</param>
        /// <param name="tilling">texture tilling</param>
        /// <param name="offset">texture offset</param>
        /// <param name="texture">texture</param>
        /// <param name="posAndSizeForSpriteAtlas">posAndSizeForSpriteAtlas</param>
        // --------------------------------------------------------------------------------------------------------------------
        public void applyTextureForEditor(string layerId, Vector2 tilling, Vector2 offset, Texture2D texture, Vector4 posAndSizeForSpriteAtlas)
        {
            if (Application.isPlaying)
            {
                Debug.LogWarning("Application.isPlaying");
                return;
            }

            if (!this.m_refRenderer)
            {
                this.m_refRenderer = this.GetComponent<Renderer>();
            }

            // -------------------

            string targetTexName = "_" + layerId + "Tex";
            string posSizeVecName = "_" + layerId + "TexPos";

            // set texture
            {

                if (this.m_refRenderer.sharedMaterial.HasProperty(targetTexName))
                {
                    this.m_refRenderer.sharedMaterial.SetTexture(targetTexName, texture);
                    this.m_refRenderer.sharedMaterial.SetTextureOffset(targetTexName, offset);
                    this.m_refRenderer.sharedMaterial.SetTextureScale(targetTexName, tilling);
                }

                else
                {
                    UnityEditor.EditorUtility.DisplayDialog("Error", "Invalid property : " + targetTexName, "Ok");
                    Debug.LogError("Invalid property : " + targetTexName);
                }

            }

            //
            {

                if (this.m_refRenderer.sharedMaterial.HasProperty(posSizeVecName))
                {
                    this.m_refRenderer.sharedMaterial.SetVector(posSizeVecName, posAndSizeForSpriteAtlas);
                }

            }

        }

        /// <summary>
        /// Is mobile shader
        /// </summary>
        /// <returns>mobile shader</returns>
        // --------------------------------------------------------------------------------------------------------------------
        public bool isMobileShaderForEditor()
        {

            Material mat = this.GetComponent<Renderer>().sharedMaterial;

            if (mat)
            {
                return !mat.HasProperty("_Layer2Tex");
            }

            return false;

        }

        /// <summary>
        /// Is SpriteAtlas shader
        /// </summary>
        /// <returns>SpriteAtlas shader</returns>
        // --------------------------------------------------------------------------------------------------------------------
        public bool isSpriteAtlasShaderForEditor()
        {

            Material mat = this.GetComponent<Renderer>().sharedMaterial;
            
            if(mat)
            {
                return mat.HasProperty("_Layer0TexPos");
            }

            return false;

        }

#endif

    }

}
