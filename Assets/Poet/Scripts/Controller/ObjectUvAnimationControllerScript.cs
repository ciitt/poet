﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poet
{

    /// <summary>
    /// Uv animation controller
    /// </summary>
    [RequireComponent(typeof(Renderer))]
    public class ObjectUvAnimationControllerScript : PoetBaseReceiverScript
    {

        [Serializable]
        protected class TexNameAndOffset : IdentifierInterface
        {

            public string texName = "";
            public float offsetSpeedX = 0.0f;
            public float offsetSpeedY = 0.0f;
            public Vector2 currentOffset = Vector2.zero;

            public TexNameAndOffset(string _texName, float _offsetSpeedX, float _offsetSpeedY)
            {
                this.texName = _texName;
                this.offsetSpeedX = _offsetSpeedX;
                this.offsetSpeedY = _offsetSpeedY;
            }

            public string getIdentifier()
            {
                return this.texName;
            }

        }

        /// <summary>
        /// User progress data
        /// </summary>
        [Serializable]
        protected class UserProgressData
        {

            public List<TexNameAndOffset> texNameAndOffsetList = new List<TexNameAndOffset>();

            public float resumeTime = 0.0f;
            public FuncNameAndParamsList fnapList = null;

            public void clear()
            {
                this.resumeTime = 0.0f;
                this.texNameAndOffsetList.Clear();
                this.fnapList = null;
            }

        }

        /// <summary>
        /// Csv header for uv animation
        /// </summary>
        [SerializeField]
        [Tooltip("Csv header for uv animation")]
        protected string m_csvHeaderForUvAnim = "CharacterA UV Animation";

        /// <summary>
        /// Reference to Renderer
        /// </summary>
        protected Renderer m_refRenderer = null;

        /// <summary>
        /// UserProgressData
        /// </summary>
        protected UserProgressData m_userProgressData = new UserProgressData();

        /// <summary>
        /// Texture name and offset speed dictionary
        /// </summary>
        protected Dictionary<string, TexNameAndOffset> m_texNameAndOffsetDict = new Dictionary<string, TexNameAndOffset>();

        // ------------------------------------------------------------------------------------------

        /// <summary>
        /// Material
        /// </summary>
        protected Material material { get { return this.m_refRenderer.material; } }

        // ------------------------------------------------------------------------------------------

        /// <summary>
        /// initOnAwake() is called after Awake().
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected override void initOnAwake()
        {

            // m_refSpriteRenderer
            {
                this.m_refRenderer = this.GetComponent<Renderer>();
            }

        }

        /// <summary>
        /// OnDestroy
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected virtual void OnDestroy()
        {

            if (this.material)
            {
                Destroy(this.material);
            }

        }

        /// <summary>
        /// Update
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected virtual void Update()
        {

            foreach(var kv in this.m_texNameAndOffsetDict)
            {

                kv.Value.currentOffset.x = (kv.Value.currentOffset.x + kv.Value.offsetSpeedX) % 1.0f;
                kv.Value.currentOffset.y = (kv.Value.currentOffset.y + kv.Value.offsetSpeedY) % 1.0f;

                this.material.SetTextureOffset(
                    kv.Key,
                    kv.Value.currentOffset
                    );

            }

        }

        /// <summary>
        /// Solve functions
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected void solveFunctions(FuncNameAndParamsList fnapList, float resumeTime)
        {

            if (fnapList == null || fnapList.list.Count <= 0)
            {
                return;
            }

            // -------------------

            List<TexNameAndOffset> addList = new List<TexNameAndOffset>();
            List<string> removeList = new List<string>();

            foreach (var fnap in fnapList.list)
            {

                if (fnap.functionName == "Play")
                {

                    addList.Add(new TexNameAndOffset(
                        fnap.getStringParameter(0),
                        fnap.getFloatParameter(1) * 0.001f,
                        fnap.getFloatParameter(2) * 0.001f
                        ));

                    
                }

                else if (fnap.functionName == "Stop")
                {
                    removeList.Add(fnap.getStringParameter(0));
                }

                else if (fnap.functionName == "StopAll")
                {
                    StartCoroutine(this.stopAllUvAnimation(fnap, fnapList, resumeTime));
                    return;
                }

            }

            if(addList.Count > 0 || removeList.Count > 0)
            {
                StartCoroutine(this.startOrStopUvAnimation(fnapList, addList, removeList, resumeTime));
            }

        }

        /// <summary>
        /// Start or stop
        /// </summary>
        /// <param name="fnapList">FuncNameAndParamsList</param>
        /// <param name="addList">list to add</param>
        /// <param name="removeList">list to remove</param>
        /// <param name="resumeTime">resume time</param>
        /// <returns>IEnumerator</returns>
        // ------------------------------------------------------------------------------------------
        protected IEnumerator startOrStopUvAnimation(
            FuncNameAndParamsList fnapList,
            List<TexNameAndOffset> addList,
            List<string> removeList,
            float resumeTime
            )
        {

            if (fnapList == null)
            {
                yield break;
            }

            // -------------------

            // m_userProgressData
            {
                this.m_userProgressData.resumeTime = resumeTime;
                this.m_userProgressData.fnapList = fnapList;
            }

            // wait for invoke
            {
                yield return this.waitInvokeTime(fnapList.invokeTime, resumeTime, () =>
                {
                    this.m_userProgressData.resumeTime += Time.deltaTime;
                });
                this.m_userProgressData.resumeTime = Mathf.Max(fnapList.invokeTime, resumeTime);
            }

            // remove
            {

                foreach (var val in removeList)
                {

                    if (this.m_texNameAndOffsetDict.ContainsKey(val))
                    {
                        this.m_texNameAndOffsetDict.Remove(val);
                    }

                }

            }

            // add
            {

                foreach(var val in addList)
                {

                    if (this.m_texNameAndOffsetDict.ContainsKey(val.texName))
                    {
                        this.m_texNameAndOffsetDict[val.texName] = val;
                    }

                    else
                    {
                        val.currentOffset = this.material.GetTextureOffset(val.texName);
                        this.m_texNameAndOffsetDict.Add(val.texName, val);
                    }

                }

            }

            // m_userProgressData
            {
                this.m_userProgressData.clear();
            }

        }

        /// <summary>
        /// Stop all
        /// </summary>
        /// <param name="fnap">FuncNameAndParams</param>
        /// <param name="fnapList">FuncNameAndParamsList</param>
        /// <param name="resumeTime">resume time</param>
        /// <returns>IEnumerator</returns>
        // ------------------------------------------------------------------------------------------
        protected IEnumerator stopAllUvAnimation(FuncNameAndParams fnap, FuncNameAndParamsList fnapList, float resumeTime)
        {

            if (fnap == null || fnapList == null)
            {
                yield break;
            }

            // -------------------

            // m_userProgressData
            {
                this.m_userProgressData.resumeTime = resumeTime;
                this.m_userProgressData.fnapList = fnapList;
            }

            // wait for invoke
            {
                yield return this.waitInvokeTime(fnapList.invokeTime, resumeTime, () =>
                {
                    this.m_userProgressData.resumeTime += Time.deltaTime;
                });
                this.m_userProgressData.resumeTime = Mathf.Max(fnapList.invokeTime, resumeTime);
            }

            // clear
            {
                this.m_texNameAndOffsetDict.Clear();
            }

            // m_userProgressData
            {
                this.m_userProgressData.clear();
            }

        }

        /// <summary>
        /// PoetSequenceState receiver
        /// </summary>
        /// <param name="psState">PoetSequenceState</param>
        // ------------------------------------------------------------------------------------------
        protected override void onPoetSequenceState(PoetSequenceState psState)
        {

            if (psState.stateEnum == PoetSequenceState.StateEnum.PreProcess)
            {

                if (psState.headerAndCells.ContainsKey(this.m_csvHeaderForUvAnim))
                {

                    this.m_currentFnapList.clear();

                    CsvParser.parseFunctionsInCell(psState.headerAndCells[this.m_csvHeaderForUvAnim], this.m_currentFnapList);

                    if (this.m_currentFnapList.preProcess)
                    {
                        this.solveFunctions(new FuncNameAndParamsList(this.m_currentFnapList), 0.0f);
                    }

                }

            }

            else if (psState.stateEnum == PoetSequenceState.StateEnum.MainProcess)
            {

                if (psState.headerAndCells.ContainsKey(this.m_csvHeaderForUvAnim))
                {

                    if (!this.m_currentFnapList.preProcess)
                    {
                        this.solveFunctions(new FuncNameAndParamsList(this.m_currentFnapList), 0.0f);
                    }

                }

            }

        }

        /// <summary>
        /// PoetInitState receiver
        /// </summary>
        /// <param name="piState">PoetInitState</param>
        // ------------------------------------------------------------------------------------------
        protected override void onPoetInitState(PoetInitState piState)
        {

#if UNITY_EDITOR

            if (piState.headerAndCells.ContainsKey(this.m_csvHeaderForUvAnim))
            {

                FuncNameAndParamsList fnapList = CsvParser.parseFunctionsInCell(piState.headerAndCells[this.m_csvHeaderForUvAnim]);

                foreach (var val in fnapList.list)
                {

                    if (
                        val.functionName != "Play" &&
                        val.functionName != "Stop" &&
                        val.functionName != "StopAll"
                        )
                    {

                        Debug.LogWarning(
                            "Unknown function name : " +
                            val.functionName + " : " +
                            this.m_csvHeaderForUvAnim + " : " +
                            piState.textAssetName + " : " +
                            piState.index
                            );

                    }

                }

            }

#endif

        }

        /// <summary>
        /// TextShowingState receiver
        /// </summary>
        /// <param name="tsState">TextShowingState</param>
        // ------------------------------------------------------------------------------------------
        protected override void onTextShowingState(TextShowingState tsState)
        {

        }

        /// <summary>
        /// UserProgressDataSignal receiver
        /// </summary>
        /// <param name="updSignal">UserProgressDataSignal</param>
        // ------------------------------------------------------------------------------------------
        protected override void onUserProgressDataSignal(UserProgressDataSignal updSignal)
        {

            this.m_userProgressData.texNameAndOffsetList.Clear();
            this.m_userProgressData.texNameAndOffsetList.AddRange(this.m_texNameAndOffsetDict.Values);

            updSignal.addDataAction(this.transform, this, JsonUtility.ToJson(this.m_userProgressData));

        }

        /// <summary>
        /// SceneChangeState receiver
        /// </summary>
        /// <param name="scState">SceneChangeState</param>
        // ------------------------------------------------------------------------------------------
        protected override void onSceneChangeStateReceiver(SSC.SceneChangeState scState)
        {

            if (scState.stateEnum == SSC.SceneChangeState.StateEnum.AllStartupsDone)
            {

                PoetSceneChangeManager pscm = PoetSceneChangeManager.Instance as PoetSceneChangeManager;

                if (pscm.isLoadingCurrentSceneWithUserProgressData())
                {

                    this.m_userProgressData = pscm.getDataFromCurrentUserProgressData<UserProgressData>(this.transform, this);

                    this.m_texNameAndOffsetDict = Funcs.listToDictionary<TexNameAndOffset>(this.m_userProgressData.texNameAndOffsetList);

                }

            }

            else if (scState.stateEnum == SSC.SceneChangeState.StateEnum.ScenePlaying)
            {

                PoetSceneChangeManager pscm = PoetSceneChangeManager.Instance as PoetSceneChangeManager;

                if (pscm.isLoadingCurrentSceneWithUserProgressData())
                {

                    if (this.m_userProgressData.fnapList != null)
                    {
                        this.solveFunctions(this.m_userProgressData.fnapList, this.m_userProgressData.resumeTime);
                    }

                }

            }

        }

        /// <summary>
        /// PauseState receiver
        /// </summary>
        /// <param name="pState">PauseState</param>
        // ------------------------------------------------------------------------------------------
        protected override void onPauseStateReceiver(SSC.PauseState pState)
        {

        }

    }

}
