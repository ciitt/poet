﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poet
{

    /// <summary>
    /// Script animation
    /// </summary>
    public class ObjectScriptAnimationControllerScript : PoetBaseReceiverScript
    {

        /// <summary>
        /// User progress data
        /// </summary>
        [Serializable]
        protected class UserProgressData
        {

            public float resumeTime = 0.0f;
            public FuncNameAndParamsList fnapList = null;

            public void clear()
            {
                this.resumeTime = 0.0f;
                this.fnapList = null;
            }

        }

        /// <summary>
        /// Csv header for animation
        /// </summary>
        [SerializeField]
        [Tooltip("Csv header for animation")]
        protected string m_csvHeaderForScriptAnimation = "CharacterA Script Animation";

        /// <summary>
        /// Original local position
        /// </summary>
        protected Vector3 m_oriLocalPosition;

        /// <summary>
        /// UserProgressData
        /// </summary>
        protected UserProgressData m_userProgressData = new UserProgressData();

        /// <summary>
        /// initOnAwake() is called after Awake().
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected override void initOnAwake()
        {
            this.m_oriLocalPosition = this.transform.localPosition;
        }

        /// <summary>
        /// Solve functions
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected void solveFunctions(FuncNameAndParamsList fnapList, float resumeTime)
        {

            if (fnapList == null)
            {
                return;
            }

            // -------------------

            foreach (var fnap in fnapList.list)
            {

                if (fnap.functionName == "Shake")
                {
                    StopAllCoroutines();
                    StartCoroutine(this.startShake(fnap, fnapList, resumeTime));
                    break;
                }

            }

        }

        /// <summary>
        /// Start setAnimTriggerIE
        /// </summary>
        /// <param name="fnap">FuncNameAndParams</param>
        /// <param name="fnapList">FuncNameAndParamsList</param>
        /// <param name="resumeTime">resume time</param>
        // ------------------------------------------------------------------------------------------
        protected IEnumerator startShake(FuncNameAndParams fnap, FuncNameAndParamsList fnapList, float resumeTime)
        {

            if (fnap == null || fnapList == null)
            {
                yield break;
            }

            // -------------------

            // m_userProgressData
            {
                this.m_userProgressData.resumeTime = resumeTime;
                this.m_userProgressData.fnapList = fnapList;
            }

            // wait for invoke
            {
                yield return this.waitInvokeTime(fnapList.invokeTime, resumeTime, () =>
                {
                    this.m_userProgressData.resumeTime += Time.deltaTime;
                });
                this.m_userProgressData.resumeTime = Mathf.Max(fnapList.invokeTime, resumeTime);
            }

            // m_userProgressData
            {
                this.m_userProgressData.clear();
            }

            StartCoroutine(this.shakeIE(
                fnap.getFloatParameter(0, 3.0f),
                fnap.getFloatParameter(1, 5.0f),
                fnap.getFloatParameter(2, 0.3f),
                fnapList.invokeTime,
                resumeTime
                ));

        }

        /// <summary>
        /// SetTrigger IEnumerator
        /// </summary>
        /// <param name="triggerName">trigger name</param>
        /// <param name="invokeTime">invoke time</param>
        /// <param name="resumeTime">resume time</param>
        /// <returns></returns>
        // ------------------------------------------------------------------------------------------
        protected IEnumerator shakeIE(
            float shakeTime,
            float frequency,
            float amplifier,
            float invokeTime,
            float resumeTime
            )
        {

            var pauseState = PoetReduxManager.Instance.PauseStateWatcher.state();

            // 
            {

                float timer = 0.0f;

                Vector3 oriLocalPosition = this.m_oriLocalPosition;
                Vector3 temp = Vector3.zero;

                float val = 0.0f;

                while (timer < shakeTime)
                {

                    // break
                    {
                        if (PoetOneSceneManager.isAvailable() && PoetOneSceneManager.Instance.shouldSkip())
                        {
                            break;
                        }
                    }

                    // pauseState
                    {
                        if (pauseState.pause)
                        {
                            yield return null;
                            continue;
                        }
                    }

                    // val
                    {
                        val = (timer / shakeTime);
                    }

                    // position
                    {

                        temp.y = Mathf.Sin(Mathf.Lerp(0.0f, Mathf.PI * 2 * frequency, val)) * (1 - val) * amplifier;

                        this.transform.localPosition = oriLocalPosition - temp;

                    }

                    // timer
                    {
                        timer += Time.deltaTime;
                    }

                    yield return null;

                }

                // finish
                {
                    this.transform.localPosition = oriLocalPosition;
                }

            }

            // m_userProgressData
            {
                this.m_userProgressData.clear();
            }

        }

        /// <summary>
        /// PoetSequenceState receiver
        /// </summary>
        /// <param name="psState">PoetSequenceState</param>
        // ------------------------------------------------------------------------------------------
        protected override void onPoetSequenceState(PoetSequenceState psState)
        {

            if (psState.stateEnum == PoetSequenceState.StateEnum.PreProcess)
            {

                if (psState.headerAndCells.ContainsKey(this.m_csvHeaderForScriptAnimation))
                {

                    this.m_currentFnapList.clear();

                    CsvParser.parseFunctionsInCell(psState.headerAndCells[this.m_csvHeaderForScriptAnimation], this.m_currentFnapList);

                    if (this.m_currentFnapList.preProcess)
                    {
                        this.solveFunctions(new FuncNameAndParamsList(this.m_currentFnapList), 0.0f);
                    }

                }

            }

            else if (psState.stateEnum == PoetSequenceState.StateEnum.MainProcess)
            {

                if (psState.headerAndCells.ContainsKey(this.m_csvHeaderForScriptAnimation))
                {

                    if (!this.m_currentFnapList.preProcess)
                    {
                        this.solveFunctions(new FuncNameAndParamsList(this.m_currentFnapList), 0.0f);
                    }

                }

            }

        }

        /// <summary>
        /// PoetInitState receiver
        /// </summary>
        /// <param name="piState">PoetInitState</param>
        // ------------------------------------------------------------------------------------------
        protected override void onPoetInitState(PoetInitState piState)
        {

#if UNITY_EDITOR

            if (piState.headerAndCells.ContainsKey(this.m_csvHeaderForScriptAnimation))
            {

                FuncNameAndParamsList fnapList = CsvParser.parseFunctionsInCell(piState.headerAndCells[this.m_csvHeaderForScriptAnimation]);

                foreach (var val in fnapList.list)
                {

                    if (
                        val.functionName != "Shake"
                        )
                    {

                        Debug.LogWarning(
                            "Unknown function name : " +
                            val.functionName + " : " +
                            this.m_csvHeaderForScriptAnimation + " : " +
                            piState.textAssetName + " : " +
                            piState.index
                            );

                    }

                }

            }

#endif

        }

        /// <summary>
        /// TextShowingState receiver
        /// </summary>
        /// <param name="tsState">TextShowingState</param>
        // ------------------------------------------------------------------------------------------
        protected override void onTextShowingState(TextShowingState tsState)
        {

        }

        /// <summary>
        /// UserProgressDataSignal receiver
        /// </summary>
        /// <param name="updSignal">UserProgressDataSignal</param>
        // ------------------------------------------------------------------------------------------
        protected override void onUserProgressDataSignal(UserProgressDataSignal updSignal)
        {
            updSignal.addDataAction(this.transform, this, JsonUtility.ToJson(this.m_userProgressData));
        }

        /// <summary>
        /// SceneChangeState receiver
        /// </summary>
        /// <param name="scState">SceneChangeState</param>
        // ------------------------------------------------------------------------------------------
        protected override void onSceneChangeStateReceiver(SSC.SceneChangeState scState)
        {

            if (scState.stateEnum == SSC.SceneChangeState.StateEnum.AllStartupsDone)
            {

                PoetSceneChangeManager pscm = PoetSceneChangeManager.Instance as PoetSceneChangeManager;

                if (pscm.isLoadingCurrentSceneWithUserProgressData())
                {
                    this.m_userProgressData = pscm.getDataFromCurrentUserProgressData<UserProgressData>(this.transform, this);
                }

            }

            else if (scState.stateEnum == SSC.SceneChangeState.StateEnum.ScenePlaying)
            {

                PoetSceneChangeManager pscm = PoetSceneChangeManager.Instance as PoetSceneChangeManager;

                if (pscm.isLoadingCurrentSceneWithUserProgressData())
                {

                    if (this.m_userProgressData.fnapList != null)
                    {
                        this.solveFunctions(this.m_userProgressData.fnapList, this.m_userProgressData.resumeTime);
                    }

                }

            }

        }

        /// <summary>
        /// PauseState receiver
        /// </summary>
        /// <param name="pState">PauseState</param>
        // ------------------------------------------------------------------------------------------
        protected override void onPauseStateReceiver(SSC.PauseState pState)
        {

        }

    }

}
