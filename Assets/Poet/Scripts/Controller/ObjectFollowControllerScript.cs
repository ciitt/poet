﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poet
{

    /// <summary>
    /// Object controller for Follow
    /// </summary>
    public partial class ObjectFollowAndLookAtControllerScript : PoetBaseReceiverScript
    {

        /// <summary>
        /// Csv header for Follow
        /// </summary>
        [SerializeField]
        [Tooltip("Csv header for Follow")]
        protected string m_csvHeaderForFollow = "CharacterA Follow";

        /// <summary>
        /// Identifier for default target to follow
        /// </summary>
        [SerializeField]
        [Tooltip("Identifier for default target to follow")]
        protected string m_defaultFollowTargetIdentifier = "";

        /// <summary>
        /// Follow speed
        /// </summary>
        [SerializeField]
        [Tooltip("Follow speed")]
        [Range(0.0f, 1.0f)]
        protected float m_followSpeed = 0.1f;

        /// <summary>
        /// Reference to current target to follow
        /// </summary>
        protected Transform m_refCurrentFollowTarget = null;

        /// <summary>
        /// IEnumerator for Follow
        /// </summary>
        protected IEnumerator m_followIE = null;

        /// <summary>
        /// LateUpdate for follow
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected virtual void LateUpdateForFollow()
        {

            if (this.m_refPauseState.pause || this.m_refSceneChangeState.stateEnum != SSC.SceneChangeState.StateEnum.ScenePlaying)
            {
                return;
            }

            // --------------------------

            if (this.m_refCurrentFollowTarget)
            {
                this.transform.position = Vector3.Lerp(this.transform.position, this.m_refCurrentFollowTarget.position, this.m_followSpeed);
            }

        }

        /// <summary>
        /// Is current target to follow default target
        /// </summary>
        /// <returns>yes</returns>
        // ------------------------------------------------------------------------------------------
        protected bool isCurrentFollowTargetIsDefaultTarget()
        {
            return (this.m_userProgressData.followTargetIdentifier == this.m_defaultFollowTargetIdentifier);
        }

        /// <summary>
        /// Set current follow target
        /// </summary>
        /// <param name="followTargetIdentifier">followTargetIdentifier</param>
        /// <param name="immediately">set immediately</param>
        // ------------------------------------------------------------------------------------------
        protected void setCurrentFollowTarget(string followTargetIdentifier, bool immediately)
        {

            if(!PositionAndRotateTargetManager.isAvailable())
            {
                return;
            }

            // ------------

            // m_userProgressData
            {
                this.m_userProgressData.followTargetIdentifier = followTargetIdentifier;
            }

            // set
            {

                this.m_refCurrentFollowTarget =
                    (!string.IsNullOrEmpty(followTargetIdentifier)) ?
                    PositionAndRotateTargetManager.Instance.getTargetTransform(followTargetIdentifier) :
                    null
                    ;

            }

            if (this.m_refCurrentFollowTarget && immediately)
            {
                this.transform.position = this.m_refCurrentFollowTarget.position;
            }

        }

        /// <summary>
        /// Stop follow coroutine
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected void stopFollowCoroutine()
        {

            if (this.m_followIE != null)
            {
                StopCoroutine(this.m_followIE);
            }

        }

        /// <summary>
        /// Solve functions
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected void solveFollowFunctions(FuncNameAndParamsList fnapList, float resumeTime)
        {

            if (fnapList == null)
            {
                return;
            }

            // -------------------

            foreach (var fnap in fnapList.list)
            {

                if (fnap.functionName == "FollowTransform")
                {
                    this.stopFollowCoroutine();
                    StartCoroutine(this.m_followIE = this.startFollowTransform(fnap, fnapList, resumeTime));
                    break;
                }

                else if (fnap.functionName == "FollowBezierFromTo")
                {
                    this.stopFollowCoroutine();
                    StartCoroutine(this.m_followIE = this.startFollowBezierFromTo(fnap, fnapList, resumeTime));
                    break;
                }

                else if (fnap.functionName == "Stop")
                {
                    this.stopFollowCoroutine();
                    StartCoroutine(this.m_followIE = this.startFollowStop(fnap, fnapList, resumeTime));
                    break;
                }

                else if (fnap.functionName == "Resume")
                {
                    this.stopFollowCoroutine();
                    StartCoroutine(this.m_followIE = this.startFollowResume(fnap, fnapList, resumeTime));
                    break;
                }

            }

        }

        /// <summary>
        /// Common IEnumerator for follow
        /// </summary>
        /// <param name="fnap">FuncNameAndParams</param>
        /// <param name="fnapList">FuncNameAndParamsList</param>
        /// <param name="resumeTime">resume time</param>
        /// <param name="mainFunc">main</param>
        // ------------------------------------------------------------------------------------------
        protected IEnumerator commonFollowIE(FuncNameAndParams fnap, FuncNameAndParamsList fnapList, float resumeTime)
        {

            if (fnap == null || fnapList == null)
            {
                yield break;
            }

            // -------------------

            // m_userProgressData
            {
                this.m_userProgressData.resumeTimeForFollow = resumeTime;
                this.m_userProgressData.fnapListForFollow = fnapList;
            }

            // wait for invoke
            {
                yield return this.waitInvokeTime(fnapList.invokeTime, resumeTime, () =>
                {
                    this.m_userProgressData.resumeTimeForFollow += Time.deltaTime;
                });
                this.m_userProgressData.resumeTimeForFollow = Mathf.Max(fnapList.invokeTime, resumeTime);
            }

        }

        /// <summary>
        /// Function for FollowTransform
        /// </summary>
        /// <param name="fnap">FuncNameAndParams</param>
        /// <param name="fnapList">FuncNameAndParamsList</param>
        /// <param name="resumeTime">resume time</param>
        /// <returns>IEnumerator</returns>
        // ------------------------------------------------------------------------------------------
        protected IEnumerator startFollowTransform(FuncNameAndParams fnap, FuncNameAndParamsList fnapList, float resumeTime)
        {

            yield return commonFollowIE(fnap, fnapList, resumeTime);

            this.m_userProgressData.clearFollow();

            this.setCurrentFollowTarget(fnap.getStringParameter(0), false);

            this.m_followIE = null;

        }

        /// <summary>
        /// Function for FollowBezierFromTo
        /// </summary>
        /// <param name="fnap">FuncNameAndParams</param>
        /// <param name="fnapList">FuncNameAndParamsList</param>
        /// <param name="resumeTime">resume time</param>
        /// <returns>IEnumerator</returns>
        // ------------------------------------------------------------------------------------------
        protected IEnumerator startFollowBezierFromTo(FuncNameAndParams fnap, FuncNameAndParamsList fnapList, float resumeTime)
        {

            if (fnap == null || fnapList == null)
            {
                yield break;
            }

            // -------------------

            yield return commonFollowIE(fnap, fnapList, resumeTime);

            // setCurrentFollowTarget
            {
                this.setCurrentFollowTarget(null, false);
            }

            StartCoroutine(this.m_followIE = this.followBezierFromTo(
                fnap.getStringParameter(0),
                fnap.getFloatParameter(1, 1.0f),
                fnap.getFloatParameter(2, 1.0f),
                fnap.getStringParameter(3, "Linear"),
                fnapList.invokeTime,
                resumeTime
                ));

        }

        /// <summary>
        /// Function for Stop
        /// </summary>
        /// <param name="fnap">FuncNameAndParams</param>
        /// <param name="fnapList">FuncNameAndParamsList</param>
        /// <param name="resumeTime">resume time</param>
        /// <returns>IEnumerator</returns>
        // ------------------------------------------------------------------------------------------
        protected IEnumerator startFollowStop(FuncNameAndParams fnap, FuncNameAndParamsList fnapList, float resumeTime)
        {

            yield return commonFollowIE(fnap, fnapList, resumeTime);

            this.m_userProgressData.clearFollow();

            this.setCurrentFollowTarget(null, false);

            this.m_followIE = null;

        }

        /// <summary>
        /// Function for Resume
        /// </summary>
        /// <param name="fnap">FuncNameAndParams</param>
        /// <param name="fnapList">FuncNameAndParamsList</param>
        /// <param name="resumeTime">resume time</param>
        /// <returns>IEnumerator</returns>
        // ------------------------------------------------------------------------------------------
        protected IEnumerator startFollowResume(FuncNameAndParams fnap, FuncNameAndParamsList fnapList, float resumeTime)
        {

            yield return commonFollowIE(fnap, fnapList, resumeTime);

            this.m_userProgressData.clearFollow();

            string identifier = fnap.getStringParameter(0);

            if(!string.IsNullOrEmpty(identifier))
            {
                this.setCurrentFollowTarget(identifier, false);
            }
            
            this.setCurrentFollowTarget(this.m_defaultFollowTargetIdentifier, false);

            this.m_followIE = null;

        }

        /// <summary>
        /// FollowBezierFromTo IEnumerator
        /// </summary>
        /// <param name="bezierIdentifier">bezier identifier</param>
        /// <param name="secondsFromCurrentPosToStartPos">time to current pos to start pos</param>
        /// <param name="secondsFromStartPosToEndPos">time to start pos to end pos</param>
        /// <param name="easing">easing</param>
        /// <param name="invokeTime">delay seconds</param>
        /// <param name="resumeTime">resume seconds</param>
        /// <returns>IEnumerator</returns>
        // ------------------------------------------------------------------------------------------
        protected virtual IEnumerator followBezierFromTo(
            string bezierIdentifier,
            float secondsFromCurrentPosToStartPos,
            float secondsFromStartPosToEndPos,
            string easing,
            float invokeTime,
            float resumeTime
            )
        {

            if (!BezierCurveManager.isAvailable())
            {
                this.m_followIE = null;
                yield break;
            }

            BezierCurveScript bezier = BezierCurveManager.Instance.getBezierCurveScript(bezierIdentifier);

            if (!bezier)
            {
                this.m_followIE = null;
                yield break;
            }

            // ------------------------------------

            float timer = 0.0f;

            Vector3 fromPosition = Vector3.zero;
            Vector3 toPosition = Vector3.zero;
            Quaternion fromRotation = Quaternion.identity;

            var pauseState = PoetReduxManager.Instance.PauseStateWatcher.state();

            BezierPoint bezierPoint = null;

            float val = 0.0f;

            // secondsFromCurrentPosToStartPos
            {

                timer = Mathf.Max(resumeTime - invokeTime, 0.0f);
                this.m_userProgressData.resumeTimeForFollow = Mathf.Max(invokeTime, resumeTime);

                bezierPoint = bezier.pointAtBezierCurveDetail(0.0f);

                fromPosition = this.transform.position;
                toPosition = bezierPoint.point;

                fromRotation = this.transform.rotation;

                // firstPosition fromPosition
                {

                    if (timer <= 0.0f)
                    {
                        this.m_userProgressData.firstPositionForBezier = fromPosition;
                    }

                    else
                    {
                        fromPosition = this.m_userProgressData.firstPositionForBezier;
                    }

                }

                while (timer < secondsFromCurrentPosToStartPos)
                {

                    // break
                    {
                        if (PoetOneSceneManager.isAvailable() && PoetOneSceneManager.Instance.shouldSkip())
                        {
                            break;
                        }
                    }

                    // pauseState
                    {
                        if (pauseState.pause)
                        {
                            yield return null;
                            continue;
                        }
                    }

                    // val
                    {
                        val = PoetAnimationCurveManager.Instance.evaluate(easing, timer / secondsFromCurrentPosToStartPos);
                    }

                    // position
                    {
                        this.transform.position = Vector3.LerpUnclamped(
                            fromPosition,
                            toPosition,
                            val
                            );
                    }

                    // timer
                    {
                        timer += Time.deltaTime;
                        this.m_userProgressData.resumeTimeForFollow += Time.deltaTime;
                    }

                    yield return null;

                }

                // finish
                {
                    this.transform.position = bezierPoint.point;
                }

            }

            // secondsFromStartPosToEndPos
            {

                timer = Mathf.Max(resumeTime - invokeTime - secondsFromCurrentPosToStartPos, 0.0f);
                this.m_userProgressData.resumeTimeForFollow = Mathf.Max(invokeTime + secondsFromCurrentPosToStartPos, resumeTime);

                while (timer < secondsFromStartPosToEndPos)
                {

                    // break
                    {
                        if (PoetOneSceneManager.isAvailable() && PoetOneSceneManager.Instance.shouldSkip())
                        {
                            break;
                        }
                    }

                    // pauseState
                    {
                        if (pauseState.pause)
                        {
                            yield return null;
                            continue;
                        }
                    }

                    // val
                    {
                        val = PoetAnimationCurveManager.Instance.evaluate(easing, timer / secondsFromStartPosToEndPos);
                    }

                    bezierPoint = bezier.pointAtBezierCurveDetail(val);

                    // position
                    {
                        this.transform.position = bezierPoint.point;
                    }

                    // timer
                    {
                        timer += Time.deltaTime;
                        this.m_userProgressData.resumeTimeForFollow += Time.deltaTime;
                    }

                    yield return null;

                }

            }

            // finish resumeTime
            {

                this.m_userProgressData.resumeTimeForFollow =
                    invokeTime +
                    secondsFromCurrentPosToStartPos +
                    secondsFromStartPosToEndPos
                    ;

            }

            // finish
            {

                bezierPoint = bezier.pointAtBezierCurveDetail(1.0f);

                {
                    this.transform.position = bezierPoint.point;
                }

            }

            this.m_followIE = null;

        }

        /// <summary>
        /// PoetInitState receiver for follow
        /// </summary>
        /// <param name="piState">PoetInitState</param>
        // ------------------------------------------------------------------------------------------
        protected virtual void onPoetInitStateForFollow(PoetInitState piState)
        {

#if UNITY_EDITOR

            if (piState.headerAndCells.ContainsKey(this.m_csvHeaderForFollow))
            {

                FuncNameAndParamsList fnapList = CsvParser.parseFunctionsInCell(piState.headerAndCells[this.m_csvHeaderForFollow]);

                foreach (var val in fnapList.list)
                {

                    if (
                        val.functionName != "FollowTransform" &&
                        val.functionName != "FollowBezierFromTo" &&
                        val.functionName != "Stop" &&
                        val.functionName != "Resume"
                        )
                    {

                        Debug.LogWarning(
                            "Unknown function name : " +
                            val.functionName + " : " +
                            this.m_csvHeaderForFollow + " : " +
                            piState.textAssetName + " : " +
                            piState.index
                            );

                    }

                }

            }

#endif

        }

    }

}
