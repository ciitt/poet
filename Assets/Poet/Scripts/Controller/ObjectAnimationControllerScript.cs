﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poet
{

    /// <summary>
    /// Object controller for Animation
    /// </summary>
    [RequireComponent(typeof(Animator))]
    public class ObjectAnimationControllerScript : PoetBaseReceiverScript
    {

        /// <summary>
        /// User progress data
        /// </summary>
        [Serializable]
        protected class UserProgressData
        {

            public float resumeTime = 0.0f;
            public FuncNameAndParamsList fnapList = null;

            public void clear()
            {

                this.resumeTime = 0.0f;
                this.fnapList = null;

            }

        }

        /// <summary>
        /// Csv header for animation
        /// </summary>
        [SerializeField]
        [Tooltip("Csv header for animation")]
        protected string m_csvHeaderForAnimation = "CharacterA Animation";

        /// <summary>
        /// Reference to Animator
        /// </summary>
        protected Animator m_refAnimator = null;

        /// <summary>
        /// UserProgressData
        /// </summary>
        protected UserProgressData m_userProgressData = new UserProgressData();

        /// <summary>
        /// initOnAwake() is called after Awake().
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected override void initOnAwake()
        {

            this.m_refAnimator = this.GetComponent<Animator>();

        }

        /// <summary>
        /// Solve functions
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected void solveFunctions(FuncNameAndParamsList fnapList, float resumeTime)
        {

            if (fnapList == null)
            {
                return;
            }

            // -------------------

            if (fnapList.list.Count > 0)
            {
                StopAllCoroutines();
                StartCoroutine(this.startSetAnimParameters(fnapList, resumeTime));
            }

        }

        /// <summary>
        /// Start setAnimParameters
        /// </summary>
        /// <param name="fnapList">FuncNameAndParamsList</param>
        /// <param name="resumeTime">resume time</param>
        // ------------------------------------------------------------------------------------------
        protected IEnumerator startSetAnimParameters(FuncNameAndParamsList fnapList, float resumeTime)
        {

            if (fnapList == null)
            {
                yield break;
            }

            // -------------------

            // m_userProgressData
            {
                this.m_userProgressData.resumeTime = resumeTime;
                this.m_userProgressData.fnapList = fnapList;
            }

            // wait for invoke
            {
                yield return this.waitInvokeTime(fnapList.invokeTime, resumeTime, () =>
                {
                    this.m_userProgressData.resumeTime += Time.deltaTime;
                });
                this.m_userProgressData.resumeTime = fnapList.invokeTime;
            }

            // setAnimParameters
            {
                this.setAnimParameters(fnapList);
            }

            // m_userProgressData
            {
                this.m_userProgressData.clear();
            }

        }

        /// <summary>
        /// setAnimParameters
        /// </summary>
        /// <param name="fnapList">FuncNameAndParamsList</param>
        // ------------------------------------------------------------------------------------------
        protected void setAnimParameters(
            FuncNameAndParamsList fnapList
            )
        {

            foreach (var fnap in fnapList.list)
            {

                if(fnap.functionName == "SetTrigger")
                {
                    this.m_refAnimator.SetTrigger(fnap.getStringParameter(0));
                }

                else if (fnap.functionName == "SetBool")
                {
                    this.m_refAnimator.SetBool(fnap.getStringParameter(0), fnap.getIntParameter(1) != 0);
                }

                else if (fnap.functionName == "SetFloat")
                {
                    this.m_refAnimator.SetFloat(fnap.getStringParameter(0), fnap.getFloatParameter(1));
                }

                else if (fnap.functionName == "SetInteger")
                {
                    this.m_refAnimator.SetInteger(fnap.getStringParameter(0), fnap.getIntParameter(1));
                }

            }

        }

        /// <summary>
        /// PoetSequenceState receiver
        /// </summary>
        /// <param name="psState">PoetSequenceState</param>
        // ------------------------------------------------------------------------------------------
        protected override void onPoetSequenceState(PoetSequenceState psState)
        {

            if (psState.stateEnum == PoetSequenceState.StateEnum.PreProcess)
            {

                if (psState.headerAndCells.ContainsKey(this.m_csvHeaderForAnimation))
                {

                    this.m_currentFnapList.clear();

                    CsvParser.parseFunctionsInCell(psState.headerAndCells[this.m_csvHeaderForAnimation], this.m_currentFnapList);

                    if (this.m_currentFnapList.preProcess)
                    {
                        this.solveFunctions(new FuncNameAndParamsList(this.m_currentFnapList), 0.0f);
                    }

                }

            }

            else if (psState.stateEnum == PoetSequenceState.StateEnum.MainProcess)
            {

                if (psState.headerAndCells.ContainsKey(this.m_csvHeaderForAnimation))
                {

                    if (!this.m_currentFnapList.preProcess)
                    {
                        this.solveFunctions(new FuncNameAndParamsList(this.m_currentFnapList), 0.0f);
                    }

                }

            }

        }

        /// <summary>
        /// PoetInitState receiver
        /// </summary>
        /// <param name="piState">PoetInitState</param>
        // ------------------------------------------------------------------------------------------
        protected override void onPoetInitState(PoetInitState piState)
        {

#if UNITY_EDITOR

            if (piState.headerAndCells.ContainsKey(this.m_csvHeaderForAnimation))
            {

                FuncNameAndParamsList fnapList = CsvParser.parseFunctionsInCell(piState.headerAndCells[this.m_csvHeaderForAnimation]);

                foreach (var val in fnapList.list)
                {

                    if (
                        val.functionName != "SetTrigger" &&
                        val.functionName != "SetBool" &&
                        val.functionName != "SetFloat" &&
                        val.functionName != "SetInteger"
                        )
                    {

                        Debug.LogWarning(
                            "Unknown function name : " +
                            val.functionName + " : " +
                            this.m_csvHeaderForAnimation + " : " +
                            piState.textAssetName + " : " +
                            piState.index
                            );

                    }

                }

            }

#endif

        }

        /// <summary>
        /// TextShowingState receiver
        /// </summary>
        /// <param name="tsState">TextShowingState</param>
        // ------------------------------------------------------------------------------------------
        protected override void onTextShowingState(TextShowingState tsState)
        {

        }

        /// <summary>
        /// UserProgressDataSignal receiver
        /// </summary>
        /// <param name="updSignal">UserProgressDataSignal</param>
        // ------------------------------------------------------------------------------------------
        protected override void onUserProgressDataSignal(UserProgressDataSignal updSignal)
        {
            updSignal.addDataAction(this.transform, this, JsonUtility.ToJson(this.m_userProgressData));
        }

        /// <summary>
        /// SceneChangeState receiver
        /// </summary>
        /// <param name="scState">SceneChangeState</param>
        // ------------------------------------------------------------------------------------------
        protected override void onSceneChangeStateReceiver(SSC.SceneChangeState scState)
        {

            if (scState.stateEnum == SSC.SceneChangeState.StateEnum.ScenePlaying)
            {

                PoetSceneChangeManager pscm = PoetSceneChangeManager.Instance as PoetSceneChangeManager;

                if (pscm.isLoadingCurrentSceneWithUserProgressData())
                {

                    this.m_userProgressData = pscm.getDataFromCurrentUserProgressData<UserProgressData>(this.transform, this);

                    if (this.m_userProgressData.fnapList != null)
                    {
                        this.solveFunctions(this.m_userProgressData.fnapList, this.m_userProgressData.resumeTime);
                    }

                }

            }

        }

        /// <summary>
        /// PauseState receiver
        /// </summary>
        /// <param name="pState">PauseState</param>
        // ------------------------------------------------------------------------------------------
        protected override void onPauseStateReceiver(SSC.PauseState pState)
        {
            this.m_refAnimator.speed = (pState.pause) ? 0.0f : 1.0f;
        }

        /// <summary>
        /// Reset
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected override void Reset()
        {

            this.m_receiverMask =
                ReceiverMask.UsePoetSequenceStateReceiver |
                ReceiverMask.UsePoetInitStateReceiver |
                ReceiverMask.UseUserProgressDataSignalReceiver |
                ReceiverMask.UseSceneChangeStateReceiver |
                ReceiverMask.UsePauseStateReceiver
                ;

        }

    }

}
