﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poet
{

    /// <summary>
    /// Object controller for Move
    /// </summary>
    public class ObjectMoveControllerScript : PoetBaseReceiverScript
    {

        /// <summary>
        /// User progress data
        /// </summary>
        [Serializable]
        protected class UserProgressData
        {

            public float resumeTime = 0.0f;
            public FuncNameAndParamsList fnapList = null;

            public Vector3 firstPosition = Vector3.zero;

            public void clear()
            {

                this.resumeTime = 0.0f;
                this.fnapList = null;

                this.firstPosition = Vector3.zero;

            }

        }

        /// <summary>
        /// Csv header for Move
        /// </summary>
        [SerializeField]
        [Tooltip("Csv header for Move")]
        protected string m_csvHeaderForMove = "CharacterA Move";

        /// <summary>
        /// Local position movement
        /// </summary>
        [SerializeField]
        [Tooltip("Local position movement")]
        protected bool m_localMovement = false;

        /// <summary>
        /// UserProgressData
        /// </summary>
        protected UserProgressData m_userProgressData = new UserProgressData();

        /// <summary>
        /// initOnAwake() is called after Awake().
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected override void initOnAwake()
        {

        }

        /// <summary>
        /// Solve functions
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected void solveFunctions(FuncNameAndParamsList fnapList, float resumeTime)
        {

            if (fnapList == null)
            {
                return;
            }

            // -------------------

            foreach (var fnap in fnapList.list)
            {

                if (fnap.functionName == "MoveFromTo")
                {
                    StopAllCoroutines();
                    StartCoroutine(this.startMoveFromTo(fnap, fnapList, resumeTime));
                    break;
                }

                else if (fnap.functionName == "MoveTo")
                {
                    StopAllCoroutines();
                    StartCoroutine(this.startMoveTo(fnap, fnapList, resumeTime, false));
                    break;
                }

                else if (fnap.functionName == "MoveToAndLookForward")
                {
                    StopAllCoroutines();
                    StartCoroutine(this.startMoveTo(fnap, fnapList, resumeTime, true));
                    break;
                }

            }

        }

        /// <summary>
        /// Common IEnumerator
        /// </summary>
        /// <param name="fnap">FuncNameAndParams</param>
        // ------------------------------------------------------------------------------------------
        protected IEnumerator commonIE(FuncNameAndParams fnap, FuncNameAndParamsList fnapList, float resumeTime)
        {

            if (fnap == null || fnapList == null)
            {
                yield break;
            }

            // -------------------

            // m_userProgressData
            {
                this.m_userProgressData.resumeTime = resumeTime;
                this.m_userProgressData.fnapList = fnapList;
            }

            // wait for invoke
            {
                yield return this.waitInvokeTime(fnapList.invokeTime, resumeTime, () =>
                {
                    this.m_userProgressData.resumeTime += Time.deltaTime;
                });
                this.m_userProgressData.resumeTime = Mathf.Max(fnapList.invokeTime, resumeTime);
            }

        }

        /// <summary>
        /// Function for MoveFromTo
        /// </summary>
        /// <param name="fnap">FuncNameAndParams</param>
        // ------------------------------------------------------------------------------------------
        protected IEnumerator startMoveFromTo(FuncNameAndParams fnap, FuncNameAndParamsList fnapList, float resumeTime)
        {

            if (fnap == null || fnapList == null)
            {
                yield break;
            }

            // -------------------

            yield return this.commonIE(fnap, fnapList, resumeTime);

            StartCoroutine(this.moveFromTo(
                fnap.getStringParameter(0),
                fnap.getStringParameter(1),
                fnap.getFloatParameter(2, 1.0f),
                fnap.getFloatParameter(3, 1.0f),
                fnap.getStringParameter(4, "EaseOut"),
                fnapList.invokeTime,
                resumeTime
                ));

        }

        /// <summary>
        /// Function for MoveTo
        /// </summary>
        /// <param name="fnap">FuncNameAndParams</param>
        /// <param name="fnapList">FuncNameAndParamsList</param>
        /// <param name="resumeTime">resume time</param>
        /// <param name="lookForward">lookForward</param>
        // ------------------------------------------------------------------------------------------
        protected IEnumerator startMoveTo(FuncNameAndParams fnap, FuncNameAndParamsList fnapList, float resumeTime, bool lookForward)
        {

            if (fnap == null || fnapList == null)
            {
                yield break;
            }

            // -------------------

            yield return this.commonIE(fnap, fnapList, resumeTime);

            StartCoroutine(this.moveTo(
                fnap.getStringParameter(0),
                fnap.getFloatParameter(1, 1.0f),
                fnap.getStringParameter(2, "EaseInOutOver"),
                lookForward,
                fnapList.invokeTime,
                resumeTime
                ));

        }

        /// <summary>
        /// MoveFromTo IEnumerator
        /// </summary>
        /// <param name="fromIdentifier">from identifier</param>
        /// <param name="toIdentifier">to identifier</param>
        /// <param name="secondsFromCurrentPosToStartPos">time to current pos to start pos</param>
        /// <param name="secondsFromStartPosToEndPos">time to start pos to end pos</param>
        /// <param name="easing">easing</param>
        /// <param name="invokeTime">delay seconds</param>
        /// <param name="resumeTime">resume seconds</param>
        /// <returns>IEnumerator</returns>
        // ------------------------------------------------------------------------------------------
        protected virtual IEnumerator moveFromTo(
            string fromIdentifier,
            string toIdentifier,
            float secondsFromCurrentPosToStartPos,
            float secondsFromStartPosToEndPos,
            string easing,
            float invokeTime,
            float resumeTime
            )
        {

            if (!PositionAndRotateTargetManager.isAvailable())
            {
                yield break;
            }

            // ------------------------------------

            float timer = 0.0f;

            Vector3 fromPosition = Vector3.zero;
            Vector3 toPosition = Vector3.zero;

            var pauseState = PoetReduxManager.Instance.PauseStateWatcher.state();

            // secondsFromCurrentPosToFromPos
            {

                timer = Mathf.Max(resumeTime - invokeTime, 0.0f);
                this.m_userProgressData.resumeTime = Mathf.Max(invokeTime, resumeTime);

                fromPosition =
                    (this.m_localMovement) ?
                    this.transform.localPosition :
                    this.transform.position
                    ;

                toPosition =
                    (this.m_localMovement) ?
                    PositionAndRotateTargetManager.Instance.getLocalPosition(fromIdentifier) :
                    PositionAndRotateTargetManager.Instance.getTargetTransformPosition(fromIdentifier)
                    ;

                // firstPosition fromPosition
                {

                    if (timer <= 0.0f)
                    {
                        this.m_userProgressData.firstPosition = fromPosition;
                    }

                    else
                    {
                        fromPosition = this.m_userProgressData.firstPosition;
                    }

                }

                while (timer < secondsFromCurrentPosToStartPos)
                {

                    // break
                    {
                        if (PoetOneSceneManager.isAvailable() && PoetOneSceneManager.Instance.shouldSkip())
                        {
                            break;
                        }
                    }

                    // pauseState
                    {
                        if (pauseState.pause)
                        {
                            yield return null;
                            continue;
                        }
                    }

                    // position
                    {

                        if (this.m_localMovement)
                        {

                            this.transform.localPosition = Vector3.LerpUnclamped(
                                fromPosition,
                                toPosition,
                                PoetAnimationCurveManager.Instance.evaluate(easing, timer / secondsFromCurrentPosToStartPos)
                                );
                        }

                        else
                        {

                            this.transform.position = Vector3.LerpUnclamped(
                                fromPosition,
                                toPosition,
                                PoetAnimationCurveManager.Instance.evaluate(easing, timer / secondsFromCurrentPosToStartPos)
                                );

                        }

                    }

                    // timer
                    {
                        timer += Time.deltaTime;
                        this.m_userProgressData.resumeTime += Time.deltaTime;
                    }

                    yield return null;

                }

                // finish
                {

                    if (this.m_localMovement)
                    {
                        this.transform.localPosition = toPosition;
                    }

                    else
                    {
                        this.transform.position = toPosition;
                    }

                }

            }

            // secondsFromFromPosToToPos
            {

                timer = Mathf.Max(resumeTime - invokeTime - secondsFromCurrentPosToStartPos, 0.0f);
                this.m_userProgressData.resumeTime = Mathf.Max(invokeTime + secondsFromCurrentPosToStartPos, resumeTime);

                fromPosition =
                    (this.m_localMovement) ?
                    PositionAndRotateTargetManager.Instance.getLocalPosition(fromIdentifier) :
                    PositionAndRotateTargetManager.Instance.getTargetTransformPosition(fromIdentifier)
                    ;

                toPosition =
                    (this.m_localMovement) ?
                    PositionAndRotateTargetManager.Instance.getLocalPosition(toIdentifier) :
                    PositionAndRotateTargetManager.Instance.getTargetTransformPosition(toIdentifier)
                    ;

                while (timer < secondsFromStartPosToEndPos)
                {

                    // break
                    {
                        if (PoetOneSceneManager.isAvailable() && PoetOneSceneManager.Instance.shouldSkip())
                        {
                            break;
                        }
                    }

                    // pauseState
                    {
                        if (pauseState.pause)
                        {
                            yield return null;
                            continue;
                        }
                    }

                    // position
                    {

                        if (this.m_localMovement)
                        {

                            this.transform.localPosition = Vector3.LerpUnclamped(
                                fromPosition,
                                toPosition,
                                PoetAnimationCurveManager.Instance.evaluate(easing, timer / secondsFromStartPosToEndPos)
                                );
                        }

                        else
                        {

                            this.transform.position = Vector3.LerpUnclamped(
                                fromPosition,
                                toPosition,
                                PoetAnimationCurveManager.Instance.evaluate(easing, timer / secondsFromStartPosToEndPos)
                                );

                        }

                    }

                    // timer
                    {
                        timer += Time.deltaTime;
                        this.m_userProgressData.resumeTime += Time.deltaTime;
                    }

                    yield return null;

                }

                // finish
                {

                    if (this.m_localMovement)
                    {
                        this.transform.localPosition = toPosition;
                    }

                    else
                    {
                        this.transform.position = toPosition;
                    }

                }

            }

            // clear m_userProgressData
            {
                this.m_userProgressData.clear();
            }

        }

        /// <summary>
        /// MoveTo IEnumerator
        /// </summary>
        /// <param name="toIdentifier">to identifier</param>
        /// <param name="secondsFromCurrentPosToEndPos">time</param>
        /// <param name="easing">easing</param>
        /// <param name="lookForward">lookForward</param>
        /// <param name="invokeTime">delay seconds</param>
        /// <param name="resumeTime">resume seconds</param>
        /// <returns>IEnumerator</returns>
        // ------------------------------------------------------------------------------------------
        protected virtual IEnumerator moveTo(
            string toIdentifier,
            float secondsFromCurrentPosToEndPos,
            string easing,
            bool lookForward,
            float invokeTime,
            float resumeTime
            )
        {

            if (!PositionAndRotateTargetManager.isAvailable())
            {
                yield break;
            }

            // ------------------------------------

            float timer = 0.0f;

            Vector3 fromPosition = Vector3.zero;
            Vector3 toPosition = Vector3.zero;

            Transform toTransform = null;

            var pauseState = PoetReduxManager.Instance.PauseStateWatcher.state();

            // secondsFromCurrentPosToEndPos
            {

                timer = Mathf.Max(resumeTime - invokeTime, 0.0f);
                this.m_userProgressData.resumeTime = Mathf.Max(invokeTime, resumeTime);

                fromPosition =
                    (this.m_localMovement) ?
                    this.transform.localPosition :
                    this.transform.position
                    ;

                toPosition =
                    (this.m_localMovement) ?
                    PositionAndRotateTargetManager.Instance.getLocalPosition(toIdentifier) :
                    PositionAndRotateTargetManager.Instance.getTargetTransformPosition(toIdentifier)
                    ;

                toTransform =
                    (this.m_localMovement) ? 
                    null :
                    PositionAndRotateTargetManager.Instance.getTargetTransform(toIdentifier)
                    ;

                // firstPosition fromPosition
                {

                    if (timer <= 0.0f)
                    {
                        this.m_userProgressData.firstPosition = fromPosition;
                    }

                    else
                    {
                        fromPosition = this.m_userProgressData.firstPosition;
                    }

                }

                while (timer < secondsFromCurrentPosToEndPos)
                {

                    // break
                    {
                        if (PoetOneSceneManager.isAvailable() && PoetOneSceneManager.Instance.shouldSkip())
                        {
                            break;
                        }
                    }

                    // pauseState
                    {
                        if (pauseState.pause)
                        {
                            yield return null;
                            continue;
                        }
                    }

                    // position
                    {

                        if (this.m_localMovement)
                        {

                            this.transform.localPosition = Vector3.LerpUnclamped(
                                fromPosition,
                                toPosition,
                                PoetAnimationCurveManager.Instance.evaluate(easing, timer / secondsFromCurrentPosToEndPos)
                                );

                        }

                        else
                        {

                            this.transform.position = Vector3.LerpUnclamped(
                                fromPosition,
                                toPosition,
                                PoetAnimationCurveManager.Instance.evaluate(easing, timer / secondsFromCurrentPosToEndPos)
                                );

                        }

                    }

                    // look
                    {

                        if(lookForward && toTransform)
                        {

                            this.transform.rotation = Quaternion.Slerp(
                                this.transform.rotation,
                                Quaternion.LookRotation(toTransform.forward),
                                timer / secondsFromCurrentPosToEndPos
                                );

                        }

                    }

                    // timer
                    {
                        timer += Time.deltaTime;
                        this.m_userProgressData.resumeTime += Time.deltaTime;
                    }

                    yield return null;

                }

            }
            // finish
            {

                // pos
                {

                    if (this.m_localMovement)
                    {
                        this.transform.localPosition = toPosition;
                    }

                    else
                    {
                        this.transform.position = toPosition;
                    }

                }

                // look
                {

                    if (lookForward && toTransform)
                    {
                        this.transform.rotation = Quaternion.LookRotation(toTransform.forward);
                    }

                }

            }
            
            // clear m_userProgressData
            {
                this.m_userProgressData.clear();
            }

        }

        /// <summary>
        /// PoetSequenceState receiver
        /// </summary>
        /// <param name="psState">PoetSequenceState</param>
        // ------------------------------------------------------------------------------------------
        protected override void onPoetSequenceState(PoetSequenceState psState)
        {

            if (psState.stateEnum == PoetSequenceState.StateEnum.PreProcess)
            {

                if (psState.headerAndCells.ContainsKey(this.m_csvHeaderForMove))
                {

                    this.m_currentFnapList.clear();

                    CsvParser.parseFunctionsInCell(psState.headerAndCells[this.m_csvHeaderForMove], this.m_currentFnapList);

                    if (this.m_currentFnapList.preProcess)
                    {
                        this.solveFunctions(new FuncNameAndParamsList(this.m_currentFnapList), 0.0f);
                    }

                }

            }

            else if (psState.stateEnum == PoetSequenceState.StateEnum.MainProcess)
            {

                if (psState.headerAndCells.ContainsKey(this.m_csvHeaderForMove))
                {

                    if (!this.m_currentFnapList.preProcess)
                    {
                        this.solveFunctions(new FuncNameAndParamsList(this.m_currentFnapList), 0.0f);
                    }

                }

            }

        }

        /// <summary>
        /// PoetInitState receiver
        /// </summary>
        /// <param name="piState">PoetInitState</param>
        // ------------------------------------------------------------------------------------------
        protected override void onPoetInitState(PoetInitState piState)
        {

#if UNITY_EDITOR

            if (piState.headerAndCells.ContainsKey(this.m_csvHeaderForMove))
            {

                FuncNameAndParamsList fnapList = CsvParser.parseFunctionsInCell(piState.headerAndCells[this.m_csvHeaderForMove]);

                foreach (var val in fnapList.list)
                {

                    if (
                        val.functionName != "MoveFromTo" &&
                        val.functionName != "MoveTo" &&
                        val.functionName != "MoveToAndLookForward"
                        )
                    {

                        Debug.LogWarning(
                            "Unknown function name : " +
                            val.functionName + " : " +
                            this.m_csvHeaderForMove + " : " +
                            piState.textAssetName + " : " +
                            piState.index
                            );

                    }

                }

            }

#endif

        }

        /// <summary>
        /// TextShowingState receiver
        /// </summary>
        /// <param name="tsState">TextShowingState</param>
        // ------------------------------------------------------------------------------------------
        protected override void onTextShowingState(TextShowingState tsState)
        {

        }

        /// <summary>
        /// UserProgressDataSignal receiver
        /// </summary>
        /// <param name="updSignal">UserProgressDataSignal</param>
        // ------------------------------------------------------------------------------------------
        protected override void onUserProgressDataSignal(UserProgressDataSignal updSignal)
        {
            updSignal.addDataAction(this.transform, this, JsonUtility.ToJson(this.m_userProgressData));
        }

        /// <summary>
        /// SceneChangeState receiver
        /// </summary>
        /// <param name="scState">SceneChangeState</param>
        // ------------------------------------------------------------------------------------------
        protected override void onSceneChangeStateReceiver(SSC.SceneChangeState scState)
        {

            if (scState.stateEnum == SSC.SceneChangeState.StateEnum.AllStartupsDone)
            {

                PoetSceneChangeManager pscm = PoetSceneChangeManager.Instance as PoetSceneChangeManager;

                if (pscm.isLoadingCurrentSceneWithUserProgressData())
                {
                    this.m_userProgressData = pscm.getDataFromCurrentUserProgressData<UserProgressData>(this.transform, this);
                }

            }

            else if (scState.stateEnum == SSC.SceneChangeState.StateEnum.ScenePlaying)
            {

                PoetSceneChangeManager pscm = PoetSceneChangeManager.Instance as PoetSceneChangeManager;

                if (pscm.isLoadingCurrentSceneWithUserProgressData())
                {

                    if (this.m_userProgressData.fnapList != null)
                    {
                        this.solveFunctions(this.m_userProgressData.fnapList, this.m_userProgressData.resumeTime);
                    }

                }

            }

        }

        /// <summary>
        /// PauseState receiver
        /// </summary>
        /// <param name="pState">PauseState</param>
        // ------------------------------------------------------------------------------------------
        protected override void onPauseStateReceiver(SSC.PauseState pState)
        {

        }

    }

}
