﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace PoetSample
{

    [RequireComponent(typeof(Text))]
    public class PoetSampleNowLoadingProgressTextScript : MonoBehaviour
    {

        Text m_refText = null;

        void Awake()
        {
            this.m_refText = this.GetComponent<Text>();
            this.enabled = false;
        }

        void OnEnable()
        {
            this.m_refText.text = "0.000 / 0";
        }

        void Update()
        {

            int denominator = SSC.SceneChangeManager.Instance.progressDenominator();

            if (denominator > 0)
            {
                this.m_refText.text =
                    SSC.SceneChangeManager.Instance.progressNumerator().ToString("0.000") +
                    " / " +
                    denominator.ToString("0");
            }

        }

    }

}
