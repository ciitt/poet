﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PoetDemoSample
{

    public class RotateScript : MonoBehaviour
    {

        public float m_speed = 0.1f;

        /// <summary>
        /// Reference to SceneChangeState
        /// </summary>
        protected SSC.SceneChangeState m_refSceneChangeState = null;

        /// <summary>
        /// Reference to PauseState
        /// </summary>
        protected SSC.PauseState m_refPauseState = null;

        void Start()
        {

            // PoetReduxManager
            {

                Poet.PoetReduxManager prm = Poet.PoetReduxManager.Instance as Poet.PoetReduxManager;

                this.m_refSceneChangeState = prm.SceneChangeStateWatcher.state();
                this.m_refPauseState = prm.PauseStateWatcher.state();

            }

        }

        void Update()
        {

            if(!this.m_refPauseState.pause && this.m_refSceneChangeState.stateEnum == SSC.SceneChangeState.StateEnum.ScenePlaying)
            {
                this.transform.Rotate(Vector3.up * this.m_speed);
            }
            
        }

    }

}
