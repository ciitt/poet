﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PoetDemoSample
{

    public class DemoSampleSolveJumpIfManager : Poet.SolveJumpIfManager
    {

        public DemoSampleItemFlagScript m_refDemoSampleItemFlagScript = null;

        [Space(20.0f)]
        [Multiline]
        public string m_note = "You should remove this script\nand implement your SolveJumpIfManager.";

        // -------------------------------------------------------------------------------------
        protected override void initOnAwake()
        {
            
        }

        // -------------------------------------------------------------------------------------
        public override string solveJumpIfFlag(string flagIdentifier, string comparison, string val, string identifierToJump)
        {

            int identifierValue = 0;
            int value = 0;

            if (!this.m_refDemoSampleItemFlagScript)
            {
                return "";
            }

            if (!int.TryParse(val, out value))
            {
                return "";
            }

            // ----------------------------------

            if (flagIdentifier == "Hiroshi Friendship")
            {
                identifierValue = this.m_refDemoSampleItemFlagScript.Flags.hiroshiFriendship;
            }

            else if (flagIdentifier == "Takashi Friendship")
            {
                identifierValue = this.m_refDemoSampleItemFlagScript.Flags.takashiFriendship;
            }

            else if (flagIdentifier == "Kiyoshi Friendship")
            {
                identifierValue = this.m_refDemoSampleItemFlagScript.Flags.kiyoshiFriendship;
            }

            // ----------------------------------

            if (this.compare(identifierValue, value, comparison))
            {
                return identifierToJump;
            }

            return "";
        }

        // -------------------------------------------------------------------------------------
        public override string solveJumpIfItem(string itemIdentifier, string comparison, string val, string identifierToJump)
        {

            int identifierValue = 0;
            int value = 0;

            if (!this.m_refDemoSampleItemFlagScript)
            {
                return "";
            }

            if (!int.TryParse(val, out value))
            {
                return "";
            }

            // ----------------------------------

            if (itemIdentifier == "Coin")
            {
                identifierValue = this.m_refDemoSampleItemFlagScript.Items.coin;
            }

            else if (itemIdentifier == "Pen")
            {
                identifierValue = this.m_refDemoSampleItemFlagScript.Items.pen;
            }


            // ----------------------------------

            if (this.compare(identifierValue, value, comparison))
            {
                return identifierToJump;
            }

            return "";
        }

        // -------------------------------------------------------------------------------------
        public bool getTheNumberOfItems(string itemIdentifier, out int retVal)
        {

            bool temp = false;
            retVal = 0;

            if (itemIdentifier == "Coin")
            {
                retVal = this.m_refDemoSampleItemFlagScript.Items.coin;
                temp = true;
            }

            else if (itemIdentifier == "Pen")
            {
                retVal = this.m_refDemoSampleItemFlagScript.Items.pen;
                temp = true;
            }

            return temp;

        }

    }

}
