﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PoetDemoSample
{

    public class DemoSampleSystemManager : Poet.SystemManager
    {

        /// <summary>
        /// Read config data
        /// </summary>
        /// <returns>IEnumerator</returns>
        // -------------------------------------------------------------------------------------
        protected override IEnumerator readConfigData()
        {

            yield return this.readDataIE<DemoSampleConfigDataSO>(this.poetConfigFilePath(), (result, error) =>
            {

                if (result != null)
                {

                    this.m_configDataSO = result;

#if UNITY_EDITOR
                    Debug.Log("(#if UNITY_EDITOR) Read config data from " + (this.m_usePlayerPrefs ? "PlayerPrefs" : "hard disk"));
#endif

                }

                else
                {

                    if (!string.IsNullOrEmpty(error))
                    {
                        Debug.LogError(error);
                    }

                    this.m_configDataSO = Instantiate(this.m_configDataSO);

                }

            }, true);

            // print(this.configDataSO<DemoSampleConfigDataSO>().testVal);

        }

        /// <summary>
        /// Encrypt text data
        /// </summary>
        /// <param name="data">text data</param>
        /// <param name="pass">pass</param>
        /// <returns>encrypted data</returns>
        // -------------------------------------------------------------------------------------
        protected override byte[] encryptTextData(string data, string pass)
        {
            return base.encryptTextData(data, pass);
        }

        /// <summary>
        /// Decrypt binary data
        /// </summary>
        /// <param name="data">binary data</param>
        /// <param name="pass">pass</param>
        /// <returns>text data</returns>
        // -------------------------------------------------------------------------------------
        protected override string decryptBinaryData(byte[] data, string pass)
        {
            return base.decryptBinaryData(data, pass); ;
        }

    }

}
