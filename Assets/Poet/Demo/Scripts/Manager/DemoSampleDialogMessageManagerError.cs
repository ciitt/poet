﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Poet;

namespace PoetDemoSample
{

    public partial class DemoSampleDialogMessageManager : Poet.PoetDialogMessageManager
    {

        /// <summary>
        /// Error message for save data error
        /// </summary>
        /// <returns>any</returns>
        // ------------------------------------------------------------------------------------
        public override System.Object saveDataErrorMessage(string rawErrorMessage)
        {

            SystemLanguage sl = SystemManager.Instance.systemLanguage;

            if (sl == SystemLanguage.Japanese)
            {

                SSC.DialogMessages messages = new SSC.DialogMessages();

                messages.category = SSC.DialogMessages.MessageCategory.Error;
                messages.title = "Error";
                messages.mainMessage = "セーブエラー。";
                messages.anyMessage = rawErrorMessage;

                return messages;

            }

            return base.saveDataErrorMessage(rawErrorMessage);

        }

        /// <summary>
        /// Error message for load data error
        /// </summary>
        /// <returns>any</returns>
        // ------------------------------------------------------------------------------------
        public override System.Object loadDataErrorMessage(string rawErrorMessage)
        {

            SystemLanguage sl = SystemManager.Instance.systemLanguage;

            if (sl == SystemLanguage.Japanese)
            {

                SSC.DialogMessages messages = new SSC.DialogMessages();

                messages.category = SSC.DialogMessages.MessageCategory.Error;
                messages.title = "Error";
                messages.mainMessage = "ロードエラー。";
                messages.anyMessage = rawErrorMessage;

                return messages;

            }

            return base.loadDataErrorMessage(rawErrorMessage);

        }


        /// <summary>
        /// Error message for AssetBundle startup error
        /// </summary>
        /// <returns>any</returns>
        // ------------------------------------------------------------------------------------
        public override System.Object assetBundleStartupErrorMessage(string errorUrl, string rawErrorMessage)
        {

            SystemLanguage sl = SystemManager.Instance.systemLanguage;

            if (sl == SystemLanguage.Japanese)
            {

                SSC.DialogMessages messages = new SSC.DialogMessages();

                messages.category = SSC.DialogMessages.MessageCategory.Error;
                messages.title = "Error";
                messages.urlIfNeeded = errorUrl;
                messages.mainMessage = "AssetBundle スタートアップエラー。";
                messages.subMessage = "リトライしますか ?";
                messages.anyMessage = rawErrorMessage;

                return messages;

            }

            return base.assetBundleStartupErrorMessage(errorUrl, rawErrorMessage);

        }

        /// <summary>
        /// Error message for Www startup error
        /// </summary>
        /// <returns>any</returns>
        // ------------------------------------------------------------------------------------
        public override System.Object wwwStartupErrorMessage(string errorUrl, string rawErrorMessage)
        {

            SystemLanguage sl = SystemManager.Instance.systemLanguage;

            if (sl == SystemLanguage.Japanese)
            {

                SSC.DialogMessages messages = new SSC.DialogMessages();

                messages.category = SSC.DialogMessages.MessageCategory.Error;
                messages.title = "Error";
                messages.urlIfNeeded = errorUrl;
                messages.mainMessage = "WWW スタートアップエラー。";
                messages.subMessage = "リトライしますか ?";
                messages.anyMessage = rawErrorMessage;

                return messages;

            }

            return base.wwwStartupErrorMessage(errorUrl, rawErrorMessage);

        }

        /// <summary>
        /// Error message for IE startup error
        /// </summary>
        /// <returns>any</returns>
        // ------------------------------------------------------------------------------------
        public override System.Object ieStartupErrorMessage(string rawErrorMessage)
        {

            SystemLanguage sl = SystemManager.Instance.systemLanguage;

            if (sl == SystemLanguage.Japanese)
            {

                SSC.DialogMessages messages = new SSC.DialogMessages();

                messages.category = SSC.DialogMessages.MessageCategory.Error;
                messages.title = "Error";
                messages.mainMessage = "IEnumerator スタートアップエラー。";
                messages.subMessage = "リトライしますか ?";
                messages.anyMessage = rawErrorMessage;

                return messages;

            }

            return base.ieStartupErrorMessage(rawErrorMessage);

        }

        /// <summary>
        /// Error message for user progress data error
        /// </summary>
        /// <returns>any</returns>
        // ------------------------------------------------------------------------------------
        public override System.Object convertUserProgressDataErrorMessage(string gameObjectName, string scriptName)
        {

            SystemLanguage sl = SystemManager.Instance.systemLanguage;

            if (sl == SystemLanguage.Japanese)
            {

                SSC.DialogMessages messages = new SSC.DialogMessages();

                messages.category = SSC.DialogMessages.MessageCategory.Error;
                messages.title = "Error";
                messages.mainMessage = "データ変換エラー。";
                messages.anyMessage = gameObjectName + " : " + scriptName;

                return messages;

            }

            return base.convertUserProgressDataErrorMessage(gameObjectName, scriptName);

        }

    }

}
