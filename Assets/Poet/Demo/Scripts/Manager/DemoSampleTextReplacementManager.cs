﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PoetDemoSample
{

    public class DemoSampleTextReplacementManager : Poet.PoetTextReplacementManager
    {

        public DemoSampleItemFlagScript m_refDemoSampleItemFlagScript = null;

        [Space(20.0f)]
        [Multiline]
        public string m_note = "You should remove this script\nand implement your PoetTextReplacementManager.";

        protected override void initOnAwake()
        {
            
        }

        public override string getReplacementText(string identifier)
        {

            if(!this.m_refDemoSampleItemFlagScript)
            {
                return "";
            }

            // ---------------------

            if(identifier == "[GetCoinCount]")
            {
                return this.m_refDemoSampleItemFlagScript.Items.coin.ToString();
            }

            //else if (identifier == "[Something]")
            //{
            //    return "Anything";
            //}

            return "";

        }

    }

}
