﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Poet;

namespace PoetDemoSample
{

    public partial class DemoSampleDialogMessageManager : Poet.PoetDialogMessageManager
    {

        [Space(20.0f)]
        [Multiline]
        public string m_note = "You should remove this script\nand implement your PoetDialogMessageManager.";

        /// <summary>
        /// Confirmation message for quit
        /// </summary>
        /// <returns>any</returns>
        // ------------------------------------------------------------------------------------
        public override System.Object quitApplicationConfirmationMessage()
        {

            SystemLanguage sl = SystemManager.Instance.systemLanguage;

            if(sl == SystemLanguage.Japanese)
            {

                SSC.DialogMessages messages = new SSC.DialogMessages();

                messages.category = SSC.DialogMessages.MessageCategory.Confirmation;
                messages.title = "Confirmation";
                messages.mainMessage = "ゲームを終了しますか ?";

                return messages;

            }

            return base.quitApplicationConfirmationMessage();

        }

        /// <summary>
        /// Confirmation message for error
        /// </summary>
        /// <returns>any</returns>
        // ------------------------------------------------------------------------------------
        public override System.Object backToTitleConfirmationMessage()
        {

            SystemLanguage sl = SystemManager.Instance.systemLanguage;

            if (sl == SystemLanguage.Japanese)
            {

                SSC.DialogMessages messages = new SSC.DialogMessages();

                messages.category = SSC.DialogMessages.MessageCategory.Confirmation;
                messages.title = "Confirmation";
                messages.mainMessage = "タイトルに戻りますか ?";

                return messages;

            }

            return base.backToTitleConfirmationMessage();

        }

        /// <summary>
        /// Confirmation message for album
        /// </summary>
        /// <returns>any</returns>
        // ------------------------------------------------------------------------------------
        public override System.Object backToAlbumConfirmationMessage()
        {

            SystemLanguage sl = SystemManager.Instance.systemLanguage;

            if (sl == SystemLanguage.Japanese)
            {

                SSC.DialogMessages messages = new SSC.DialogMessages();

                messages.category = SSC.DialogMessages.MessageCategory.Confirmation;
                messages.title = "Confirmation";
                messages.mainMessage = "アルバムシーンに戻りますか ?";

                return messages;

            }

            return base.backToAlbumConfirmationMessage();

        }

        /// <summary>
        /// Confirmation message for back to title
        /// </summary>
        /// <returns>any</returns>
        // ------------------------------------------------------------------------------------
        public override System.Object backToTitleBecauseOfErrorMessage()
        {

            SystemLanguage sl = SystemManager.Instance.systemLanguage;

            if (sl == SystemLanguage.Japanese)
            {

                SSC.DialogMessages messages = new SSC.DialogMessages();

                messages.category = SSC.DialogMessages.MessageCategory.Confirmation;
                messages.title = "Confirmation";
                messages.mainMessage = "タイトルに戻ります。";

                return messages;

            }

            return base.backToTitleBecauseOfErrorMessage();

        }

        /// <summary>
        /// Confirmation message for overwriting user progress data
        /// </summary>
        /// <returns>any</returns>
        // ------------------------------------------------------------------------------------
        public override System.Object overwriteUserProgressDataMessage()
        {

            SystemLanguage sl = SystemManager.Instance.systemLanguage;

            if (sl == SystemLanguage.Japanese)
            {

                SSC.DialogMessages messages = new SSC.DialogMessages();

                messages.category = SSC.DialogMessages.MessageCategory.Confirmation;
                messages.title = "Confirmation";
                messages.mainMessage = "上書きしますか ?";

                return messages;

            }

            return base.overwriteUserProgressDataMessage();

        }

        /// <summary>
        /// Confirmation message for saving user progress data
        /// </summary>
        /// <returns>any</returns>
        // ------------------------------------------------------------------------------------
        public override System.Object saveUserProgressDataMessage()
        {

            SystemLanguage sl = SystemManager.Instance.systemLanguage;

            if (sl == SystemLanguage.Japanese)
            {

                SSC.DialogMessages messages = new SSC.DialogMessages();

                messages.category = SSC.DialogMessages.MessageCategory.Confirmation;
                messages.title = "Confirmation";
                messages.mainMessage = "ここにセーブしますか ?";

                return messages;

            }

            return base.saveUserProgressDataMessage();

        }

        /// <summary>
        /// Confirmation message for saving user progress data
        /// </summary>
        /// <returns>any</returns>
        // ------------------------------------------------------------------------------------
        public override System.Object saveUserProgressDataProgressMessage()
        {

            SystemLanguage sl = SystemManager.Instance.systemLanguage;

            if (sl == SystemLanguage.Japanese)
            {

                SSC.DialogMessages messages = new SSC.DialogMessages();

                messages.category = SSC.DialogMessages.MessageCategory.Other;
                messages.title = "Save";
                messages.mainMessage = "セーブ中 ...";

                return messages;

            }

            return base.saveUserProgressDataProgressMessage();

        }


        /// <summary>
        /// Confirmation message for loading user progress data
        /// </summary>
        /// <returns>any</returns>
        // ------------------------------------------------------------------------------------
        public override System.Object loadUserProgressDataMessage()
        {

            SystemLanguage sl = SystemManager.Instance.systemLanguage;

            if (sl == SystemLanguage.Japanese)
            {

                SSC.DialogMessages messages = new SSC.DialogMessages();

                messages.category = SSC.DialogMessages.MessageCategory.Confirmation;
                messages.title = "Confirmation";
                messages.mainMessage = "このデータをロードしますか ?";

                return messages;

            }

            return base.loadUserProgressDataMessage();

        }


        /// <summary>
        /// Confirmation message for quick save
        /// </summary>
        /// <returns>any</returns>
        // ------------------------------------------------------------------------------------
        public override System.Object quickSaveMessage()
        {

            SystemLanguage sl = SystemManager.Instance.systemLanguage;

            if (sl == SystemLanguage.Japanese)
            {

                SSC.DialogMessages messages = new SSC.DialogMessages();

                messages.category = SSC.DialogMessages.MessageCategory.Confirmation;
                messages.title = "Confirmation";
                messages.mainMessage = "クイックセーブしますか ?";

                return messages;

            }

            return base.quickSaveMessage();

        }

        /// <summary>
        /// Confirmation message for quick load
        /// </summary>
        /// <returns>any</returns>
        // ------------------------------------------------------------------------------------
        public override System.Object quickLoadMessage()
        {

            SystemLanguage sl = SystemManager.Instance.systemLanguage;

            if (sl == SystemLanguage.Japanese)
            {

                SSC.DialogMessages messages = new SSC.DialogMessages();

                messages.category = SSC.DialogMessages.MessageCategory.Confirmation;
                messages.title = "Confirmation";
                messages.mainMessage = "クイックロードしますか ?";

                return messages;

            }

            return base.quickLoadMessage();

        }

    }

}
