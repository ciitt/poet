﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PoetDemoSample
{

    [Serializable]
    public class DemoSampleFlagSO : ScriptableObject
    {

        public int hiroshiFriendship = 0;
        public int takashiFriendship = 0;
        public int kiyoshiFriendship = 0;

    }

}
