﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PoetDemoSample
{

    [Serializable]
    public class DemoSampleItemSO : ScriptableObject
    {

        public int coin = 0;
        public int pen = 0;

    }

}
