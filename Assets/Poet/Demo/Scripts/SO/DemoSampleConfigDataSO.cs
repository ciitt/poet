﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PoetDemoSample
{

    /// <summary>
    /// Custom config data SO
    /// </summary>
    [Serializable]
    public class DemoSampleConfigDataSO : Poet.PoetConfigDataSO
    {

        public int testVal = 10;

    }

}
