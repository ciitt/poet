﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PoetDemoSample
{

    public class DemoSampleItemFlagScript : MonoBehaviour
    {

        /// <summary>
        /// User progress data
        /// </summary>
        [Serializable]
        protected class UserProgressData
        {
            public int testVal = 10;

            public string itemsJson = "";
            public string flagsJson = "";

            public void clear()
            {
                this.itemsJson = "";
                this.flagsJson = "";
            }

        }

        public string m_csvHeader = "Get";

        public DemoSampleItemSO m_itemsInInspector = null;
        public DemoSampleFlagSO m_flagsInInspector = null;

        DemoSampleItemSO m_currentItems = null;
        DemoSampleFlagSO m_currentFlags = null;

        /// <summary>
        /// UserProgressData
        /// </summary>
        UserProgressData m_userProgressData = new UserProgressData();

        public DemoSampleItemSO Items { get { return this.m_currentItems; } }
        public DemoSampleFlagSO Flags { get { return this.m_currentFlags; } }

        [Space(20.0f)]
        [Multiline]
        public string m_note = "You should remove this script\nand create your script for\nitems, flags and so on.";

        // ------------------------------------------------------------------------------------------
        void Start()
        {

            this.clearItemsAndFlags();

            // PoetReduxManager
            {

                Poet.PoetReduxManager prm = Poet.PoetReduxManager.Instance as Poet.PoetReduxManager;

                prm.addPoetSequenceStateReceiver(this.onPoetSequenceState);
                prm.addUserProgressDataSignalReceiver(this.onUserProgressDataSignal);
                prm.addSceneChangeStateReceiver(this.onSceneChangeStateReceiver);

            }

        }

        /// <summary>
        /// Clear
        /// </summary>
        // ------------------------------------------------------------------------------------------
        void clearItemsAndFlags()
        {

            this.m_userProgressData.clear();

            // m_items
            {

                if (this.m_itemsInInspector)
                {
                    this.m_currentItems = Instantiate(this.m_itemsInInspector);
                }

                else
                {
                    this.m_currentItems = ScriptableObject.CreateInstance<DemoSampleItemSO>();
                }

            }

            // m_flags
            {

                if (this.m_flagsInInspector)
                {
                    this.m_currentFlags = Instantiate(this.m_flagsInInspector);
                }

                else
                {
                    this.m_currentFlags = ScriptableObject.CreateInstance<DemoSampleFlagSO>();
                }

            }

        }

        /// <summary>
        /// PoetSequenceState receiver
        /// </summary>
        /// <param name="state">PoetSequenceState</param>
        // ------------------------------------------------------------------------------------------
        void onPoetSequenceState(Poet.PoetSequenceState psState)
        {

            // onPoetSequenceState receives events for Poet conversation

            if (psState.stateEnum == Poet.PoetSequenceState.StateEnum.MainProcess)
            {

                if (psState.headerAndCells.ContainsKey(this.m_csvHeader))
                {

                    var fnapList = Poet.CsvParser.parseFunctionsInCell(psState.headerAndCells[this.m_csvHeader]);

                    Poet.PoetSceneChangeManager pscm = Poet.PoetSceneChangeManager.Instance as Poet.PoetSceneChangeManager;

                    if (!pscm.isLoadingAlbumMemoryScene())
                    {

                        foreach (var fnap in fnapList.list)
                        {

                            if (fnap.functionName == "Item")
                            {

                                this.addItem(fnap.getStringParameter(0), fnap.getIntParameter(1, 1));

                                // example

                                // Item(Ball, 3)

                                // fnap.functionName == Item
                                // fnap.getStringParameter(0) == Ball
                                // fnap.getIntParameter(1) == 3

                            }

                            else if (fnap.functionName == "Flag")
                            {

                                this.addFlag(fnap.getStringParameter(0), fnap.getIntParameter(1, 1));

                                // example

                                // Flag(CubeFlag, 10)

                                // fnap.functionName == Flag
                                // fnap.getStringParameter(0) == CubeFlag
                                // fnap.getIntParameter(1) == 10

                            }

                        }

                    }

                }

            }

        }

        /// <summary>
        /// PoetInitState receiver
        /// </summary>
        /// <param name="piState">PoetInitState</param>
        // ------------------------------------------------------------------------------------------
        void onPoetInitState(Poet.PoetInitState piState)
        {

#if UNITY_EDITOR

            if (piState.headerAndCells.ContainsKey(this.m_csvHeader))
            {

                Poet.FuncNameAndParamsList fnapList = Poet.CsvParser.parseFunctionsInCell(piState.headerAndCells[this.m_csvHeader]);

                foreach (var val in fnapList.list)
                {

                    if (
                        val.functionName != "Item" &&
                        val.functionName != "Flag"
                        )
                    {

                        Debug.LogWarning(
                            "Unknown function name : " +
                            val.functionName + " : " +
                            this.m_csvHeader + " : " +
                            piState.textAssetName + " : " +
                            piState.index
                            );

                    }

                }

            }

#endif

        }

        /// <summary>
        /// UserProgressDataSignal receiver
        /// </summary>
        /// <param name="tfState">UserProgressDataSignal</param>
        // ------------------------------------------------------------------------------------------
        void onUserProgressDataSignal(Poet.UserProgressDataSignal updSignal)
        {

            // onUserProgressDataSignal receives events for creating user progress data

            this.m_userProgressData.itemsJson = JsonUtility.ToJson(this.m_currentItems);
            this.m_userProgressData.flagsJson = JsonUtility.ToJson(this.m_currentFlags);

            this.m_userProgressData.testVal = 20;

            updSignal.addDataAction(this.transform, this, JsonUtility.ToJson(this.m_userProgressData));

        }

        /// <summary>
        /// SceneChangeState receiver
        /// </summary>
        /// <param name="scState">SceneChangeState</param>
        // -----------------------------------------------------------------------------------------------
        void onSceneChangeStateReceiver(SSC.SceneChangeState scState)
        {

            // onSceneChangeStateReceiver receives events for scene changing

            if (scState.stateEnum == SSC.SceneChangeState.StateEnum.AllStartupsDone)
            {

                Poet.PoetSceneChangeManager pscm = Poet.PoetSceneChangeManager.Instance as Poet.PoetSceneChangeManager;

                // Current scene is being loaded with user progress data.
                if (pscm.isLoadingCurrentSceneWithUserProgressData())
                {

                    this.m_userProgressData = pscm.getDataFromCurrentUserProgressData<UserProgressData>(this.transform, this);

                    JsonUtility.FromJsonOverwrite(this.m_userProgressData.itemsJson, this.m_currentItems);
                    JsonUtility.FromJsonOverwrite(this.m_userProgressData.flagsJson, this.m_currentFlags);

                    // print(this.m_userProgressData.testVal);

                }

                // If current scene is title scene, clear data.
                else if(Poet.PoetSceneChangeManager.Instance.titleSceneName == Poet.PoetSceneChangeManager.Instance.nowLoadingSceneName)
                {
                    this.clearItemsAndFlags();
                }

            }

        }

        // ------------------------------------------------------------------------------------------
        void addItem(string itemIdentifier, int val)
        {

            if(itemIdentifier == "Coin")
            {
                this.m_currentItems.coin += val;
            }

            else if (itemIdentifier == "Pen")
            {
                this.m_currentItems.pen += val;
            }

            else
            {
                Debug.LogWarning("Unknown item : " + itemIdentifier);
            }

        }

        // ------------------------------------------------------------------------------------------
        void addFlag(string flagIdentifier, int val)
        {

            if (flagIdentifier == "Hiroshi Friendship")
            {
                this.m_currentFlags.hiroshiFriendship += val;
            }

            else if (flagIdentifier == "Takashi Friendship")
            {
                this.m_currentFlags.takashiFriendship += val;
            }

            else if (flagIdentifier == "Kiyoshi Friendship")
            {
                this.m_currentFlags.kiyoshiFriendship += val;
            }

            else
            {
                Debug.LogWarning("Unknown flag : " + flagIdentifier);
            }

        }

    }

}
