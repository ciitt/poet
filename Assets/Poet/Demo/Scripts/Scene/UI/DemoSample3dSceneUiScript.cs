﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PoetDenoSample
{

    public class DemoSample3dSceneUiScript : SSC.SceneUiManager
    {

        // ----------------------------------------------------------------------
        void Start()
        {
            Poet.PoetReduxManager prm = Poet.PoetReduxManager.Instance as Poet.PoetReduxManager;

            prm.addPoetSequenceStateReceiver(this.onPoetSequenceState);
        }

        /// <summary>
        /// PoetSequenceState receiver
        /// </summary>
        /// <param name="psState">PoetSequenceState</param>
        // ------------------------------------------------------------------------------------------
        protected virtual void onPoetSequenceState(Poet.PoetSequenceState psState)
        {

            if (psState.stateEnum == Poet.PoetSequenceState.StateEnum.StartOperation)
            {
                this.showUi("", false, false);
            }

        }

        // ----------------------------------------------------------------------
        public void showOrHideMainUi()
        {
            this.showUi(this.containsIdentifier("Main") ? "" : "Main", false, false);
        }

        // ----------------------------------------------------------------------
        public void showSaveUi()
        {
            Poet.SystemManager.Instance.createNewCurrentUserProgressDataAndShowSaveUI();
        }

        // ----------------------------------------------------------------------
        public void showLoadUi()
        {
            Poet.PoetCommonUiManager.Instance.showUi(Poet.PoetUiIdentifiers.Common.Load, true, false);
        }

        // ----------------------------------------------------------------------
        public void showQuickSaveUi()
        {
            Poet.SystemManager.Instance.quickSave();
        }

        // ----------------------------------------------------------------------
        public void showQuickLoadUi()
        {
            Poet.SystemManager.Instance.quickLoad();
        }

    }

}
