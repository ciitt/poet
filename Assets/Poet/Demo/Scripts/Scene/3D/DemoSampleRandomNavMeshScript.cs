﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace PoetDemoSample
{

    public class DemoSampleRandomNavMeshScript : Poet.SaveAndLoadNavMeshInfoScript
    {

        public Vector2 m_area = new Vector3(20.0f, 20.0f);

        /// <summary>
        /// SceneChangeState receiver
        /// </summary>
        /// <param name="scState">SceneChangeState</param>
        // ------------------------------------------------------------------------------------------
        protected override void onSceneChangeStateReceiver(SSC.SceneChangeState scState)
        {

            base.onSceneChangeStateReceiver(scState);

            if (scState.stateEnum == SSC.SceneChangeState.StateEnum.ScenePlaying)
            {

                Poet.PoetSceneChangeManager pscm = Poet.PoetSceneChangeManager.Instance as Poet.PoetSceneChangeManager;

                if (!pscm.isLoadingCurrentSceneWithUserProgressData())
                {
                    this.updateTarget();
                }

            }

        }

        // -------------------------------------------------------------------------------------
        void Update()
        {

            var scState = Poet.PoetReduxManager.Instance.SceneChangeStateWatcher.state();

            if(scState.stateEnum != SSC.SceneChangeState.StateEnum.ScenePlaying)
            {
                return;
            }

            // ------------------

            if (this.m_refNavMeshAgent && Time.frameCount % 60 == 0)
            {
                
                if (Vector3.Distance(this.transform.position, this.m_refNavMeshAgent.destination) <= this.m_refNavMeshAgent.stoppingDistance)
                {
                    this.updateTarget();
                }

            }

        }

        // -------------------------------------------------------------------------------------
        Vector3 randomPoint()
        {
            return new Vector3(
                UnityEngine.Random.Range(-this.m_area.x, this.m_area.x),
                0.0f,
                UnityEngine.Random.Range(-this.m_area.y, this.m_area.y)
                );
        }

        // -------------------------------------------------------------------------------------
        void updateTarget()
        {

            if (this.m_refNavMeshAgent)
            {
                this.m_refNavMeshAgent.SetDestination(this.randomPoint());
            }

        }

        // -------------------------------------------------------------------------------------
        public void startConversation()
        {
            this.pause(true);
            this.m_lockPause = true;
        }

        // -------------------------------------------------------------------------------------
        public void finishConversation()
        {
            this.m_lockPause = false;
            this.pause(false);
        }

    }

}
