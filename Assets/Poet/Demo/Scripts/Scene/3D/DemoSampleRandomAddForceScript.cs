﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PoetDemoSample
{

    public class DemoSampleRandomAddForceScript : MonoBehaviour
    {

        [Range(0.0f, 100.0f)]
        public float m_randX = 10.0f;

        [Range(0.0f, 100.0f)]
        public float m_randY = 1.0f;

        [Range(0.0f, 100.0f)]
        public float m_randZ = 10.0f;

        [Range(10, 500)]
        public int m_cycle = 100;

        int m_cycleCounter = 0;

        Rigidbody m_refRigidbody = null;

        // Use this for initialization
        void Start()
        {
            this.m_refRigidbody = this.GetComponent<Rigidbody>();
        }

        void FixedUpdate()
        {

            this.m_cycleCounter = (this.m_cycleCounter + 1) % this.m_cycle;

            if (!this.m_refRigidbody.isKinematic && this.m_cycleCounter == 0)
            {
                this.m_refRigidbody.AddForce(this.randomVector3(), ForceMode.VelocityChange);
            }

        }

        Vector3 randomVector3()
        {
            return new Vector3(
                UnityEngine.Random.Range(-this.m_randX, this.m_randX),
                UnityEngine.Random.Range(0.0f, this.m_randY),
                UnityEngine.Random.Range(-this.m_randZ, this.m_randZ)
                );
        }

    }

}
