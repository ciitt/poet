﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PoetDemoSample
{

    public class RotateAroundScript : MonoBehaviour
    {

        public float m_rotateSpeed = 0.1f;

        private void Update()
        {

            var scState = Poet.PoetReduxManager.Instance.SceneChangeStateWatcher.state();
            var pState = Poet.PoetReduxManager.Instance.PauseStateWatcher.state();

            if(scState.stateEnum == SSC.SceneChangeState.StateEnum.ScenePlaying && !pState.pause)
            {
                this.transform.Rotate(Vector3.up * this.m_rotateSpeed);
            }
                
        }

    }

}
