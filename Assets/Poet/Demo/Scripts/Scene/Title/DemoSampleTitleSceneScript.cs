﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace PoetDemoSample
{

    public class DemoSampleTitleSceneScript : MonoBehaviour
    {

#if UNITY_EDITOR

        [SerializeField]
        protected UnityEngine.Object m_firstScene;

        [SerializeField]
        protected UnityEngine.Object m_albumGraphicScene;

#endif

        [HideInInspector]
        [SerializeField]
        protected string m_firstSceneName = "";

        [HideInInspector]
        [SerializeField]
        protected string m_albumGraphicSceneName = "";

        /// <summary>
        /// Start
        /// </summary>
        // ---------------------------------------------------------------------------------------
        IEnumerator Start()
        {

            yield return null;

            if(SSC.SceneUiManager.isAvailable())
            {
                SSC.SceneUiManager.Instance.showUi("Title", false, false);
            }

        }

        /// <summary>
        /// Start button
        /// </summary>
        // ---------------------------------------------------------------------------------------
        public void onClickStart()
        {
            Poet.PoetSceneChangeManager.Instance.loadNextScene(this.m_firstSceneName);
        }

        /// <summary>
        /// Continue button
        /// </summary>
        // ---------------------------------------------------------------------------------------
        public void onClickContinue()
        {
            Poet.PoetCommonUiManager.Instance.showUi(Poet.PoetUiIdentifiers.Common.Load, true, false);
        }

        /// <summary>
        /// Config button
        /// </summary>
        // ---------------------------------------------------------------------------------------
        public void onClickConfig()
        {
            Poet.PoetCommonUiManager.Instance.showUi(Poet.PoetUiIdentifiers.Common.SystemConfig, true, false);
        }

        /// <summary>
        /// Album button
        /// </summary>
        // ---------------------------------------------------------------------------------------
        public void onClickAlbum()
        {

            Poet.PoetSceneChangeManager.Instance.loadNextScene(
                this.m_albumGraphicSceneName,
                true,
                "",
                Poet.PoetUiIdentifiers.AlbumScene.AlbumGraphicSelectForFirst
                );

        }

        /// <summary>
        /// Quit button
        /// </summary>
        // ---------------------------------------------------------------------------------------
        public void onClickQuit()
        {
            Poet.SystemManager.Instance.quitApp();
        }

        /// <summary>
        /// OnValidate
        /// </summary>
        // -------------------------------------------------------------------------------------------------------
        protected virtual void OnValidate()
        {

#if UNITY_EDITOR

            // m_firstScene
            {

                if (this.m_firstScene && !string.IsNullOrEmpty(this.m_firstScene.name))
                {
                    this.m_firstSceneName = this.m_firstScene.name;
                }

                else
                {
                    this.m_firstSceneName = "";
                }

            }

            //
            {

                if (this.m_albumGraphicScene && !string.IsNullOrEmpty(this.m_albumGraphicScene.name))
                {
                    this.m_albumGraphicSceneName = this.m_albumGraphicScene.name;
                }

                else
                {
                    this.m_albumGraphicSceneName = "";
                }

            }

#endif

        }

    }

}
