# Poet #

[Poet] is an asset to provide support for creating novel game with language localization.

## Demo ##

[Demo Movie](https://youtu.be/_dKH8R3RETk)

[Demo WebGL](http://bench-press-blog.com/unity/webgl/poet/index.html)

[Demo Windows Exe](https://drive.google.com/open?id=0BzPTX8aOqH4ca0RNSjFUYTlzX1E)

## How to play demo scenes, use and learn Poet ##

You can see manuals for Poet by playing demo scenes. So please follow the instructions below.

### 1. Add the following 9 scenes to your [Build Settings]. ###

* Assets/Poet/Scenes/init.unity
* Assets/Poet/Demo/Scenes/title.unity
* Assets/Poet/Demo/Scenes/poet howto.unity
* Assets/Poet/Demo/Scenes/sample 2d scene 01.unity
* Assets/Poet/Demo/Scenes/sample 3d scene 01.unity
* Assets/Poet/Demo/Scenes/album.unity
* Assets/Poet/Demo/Scenes/hiroshi route.unity
* Assets/Poet/Demo/Scenes/takashi route.unity
* Assets/Poet/Demo/Scenes/kiyoshi route.unity

* (Assets/Poet/Scenes/init webgl.unity) for WebGL
* (Assets/Poet/Demo/Scenes/sample 2d scene mobile.unity) for mobile test
	 
### 2. Play Assets/Poet/Scenes/init.unity ###

Play Assets/Poet/Scenes/init.unity

### 3. Click [Start] in title scene ###

Click [Start] in title scene

### 4. Click [How to use Poet] in frist choices ###

Click [How to use Poet] in frist choices.

## About CSV ##

Poet shows conversation text by parsing CSV file. So please see CSV files in [Assets/Poet/Demo/Csv] folder for your understanding.

## Features ##

* Conversation text with supporting language localization by CSV
* Speaker name with supporting language localization by CSV
* Conversation operations by CSV
* Show choices with supporting language localization by CSV
* Show screen effects by CSV
* Show popup text with supporting language localization by CSV
* Play audio in Resource folder by CSV
* Play audio in AssetBundle by CSV
* (Play voice audio with supporting language localization by CSV)
* Change textures for 2D novel character by CSV
* Load textures from AssetBundle for 2D novel character
* Custom shader for 2D novel character
* Play or stop particles by CSV
* Get items or flags by CSV
* Move GameObject by CSV
* Follow GameObject by CSV
* Look at GameObject by CSV
* Play Animator's Animation by CSV
* Play UV Animation by CSV
* Set destination for NavMeshAgent by CSV
* Replace a part of main text with variable value
* Load CSV from AssetBundle
* Dialog messages with supporting language localization
* Conversation text with scroll view
* Feature to save and load user progress data
* Save data to PlayerPrefs
* Save data to harddisk
* Quick save
* Quick load
* Text logs with voices
* Auto mode for conversation
* Skip mode for conversation
* Custom editor for creating 2D novel character.
* Album scene for graphic data
* Album scene for a scene to replay
* Album scene for music
* Sample 2D scene
* Sample 3D scene
* (Anima2D support)

## Unity Version ##
* 2017.1.1f1

## License ##
* MIT

## Contact ##
* ciittwu.ktwyktw@gmail.com
* https://www.facebook.com/Tatsuya-Sato-347036799013453/